## Short Description

The Shape of Life is a simple application exploring variations of Conway’s Game of Life.

## Long Description

Explore variations of Conway’s Game of Life with The Shape of Life.

With The Shape of Life you can:

- Save and organise your own library of games
- Explore 19 distinct tilings (or cell types)
- Customise the evolution rules, also by introducing randomic behaviours
- Control the size of the games and speed of evolution
- Customise the colours of the automaton, or use the system theme (and Android 12 'Material You' colour palette)
- Carefully configure the automaton and make it evolve step by step, or simply start the game and interact with the automaton as it is evolving

The app is completely free and open source. There are no advertisements, notifications or intrusive permissions in the app.