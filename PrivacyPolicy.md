# Privacy Policy

All data provided to The Shape of Life is only stored locally in your device. The Shape of Life does not upload your data anywhere. The developers of The Shape of Life do not have access to your data.

Your data is not shared with any 3rd parties. The Shape of Life does not include any advertisement libraries or any 3rd party tracking (analytics) code, such as Google Analytics or Facebook SDK.

If you have activated “backup & reset” in your phone settings (Settings / Backup & Reset / Back up my data), you should be aware that Android itself will periodically save a copy of your phone’s data in Google’s servers. The developers of The Shape of Life do not have access to this data.