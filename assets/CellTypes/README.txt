Each folder is associated to a cell type.

- Coordinates.png shows how the cells are arranged into a grid. Each cell is annotated with its x,y coordinates.
- Icon.svg is the svg used to generate the xml image associated to the cell type
- Points.svg shows how points are named for the cell type. The names of the points are the same as the ones used in the code.