package com.manuel.pagliai.tsol.repository


import androidx.test.espresso.matcher.ViewMatchers.assertThat
import com.manuel.pagliai.tsol.di.AppModule
import com.manuel.pagliai.tsol.domain.*
import com.manuel.pagliai.tsol.domain.cells.CellTypes

import com.manuel.pagliai.tsol.repository.games.GameDAO
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.io.IOException
import javax.inject.Inject

@HiltAndroidTest
@UninstallModules(AppModule::class)
class TheShapeOfLifeDBTest {

    @get:Rule
    val hiltAndroidRule = HiltAndroidRule(this)

    @Inject
    lateinit var gameDAO: GameDAO

    @Inject
    lateinit var db: TheShapeOfLifeDB

    @Inject
    lateinit var cellTypes : CellTypes

    @Before
    fun setup() {
        hiltAndroidRule.inject()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    fun assertGetById() = runBlocking {

        assertThat(gameDAO.getGameById(1), nullValue())

        val game1 = Stubs.savedGame(1, "#1", cellTypes.defaultCellType)
        gameDAO.saveGame(game1)
        assertThat(gameDAO.getGameById(1), equalTo(game1))
        assertThat(gameDAO.getGameById(1), not(sameInstance(game1)))
        assertThat(gameDAO.getGameById(2), nullValue())

        val game2 = Stubs.savedGame(2, "#2", cellTypes.defaultCellType)
        assertThat(game1, not(equalTo(game2)))

        gameDAO.saveGame(game2)

        assertThat(gameDAO.getGameById(1), equalTo(game1))
        assertThat(gameDAO.getGameById(2), equalTo(game2))

        gameDAO.delete(game1)
        assertThat(gameDAO.getGameById(1), nullValue())
    }

    @Test
    fun getAllGames() = runBlocking {
        val game1 = Stubs.savedGame(1, "#1", cellTypes.defaultCellType) { it.copy(modifiedAt = 100) }
        val game2 = Stubs.savedGame(2, "#2", cellTypes.defaultCellType) { it.copy(modifiedAt = 200) }
        val game3 = Stubs.savedGame(3, "#3", cellTypes.defaultCellType) { it.copy(modifiedAt = 300) }

        assertThat(gameDAO.getAllGames().first(), equalTo(listOf()))

        gameDAO.saveGame(game1)
        gameDAO.saveGame(game2)

        assertThat(gameDAO.getAllGames().first(), equalTo(listOf(game2, game1)))

        gameDAO.saveGame(game3)

        assertThat(gameDAO.getAllGames().first(), equalTo(listOf(game3, game2, game1)))

        gameDAO.delete(game1)

        assertThat(gameDAO.getAllGames().first(), equalTo(listOf(game3, game2)))
    }

}
