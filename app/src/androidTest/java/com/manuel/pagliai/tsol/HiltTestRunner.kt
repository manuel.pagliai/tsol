package com.manuel.pagliai.tsol

import android.app.Application
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner
import dagger.hilt.android.testing.HiltTestApplication

@Suppress("unused") // Used and referenced in the test runner task
class HiltTestRunner : AndroidJUnitRunner() {
    override fun newApplication(cl: ClassLoader?, name: String?, context: Context): Application {
        return super.newApplication(cl, HiltTestApplication::class.java.name, context)
    }
}