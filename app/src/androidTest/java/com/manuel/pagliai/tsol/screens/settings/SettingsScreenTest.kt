package com.manuel.pagliai.tsol.screens.settings

import android.content.res.Resources
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.manuel.pagliai.tsol.MainActivity
import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.Screens
import com.manuel.pagliai.tsol.TestTags
import com.manuel.pagliai.tsol.di.AppModule
import com.manuel.pagliai.tsol.repository.preferences.PreferencesState
import com.manuel.pagliai.tsol.screens.settings.SettingsScreen
import com.manuel.pagliai.tsol.screens.settings.SettingsScreenViewModel
import com.manuel.pagliai.tsol.ui.theme.TSoLTheme
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@HiltAndroidTest
@UninstallModules(AppModule::class)
class SettingsScreenTest {

    @get:Rule(order = 0)
    val hiltAndroidRule = HiltAndroidRule(this)

    @get:Rule(order = 1)
    val composeRule = createAndroidComposeRule<MainActivity>()

    private val resources: Resources
        get() = composeRule.activity.resources

    @OptIn(ExperimentalAnimationApi::class)
    @Before
    fun setup() {
        hiltAndroidRule.inject()
    }

    @Test
    fun systemColors() {

        val settingsScreenViewModel : SettingsScreenViewModel = mockk(relaxed = true)
        val systemColorFalse = PreferencesState(
            gridColor = Color.Red,
            backgroundColor = Color.Black,
            cellsColor = Color.Blue,
            newGamesDefaultCellSize = 1,
            newGamesDefaultSpeed = 2,
            randomiseNewGamesCells = true,
            showNextGenAnimationIdle = false,
            useSystemColors = false
        )

        val systemColorTrue = PreferencesState(
            gridColor = Color.Gray,
            backgroundColor = Color.Green,
            cellsColor = Color.Yellow,
            newGamesDefaultCellSize = 3,
            newGamesDefaultSpeed = 4,
            randomiseNewGamesCells = false,
            showNextGenAnimationIdle = true,
            useSystemColors = true
        )

        val systemColorState = mutableStateOf(systemColorTrue)

        every {
            settingsScreenViewModel.preferencesState
        }.returns(systemColorState)

        composeRule.setContent {
            TSoLTheme {
                Surface(
                    color = MaterialTheme.colorScheme.background,
                ) {
                    val navController = rememberNavController()
                    NavHost(
                        navController = navController,
                        startDestination = Screens.Game.route
                    ) {
                        composable(Screens.Game.route) {
                            SettingsScreen(navController = navController, viewModel = settingsScreenViewModel)
                        }
                    }
                }
            }
        }

        composeRule.onNodeWithText(resources.getString(R.string.cells_color)).assertDoesNotExist()
        composeRule.onNodeWithText(resources.getString(R.string.grid_color)).assertDoesNotExist()
        composeRule.onNodeWithText(resources.getString(R.string.background_color)).assertDoesNotExist()


        composeRule.onNodeWithTag(TestTags.USE_SYSTEM_COLOR_SWITCH).performClick()

        verify(exactly = 1) { settingsScreenViewModel.updateUseSystemColors(false) }


        ((settingsScreenViewModel.preferencesState) as MutableState).value = systemColorFalse

        composeRule.waitForIdle()

        composeRule.onNodeWithText(resources.getString(R.string.cells_color)).assertIsDisplayed()
        composeRule.onNodeWithText(resources.getString(R.string.grid_color)).assertIsDisplayed()
        composeRule.onNodeWithText(resources.getString(R.string.background_color)).assertIsDisplayed()
    }
}