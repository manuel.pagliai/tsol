package com.manuel.pagliai.tsol.screens.welcome

import android.content.res.Resources
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.navigation.NavController
import com.manuel.pagliai.tsol.MainActivity
import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.di.AppModule
import com.manuel.pagliai.tsol.domain.Cells
import com.manuel.pagliai.tsol.domain.GameRules
import com.manuel.pagliai.tsol.domain.Grid
import com.manuel.pagliai.tsol.domain.SavedGame
import com.manuel.pagliai.tsol.domain.cells.SquareCellType
import com.manuel.pagliai.tsol.domain.engine.AutomatonNextGenCalculator
import com.manuel.pagliai.tsol.domain.engine.ScreenOrientation
import com.manuel.pagliai.tsol.screens.game.AutomatonColors
import com.manuel.pagliai.tsol.ui.theme.TSoLTheme
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import kotlin.random.Random

/**
 * Also see [com.manuel.pagliai.tsol.MainActivityTest] for test coverage of the welcome screen.
 */
@HiltAndroidTest
@UninstallModules(AppModule::class)
class WelcomeScreenTest {

    @get:Rule(order = 0)
    val hiltAndroidRule = HiltAndroidRule(this)

    @get:Rule(order = 1)
    val composeRule = createAndroidComposeRule<MainActivity>()

    private val resources: Resources
        get() = composeRule.activity.resources

    @RelaxedMockK
    lateinit var viewModelMock: WelcomeScreenViewModel

    @MockK
    lateinit var navControllerMock: NavController

    private lateinit var savedGame: SavedGame

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setup() {
        MockKAnnotations.init(this)
    }

    @OptIn(ExperimentalFoundationApi::class)
    @Test
    fun testWelcomeScreenContent() {

        savedGame = SavedGame(
            cellType = SquareCellType(),
            cells = Cells(grid = Grid(20, 10)),
            rules = GameRules.B2S34,
            previewAspectRatio = 2f,
            modifiedAt = 0,
            createdAt = 0,
            id = 1,
            orientation = ScreenOrientation.LANDSCAPE,
            speed = 5,
            cellSize = 5,
            name = "savedGame1"
        )

        every {
            viewModelMock.animatedPreviewsCache
        } returns ProcessingOnFutureGenerationsCache(10, 10, AutomatonNextGenCalculator(Random(0)))

        val games : MutableState<List<SavedGame>> = mutableStateOf(emptyList())
        every { viewModelMock.allGames } returns games
        every { viewModelMock.automatonColors } returns mutableStateOf(AutomatonColors(cellsColor = null, gridColor = null, backgroundColor = null))

        composeRule.setContent {
            TSoLTheme {
                WelcomeScreen(navController = navControllerMock, viewModel = viewModelMock)
            }
        }

        composeRule.onNodeWithText("Tap + to create one").assertIsDisplayed()

        games.value = listOf(savedGame)

        composeRule.onNodeWithText("Tap + to create one").assertDoesNotExist()
        composeRule.onNodeWithText(savedGame.name, substring = true).assertIsDisplayed()
    }

    @OptIn(ExperimentalFoundationApi::class)
    @Test
    fun testTryLoadingTheAGameWithADifferentOrientation() {
        savedGame = SavedGame(
            cellType = SquareCellType(),
            cells = Cells(grid = Grid(20, 10)),
            rules = GameRules.B2S34,
            previewAspectRatio = 2f,
            modifiedAt = 0,
            createdAt = 0,
            id = 1,
            orientation = ScreenOrientation.LANDSCAPE,
            speed = 5,
            cellSize = 5,
            name = "savedGame1"
        )

        every {
            viewModelMock.animatedPreviewsCache
        } returns ProcessingOnFutureGenerationsCache(10, 10, AutomatonNextGenCalculator(Random(0)))

        val games : MutableState<List<SavedGame>> = mutableStateOf(listOf(savedGame))
        every { viewModelMock.allGames } returns games
        every { viewModelMock.automatonColors } returns mutableStateOf(AutomatonColors(cellsColor = null, gridColor = null, backgroundColor = null))

        composeRule.setContent {
            TSoLTheme {
                WelcomeScreen(navController = navControllerMock, viewModel = viewModelMock)
            }
        }

        composeRule.onNodeWithText(savedGame.name).performClick()
        composeRule.waitForIdle()
        composeRule.onNodeWithText(resources.getString(R.string.rotate_your_device_to_open_game_dialog_title)).assertIsDisplayed()
        composeRule.onNodeWithText(resources.getString(R.string.got_it)).performClick()
    }
}