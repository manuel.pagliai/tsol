package com.manuel.pagliai.tsol

import android.content.res.Resources
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.test.espresso.Espresso
import com.manuel.pagliai.tsol.di.AppModule
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@HiltAndroidTest
@UninstallModules(AppModule::class)
class MainActivityTest {

    @get:Rule(order = 0)
    val hiltAndroidRule = HiltAndroidRule(this)

    @get:Rule(order = 1)
    val composeRule = createAndroidComposeRule<MainActivity>()

    private val resources: Resources
        get() = composeRule.activity.resources

    @OptIn(ExperimentalAnimationApi::class)
    @Before
    fun setup() {
        hiltAndroidRule.inject()
        composeRule.setContent {
            composeRule.activity.MainActivityContent()
        }
    }


    @Test
    fun navigation() {

        // assert we are in the welcome screen and there are no games
        composeRule.onNodeWithText(resources.getString(R.string.app_name_long)).assertIsDisplayed()
        composeRule.onNodeWithTag(TestTags.CREATE_NEW_GAME_BUTTON).assertExists()
        composeRule.onNodeWithTag(TestTags.CREATE_NEW_GAME_BUTTON).assertIsDisplayed()
        composeRule.onNodeWithText(resources.getString(R.string.you_dont_have_any_games)).assertIsDisplayed()


        // go to settings
        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.menu_cd)
        ).performClick()
        composeRule.waitForIdle()
        composeRule.onNodeWithText(
            resources.getString(R.string.settings)
        ).performClick()
        composeRule.waitForIdle()


        // assert we are in the settings screen
        composeRule.onNodeWithText(
            resources.getString(R.string.show_next_gen_animation)
        ).assertIsDisplayed()


        // go back
        Espresso.pressBack()
        composeRule.waitForIdle()

        // go to the about page
        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.menu_cd)
        ).performClick()
        composeRule.waitForIdle()
        composeRule.onNodeWithText(
            resources.getString(R.string.about)
        ).performClick()
        composeRule.waitForIdle()

        // assert we are in the about page.
        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.application_icon_cd)
        ).assertIsDisplayed()

        // go back
        Espresso.pressBack()
        composeRule.waitForIdle()


        // assert we are in the welcome screen and there are no games
        composeRule.onNodeWithText(resources.getString(R.string.app_name_long)).assertIsDisplayed()
        composeRule.onNodeWithTag(TestTags.CREATE_NEW_GAME_BUTTON).assertExists()
        composeRule.onNodeWithTag(TestTags.CREATE_NEW_GAME_BUTTON).assertIsDisplayed()
        composeRule.onNodeWithText(resources.getString(R.string.you_dont_have_any_games)).assertIsDisplayed()



        val originalGameName = "original123"
        val renamedGameName = "renamed456"
        val copyGameName = "copy789"

        // create a new name called originalGameName
        composeRule.onNodeWithTag(TestTags.CREATE_NEW_GAME_BUTTON).performClick()
        composeRule.waitForIdle()
        composeRule.onNodeWithTag(TestTags.FIRST_GAME_NAME_TEXT_FIELD).performTextInput(originalGameName)
        composeRule.onNodeWithText(resources.getString(R.string.ok)).performClick()
        composeRule.waitForIdle()


        // assert we are in the game screen
        composeRule.onNodeWithContentDescription(resources.getString(R.string.play_cd)).assertIsDisplayed()

        // rename to renamedGameName
        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.menu_cd)
        ).performClick()
        composeRule.waitForIdle()
        composeRule.onNodeWithText(
            resources.getString(R.string.open_rename_dialog)
        ).performClick()
        composeRule.waitForIdle()
        composeRule.onNodeWithTag(TestTags.RENAME_GAME_TEXT_FIELD).performTextInput(renamedGameName)
        composeRule.onNodeWithText(resources.getString(R.string.rename_game_label)).performClick()
        composeRule.waitForIdle()

        // also create a copy of the game called copyGameName
        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.menu_cd)
        ).performClick()
        composeRule.waitForIdle()
        composeRule.onNodeWithText(
            resources.getString(R.string.open_create_copy_dialog)
        ).performClick()
        composeRule.waitForIdle()
        composeRule.onNodeWithTag(TestTags.CREATE_GAME_COPY_TEXT_FIELD).performTextInput(copyGameName)
        composeRule.onAllNodesWithText(
            resources.getString(R.string.create_game_copy_label)).filter(hasClickAction())[0].performClick()
        composeRule.waitForIdle()

        // go back to the welcome screen
        Espresso.pressBack()
        composeRule.waitForIdle()

        // assert we are back in the welcome screen
        // but there are games
        composeRule.onNodeWithText(resources.getString(R.string.app_name_long)).assertIsDisplayed()
        composeRule.onNodeWithTag(TestTags.CREATE_NEW_GAME_BUTTON).assertExists()
        composeRule.onNodeWithTag(TestTags.CREATE_NEW_GAME_BUTTON).assertIsDisplayed()
        composeRule.onNodeWithText(resources.getString(R.string.you_dont_have_any_games)).assertDoesNotExist()
        composeRule.onNodeWithText(renamedGameName).assertIsDisplayed()
        composeRule.onNodeWithText(copyGameName).assertIsDisplayed()


        // 2 games saved
        val savedGames = composeRule.onAllNodesWithTag(TestTags.SAVED_GAME_ITEM)
            .fetchSemanticsNodes()
        assertEquals(2, savedGames.size)

        repeat(2) {
            // delete the games
            composeRule.onAllNodesWithTag(TestTags.SAVED_GAME_THREE_DOTS_MENU_TEST_TAG)[0].performClick()
            composeRule.waitForIdle()
            composeRule.onNodeWithText(resources.getString(R.string.delete)).performClick()
            composeRule.waitForIdle()
            composeRule.onNodeWithText(resources.getString(R.string.cancel_label)).assertIsDisplayed()
            composeRule.onNodeWithText(resources.getString(R.string.delete)).performClick()
            composeRule.waitForIdle()
        }

        // assert we are in the welcome screen and there are no games
        composeRule.onNodeWithText(resources.getString(R.string.app_name_long)).assertIsDisplayed()
        composeRule.onNodeWithTag(TestTags.CREATE_NEW_GAME_BUTTON).assertExists()
        composeRule.onNodeWithTag(TestTags.CREATE_NEW_GAME_BUTTON).assertIsDisplayed()
        composeRule.onNodeWithText(resources.getString(R.string.you_dont_have_any_games)).assertIsDisplayed()

    }

}