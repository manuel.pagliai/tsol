package com.manuel.pagliai.tsol.repository.preferences

import android.content.Context
import androidx.compose.ui.graphics.Color
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.preferencesDataStore
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.manuel.pagliai.tsol.domain.GameSettings
import com.manuel.pagliai.tsol.screens.game.GameState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.*
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.not
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class PreferencesRepositoryTest {

    private lateinit var dataStore: DataStore<Preferences>
    private lateinit var preferenceRepository: PreferencesRepository
    private lateinit var scope : CoroutineScope

    @Before
    fun createRepository() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        scope = CoroutineScope(Dispatchers.Default)
        dataStore =  preferencesDataStore("PreferencesRepositoryTest-${System.currentTimeMillis()}", scope = scope).getValue(context, Unit::javaClass)
        preferenceRepository = PreferencesRepository(
           dataStore
        )
    }

    @After
    fun cancel() {
        runBlocking {
            dataStore.edit { it.clear() }
        }
        scope.cancel()
    }

    @Test
    fun updateShowNexGenWhenIdle() = runBlocking {
        assertTrue(preferenceRepository.preferencesState.first().showNextGenAnimationIdle)
        assertTrue(preferenceRepository.gameSettings.first().showNextGenAnimationIdle)

        preferenceRepository.updateShowNexGenWhenIdle(false)
        assertFalse(preferenceRepository.preferencesState.first().showNextGenAnimationIdle)
        assertFalse(preferenceRepository.gameSettings.first().showNextGenAnimationIdle)

        preferenceRepository.updateShowNexGenWhenIdle(true)
        assertTrue(preferenceRepository.preferencesState.first().showNextGenAnimationIdle)
        assertTrue(preferenceRepository.gameSettings.first().showNextGenAnimationIdle)
    }

    @Test
    fun updateRandomiseNewGamesCells() = runBlocking {
        assertFalse(preferenceRepository.preferencesState.first().randomiseNewGamesCells)
        assertFalse(preferenceRepository.gameSettings.first().randomiseNewGamesCells)

        preferenceRepository.updateRandomiseNewGamesCells(true)
        assertTrue(preferenceRepository.preferencesState.first().randomiseNewGamesCells)
        assertTrue(preferenceRepository.gameSettings.first().randomiseNewGamesCells)

        preferenceRepository.updateRandomiseNewGamesCells(false)
        assertFalse(preferenceRepository.preferencesState.first().randomiseNewGamesCells)
        assertFalse(preferenceRepository.gameSettings.first().randomiseNewGamesCells)
    }

    @Test
    fun updateUseSystemColors() = runBlocking {
        assertTrue(preferenceRepository.preferencesState.first().useSystemColors)
        assertNull(preferenceRepository.gameSettings.first().cellsColor)
        assertNull(preferenceRepository.gameSettings.first().gridColor)
        assertNull(preferenceRepository.gameSettings.first().backgroundColor)

        preferenceRepository.updateUseSystemColors(false)
        assertFalse(preferenceRepository.preferencesState.first().useSystemColors)
        assertNotNull(preferenceRepository.gameSettings.first().cellsColor)
        assertNotNull(preferenceRepository.gameSettings.first().gridColor)
        assertNotNull(preferenceRepository.gameSettings.first().backgroundColor)

        preferenceRepository.updateUseSystemColors(true)
        assertTrue(preferenceRepository.preferencesState.first().useSystemColors)
        assertNull(preferenceRepository.gameSettings.first().cellsColor)
        assertNull(preferenceRepository.gameSettings.first().gridColor)
        assertNull(preferenceRepository.gameSettings.first().backgroundColor)
    }


    private suspend fun testColorPreference(
        colorFromPreferencesState : (PreferencesState) -> Color,
        colorFromGameSettings : (GameSettings) -> Color?,
        updateColor: suspend (Color) -> Unit
        ) {
        preferenceRepository.updateUseSystemColors(false)
        val newColor1 = Color(42, 42, 42)
        val newColor2 = Color(22, 22, 22)

        assertThat(colorFromPreferencesState(preferenceRepository.preferencesState.first()), not(equalTo(newColor1)))
        assertThat(colorFromGameSettings(preferenceRepository.gameSettings.first()), not(equalTo(newColor1)))
        assertThat(colorFromPreferencesState(preferenceRepository.preferencesState.first()), not(equalTo(newColor2)))
        assertThat(colorFromGameSettings(preferenceRepository.gameSettings.first()), not(equalTo(newColor2)))
        assertThat(colorFromGameSettings(preferenceRepository.gameSettings.first()), notNullValue())

        updateColor(newColor1)

        assertThat(colorFromPreferencesState(preferenceRepository.preferencesState.first()), equalTo(newColor1))
        assertThat(colorFromGameSettings(preferenceRepository.gameSettings.first()), equalTo(newColor1))

        preferenceRepository.updateUseSystemColors(true)

        assertThat(colorFromPreferencesState(preferenceRepository.preferencesState.first()), equalTo(newColor1))
        assertThat(colorFromGameSettings(preferenceRepository.gameSettings.first()), nullValue())

        preferenceRepository.updateUseSystemColors(false)

        assertThat(colorFromPreferencesState(preferenceRepository.preferencesState.first()), equalTo(newColor1))
        assertThat(colorFromGameSettings(preferenceRepository.gameSettings.first()), equalTo(newColor1))

        updateColor(newColor2)

        assertThat(colorFromPreferencesState(preferenceRepository.preferencesState.first()), equalTo(newColor2))
        assertThat(colorFromGameSettings(preferenceRepository.gameSettings.first()), equalTo(newColor2))

        preferenceRepository.updateUseSystemColors(true)

        assertThat(colorFromPreferencesState(preferenceRepository.preferencesState.first()), equalTo(newColor2))
        assertThat(colorFromGameSettings(preferenceRepository.gameSettings.first()), nullValue())
    }

    @Test
    fun updateCellsColor() = runBlocking {
        testColorPreference(
            colorFromGameSettings = { it.cellsColor },
            colorFromPreferencesState = { it.cellsColor },
            updateColor = { preferenceRepository.updateCellsColor(it) }
        )
    }

    @Test
    fun updateGridColor() = runBlocking {
        testColorPreference(
            colorFromGameSettings = { it.gridColor },
            colorFromPreferencesState = { it.gridColor },
            updateColor = { preferenceRepository.updateGridColor(it) }
        )
    }

    @Test
    fun updateBackgroundColor() = runBlocking {
        testColorPreference(
            colorFromGameSettings = { it.backgroundColor },
            colorFromPreferencesState = { it.backgroundColor },
            updateColor = { preferenceRepository.updateBackgroundColor(it) }
        )
    }

    @Test
    fun updateNewGamesDefaultSpeed() = runBlocking {
        assertThat(preferenceRepository.preferencesState.first().newGamesDefaultSpeed, equalTo(GameState.NEW_GAMES_DEFAULT_SPEED_PREFERENCE_DEFAULT))
        assertThat(preferenceRepository.gameSettings.first().newGamesDefaultSpeed, equalTo(GameState.NEW_GAMES_DEFAULT_SPEED_PREFERENCE_DEFAULT))

        val faster = GameState.NEW_GAMES_DEFAULT_SPEED_PREFERENCE_DEFAULT + 1
        val slower = GameState.NEW_GAMES_DEFAULT_SPEED_PREFERENCE_DEFAULT - 1

        preferenceRepository.updateNewGamesDefaultSpeed(faster)
        assertThat(preferenceRepository.preferencesState.first().newGamesDefaultSpeed, equalTo(faster))
        assertThat(preferenceRepository.gameSettings.first().newGamesDefaultSpeed, equalTo(faster))

        preferenceRepository.updateNewGamesDefaultSpeed(slower)
        assertThat(preferenceRepository.preferencesState.first().newGamesDefaultSpeed, equalTo(slower))
        assertThat(preferenceRepository.gameSettings.first().newGamesDefaultSpeed, equalTo(slower))
    }

    fun updateNewGamesDefaultCellSize() = runBlocking {
        assertThat(preferenceRepository.preferencesState.first().newGamesDefaultCellSize, equalTo(GameState.NEW_GAMES_DEFAULT_CELL_SIZE_PREFERENCE_DEFAULT))
        assertThat(preferenceRepository.gameSettings.first().newGamesDefaultCellSize, equalTo(GameState.NEW_GAMES_DEFAULT_CELL_SIZE_PREFERENCE_DEFAULT))

        val larger = GameState.NEW_GAMES_DEFAULT_CELL_SIZE_PREFERENCE_DEFAULT + 1
        val smaller = GameState.NEW_GAMES_DEFAULT_CELL_SIZE_PREFERENCE_DEFAULT - 1

        preferenceRepository.updateNewGamesDefaultCellSize(larger)
        assertThat(preferenceRepository.preferencesState.first().newGamesDefaultCellSize, equalTo(larger))
        assertThat(preferenceRepository.gameSettings.first().newGamesDefaultCellSize, equalTo(larger))

        preferenceRepository.updateNewGamesDefaultCellSize(smaller)
        assertThat(preferenceRepository.preferencesState.first().newGamesDefaultCellSize, equalTo(smaller))
        assertThat(preferenceRepository.gameSettings.first().newGamesDefaultCellSize, equalTo(smaller))
    }

}