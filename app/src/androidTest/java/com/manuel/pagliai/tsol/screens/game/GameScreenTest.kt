package com.manuel.pagliai.tsol.screens.game


import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.manuel.pagliai.tsol.MainActivity
import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.Screens
import com.manuel.pagliai.tsol.di.AppModule
import com.manuel.pagliai.tsol.ui.theme.TSoLTheme
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject
import android.content.res.Resources
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.test.*
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.test.espresso.Espresso
import com.manuel.pagliai.tsol.TestTags
import com.manuel.pagliai.tsol.domain.CellType
import com.manuel.pagliai.tsol.domain.Cells
import com.manuel.pagliai.tsol.domain.cells.CellTypes
import com.manuel.pagliai.tsol.domain.cells.KisrhombilleCellType
import com.manuel.pagliai.tsol.domain.cells.TriakisCellType
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*

private val Cells.aliveCellCount: Int
    get() {
        var count = 0
        repeat(size) {
            if (this[it]) {
                count++
            }
        }
        return count
    }

@HiltAndroidTest
@UninstallModules(AppModule::class)
class GameScreenTest {

    @get:Rule(order = 0)
    val hiltAndroidRule = HiltAndroidRule(this)

    @get:Rule(order = 1)
    val composeRule = createAndroidComposeRule<MainActivity>()

    @Inject
    lateinit var cellTypes: CellTypes

    private lateinit var gameViewModel: GameViewModel

    private val resources: Resources
        get() = composeRule.activity.resources

    @OptIn(ExperimentalAnimationApi::class)
    @Before
    fun setup() {
        hiltAndroidRule.inject()
        composeRule.setContent {
            TSoLTheme {
                Surface(
                    color = MaterialTheme.colorScheme.background,
                ) {
                    val navController = rememberNavController()
                    NavHost(
                        navController = navController,
                        startDestination = Screens.Game.route
                    ) {
                        composable(Screens.Game.route) {
                            gameViewModel = hiltViewModel()
                            GameScreen(navController = navController, viewModel = gameViewModel)
                        }
                    }
                }
            }
        }

        composeRule.waitUntil {
            composeRule.onAllNodesWithText(resources.getString(R.string.cancel_label))
                .fetchSemanticsNodes(false).isNotEmpty()
        }

        composeRule.onNodeWithText(
            resources.getString(R.string.cancel_label)
        ).performClick()

    }

    @Test
    fun changeCellTypeDialog() {

        fun changeCellType(cellType: CellType) {
            composeRule.onNodeWithContentDescription(
                resources.getString(R.string.change_cell_type_cd)
            ).performClick()

            composeRule.onNodeWithTag(TestTags.CHANGE_CELL_TYPE_DIALOG).assertExists()

            composeRule.onNodeWithTag(TestTags.CHANGE_CELL_TYPE_DIALOG).performScrollToNode(
                hasText(resources.getString(cellType.name))
            )

            composeRule.onNodeWithText(
                resources.getString(cellType.name)
            ).performClick()

            composeRule.waitForIdle()

            composeRule.onNodeWithTag(TestTags.CHANGE_CELL_TYPE_DIALOG).performScrollToNode(
                hasText(resources.getString(R.string.apply_label))
            )

            composeRule.onNodeWithText(resources.getString(R.string.apply_label)).performClick()

            composeRule.waitForIdle()


            if (cellType !is KisrhombilleCellType
                && cellType !is TriakisCellType
            ) { // test does not work for these particular cell types. We will need to scroll further, but it does not work for some reason.
                composeRule.waitUntil(3000) { cellType == gameViewModel.gameState.value?.cellType }
            }
        }

        cellTypes.list.forEach { cellType ->
            if (cellType == cellTypes.defaultCellType)
                return@forEach
            changeCellType(cellType)
        }
        changeCellType(cellTypes.defaultCellType)

    }

    @OptIn(ExperimentalTestApi::class)
    @Test
    fun changeSpeed() {

        fun changeSpeed(progress: Float) {
            composeRule.onNodeWithContentDescription(
                resources.getString(R.string.menu_cd)
            ).performClick()

            composeRule.waitForIdle()

            composeRule.onNodeWithText(
                resources.getString(R.string.open_change_speed_dialog)
            ).performClick()

            composeRule.waitForIdle()

            composeRule.onNodeWithTag(TestTags.CHANGE_SPEED_SCROLLBAR).performMouseInput {
                this.dragAndDrop(
                    Offset(left, centerY),
                    Offset((right - left) * progress + left, centerY)
                )
            }

            composeRule.waitForIdle()

            dismissDialog()

            composeRule.waitForIdle()
        }

        addGlider()

        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.play_cd)
        ).performClick()

        for (i in GameState.MIN_SPEED until GameState.MAX_SPEED) {
            changeSpeed(i.toFloat() / (GameState.MAX_SPEED - GameState.MIN_SPEED))
        }
    }

    private fun addGlider() {
        val grid = gameViewModel.gameState.value!!.currentGenCells.grid
        runBlocking {
            gameViewModel.onCellSelected(grid.toLinear(x = 1, y = 0))
            gameViewModel.onCellSelected(grid.toLinear(x = 2, y = 1))
            gameViewModel.onCellSelected(grid.toLinear(x = 0, y = 2))
            gameViewModel.onCellSelected(grid.toLinear(x = 1, y = 2))
            gameViewModel.onCellSelected(grid.toLinear(x = 2, y = 2))
        }

        composeRule.waitUntil {
            gameViewModel.gameState.value!!.currentGenCells.aliveCellCount == 5
        }
    }

    private fun dismissDialog() {
        Espresso.pressBack()
    }

    @OptIn(ExperimentalTestApi::class)
    @Test
    fun changeCellSize() {
        fun changeCellSize(progress: Float) {
            composeRule.onNodeWithContentDescription(
                resources.getString(R.string.menu_cd)
            ).performClick()

            composeRule.waitForIdle()

            composeRule.onNodeWithText(
                resources.getString(R.string.open_change_cell_size_dialog)
            ).performClick()

            composeRule.waitForIdle()

            composeRule.onNodeWithTag(TestTags.CHANGE_CELL_SIZE_SCROLLBAR).performMouseInput {
                this.dragAndDrop(
                    Offset(left, centerY),
                    Offset((right - left) * progress + left, centerY)
                )
            }

            composeRule.waitForIdle()

            dismissDialog()

            composeRule.waitForIdle()
        }

        addGlider()

        for (i in GameState.MIN_CELL_SIZE until GameState.MAX_CELL_SIZE) {
            changeCellSize(i.toFloat() / (GameState.MAX_CELL_SIZE - GameState.MIN_CELL_SIZE))
        }

        assertTrue(gameViewModel.gameState.value!!.currentGenCells.alive)
    }

    @Test
    fun rename() {
        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.menu_cd)
        ).performClick()

        composeRule.onNodeWithText(
            resources.getString(R.string.open_rename_dialog)
        ).performClick()

        composeRule.waitForIdle()

        composeRule.onNodeWithText(
            resources.getString(R.string.rename_dialog_title)
        ).assertIsDisplayed()

        composeRule.onNodeWithTag(TestTags.RENAME_GAME_TEXT_FIELD).performTextInput("TestR3nam3123")
        composeRule.waitForIdle()
        composeRule.onNodeWithText(resources.getString(R.string.rename_game_label)).performClick()
        composeRule.waitForIdle()

        composeRule.onNodeWithText(
            resources.getString(R.string.rename_dialog_title)
        ).assertDoesNotExist()
        composeRule.onNodeWithText("TestR3nam3123").assertIsDisplayed()
    }

    @Test
    fun createCopy() {
        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.menu_cd)
        ).performClick()

        composeRule.waitForIdle()

        composeRule.onNodeWithText(
            resources.getString(R.string.open_create_copy_dialog)
        ).performClick()

        composeRule.waitForIdle()

        composeRule.onAllNodesWithText(
            resources.getString(R.string.create_copy_dialog_title)
        ).filter(hasNoClickAction())[0].assertIsDisplayed()

        composeRule.onNodeWithTag(TestTags.CREATE_GAME_COPY_TEXT_FIELD)
            .performTextInput("TestCopy123")
        composeRule.waitForIdle()
        composeRule.onAllNodesWithText(resources.getString(R.string.create_game_copy_label)).filter(
            hasClickAction()
        )[0].performClick()
        composeRule.waitForIdle()

        composeRule.onNodeWithText(
            resources.getString(R.string.create_copy_dialog_title)
        ).assertDoesNotExist()
        composeRule.onNodeWithText("TestCopy123").assertIsDisplayed()
    }

    @Test
    fun testUndoRedo() {

        // can't undo/redo

        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.undo_cd)
        ).assertIsNotEnabled()

        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.redo_cd)
        ).assertIsNotEnabled()


        // 5 edits
        addGlider()

        assertEquals(5, currentCellCount())

        // can undo but not redo
        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.redo_cd)
        ).assertIsNotEnabled()

        // undo once
        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.undo_cd)
        ).apply {
            assertIsEnabled()
            performClick()
        }

        composeRule.waitForIdle()

        // can undo and redo
        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.undo_cd)
        ).assertIsEnabled()

        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.redo_cd)
        ).assertIsEnabled()

        composeRule.waitForIdle()

        assertEquals(4, currentCellCount())

        // undo the rest
        repeat(4) {
            composeRule.onNodeWithContentDescription(
                resources.getString(R.string.undo_cd)
            ).apply {
                assertIsEnabled()
                performClick()
            }
            composeRule.waitForIdle()
        }

        composeRule.waitForIdle()

        assertEquals(0, currentCellCount())

        // can only redo
        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.undo_cd)
        ).assertIsNotEnabled()

        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.redo_cd)
        ).assertIsEnabled()

        // redo
        repeat(5) {
            composeRule.onNodeWithContentDescription(
                resources.getString(R.string.redo_cd)
            ).apply {
                assertIsEnabled()
                performClick()
            }
            composeRule.waitForIdle()
            composeRule.onNodeWithContentDescription(
                resources.getString(R.string.undo_cd)
            ).assertIsEnabled()
        }

        composeRule.waitForIdle()

        assertEquals(5, currentCellCount())

        // clear
        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.clear_cd)
        ).performClick()
        composeRule.waitForIdle()

        assertEquals(0, currentCellCount())

        // can only undo
        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.redo_cd)
        ).assertIsNotEnabled()

        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.undo_cd)
        ).apply {
            assertIsEnabled()
            performClick()
        }

        composeRule.waitForIdle()

        assertEquals(5, currentCellCount())

        // redo
        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.redo_cd)
        ).apply {
            assertIsEnabled()
            performClick()
        }

        composeRule.waitForIdle()

        assertEquals(0, currentCellCount())
    }


    private fun currentCellCount(): Int =
        gameViewModel.gameState.value!!.currentGenCells.aliveCellCount

    @Test
    fun testClear() {
        addGlider()
        assertTrue(gameViewModel.gameState.value!!.currentGenCells.alive)
        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.clear_cd)
        ).performClick()
        composeRule.waitForIdle()
        assertFalse(gameViewModel.gameState.value!!.currentGenCells.alive)
    }

    @Test
    fun testRestore() {
        addGlider()

        // play
        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.play_cd)
        ).performClick()

        composeRule.waitUntil(15_000) {
            isGenerationDisplayed(5)
        }

        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.clear_cd)
        ).assertDoesNotExist()

        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.restore_cd)
        ).assertIsEnabled()

        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.restore_cd)
        ).performClick()

        composeRule.waitUntil(15_000) {
            isGenerationDisplayed(3)
        }

        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.pause_cd)
        ).apply {
            assertIsEnabled()
            performClick()
        }

        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.restore_cd)
        ).assertIsEnabled()

        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.restore_cd)
        ).performClick()


        composeRule.waitUntil(15_000) {
            isGenerationDisplayed(0)
        }

        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.restore_cd)
        ).assertDoesNotExist()

        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.clear_cd)
        ).assertIsDisplayed()

    }

    @Test
    fun playPause() {

        addGlider()

        val startCells = gameViewModel.gameState.value!!.currentGenCells

        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.play_cd)
        ).performClick()

        composeRule.waitUntil(15_000) {
            isGenerationDisplayed(5)
        }

        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.pause_cd)
        ).performClick()

        val endCells = gameViewModel.gameState.value!!.currentGenCells

        assertTrue(endCells.alive)
        assertNotEquals(startCells, endCells)
    }

    private fun isGenerationDisplayed(gen: Int): Boolean {
        return hasText("$gen").matches(
            composeRule.onNodeWithTag(TestTags.AUTOMATON_GENERATION_LABEL).fetchSemanticsNode()
        )
    }

    @Test
    fun nextGenPrevGen() {
        addGlider()

        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.previous_generation_cd)
        ).assertIsNotEnabled()

        // click next
        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.next_generation_cd)
        ).apply {
            assertIsEnabled()
            assertIsDisplayed()
            performClick()
        }

        composeRule.waitUntil {
            isGenerationDisplayed(1)
        }

        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.previous_generation_cd)
        ).assertIsEnabled()


        // click next again
        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.next_generation_cd)
        ).performClick()

        composeRule.waitUntil {
            isGenerationDisplayed(2)
        }

        // prev gen
        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.previous_generation_cd)
        ).performClick()

        composeRule.waitUntil {
            isGenerationDisplayed(1)
        }

        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.previous_generation_cd)
        ).assertIsEnabled()

        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.next_generation_cd)
        ).assertIsEnabled()

        // prev gen (again)
        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.previous_generation_cd)
        ).performClick()

        // cannot prev gen no more
        composeRule.waitUntil {
            isGenerationDisplayed(0)
        }

        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.previous_generation_cd)
        ).assertIsNotEnabled()

        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.next_generation_cd)
        ).assertIsEnabled()


    }

    @Test
    fun playAndPrevGen() {
        addGlider()

        // play
        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.play_cd)
        ).performClick()

        composeRule.waitUntil(15_000) {
            isGenerationDisplayed(5)
        }

        // pause
        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.pause_cd)
        ).performClick()

        // prev gen
        repeat(5) {
            composeRule.onNodeWithContentDescription(
                resources.getString(R.string.previous_generation_cd)
            ).assertIsEnabled()

            composeRule.onNodeWithContentDescription(
                resources.getString(R.string.next_generation_cd)
            ).assertIsEnabled()

            composeRule.onNodeWithContentDescription(
                resources.getString(R.string.previous_generation_cd)
            ).performClick()
        }

        // prev gen the others
        repeat(gameViewModel.gameState.value!!.generation) {
            composeRule.onNodeWithContentDescription(
                resources.getString(R.string.previous_generation_cd)
            ).performClick()
        }

        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.previous_generation_cd)
        ).assertIsNotEnabled()

        composeRule.onNodeWithContentDescription(
            resources.getString(R.string.next_generation_cd)
        ).assertIsEnabled()

    }

}