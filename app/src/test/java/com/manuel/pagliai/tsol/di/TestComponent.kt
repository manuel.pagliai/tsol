package com.manuel.pagliai.tsol.di

import com.manuel.pagliai.tsol.domain.cells.CellTypeTest
import com.manuel.pagliai.tsol.domain.cells.CellTypesTest
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShapeTilingTest
import com.manuel.pagliai.tsol.domain.engine.GameEngineTest
import com.manuel.pagliai.tsol.domain.engine.GameModelTest
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface TestComponent {
    fun inject(cellTypesTest: CellTypesTest)
    fun inject(cellTypeTest: CellTypeTest)
    fun inject(periodicShapeTilingTest: PeriodicShapeTilingTest)
    fun inject(gameModelTest: GameModelTest)
    fun inject(gameEngineTest: GameEngineTest)
}