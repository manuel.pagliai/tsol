package com.manuel.pagliai.tsol.domain

import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import kotlin.random.Random

class GameRulesTest  {

    private lateinit var random: Random

    @Before
    fun setup() {
        random = Random(0)
    }

    @Test
    fun encoding() {
        fun encoding(gameRules: GameRules) {
            assertEquals(gameRules, GameRules.Encoding.decode(GameRules.Encoding.encode(gameRules)))
        }

        encoding(GameRules.B234S23)
        encoding(GameRules.B2S34)
        encoding(GameRules.B34S356)
        encoding(GameRules.B234S23)
        encoding(GameRules.B45S2345)
        encoding(GameRules(birthWith = emptyList(), surviveWith = emptyList()))
        encoding(GameRules(birthWith = emptyList(), surviveWith = emptyList(), pRandomBirth = 0.1f))
        encoding(GameRules(birthWith = emptyList(), surviveWith = emptyList(), pRandomDeath = 0.1f))
        encoding(GameRules(birthWith = emptyList(), surviveWith = emptyList(), pRandomDeath = 0.1f, pRandomBirth = 0.2f))

        repeat(100_000) {
            val birthWith = IntArray(random.nextInt(0, 20)) {random.nextInt(100)}.toSet().toList()
            val surviveWith = IntArray(random.nextInt(0, 20)) {random.nextInt(100)}.toSet().toList()
            val pRandomBirth = random.nextFloat()
            val pRandomDeath = random.nextFloat()
            encoding(GameRules(birthWith = birthWith, surviveWith = surviveWith))
            encoding(GameRules(birthWith = birthWith, surviveWith = surviveWith, pRandomBirth = pRandomBirth))
            encoding(GameRules(birthWith = birthWith, surviveWith = surviveWith, pRandomDeath = pRandomDeath))
            encoding(GameRules(birthWith = birthWith, surviveWith = surviveWith, pRandomDeath = pRandomDeath, pRandomBirth = pRandomBirth))
        }
    }

    @Test
    fun aliveInNextGeneration() {
        val b2s34 = GameRules(birthWith = listOf(2), surviveWith = listOf(3,4))
        val always07 = mockk<Random>()
        every { always07.nextDouble() }.returns(0.7)

        // birth
        assertTrue(b2s34.aliveInNextGen(false, 2, random))
        assertFalse(b2s34.aliveInNextGen(false, 3, random))
        assertFalse(b2s34.aliveInNextGen(false, 4, random))


        // survival
        assertFalse(b2s34.aliveInNextGen(true, 2, random))
        assertTrue(b2s34.aliveInNextGen(true, 3, random))
        assertTrue(b2s34.aliveInNextGen(true, 4, random))

        // random death
        assertFalse(b2s34.copy(pRandomDeath = 0.8f).aliveInNextGen(true, 3, always07))
        assertTrue(b2s34.copy(pRandomDeath = 0.5f).aliveInNextGen(true, 3, always07))
        assertFalse(b2s34.copy(pRandomDeath = 0.8f).aliveInNextGen(false, 3, always07))
        assertFalse(b2s34.copy(pRandomDeath = 0.5f).aliveInNextGen(false, 3, always07))
        assertFalse(b2s34.copy(pRandomDeath = 0.8f).aliveInNextGen(false, 2, always07))
        assertTrue(b2s34.copy(pRandomDeath = 0.5f).aliveInNextGen(false, 2, always07))

        // random birth
        assertTrue(b2s34.copy(pRandomBirth = 0.8f).aliveInNextGen(false, 0, always07))
        assertFalse(b2s34.copy(pRandomBirth = 0.5f).aliveInNextGen(false, 0, always07))
        assertTrue(b2s34.copy(pRandomBirth = 0.8f).aliveInNextGen(true, 5, always07))
        assertFalse(b2s34.copy(pRandomBirth = 0.5f).aliveInNextGen(true, 5, always07))
        assertTrue(b2s34.copy(pRandomBirth = 0.8f).aliveInNextGen(false, 2, always07))
        assertTrue(b2s34.copy(pRandomBirth = 0.5f).aliveInNextGen(false, 2, always07))
    }

    @Test
    fun testHashCode_Equals() {
        val b2s34 = GameRules(birthWith = listOf(2), surviveWith = listOf(3,4))
        val anotherB2s34 = GameRules(birthWith = listOf(2), surviveWith = listOf(3,4))
        val b2s345 = GameRules(birthWith = listOf(2), surviveWith = listOf(3,4,5))
        val anotherB2s345 = GameRules(birthWith = listOf(2), surviveWith = listOf(3,4,5))
        val b23s34 = GameRules(birthWith = listOf(2, 3), surviveWith = listOf(3,4))
        val anotherB23s34 = GameRules(birthWith = listOf(2, 3), surviveWith = listOf(3,4))

        assertEquals(b2s34, anotherB2s34)
        assertEquals(b2s34.hashCode(), anotherB2s34.hashCode())
        assertNotEquals(b2s34, "")
        assertNotEquals(b2s34, null)

        assertEquals(b2s345, anotherB2s345)
        assertEquals(b2s345.hashCode(), anotherB2s345.hashCode())

        assertEquals(b23s34, anotherB23s34)
        assertEquals(b23s34.hashCode(), anotherB23s34.hashCode())

        assertNotEquals(b2s34, b23s34)
        assertNotEquals(b2s34, b2s345)
        assertNotEquals(b23s34, b2s345)
    }

   @Test
    fun deterministic() {
        val b2s34 = GameRules(birthWith = listOf(2), surviveWith = listOf(3,4))
        assertTrue(b2s34.isDeterministicGame)
        assertFalse(b2s34.copy(pRandomBirth = 0.1f).isDeterministicGame)
        assertFalse(b2s34.copy(pRandomDeath = 0.1f).isDeterministicGame)
        assertFalse(b2s34.copy(pRandomDeath = 0.1f, pRandomBirth = 0.3f).isDeterministicGame)
    }
}