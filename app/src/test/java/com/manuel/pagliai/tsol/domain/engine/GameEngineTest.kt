package com.manuel.pagliai.tsol.domain.engine

import com.manuel.pagliai.tsol.TestCanvasConfiguration
import com.manuel.pagliai.tsol.di.DaggerTestComponent
import com.manuel.pagliai.tsol.domain.Cells
import com.manuel.pagliai.tsol.domain.GameRules
import com.manuel.pagliai.tsol.domain.Grid
import com.manuel.pagliai.tsol.domain.SavedGame
import com.manuel.pagliai.tsol.domain.cells.CellTypes
import com.manuel.pagliai.tsol.domain.cells.HexagonalCellType
import com.manuel.pagliai.tsol.domain.cells.SquareCellType
import com.manuel.pagliai.tsol.domain.GameSettings
import com.manuel.pagliai.tsol.screens.game.GameControls
import com.manuel.pagliai.tsol.screens.game.GameState
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.*
import org.junit.Assert.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import javax.inject.Inject
import kotlin.random.Random


class GameEngineTest {

    private lateinit var random: Random
    private lateinit var executorService: ExecutorService
    private lateinit var gameModel: GameModel
    lateinit var uut: GameEngine
    private lateinit var dispatcher: CoroutineDispatcher

    @Inject
    lateinit var cellTypes: CellTypes

    @MockK
    lateinit var lifeSaverMock: LifeSaver

    @MockK
    lateinit var idleTrackerMock: IdleTracker

    @ExperimentalCoroutinesApi
    @Before
    fun setUp() {
        dispatcher = StandardTestDispatcher()
        Dispatchers.setMain(dispatcher)
        DaggerTestComponent.create().inject(this)
        MockKAnnotations.init(this, relaxUnitFun = true)
        coEvery { lifeSaverMock.save(any()) } returns 1
        random = Random(0)
        executorService = Executors.newSingleThreadExecutor()
        gameModel = GameModel(
            editHistory = HistoryBuffer(100),
            executionHistory = HistoryBuffer(100),
            loopDetector = ProbabilisticLoopDetector(10000),
            clock = Clock(),
            automatonNextGenCalculator = AutomatonNextGenCalculator(random)
        )
        uut = GameEngine(
            engineExecutorService = executorService,
            cellTypes = cellTypes,
            _model = gameModel,
            idleTracker = idleTrackerMock,
            lifeSaver = lifeSaverMock,
            random = random
        )

        runBlocking {
            uut.initialiseNewGame(
                name = "Test",
                orientation = ScreenOrientation.PORTRAIT,
                canvasHeight = TestCanvasConfiguration.PIXEL2_PORTRAIT.canvasHeight,
                canvasWidth = TestCanvasConfiguration.PIXEL2_PORTRAIT.canvasWidth,
                displayDensity = TestCanvasConfiguration.PIXEL2_PORTRAIT.displayDensity,
                gameSettings = GameSettings(
                    showNextGenAnimationIdle = false,
                    randomiseNewGamesCells = false,
                    cellsColor = null,
                    gridColor = null,
                    backgroundColor = null,
                    newGamesDefaultCellSize = 5,
                    newGamesDefaultSpeed = 5
                )
            )
        }
    }

    @ExperimentalCoroutinesApi
    @After
    fun tearDown() {
        executorService.shutdown()
        Dispatchers.resetMain()
    }

    @Test
    fun speed() = runBlocking {
        val oldSpeed = uut.firstGameState().speed
        val newSpeed = oldSpeed + 1
        uut.changeSpeed(newSpeed)
        assertEquals(newSpeed, uut.firstGameState().speed)
    }

    @Test
    fun nextGenNotAlive() = runBlocking {
        val oldGame = uut.firstGameState()
        uut.nextGen()
        val newGame = uut.firstGameState()

        assertFalse(oldGame.currentGenCells.alive)
        assertFalse(newGame.currentGenCells.alive)

        assertSame(newGame.currentGenCells, oldGame.nextGenCells)
        assertEquals(newGame.nextGenCells, oldGame.nextGenCells)

        assertTrue(newGame.isLooping)
        assertFalse(oldGame.isLooping)
        assertEquals(newGame.generation, oldGame.generation + 1)
    }

    @Test
    fun sceneUpdateGlider_prevGen() = runBlocking {
        setupGlider()

        val oldGame = uut.firstGameState()

        uut.nextGen()
        val newGame = uut.firstGameState()

        assertTrue(oldGame.currentGenCells.alive)
        assertTrue(newGame.currentGenCells.alive)

        assertSame(newGame.currentGenCells, oldGame.nextGenCells)
        assertNotEquals(newGame.nextGenCells, oldGame.nextGenCells)
        assertNotEquals(newGame.currentGenCells, oldGame.currentGenCells)
        assertFalse(newGame.isLooping)
        assertFalse(oldGame.isLooping)
        assertEquals(newGame.generation, oldGame.generation + 1)

        uut.prevGen()

        assertEquals(oldGame.resetTimestamps(), uut.firstGameState().resetTimestamps())
    }

    private suspend fun setupGlider() {
        val grid = uut.firstGameState().currentGenCells.grid
        uut.cellSelected(grid.toLinear(1, 0))
        uut.cellSelected(grid.toLinear(2, 1))
        uut.cellSelected(grid.toLinear(0, 2))
        uut.cellSelected(grid.toLinear(1, 2))
        uut.cellSelected(grid.toLinear(2, 2))

    }

    @Test
    fun controls() = runBlocking {

        val initialControls = uut.firstGameState().controls

        assertTrue(initialControls.canChangeCellType)
        assertFalse(initialControls.canNextGen)
        assertFalse(initialControls.canPrevGen)
        assertFalse(initialControls.canUndo)
        assertFalse(initialControls.canRedo)
        assertFalse(initialControls.isPlaying)
        assertTrue(initialControls.addCellsOnSelection)
        assertFalse(initialControls.canClear)

        // select a cell
        runBlocking {
            uut.cellSelected(42)
        }

        assertEquals(
            initialControls.copy(
                canUndo = true,
                canNextGen = true,
                canClear = true
            ), uut.firstGameState().controls
        )

        // select the same cell, nothing changes
        runBlocking {
            uut.cellSelected(42)
        }

        assertEquals(
            initialControls.copy(
                canUndo = true,
                canNextGen = true,
                canClear = true
            ), uut.firstGameState().controls
        )

        // select another cell then undo -> enable redo
        uut.cellSelected(43)
        assertTrue(uut.firstGameState().controls.canUndo)
        uut.undo()


        assertEquals(
            initialControls.copy(
                canUndo = true,
                canRedo = true,
                canNextGen = true,
                canClear = true
            ), uut.firstGameState().controls
        )

        // nextgen -> can reset but edit history is lost

        runBlocking { uut.nextGen() }

        assertEquals(
            initialControls.copy(
                canPrevGen = true,
                canRedo = false,
                canClear = false,
                canNextGen = true
            ), uut.firstGameState().controls
        )

        // prevGen -> edit history is back

        runBlocking { uut.prevGen() }

        assertEquals(
            initialControls.copy(
                canUndo = true,
                canRedo = true,
                canNextGen = true,
                canClear = true
            ), uut.firstGameState().controls
        )

        // clear and setup a glider

        runBlocking {
            assertTrue(uut.firstGameState().controls.canClear)
            uut.clearCells()
            assertFalse(uut.firstGameState().controls.canClear)
        }

        setupGlider()

        // start
        uut.runFor(3, pause = false)

        val runningAutomatonControlsViewState = uut.firstGameState().controls

        assertFalse(runningAutomatonControlsViewState.canChangeCellType)
        assertFalse(runningAutomatonControlsViewState.canNextGen)
        assertFalse(runningAutomatonControlsViewState.canPrevGen)
        assertFalse(runningAutomatonControlsViewState.canUndo)
        assertFalse(runningAutomatonControlsViewState.canRedo)
        assertTrue(runningAutomatonControlsViewState.isPlaying)
        assertTrue(runningAutomatonControlsViewState.addCellsOnSelection)
        assertFalse(runningAutomatonControlsViewState.canClear)


        uut.pause()


        assertEquals(
            runningAutomatonControlsViewState.copy(
                isPlaying = false,
                canPrevGen = true,
                canNextGen = true,
                canChangeCellType = true
            ),
            uut.firstGameState().controls
        )
    }

    @Test
    fun prevGen() = runBlocking {
        setupGlider()
        val initialGliderState = uut.firstGameState()

        uut.nextGen()

        val nextState = uut.firstGameState()
        assertNotEquals(nextState, initialGliderState)


        uut.prevGen()

        val newInitialState = uut.firstGameState()

        assertNotSame(newInitialState, initialGliderState)
        assertEquals(newInitialState.resetTimestamps(), initialGliderState.resetTimestamps())
    }

    @Test
    fun prevGen_Play() = runBlocking {

        suspend fun prevGenWithCheck() {
            assertTrue(uut.firstGameState().controls.canPrevGen)
            uut.prevGen()
        }

        setupGlider()

        uut.runFor(3)


        prevGenWithCheck()
        prevGenWithCheck()
        prevGenWithCheck()
    }

    @Test
    fun givingLifeToCellAlreadyAliveIsNotAnEdit() = runBlocking {
        assertFalse(uut.firstGameState().currentGenCells[3])

        uut.cellSelected(3)
        uut.cellSelected(3)

        assertTrue(uut.firstGameState().currentGenCells[3])

        runBlocking { uut.undo() }

        assertFalse(uut.firstGameState().currentGenCells[3])
    }

    @Test
    fun cellSelected() = runBlocking {
        assertTrue(uut.firstGameState().controls.addCellsOnSelection)

        assertFalse(uut.firstGameState().currentGenCells[42])
        uut.cellSelected(42)
        assertTrue(uut.firstGameState().currentGenCells[42])

        uut.setAddCellsOnSelection(false)
        assertFalse(uut.firstGameState().controls.addCellsOnSelection)

        uut.cellSelected(42)
        assertFalse(uut.firstGameState().currentGenCells[42])
    }

    @Test
    fun nextGen() = runBlocking {
        setupGlider()
        nextGenWithAssertions()
        nextGenWithAssertions()
        nextGenWithAssertions()
        nextGenWithAssertions().currentGenCells.let {
            assertTrue(it[2, 1])
            assertTrue(it[3, 2])
            assertTrue(it[1, 3])
            assertTrue(it[2, 3])
            assertTrue(it[3, 3])
        }

    }

    private suspend fun nextGenWithAssertions(): GameState {
        val before = uut.firstGameState()
        uut.nextGen()
        val after = uut.firstGameState()
        assertEquals(after.generation, before.generation + 1)
        uut.prevGen()
        val before2 = uut.firstGameState()
        assertEquals(before.resetTimestamps(), before2.resetTimestamps())
        uut.nextGen()
        val after2 = uut.firstGameState()
        assertEquals(after.resetTimestamps(), after2.resetTimestamps())
        return after2
    }

    @Test
    fun undoRedo() = runBlocking {
        suspend fun undoWithChecks() {
            assertTrue(uut.firstGameState().controls.canUndo)
            uut.undo()
        }

        suspend fun redoWithChecks() {
            assertTrue(uut.firstGameState().controls.canRedo)
            uut.redo()
        }

        assertFalse(uut.firstGameState().controls.canUndo)

        // give life to three cells (one at a time)

        uut.cellSelected(0)
        uut.cellSelected(1)
        uut.cellSelected(2)

        assertTrue(uut.firstGameState().controls.canClear)


        // clear + undo
        uut.clearCells()


        assertFalse(uut.firstGameState().currentGenCells[0])
        assertFalse(uut.firstGameState().currentGenCells[1])
        assertFalse(uut.firstGameState().currentGenCells[2])


        undoWithChecks()
        assertTrue(uut.firstGameState().currentGenCells[0])
        assertTrue(uut.firstGameState().currentGenCells[1])
        assertTrue(uut.firstGameState().currentGenCells[2])

        // sequence of undo/redo

        undoWithChecks()
        assertTrue(uut.firstGameState().currentGenCells[0])
        assertTrue(uut.firstGameState().currentGenCells[1])
        assertFalse(uut.firstGameState().currentGenCells[2])


        undoWithChecks()
        assertTrue(uut.firstGameState().currentGenCells[0])
        assertFalse(uut.firstGameState().currentGenCells[1])
        assertFalse(uut.firstGameState().currentGenCells[2])

        undoWithChecks()
        assertFalse(uut.firstGameState().currentGenCells[0])
        assertFalse(uut.firstGameState().currentGenCells[1])
        assertFalse(uut.firstGameState().currentGenCells[2])

        redoWithChecks()
        assertTrue(uut.firstGameState().currentGenCells[0])
        assertFalse(uut.firstGameState().currentGenCells[1])
        assertFalse(uut.firstGameState().currentGenCells[2])

        redoWithChecks()
        assertTrue(uut.firstGameState().currentGenCells[0])
        assertTrue(uut.firstGameState().currentGenCells[1])
        assertFalse(uut.firstGameState().currentGenCells[2])

        undoWithChecks()
        assertTrue(uut.firstGameState().currentGenCells[0])
        assertFalse(uut.firstGameState().currentGenCells[1])
        assertFalse(uut.firstGameState().currentGenCells[2])

        redoWithChecks()
        assertTrue(uut.firstGameState().currentGenCells[0])
        assertTrue(uut.firstGameState().currentGenCells[1])
        assertFalse(uut.firstGameState().currentGenCells[2])

        redoWithChecks()
        assertTrue(uut.firstGameState().currentGenCells[0])
        assertTrue(uut.firstGameState().currentGenCells[1])
        assertTrue(uut.firstGameState().currentGenCells[2])

        undoWithChecks()
        assertTrue(uut.firstGameState().currentGenCells[0])
        assertTrue(uut.firstGameState().currentGenCells[1])
        assertFalse(uut.firstGameState().currentGenCells[2])

        // next gen -> history is lost

        uut.nextGen()

        assertFalse(uut.firstGameState().controls.canUndo)
        assertFalse(uut.firstGameState().controls.canRedo)

        // prev gen ->// play -> history is lost

        runBlocking {
            uut.prevGen()
        }

        assertTrue(uut.firstGameState().controls.canUndo)
        assertTrue(uut.firstGameState().controls.canRedo)

        // play -> history is lost

        uut.runFor(3)


        assertFalse(uut.firstGameState().controls.canUndo)
        assertFalse(uut.firstGameState().controls.canRedo)

        assertFalse(uut.firstGameState().controls.canClear)
        uut.restoreLastEdited()


        assertTrue(uut.firstGameState().controls.canUndo)
        assertTrue(uut.firstGameState().controls.canRedo)

        undoWithChecks()
        assertTrue(uut.firstGameState().currentGenCells[0])
        assertFalse(uut.firstGameState().currentGenCells[1])
        assertFalse(uut.firstGameState().currentGenCells[2])

        redoWithChecks()
        assertTrue(uut.firstGameState().currentGenCells[0])
        assertTrue(uut.firstGameState().currentGenCells[1])
        assertFalse(uut.firstGameState().currentGenCells[2])

        redoWithChecks()
        assertTrue(uut.firstGameState().currentGenCells[0])
        assertTrue(uut.firstGameState().currentGenCells[1])
        assertTrue(uut.firstGameState().currentGenCells[2])
    }

    @Test
    fun automatonRendered() = runBlocking {
        setupGlider()

        uut.runFor(5, pause = false)
        assertEquals(5, uut.firstGameState().generation)

        val timeMillis = GameEngine.DELAYS[uut.firstGameState().speed]

        Thread.sleep(3 * timeMillis)
        assertEquals(5, uut.firstGameState().generation)

        // automatonRendered will schedule the next automaton update

        uut.onAutomatonRenderCompleted()
        delay(2 * timeMillis)


        assertEquals(6, uut.firstGameState().generation)
    }

    @Test
    fun playPause() = runBlocking {
        setupGlider()
        val initialState = uut.firstGameState()
        assertFalse(initialState.controls.isPlaying)
        uut.runFor(3, pause = false)
        assertEquals(3, uut.firstGameState().generation)

        assertEquals(
            GameControls(
                isPlaying = true,
                canChangeCellType = false,
                canNextGen = false,
                canPrevGen = false,
                canClear = false,
                canRedo = false,
                canUndo = false,
                addCellsOnSelection = true
            ), uut.firstGameState().controls
        )

        // restore  (while playing)
        uut.restoreLastEdited()

        assertEquals(
            initialState.resetTimestamps()
                .copy(
                    controls = GameControls(
                        isPlaying = true,
                        canChangeCellType = false,
                        canNextGen = false,
                        canPrevGen = false,
                        canClear = false,
                        canRedo = false,
                        canUndo = false,
                        addCellsOnSelection = true
                    )
                ),
            uut.firstGameState().resetTimestamps()
        )

        uut.runFor(4, start = false, pause = false)
        assertEquals(4, uut.firstGameState().generation)

        // pause and assert controls
        uut.pause()

        assertEquals(
            GameControls(
                isPlaying = false,
                canChangeCellType = true,
                canNextGen = true,
                canPrevGen = true,
                canClear = false,
                canRedo = false,
                canUndo = false,
                addCellsOnSelection = true
            ), uut.firstGameState().controls
        )


        uut.restoreLastEdited()

        assertEquals(initialState.resetTimestamps(), uut.firstGameState().resetTimestamps())
    }

    @Test
    fun updateCells() = runBlocking {
        val initialState = uut.firstGameState()
        assertTrue(initialState.cellType is SquareCellType)
        val hexCellType = cellTypes[HexagonalCellType::class]
        uut.changeCellTypeOrRules(
            hexCellType,
            displayDensity = 1f,
            canvasWidth = 1000f,
            canvasHeight = 2000f,
            newRules = hexCellType.defaultGameRules
        )

        assertTrue(uut.firstGameState().cellType is HexagonalCellType)
        assertNotEquals(initialState.rules, uut.firstGameState().rules)
    }

    @Test
    fun resize() = runBlocking {
        for (cc in TestCanvasConfiguration.all()) {
            for (cellType in cellTypes.list) {
                uut.changeCellTypeOrRules(
                    newCellType = cellType,
                    newRules = cellType.defaultGameRules,
                    canvasWidth = cc.canvasWidth,
                    canvasHeight = cc.canvasHeight,
                    displayDensity = cc.displayDensity
                )
                var lastRows = -1
                var lastColumns = -1
                // make sure not all rows or cols are the same (we use <=)
                val heights = mutableSetOf<Int>()
                val widths = mutableSetOf<Int>()

                for (cellSize in GameState.MAX_CELL_SIZE downTo GameState.MIN_CELL_SIZE) {
                    uut.changeCellSize(
                        newSize = cellSize,
                        displayDensity = cc.displayDensity,
                        canvasHeight = cc.canvasHeight,
                        canvasWidth = cc.canvasWidth
                    )
                    uut.firstGameState().currentGenCells.grid.let {
                        assertTrue(lastRows <= it.rows)
                        assertTrue(lastColumns <= it.columns)
                        lastRows = it.rows
                        lastColumns = it.columns
                        widths.add(lastColumns)
                        heights.add(lastRows)
                    }
                }

                assertTrue(heights.size > 1)
                assertTrue(widths.size > 1)
            }
        }
    }

    @Test
    fun initialiseFromSavedGame() = runBlocking {
        uut = GameEngine(
            engineExecutorService = executorService,
            cellTypes = cellTypes,
            _model = gameModel,
            idleTracker = idleTrackerMock,
            lifeSaver = lifeSaverMock,
            random = random
        )

        val cells = Cells(grid = Grid(rows = 12, columns = 80)) { random.nextBoolean() }
        val savedGame = SavedGame(
            name = "#1",
            modifiedAt = 42L,
            orientation = ScreenOrientation.LANDSCAPE,
            speed = 2,
            previewAspectRatio = 0.7f,
            cells = cells,
            cellType = cellTypes[HexagonalCellType::class],
            cellSize = 4,
            createdAt = 23L,
            id = 30L,
            rules = GameRules.B45S2345
        )

        uut.initialiseFromSavedGame(
            savedGame = savedGame,
            GameSettings(
                randomiseNewGamesCells = false,
                showNextGenAnimationIdle = false,
                cellsColor = null,
                gridColor = null,
                backgroundColor = null,
                newGamesDefaultCellSize = 5,
                newGamesDefaultSpeed = 5
            )
        )

        uut.firstGameState().let {
            assertEquals("#1", it.gameName)
            assertTrue(it.modifiedAt == 42L)
            assertEquals(ScreenOrientation.LANDSCAPE, it.screenOrientation)
            assertEquals(2, it.speed)
            assertEquals(0.7f, it.previewAspectRatio)
            assertEquals(cells, it.currentGenCells)
            assertTrue(it.cellType is HexagonalCellType)
            assertEquals(4, it.cellSize)
            assertEquals(23L, it.createdAt)
            assertEquals(30L, it.gameId)
            assertEquals(GameRules.B45S2345, it.rules)
        }
    }

}


private fun GameState.resetTimestamps(): GameState = copy(modifiedAt = 0, createdAt = 0)

fun GameEngine.runFor(n: Int, start: Boolean = true, pause: Boolean = true) = runBlocking {

    val initialState = gameState.first()!!

    if (start) {
        require(!initialState.controls.isPlaying)
        runBlocking {
            play()
        }
    } else {
        runBlocking {
            onAutomatonRenderCompleted()
        }
    }

    assertTrue(gameState.first()!!.controls.isPlaying)
    val timeMillis = GameEngine.DELAYS[initialState.speed]


    repeat(n - 1) {
        delay(2 * timeMillis)
        assertEquals(initialState.generation + it + 1, gameState.first()!!.generation)
        onAutomatonRenderCompleted()
    }
    delay(2 * timeMillis)

    if (pause) {
        runBlocking { pause() }
    }
}

suspend fun GameEngine.firstGameState() = this.gameState.first()!!