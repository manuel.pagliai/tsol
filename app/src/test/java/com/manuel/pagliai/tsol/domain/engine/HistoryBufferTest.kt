package com.manuel.pagliai.tsol.domain.engine


import org.junit.Assert.*
import org.junit.Before
import org.junit.Test


class HistoryBufferTest {

    private lateinit var intUUT : HistoryBuffer<Int>

    @Before
    fun before() {
        intUUT = HistoryBuffer(100)
    }

    @Test
    fun canGoForward_backThenAdd() {
        repeat(3) {
            intUUT.add(it)
        }

        intUUT.goBack()
        intUUT.goBack()
        intUUT.add(5)

        assertFalse(intUUT.canGoForward())
    }

    @Test
    fun canGoForward() {
        assertFalse(intUUT.canGoForward())
        intUUT.add(-1)
        assertFalse(intUUT.canGoForward())
        intUUT.goBack()
        assertTrue(intUUT.canGoForward())
        intUUT.goForward()
        assertFalse(intUUT.canGoForward())

        repeat(103) {
            intUUT.add(it)
        }

        assertFalse(intUUT.canGoForward())

        // go back as much as possible
        while(intUUT.canGoBack())
            intUUT.goBack()

        // count the number of times we can go forward
        var count = 0
        while(intUUT.canGoForward()) {
            count++
            intUUT.goForward()
        }

        assertEquals(100, count) // not 103
    }

    @Test
    fun canGoBack() {
        assertFalse(intUUT.canGoBack())
        intUUT.add(5)
        assertTrue(intUUT.canGoBack())
        intUUT.goBack()
        assertFalse(intUUT.canGoBack())

        // add 103 elements
        repeat(103) {
            intUUT.add(it)
        }

        // go back as much as possible
        var count = 0
        while(intUUT.canGoBack()) {
            count++
            intUUT.goBack()
        }

        assertEquals(100, count) /* not 103 */
    }

    @Test
    fun goBack() {

        intUUT.add(5)
        assertEquals(5, intUUT.goBack())

        intUUT.add(6)
        intUUT.add(7)
        intUUT.add(8)

        assertEquals(8, intUUT.goBack())
        assertEquals(7, intUUT.goBack())
        assertEquals(6, intUUT.goBack())

        // change the history
        intUUT.add(9)
        intUUT.add(10)
        intUUT.add(11)

        assertEquals(11, intUUT.goBack())
        assertEquals(11, intUUT.goForward())
        assertEquals(11, intUUT.goBack())
        intUUT.add(12)
        assertEquals(12, intUUT.goBack())

    }

    @Test
    fun goForward() {

        intUUT.add(5)
        intUUT.goBack()

        intUUT.add(6)
        intUUT.add(7)
        intUUT.add(8)

        assertEquals(8, intUUT.goBack())
        assertEquals(7, intUUT.goBack())
        assertEquals(6, intUUT.goBack())

        assertEquals(6, intUUT.goForward())
        assertEquals(7, intUUT.goForward())
        assertEquals(8, intUUT.goForward())

        intUUT.reset()

        // change the history
        intUUT.add(8)
        intUUT.add(9)
        intUUT.add(10)
        intUUT.add(11)

        intUUT.goBack()
        intUUT.goBack()

        // 10 and 11 are the next elements in the stack: add 12 will clear those
        intUUT.add(12)

        assertEquals(12, intUUT.goBack())
        assertEquals(9, intUUT.goBack())

        assertEquals(9, intUUT.goForward())
        assertEquals(12, intUUT.goForward())

    }

    @Test
    fun reset() {
        intUUT.add(6)
        intUUT.add(7)
        intUUT.add(8)

        intUUT.goBack()
        assertTrue(intUUT.canGoForward())
        assertTrue(intUUT.canGoBack())

        intUUT.reset()

        assertFalse(intUUT.canGoForward())
        assertFalse(intUUT.canGoBack())
    }

    @Test
    fun testErr() {
        intUUT.add(5)
        assertFalse(intUUT.canGoForward())
        try {
            intUUT.goForward()
            fail()
        } catch (e : IllegalStateException) {
            // ok
        }

        intUUT.reset()

        intUUT.add(7)
        intUUT.goBack()

        try {
            intUUT.goBack()
            fail()
        } catch (e : IllegalStateException) {
            // ok
        }
    }

    @Test
    fun peek() {
        assertNull(intUUT.peek())

        intUUT.add(0)
        assertEquals(0, intUUT.peek())

        intUUT.add(1)
        assertEquals(1, intUUT.peek())

        intUUT.add(2)
        assertEquals(2, intUUT.peek())

        intUUT.goBack()
        assertEquals(1, intUUT.peek())
        assertEquals(2, intUUT.peekNext())

        intUUT.goBack()
        assertEquals(0, intUUT.peek())
        assertEquals(1, intUUT.peekNext())

        intUUT.goForward()
        assertEquals(1, intUUT.peek())
        assertEquals(2, intUUT.peekNext())

        intUUT.goForward()
        assertEquals(2, intUUT.peek())

        intUUT.add(3)
        assertEquals(3, intUUT.peek())

        intUUT.goBack()
        assertEquals(2, intUUT.peek())
        assertEquals(3, intUUT.peekNext())

    }

    @Test
    fun peekNext() {
        assertNull(intUUT.peekNext())
        intUUT.apply {
            add(1)
            add(2)
            add(3)
        }

        assertNull(intUUT.peekNext())

        intUUT.goBack()
        assertEquals(3, intUUT.peekNext())

        intUUT.goBack()
        assertEquals(2, intUUT.peekNext())

        intUUT.goBack()
        assertEquals(1, intUUT.peekNext())
        assertNull(intUUT.peek())

        assertFalse(intUUT.canGoBack())

        intUUT.goForward()
        assertEquals(2, intUUT.peekNext())

        intUUT.goForward()
        assertEquals(3, intUUT.peekNext())

        intUUT.goForward()
        assertNull(intUUT.peekNext())
    }
}