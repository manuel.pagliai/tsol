package com.manuel.pagliai.tsol.domain.engine

import com.manuel.pagliai.tsol.domain.Cells
import com.manuel.pagliai.tsol.domain.Grid
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.util.*


class CellsEditTest {

    private lateinit var sampleCells: Cells

    @Before
    fun setup() {
        sampleCells = Cells(
            grid = Grid(rows = 30, columns = 20)
        )
    }

    @Test
    fun singleCellEdit() {
        testEdits(sampleCells,
            SingleCellEdit(0, true),
            SingleCellEdit(1, true),
            SingleCellEdit(0, false),
            SingleCellEdit(3, true)
        )

        Assert.assertFalse(sampleCells[1])
        Assert.assertTrue(SingleCellEdit(1, true).applyEdit(sampleCells)[1])
    }

    @Test
    fun cellsReplacement() {
        val cells0 = sampleCells
        val cells1 = mockk<Cells>()
        val cells2 = mockk<Cells>()
        val cells3 = mockk<Cells>()

        every { cells1.size } returns cells0.size
        every { cells2.size } returns cells0.size
        every { cells3.size } returns cells0.size

        testEdits(sampleCells,
            CellsReplacement(cells0, cells1),
            CellsReplacement(cells1, cells2),
            CellsReplacement(cells2, cells1),
            CellsReplacement(cells1, cells3)
        )

        Assert.assertNotEquals(cells1, sampleCells)
        assertEquals(cells1, CellsReplacement(cells0, cells1).applyEdit(sampleCells))
    }

    private fun testEdits(cells: Cells, vararg edits : CellsEdit) {
        val stack = Stack<Cells>()
        var a = cells
        for (i in edits.indices) {
            Assert.assertTrue(stack.isEmpty())
            assertEquals(a, cells)
            stack.add(a)
            // apply edits from 0 to i and save each result
            for (j in 0 until i) {
                a = edits[j].applyEdit(a)
                stack.add(a)
            }
            stack.pop()
            for (j in i - 1 downTo 0) {
                a = edits[j].createUndo().applyEdit(a)
                assertEquals(stack.pop(), a)
            }
        }
    }

}