package com.manuel.pagliai.tsol.domain.tiling

import org.junit.Assert.*
import org.junit.Test

class VertexTest {

    @Test
    fun operations() {
        val v1 = Vertex(x = 1f, y = 2f)

        assertEquals(v1,v1)
        assertEquals(v1+ Vertex(0f,0f), v1)
        assertNotEquals(v1, Vertex(x = 5f, y = 7f))

        assertEquals(Vertex(12f, 15f), v1.add(11f, 13f))

        assertEquals(Vertex(6f, 9f), v1 + Vertex(x = 5f, y = 7f))
        assertEquals(Vertex(-4f, -5f), v1 - Vertex(x = 5f, y = 7f))
        assertEquals(Vertex(3f, 6f), v1 * 3f)
        assertEquals(Vertex(0.5f, 1f), v1/ 2f)

        assertEquals(Vertex(5f, 2f), v1.xShift(4f))
        assertEquals(Vertex(1f, 7f), v1.yShift(5f))

        assertEquals(Vertex(2f, 1f), v1.swapXY())
        assertEquals(v1, v1.swapXY().swapXY())

        assertTrue(v1.outsideXRange(0f, 0.1f))
        assertTrue(v1.outsideXRange(3f, 10f))
        assertFalse(v1.outsideXRange(0f, 10f))
        assertFalse(v1.outsideXRange(1f, 2f))

        assertTrue(v1.outsideYRange(0f, 0.1f))
        assertTrue(v1.outsideYRange(3f, 10f))
        assertFalse(v1.outsideYRange(0f, 10f))
        assertFalse(v1.outsideYRange(1f, 2f))
    }
}