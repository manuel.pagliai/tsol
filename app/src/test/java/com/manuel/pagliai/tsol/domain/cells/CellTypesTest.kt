package com.manuel.pagliai.tsol.domain.cells

import com.manuel.pagliai.tsol.di.DaggerTestComponent
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import javax.inject.Inject

class CellTypesTest {

    @Inject
    lateinit var uut : CellTypes

    @Before
    fun setup() {
        DaggerTestComponent.create().inject(this)
    }

    @Test
    fun idsAreUnique() {
        assertEquals(
            uut.list.size,
            uut.list.map { it.id }.toSet().size
        )
    }

    @Test
    fun namesAreUnique() {
        assertEquals(
            uut.list.size,
            uut.list.map { it.name }.toSet().size
        )
    }

    @Test
    fun getById() {
        for (cellType in uut.list) {
            assertEquals(cellType, uut.getById(cellType.id))
        }
    }

    @Test
    fun list() {
        assertTrue(uut.list.isNotEmpty())
        assertTrue(uut.list.contains(uut.defaultCellType))
    }

    @Test(expected = NoSuchElementException::class)
    fun getByIdMissingId() {
        uut.getById("idontexist")
    }

}