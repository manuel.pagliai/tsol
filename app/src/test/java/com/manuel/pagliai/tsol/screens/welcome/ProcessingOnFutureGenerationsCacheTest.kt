package com.manuel.pagliai.tsol.screens.welcome

import com.manuel.pagliai.tsol.domain.*
import com.manuel.pagliai.tsol.domain.cells.SquareCellType
import com.manuel.pagliai.tsol.domain.engine.AutomatonNextGenCalculator
import com.manuel.pagliai.tsol.domain.engine.ScreenOrientation
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import kotlin.random.Random

data class SampleKeyProcessingOn(
    override val gameId: Long,
    val name : String
) : ProcessingOnFutureGenerationsCacheKey

class ProcessingOnFutureGenerationsCacheTest {

    private lateinit var uut: ProcessingOnFutureGenerationsCache<SampleKeyProcessingOn, Any>
    private lateinit var nextGenCalculator: AutomatonNextGenCalculator

    // sample values
    private val v1 = "v1"
    private val v2 = "v2"
    private val v3 = "v3"
    private val key1 = SampleKeyProcessingOn(1, "k1")


    @Before
    fun setup() {
        nextGenCalculator = AutomatonNextGenCalculator(Random(0))
        uut = ProcessingOnFutureGenerationsCache(10, 10, nextGenCalculator = nextGenCalculator)
    }

    private fun savedGame(
        gameId: Long,
        cells: Cells = Cells(grid = Grid(10, 10)),
        cellType: CellType = SquareCellType(),
        rules: GameRules = GameRules.B3S23
    ) = SavedGame(
        cellType = cellType,
        name = "#$gameId",
        cellSize = 5,
        speed = 5,
        orientation = ScreenOrientation.LANDSCAPE,
        id = gameId,
        createdAt = 1,
        modifiedAt = 1,
        previewAspectRatio = 1f,
        rules = rules,
        cells = cells
    )

    @Test
    fun testEntriesAreRemovedWhenNotMatching() = runBlocking {

        val g1 = savedGame(1)

        val k2 = SampleKeyProcessingOn(1, "k2")

        assertEquals(v1, uut.getCachedOrEval(key1, g1) { v1 })
        assertEquals(v1, uut.getCachedOrEval(key1, g1) { fail() })
        assertEquals(v2, uut.getCachedOrEval(k2, g1) { v2 })
        assertEquals(v2, uut.getCachedOrEval(k2, g1) { fail() })
        assertEquals(v3, uut.getCachedOrEval(key1, g1) { v3 })
    }

    @Test
    fun testEntriesAreRetained() = runBlocking {
        val g = savedGame(1)

        assertEquals(v1, uut.getCachedOrEval(key1, g) { v1 })
        assertEquals(v1, uut.getCachedOrEval(key1, g) { fail() })
    }

    @Test
    fun testEntriesCanBeRemoved() = runBlocking {
        val g = savedGame(1)

        assertEquals(v1, uut.getCachedOrEval(key1, g) { v1 })
        assertEquals(v1, uut.getCachedOrEval(key1, g) { fail() })

        uut.remove(g)
        assertEquals(v2, uut.getCachedOrEval(key1, g) { v2 })
    }

    @Test
    fun testEntriesWillBeEventuallyRemoved() = runBlocking {

        val games: MutableList<SavedGame> = mutableListOf()
        val keys: MutableList<SampleKeyProcessingOn> = mutableListOf()

        repeat(11) { id ->
            val g = savedGame(id.toLong())
            val k = SampleKeyProcessingOn(id.toLong(), "$id")
            games.add(g)
            keys.add(k)
            uut.getCachedOrEval(k, g) { "#$id" }
        }

        // first entry has been removed
        assertEquals(
            "newValueForOldGame",
            uut.getCachedOrEval(keys[0], games[0]) { "newValueForOldGame" }
        )

        // newer entries are still cached
        assertEquals(
            "#8",
            uut.getCachedOrEval(keys[8], games[8]) { fail() }
        )
    }

    @Test
    fun nextGenerationsAreEvaluated() = runBlocking {
        val gliderCells = Cells(Grid(rows = 20, columns = 10)).setCell(x = 0, y = 1, true)
            .setCell(x = 1, y = 2, true)
            .setCell(x = 2, y = 0, true)
            .setCell(x = 2, y = 1, true)
            .setCell(x = 2, y = 2, true)
        val gliderGame = savedGame(1, cells = gliderCells)
        assertEquals(v1, uut.getCachedOrEval(key1, gliderGame) { gens ->
            assertEquals(10, gens.size)
            assertEquals(gliderCells, gens[0])
            v1
        })
    }

    @Test
    fun nextGenerationEvaluationIsHaltedIfStillLIfeIsReached() = runBlocking {
        val oneCellSet = Cells(Grid(rows = 20, columns = 10))
            .setCell(x = 0, y = 1, true)

        val oneCellSetGame = savedGame(1, cells = oneCellSet)
        assertEquals(v1, uut.getCachedOrEval(key1, oneCellSetGame) { gens ->
            assertEquals(2, gens.size)
            assertEquals(oneCellSet, gens[0])
            assertEquals(Cells(Grid(rows = 20, columns = 10)), gens[1])
            v1
        })
    }

    @Test
    fun stillLifeIsNotReachedWithNonDeterministicRules() = runBlocking {
        val nonDeterministicGame = savedGame(1, rules = GameRules.B3S23.copy(pRandomDeath = 0.1f))
        assertFalse(nonDeterministicGame.rules.isDeterministicGame)
        assertEquals(v1, uut.getCachedOrEval(key1, nonDeterministicGame) { gens ->
            assertEquals(10, gens.size)
            v1
        })
    }

}