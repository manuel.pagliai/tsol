package com.manuel.pagliai.tsol.domain

import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import java.util.*
import kotlin.random.Random


class CellsTest {

    private lateinit var uut20x10: Cells
    private lateinit var random: Random

    @Before
    fun setup() {
        uut20x10 = Cells(grid = Grid(rows = 20, columns = 10))
        random = Random(0)
    }

    @Test
    fun encoding() {
        fun testEncodeDecode(cells: Cells) {
            assertEquals(
                cells,
                Cells.Encoding.decode(Cells.Encoding.encode(cells))
            )
        }


        testEncodeDecode(Cells(grid = Grid(10, 10)) { false })
        testEncodeDecode(Cells(grid = Grid(10, 10)) { true })
        testEncodeDecode(Cells(grid = Grid(10, 10)) { it % 2 == 1 })
        testEncodeDecode(Cells(grid = Grid(10, 10)) { it % 2 == 0 })

        repeat(100) {
            val rows = random.nextInt(1, 100)
            val cols = random.nextInt(1, 100)
            testEncodeDecode(
                Cells(grid = Grid(rows, cols)) { random.nextBoolean() }
            )

            // one hot
            val zero = Cells(grid = Grid(rows, cols))
            testEncodeDecode(zero)
            for (c in 0 until zero.size) {
                val oneHot = zero.setCell(c, true)
                testEncodeDecode(oneHot)
            }
        }
    }

    @Test
    fun setCell() {
        assertEquals(uut20x10, uut20x10.setCell(1, 1, false))
        var newCells = uut20x10.setCell(1, 1, true)
        assertTrue(newCells[1, 1])
        assertFalse(uut20x10[1, 1])

        assertFalse(uut20x10[3])
        newCells = uut20x10.setCell(3, true)
        assertTrue(newCells[3])
        assertFalse(uut20x10[3])

        repeat(uut20x10.grid.rows) { r ->
            repeat(uut20x10.grid.columns) { c ->
                val l = uut20x10.grid.toLinear(x = c, y = r)

                assertFalse(uut20x10[l])
                assertFalse(uut20x10[c, r])
                newCells = uut20x10.setCell(l, true)
                assertTrue(newCells[l])
                assertTrue(newCells[c, r])

                newCells = newCells.setCell(c, r, false)
                assertFalse(newCells[l])
                assertFalse(newCells[c, r])
                assertEquals(newCells, uut20x10)

                newCells = uut20x10.setCell(c, r, true)
                assertTrue(newCells[l])
                assertTrue(newCells[c, r])

                newCells = newCells.setCell(l, false)
                assertFalse(newCells[l])
                assertFalse(newCells[c, r])
                assertEquals(newCells, uut20x10)
            }
        }
    }


    @Test
    fun testEquals() {
        assertEquals(uut20x10, uut20x10)
        assertEquals(uut20x10, Cells(grid = uut20x10.grid))
        assertNotEquals(uut20x10, uut20x10.setCell(1, 1, true))
        assertEquals(uut20x10.setCell(1, 1, true), uut20x10.setCell(1, 1, true))
        assertNotEquals(uut20x10, Cells(BitSet(300), Grid(rows = 30, columns = 10)))
        assertNotEquals(uut20x10, Cells(BitSet(200), Grid(rows = 10, columns = 20)))
    }

    @Test
    fun testHashCode() {
        assertEquals(uut20x10.hashCode(), uut20x10.hashCode())
        assertEquals(
            uut20x10.hashCode(),
            Cells(BitSet(uut20x10.size), uut20x10.grid).hashCode()
        )
        assertNotEquals(uut20x10.hashCode(), uut20x10.setCell(1, 1, true).hashCode())
        assertEquals(
            uut20x10.setCell(1, 1, true).hashCode(),
            uut20x10.setCell(1, 1, true).hashCode()
        )
        assertNotEquals(
            uut20x10.hashCode(),
            Cells(BitSet(300), Grid(rows = 30, columns = 10)).hashCode()
        )
        assertNotEquals(
            uut20x10.hashCode(),
            Cells(BitSet(200), Grid(rows = 10, columns = 20)).hashCode()
        )
    }

}