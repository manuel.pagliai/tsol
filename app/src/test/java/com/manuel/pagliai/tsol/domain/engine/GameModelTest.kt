package com.manuel.pagliai.tsol.domain.engine

import com.manuel.pagliai.tsol.di.DaggerTestComponent
import com.manuel.pagliai.tsol.domain.Cells

import com.manuel.pagliai.tsol.domain.Grid
import com.manuel.pagliai.tsol.domain.cells.SquareCellType
import com.manuel.pagliai.tsol.domain.GameSettings
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import javax.inject.Inject
import kotlin.random.Random


class GameModelTest {

    lateinit var uut: GameModel

    @Inject
    lateinit var squareCellType: SquareCellType

    @Before
    fun setup() {
        uut = GameModel(
            executionHistory = HistoryBuffer(100),
            editHistory = HistoryBuffer(100),
            loopDetector = ProbabilisticLoopDetector(1024),
            clock = Clock(),
            automatonNextGenCalculator = AutomatonNextGenCalculator(Random(0))
        )

        DaggerTestComponent.create().inject(this)
    }

    private fun selectSomeCells(uut: GameModel, some: Int = 5) {
        for (i in 0 until some)
            uut.cellSelected(i)
    }

    @Test
    fun cannotUndoAfterNextGen() {
        uut.initForTest().also {
            selectSomeCells(it)
        }

        assertTrue(uut.gameState!!.controls.canUndo)

        // undo (once) so we can undo and redo
        uut.undo()
        assertTrue(uut.gameState!!.controls.canUndo)
        assertTrue(uut.gameState!!.controls.canRedo)

        // next gen must invalidate edit history
        uut.nextGen()
        assertFalse(uut.gameState!!.controls.canRedo)
        assertFalse(uut.gameState!!.controls.canUndo)

        // history is re-enabled in previous gen
        assertTrue(uut.gameState!!.controls.canPrevGen)
        uut.prevGen()
        assertTrue(uut.gameState!!.controls.canRedo)
        assertTrue(uut.gameState!!.controls.canUndo)
    }

    @Test
    fun cannotPrevGenAfterEdit() {
        uut.initForTest().also {
            selectSomeCells(it)
        }

        uut.nextGen()

        assertTrue(uut.gameState!!.controls.canPrevGen)

        uut.cellSelected(42)

        assertFalse(uut.gameState!!.controls.canPrevGen)
    }

    @Test
    fun undoAfterPrevGen() {
        uut.initForTest()

        uut.cellSelected(42)
        assertTrue(uut.gameState!!.currentGenCells[42])

        uut.nextGen()
        assertFalse(uut.gameState!!.currentGenCells[42])
        assertTrue(uut.gameState!!.controls.canPrevGen)

        uut.prevGen()
        assertTrue(uut.gameState!!.currentGenCells[42])
        uut.undo()
        assertFalse(uut.gameState!!.currentGenCells[42])
        uut.redo()
        assertTrue(uut.gameState!!.currentGenCells[42])
    }

    @Test
    fun editHistoryMatchesGen() {
        uut.initForTest()

        uut.cellSelected(42)
        uut.cellSelected(43)
        uut.cellSelected(44)

        assertTrue(uut.gameState!!.currentGenCells[44])

        uut.undo()

        assertFalse(uut.gameState!!.currentGenCells[44])

        // assert editHistoryMatchesGen and controls
        assertTrue(uut.editHistoryMatchesGen)
        assertTrue(uut.gameState!!.controls.canUndo)
        assertTrue(uut.gameState!!.controls.canRedo)

        // next gen -> edit history no longer applies
        uut.nextGen()

        assertFalse(uut.editHistoryMatchesGen)
        assertFalse(uut.gameState!!.controls.canUndo)
        assertFalse(uut.gameState!!.controls.canRedo)

        // prev gen, the edit history applies again
        assertTrue(uut.gameState!!.controls.canPrevGen)
        uut.prevGen()

        assertTrue(uut.editHistoryMatchesGen)
        assertTrue(uut.gameState!!.controls.canUndo)
        assertTrue(uut.gameState!!.controls.canRedo)
    }

    @Test
    fun executionStarted_multipleInvocations() {
        uut.initForTest().also {
            it.addGlider(5, 5)
        }

        // get a snapshot of the glider
        val gliderSnapshot = uut.gameState!!.currentGenCells

        // start and execute for some time
        uut.isPlaying = true
        repeat(3) {
            uut.nextGen()
        }

        // the automaton has changed
        assertNotEquals(gliderSnapshot, uut.gameState!!.currentGenCells)


        // restore cells and assert the original glider is restored
        uut.restoreLastEdited()
        assertEquals(gliderSnapshot, uut.gameState!!.currentGenCells)
    }

    @Test
    fun clearOrRestore() {
        uut.initForTest().addGlider(5, 5)

        assertTrue(uut.gameState!!.controls.canClear)

        val started = uut.gameState!!.currentGenCells

        uut.isPlaying = true // mark the thing as started

        repeat(3) {
            uut.nextGen()
        }

        assertFalse(uut.gameState!!.controls.canClear)

        uut.restoreLastEdited()

        assertEquals(started, uut.gameState!!.currentGenCells)
        assertTrue(uut.gameState!!.currentGenCells.alive)

        repeat(4) {
            uut.nextGen()
        }

        uut.isPlaying = false

        assertFalse(uut.gameState!!.controls.canClear)

        uut.restoreLastEdited()

        assertEquals(started, uut.gameState!!.currentGenCells)
        assertTrue(uut.gameState!!.controls.canClear)

        uut.clearCells() // clear

        assertFalse(uut.gameState!!.currentGenCells.alive)
        assertFalse(uut.gameState!!.controls.canClear)
    }

    @Test
    fun cannotRestoreAfterEdit() {
        uut.initForTest().addGlider(5, 5)
        uut.isPlaying = true // mark the thing as started
        repeat(3) {
            uut.nextGen()
        }
        uut.isPlaying = false

        assertFalse(uut.gameState!!.currentGenCells[42])
        uut.cellSelected(42)

        assertTrue(uut.gameState!!.controls.canClear)
    }

    @Test
    fun restoreWhileRunning() {
        uut.initForTest().addGlider(5, 5)
        uut.isPlaying = true // mark the thing as started

        uut.nextGen()

        val startedCells = uut.gameState!!.currentGenCells

        repeat(3) {
            uut.nextGen()
        }

        assertFalse(uut.gameState!!.controls.canClear)
        uut.restoreLastEdited()

        uut.nextGen()

        assertNotSame(startedCells, uut.gameState!!.currentGenCells)
        assertEquals(startedCells, uut.gameState!!.currentGenCells)
    }

    @Test
    fun editsWhileRunningAlterLastStartedAutomaton() {
        uut.initForTest().addGlider(5, 5)
        uut.isPlaying = true // mark the thing as started
        repeat(3) {
            uut.nextGen()
        }
        assertFalse(uut.gameState!!.currentGenCells[42])
        uut.cellSelected(42)
        assertTrue(uut.gameState!!.currentGenCells[42])
        val lastStarted = uut.gameState!!.currentGenCells


        repeat(3) {
            uut.nextGen()
        }

        assertNotEquals(lastStarted, uut.gameState!!.currentGenCells)

        assertFalse(uut.gameState!!.controls.canClear)

        uut.restoreLastEdited()
        assertEquals(lastStarted, uut.gameState!!.currentGenCells)

        assertFalse(uut.gameState!!.controls.canClear)
        uut.restoreLastEdited() // restore 2 (still running)
        assertEquals(lastStarted, uut.gameState!!.currentGenCells)

        uut.isPlaying = false

        assertTrue(uut.gameState!!.controls.canClear)
        uut.clearCells() // clear

        assertFalse(uut.gameState!!.currentGenCells.alive)
    }

    @Test
    fun genNavigation() {

        uut.initForTest().addGlider(5, 5)

        val gen0Cells = uut.gameState!!.currentGenCells
        assertEquals(0, uut.gameState!!.generation)

        uut.nextGen()
        assertEquals(1, uut.gameState!!.generation)
        val gen1Cells = uut.gameState!!.currentGenCells

        uut.nextGen()
        assertTrue(uut.gameState!!.controls.canPrevGen)
        assertEquals(2, uut.gameState!!.generation)
        val gen2Cells = uut.gameState!!.currentGenCells

        assertTrue(uut.gameState!!.controls.canPrevGen)
        assertTrue(uut.gameState!!.controls.canNextGen)

        uut.prevGen()

        assertTrue(uut.gameState!!.controls.canNextGen)
        assertTrue(uut.gameState!!.controls.canPrevGen)
        assertEquals(1, uut.gameState!!.generation)
        assertSame(gen1Cells, uut.gameState!!.currentGenCells)

        uut.nextGen()
        assertEquals(2, uut.gameState!!.generation)
        assertSame(gen2Cells, uut.gameState!!.currentGenCells)

        uut.prevGen()

        assertTrue(uut.gameState!!.controls.canNextGen)
        assertTrue(uut.gameState!!.controls.canPrevGen)
        assertEquals(1, uut.gameState!!.generation)
        assertSame(gen1Cells, uut.gameState!!.currentGenCells)

        uut.prevGen()

        assertTrue(uut.gameState!!.controls.canNextGen)
        assertEquals(0, uut.gameState!!.generation)
        assertSame(gen0Cells, uut.gameState!!.currentGenCells)

        uut.nextGen()

        assertTrue(uut.gameState!!.controls.canNextGen)
        assertTrue(uut.gameState!!.controls.canPrevGen)
        assertEquals(1, uut.gameState!!.generation)
        assertSame(gen1Cells, uut.gameState!!.currentGenCells)

        // an edit nukes execution history
        assertFalse(uut.gameState!!.currentGenCells[42])
        uut.cellSelected(42)

        assertTrue(uut.gameState!!.currentGenCells[42])
        assertFalse(uut.gameState!!.controls.canPrevGen)
        assertNotSame(gen2Cells, uut.gameState!!.currentGenCells)

    }

    private fun GameModel.initForTest(): GameModel {
        initialiseNewGame(
            name = "test", // irrelevant
            cellSize = 5, // irrelevant
            cellType = squareCellType,
            cells = Cells(grid = Grid(rows = 30, columns = 20)),
            gameRules = squareCellType.defaultGameRules,
            orientation = ScreenOrientation.PORTRAIT, // irrelevant
            previewAspectRatio = 1.0f, // irrelevant
            speed = 5, // irrelevant,
            gameSettings = GameSettings(
                showNextGenAnimationIdle = false,
                randomiseNewGamesCells = false,
                backgroundColor = null,
                gridColor = null,
                cellsColor = null,
                newGamesDefaultSpeed = 4,
                newGamesDefaultCellSize = 3
            )
        )
        return this
    }

    private fun GameModel.addGlider(topX: Int, topY: Int) {
        val addCellsOnSelection = this.gameState!!.controls.addCellsOnSelection
        val grid = this.gameState!!.currentGenCells.grid
        cellSelected(grid.toLinear(topX, topY))
        cellSelected(grid.toLinear(topX + 1, topY + 1))
        cellSelected(grid.toLinear(topX - 1, topY + 2))
        cellSelected(grid.toLinear(topX, topY + 2))
        cellSelected(grid.toLinear(topX + 1, topY + 2))
        this.addCellsOnSelection = addCellsOnSelection
    }

}