package com.manuel.pagliai.tsol.domain.tiling

import com.manuel.pagliai.tsol.TestCanvasConfiguration
import com.manuel.pagliai.tsol.di.DaggerTestComponent
import com.manuel.pagliai.tsol.domain.cells.CellTypes

import junit.framework.TestCase
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import javax.inject.Inject
import kotlin.random.Random

@RunWith(RobolectricTestRunner::class)
class PeriodicShapeTilingTest {

    private lateinit var uut10x20squareTiling : PeriodicShapeTiling
    private lateinit var uut10x20squareTiles : List<PeriodicShape>

    @Inject
    lateinit var cellTypes : CellTypes

    @Before
    fun setup() {
        DaggerTestComponent.create().inject(this)
        val tiles = ArrayList<PeriodicShape>()
        uut10x20squareTiling = PeriodicShapeTiling.create(200) { index ->

            val row = index/10
            val col = index - row * 10

            val p0 = Vertex(col * 10f, row * 10f)

            val cellPolygon = convexPolygonPeriodicShape(
                p0,
                p0.xShift(10f),
                p0.xShift(10f).yShift(10f),
                p0.yShift(10f)
            )
            tiles.add(cellPolygon)
            cellPolygon
        }
        uut10x20squareTiles = tiles
    }

    @Test
    fun get() {
        assertEquals(
            uut10x20squareTiles.size,
            uut10x20squareTiling.size
        )

        for (i in 0 until uut10x20squareTiling.size) {
            TestCase.assertSame(
                uut10x20squareTiles[i],
                uut10x20squareTiling[i]
            )
        }
    }

    @Test
    fun positionOf() {
        assertEquals(0, uut10x20squareTiling.positionOf(0f, 0f))
        assertEquals(0, uut10x20squareTiling.positionOf(1f, 1f))
        assertEquals(0, uut10x20squareTiling.positionOf(9f, 9f))
        assertEquals(1, uut10x20squareTiling.positionOf(19f, 9f))
        assertEquals(11, uut10x20squareTiling.positionOf(11f, 11f))
        assertEquals(190, uut10x20squareTiling.positionOf(1f, 199f))
        assertEquals(191, uut10x20squareTiling.positionOf(11f, 199f))
    }

    @Test
    fun positionOf_random() {
        val random = Random(0)
        repeat(100_000) {
            val x = random.nextFloat() * 10 * 10
            val y = random.nextFloat() * 20 * 10

            val c = (x/10).toInt()
            val r = (y/10).toInt()

            val p = uut10x20squareTiling.positionOf(x,y)
            assertEquals(r * 10 + c, p)
        }
    }

    @Test
    fun index() {
        val random = Random(0)
        cellTypes.list.forEach { cellType ->
            TestCanvasConfiguration.all().forEach { canvasConfigurations ->
                for (cellSize in 0 until 10) {
                    val grid = cellType.gridFactory.createGrid(canvasConfigurations.canvasWidth, canvasConfigurations.canvasHeight, canvasConfigurations.displayDensity, cellSize)
                    val cellCanvasAdapter = cellType.tilingOnCanvasFactory.create(
                        grid = grid,
                        canvasWidth = canvasConfigurations.canvasWidth,
                        canvasHeight = canvasConfigurations.canvasHeight
                    )
                    if (cellCanvasAdapter is PeriodicShapesTilingOnCanvas) {
                        testIndex(
                            cellCanvasAdapter.tiling,
                            times = 1000,
                            random = random,
                            canvasWidth = canvasConfigurations.canvasWidth,
                            canvasHeight = canvasConfigurations.canvasHeight
                        )
                    }
                }
            }
        }
    }

    private fun testIndex(uut: PeriodicShapeTiling, times: Int, random: Random, canvasWidth: Float, canvasHeight: Float) {
        repeat(times) {
            val x = (random.nextFloat() * canvasWidth)
            val y = (random.nextFloat() * canvasHeight)
            var p = -1
            val ps = mutableSetOf<Int>()
            for (ii in uut.shapes.indices) {
                if (uut.shapes[ii].contains(x, y)) {
                    p = ii
                    ps.add(p)
                }
            }
            // This test is not perfect. I suspect it is because of numerical errors,
            // and the same point belonging to more than one shape.
            // in practice thought the index is good enough when using the app.
            if (p >= 0) {
                assertTrue(ps.contains(uut.positionOf(x, y)))
            } else {
                assertNull(uut.positionOf(x, y))
            }
        }
    }
}