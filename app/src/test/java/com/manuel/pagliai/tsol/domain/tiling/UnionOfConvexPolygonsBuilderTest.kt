package com.manuel.pagliai.tsol.domain.tiling


import org.junit.Assert.assertEquals
import org.junit.Test


class UnionOfConvexPolygonsBuilderTest {

    @Test
    fun unionOfConvexPolygons_1() {
        val uut = unionOfConvexPolygons {
            polygon(Vertex(0f, 0f)) {
                to(to = Vertex(1f, 1f), true)
                to(to = Vertex(2f, 0f), true)
                close(true)
            }
        } as MultiPartPeriodicShape

        assertEquals(1, uut.parts.size)
        assertEquals(3, uut.allSides.size)
        assertEquals(
            ConvexPolygon.fromSingleChain(
                listOf(
                    Vertex(0f, 0f),
                    Vertex(1f, 1f),
                    Vertex(2f, 0f)
                )
            ), uut.parts[0]
        )
    }

    @Test
    fun unionOfConvexPolygons_2() {
        val uut = unionOfConvexPolygons {
            polygon(Vertex(0f, 0f)) {
                to(to = Vertex(0f, 1f), false)
                to (to = Vertex(2f, 1f), true)
                to(to = Vertex(2f, 0f), true)
                close(false)
            }
        } as MultiPartPeriodicShape

        assertEquals(1, uut.parts.size)
        assertEquals(4, uut.allSides.size)
        val p = uut.parts[0]
        assertEquals(3, p.chains.size)

        assertEquals(PolygonalChain(
            listOf(
                Vertex(0f, 0f),
                Vertex(0f, 1f)
            ),
            false
        ), p.chains[0])

        assertEquals(PolygonalChain(
            listOf(
                Vertex(0f, 1f),
                Vertex(2f, 1f),
                Vertex(2f, 0f)
            ),
            true
        ), p.chains[1])

        assertEquals(PolygonalChain(
            listOf(
                Vertex(2f, 0f),
                Vertex(0f, 0f)
            ),
            false
        ), p.chains[2])
    }

    @Test
    fun unionOfConvexPolygons_3() {
        val uut = unionOfConvexPolygons {
            polygon(Vertex(0f, 0f)) {
                to(to = Vertex(1f, 1f), true)
                to(to = Vertex(2f, 0f), true)
                close(true)
            }

            polygon(Vertex(3f,2f)) {
                to (Vertex(3f, 6f), true)
                to (Vertex(4f, 6f), true)
                to (Vertex(4f, 2f), true)
                close(false)
            }
        } as MultiPartPeriodicShape

        assertEquals(2, uut.parts.size)

        assertEquals(
            ConvexPolygon.fromSingleChain(
                listOf(
                    Vertex(0f, 0f),
                    Vertex(1f, 1f),
                    Vertex(2f, 0f)
                )
            ), uut.parts[0]
        )

        val p2 = uut.parts[1]

        assertEquals(2, p2.chains.size)

        assertEquals(
            PolygonalChain(listOf(
                Vertex(3f,2f),
                Vertex(3f, 6f),
                Vertex(4f, 6f),
                Vertex(4f, 2f)
            ), true),
            p2.chains[0]
        )

        assertEquals(
            PolygonalChain(listOf(
                Vertex(4f, 2f),
                Vertex(3f, 2f)
            ), false),
            p2.chains[1]
        )

    }
}