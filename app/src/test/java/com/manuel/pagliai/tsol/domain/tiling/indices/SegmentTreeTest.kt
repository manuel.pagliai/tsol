package com.manuel.pagliai.tsol.domain.tiling.indices

import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Test
import java.util.*
import kotlin.collections.HashSet


// Useful extensions

fun <T> SegmentTreeNode<T>.dropPayload(): Segment<Unit> =
    Segment(this.inf, this.sup, this.infClosed, this.supClosed, Unit)


val <T> SegmentTreeNode<T>.isRoot: Boolean
    get() = this.payload.parent == null

val <T> SegmentTreeNode<T>.isLeaf: Boolean
    get() = this.payload.leftChild == null && this.payload.rightChild == null

val SegmentTree<out Any>.height: Int
    get() {
        // always go left as the right child might be incomplete
        var h = 0
        var n: Segment<out SegmentTreeNodePayload<out Any>>? = root
        do {
            h++
            n = n?.payload?.leftChild
        } while (n != null)
        return h
    }

inline fun <T> SegmentTree<T>.forEachNodeIntervalBreadthFirst(cons: (SegmentTreeNode<T>) -> Unit) {
    val q = LinkedList<SegmentTreeNode<T>>()
    q.add(root)
    do {
        val n = q.pop()
        cons(n)
        n.payload.leftChild?.let { q.add(it) }
        n.payload.rightChild?.let { q.add(it) }
    } while (q.isNotEmpty())
}

private fun <T> SegmentTree<T>.queryAndCollect(point: Float): Set<Segment<T>> {
    val result = hashSetOf<Segment<T>>()
    this.onQueryResult(point) {
        result.add(it)
    }
    return result
}


class SegmentTreeTest {

    @Test
    fun testSimpleQuery() {

        val s0 = closedSegmentOf(3f, 4f)
        val s1 = closedSegmentOf(0f, 1f)
        val s2 = closedSegmentOf(0.5f, 1.5f)
        val s3 = closedSegmentOf(1f, 3.5f)


        val tree = SegmentTree.create(listOf(s0, s1, s2, s3), levelsChecks = true)

        assertEquals(setOf(s1), tree.queryAndCollect(0f))
        assertEquals(setOf(s1), tree.queryAndCollect(0.1f))

        assertEquals(setOf(s0, s3), tree.queryAndCollect(3f))
        assertEquals(setOf(s0, s3), tree.queryAndCollect(3.1f))
        assertEquals(setOf(s0), tree.queryAndCollect(3.8f))

        assertEquals(setOf(s3), tree.queryAndCollect(2f))

        assertEquals(setOf(s1, s2, s3), tree.queryAndCollect(1f))

        assertEquals(setOf<Segment<Unit>>(), tree.queryAndCollect(8f))
    }

    @Test
    fun testCreate() {

        val tree = SegmentTree.create(
            listOf(
                closedSegmentOf(3f, 4f),
                closedSegmentOf(0f, 1f),
                closedSegmentOf(0.5f, 1.5f),
                closedSegmentOf(1f, 3.5f)
            )
        )

        assertEquals(5, tree.height)

        val visited = ArrayList<SegmentTreeNode<Unit>>()

        tree.forEachNodeIntervalBreadthFirst {
            visited.add(it)
        }

        (1 until visited.size).forEach {
            Assert.assertNotEquals(tree.root, visited[it])
        }

        Assert.assertTrue(visited[0] == tree.root)
        Assert.assertFalse(visited[0].isLeaf)
        Assert.assertTrue(tree.root.isRoot)

        visited.forEach {
            if (it != tree.root) {
                Assert.assertFalse(it.isRoot)
            }
        }

        assertEquals(25, visited.size)

        // l0
        assertEquals(
            Segment(
                0f,
                4f,
                infClosed = true,
                supClosed = true,
                Unit
            ), visited[0].dropPayload()
        )

        // l1
        assertEquals(
            Segment(
                0f,
                3f,
                infClosed = true,
                supClosed = false,
                Unit
            ), visited[1].dropPayload()
        )
        assertEquals(
            Segment(
                3f,
                4f,
                infClosed = true,
                supClosed = true,
                Unit
            ), visited[2].dropPayload()
        )


        // l2
        assertEquals(
            Segment(
                0f,
                1f,
                infClosed = true,
                supClosed = false,
                Unit
            ), visited[3].dropPayload()
        )
        assertEquals(
            Segment(
                1f,
                3f,
                infClosed = true,
                supClosed = false,
                Unit
            ), visited[4].dropPayload()
        )
        assertEquals(
            Segment(
                3f,
                4f,
                infClosed = true,
                supClosed = false,
                Unit
            ), visited[5].dropPayload()
        )
        assertEquals(
            Segment(
                4f,
                4f,
                infClosed = true,
                supClosed = true,
                Unit
            ), visited[6].dropPayload()
        )

        // l3
        assertEquals(
            Segment(
                0f,
                0.5f,
                infClosed = true,
                supClosed = false,
                Unit
            ), visited[7].dropPayload()
        )

        assertEquals(
            Segment(
                0.5f,
                1f,
                infClosed = true,
                supClosed = false,
                Unit
            ), visited[8].dropPayload()
        )

        assertEquals(
            Segment(
                1f,
                1.5f,
                infClosed = true,
                supClosed = false,
                Unit
            ), visited[9].dropPayload()
        )

        assertEquals(
            Segment(
                1.5f,
                3f,
                infClosed = true,
                supClosed = false,
                Unit
            ), visited[10].dropPayload()
        )

        assertEquals(
            Segment(
                3f,
                3.5f,
                infClosed = true,
                supClosed = false,
                Unit
            ), visited[11].dropPayload()
        )

        assertEquals(
            Segment(
                3.5f,
                4f,
                infClosed = true,
                supClosed = false,
                Unit
            ), visited[12].dropPayload()
        )


        // l4 (elementary intervals, except for [4,4])

        (13..24).forEach {
            Assert.assertTrue(visited[it].isLeaf)
        }

        assertEquals(
            Segment(
                0f,
                0f,
                infClosed = true,
                supClosed = true,
                Unit
            ), visited[13].dropPayload()
        )

        assertEquals(
            Segment(
                0f,
                0.5f,
                infClosed = false,
                supClosed = false,
                Unit
            ), visited[14].dropPayload()
        )

        assertEquals(
            Segment(
                0.5f,
                0.5f,
                infClosed = true,
                supClosed = true,
                Unit
            ), visited[15].dropPayload()
        )

        assertEquals(
            Segment(
                0.5f,
                1f,
                infClosed = false,
                supClosed = false,
                Unit
            ), visited[16].dropPayload()
        )

        assertEquals(
            Segment(
                1f,
                1f,
                infClosed = true,
                supClosed = true,
                Unit
            ), visited[17].dropPayload()
        )

        assertEquals(
            Segment(
                1f,
                1.5f,
                infClosed = false,
                supClosed = false,
                Unit
            ), visited[18].dropPayload()
        )

        assertEquals(
            Segment(
                1.5f,
                1.5f,
                infClosed = true,
                supClosed = true,
                Unit
            ), visited[19].dropPayload()
        )

        assertEquals(
            Segment(
                1.5f,
                3f,
                infClosed = false,
                supClosed = false,
                Unit
            ), visited[20].dropPayload()
        )

        assertEquals(
            Segment(
                3f,
                3f,
                infClosed = true,
                supClosed = true,
                Unit
            ), visited[21].dropPayload()
        )

        assertEquals(
            Segment(
                3f,
                3.5f,
                infClosed = false,
                supClosed = false,
                Unit
            ), visited[22].dropPayload()
        )

        assertEquals(
            Segment(
                3.5f,
                3.5f,
                infClosed = true,
                supClosed = true,
                Unit
            ), visited[23].dropPayload()
        )

        assertEquals(
            Segment(
                3.5f,
                4f,
                infClosed = false,
                supClosed = false,
                Unit
            ), visited[24].dropPayload()
        )
    }


    @Test
    fun testRandomQueriesKnownIntervals() {

        val segments = listOf(
            closedSegmentOf(3f, 4f),
            closedSegmentOf(0f, 1f),
            closedSegmentOf(0.5f, 1.5f),
            closedSegmentOf(1f, 3.5f),
            closedSegmentOf(6f, 6.5f)
        )

        val tree = SegmentTree.create(
            segments
        )

        val r = Random(0)
        repeat(5000000) {
            val p = r.nextFloat() * 4f
            assertEquals(
                segments.filter {  it.contains(p) }.toSet(),
                HashSet(tree.queryAndCollect(p))
            )
        }
    }

    @Test
    fun testRandomQueriesRandomIntervals() {

        val segments = arrayListOf<Segment<Unit>>()
        val r = Random(0)

        repeat(5) {
            repeat(3000) {
                val p0 = r.nextFloat() * 100
                val p1 = r.nextFloat() * 100

                segments.add(
                    if (p0 <= p1) {
                        Segment(p0, p1, infClosed = r.nextBoolean(), supClosed = r.nextBoolean(), payload = Unit)
                    } else {
                        Segment(p1, p0, infClosed = r.nextBoolean(), supClosed = r.nextBoolean(), payload = Unit)
                    }
                )
            }

            val tree = SegmentTree.create(segments)

            repeat(50000) {
                val p = r.nextFloat() * 100
                assertEquals(
                    segments.filter { it.contains(p) }.toSet(),
                    HashSet(tree.queryAndCollect(p))
                )
            }

            for (s in segments) {
                assertEquals(
                    segments.filter { it.contains(s.inf) }.toSet(),
                    HashSet(tree.queryAndCollect(s.inf))
                )
                assertEquals(
                    segments.filter { it.contains(s.sup) }.toSet(),
                    HashSet(tree.queryAndCollect(s.sup))
                )
            }
        }

    }

    // utils

    private fun closedSegmentOf(inf: Float, sup: Float) = Segment(
        inf,
        sup,
        infClosed = true,
        supClosed = true,
        Unit
    )
}