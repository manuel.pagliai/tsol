package com.manuel.pagliai.tsol.domain.tiling

import org.junit.Assert.*
import org.junit.Test


class MultiPartPeriodicShapeTest {

    private fun square_5_5_15_15() = convexPolygonPeriodicShape(
        Vertex(5f, 5f),
        Vertex(15f, 5f),
        Vertex(15f, 15f),
        Vertex(5f, 15f)
    ) as MultiPartPeriodicShape

    private fun rect_minus77_0_75_73() = convexPolygonPeriodicShape(
        Vertex(-77f, 0f),
        Vertex(75f, 0f),
        Vertex(75f, 73f),
        Vertex(-77f, 73f)
    ) as MultiPartPeriodicShape


    @Test
    fun square_contains() {
        val square = square_5_5_15_15()
        assertEquals(4, square.allSides.size)

        assertTrue(square.contains(7f, 7f)) // inside
        assertFalse(square.contains(2f, 7f)) // left
        assertFalse(square.contains(20f, 7f)) // right
        assertFalse(square.contains(7f, 2f)) // above
        assertFalse(square.contains(7f, 20f)) // below
        assertTrue(square.contains(15f, 15f)) // edge point
        assertFalse(square.contains(-17f, 5f)) // before on same line

        assertEquals(1, square.parts.size)
        assertEquals(1, square.parts.first().chains.size)

        val singleChain = square.parts.first().chains.first()
        assertEquals(
            PolygonalChain(
                listOf(
                    Vertex(5f, 5f),
                    Vertex(15f, 5f),
                    Vertex(15f, 15f),
                    Vertex(5f, 15f),
                    Vertex(5f, 5f)
                ),
                true
            ), singleChain
        )

    }

    @Test
    fun square_sides() {
        val sides = square_5_5_15_15().allSides

        assertEquals(4, sides.size)
        assertTrue(sides.contains(Side(Vertex(5f, 5f), Vertex(15f, 5f))))
        assertTrue(sides.contains(Side(Vertex(15f, 5f), Vertex(15f, 15f))))
        assertTrue(sides.contains(Side(Vertex(15f, 15f), Vertex(5f, 15f))))
        assertTrue(sides.contains(Side(Vertex(5f, 15f), Vertex(5f, 5f))))

    }

    @Test
    fun square_ineffectiveConstraints() {
        val square = square_5_5_15_15()
        assertEquals(1, square.parts.size)

        square.verticalConstraint(20f)
        square.horizontalConstraint(20f)

        assertEquals(1, square.parts.size)
    }

    @Test
    fun square_allWrapped() {
        val square = convexPolygonPeriodicShape(
            Vertex(12f, 12f),
            Vertex(15f, 12f),
            Vertex(15f, 15f),
            Vertex(12f, 15f)
        ) as MultiPartPeriodicShape

        square.verticalConstraint(10f)

        assertEquals(1, square.parts.size)
        val wrapped = square.parts.first()
        assertEquals(
            wrapped.sides,
            convexPolygonPeriodicShape(
                Vertex(2f, 12f),
                Vertex(5f, 12f),
                Vertex(5f, 15f),
                Vertex(2f, 15f)
            ).allSides
        )

    }

    @Test
    fun square_verticalConstraint() {

        // split vertically
        val splitVertically = square_5_5_15_15()
        splitVertically.verticalConstraint(13f)

        assertTrue(splitVertically.contains(1f, 6f))
        assertEquals(2, splitVertically.parts.size)

        val sides = splitVertically.allSides
        assertEquals(8, sides.size) // two rectangles

        assertTrue(
            sides.containsAll(
                convexPolygonPeriodicShape(
                    Vertex(0f, 5f),
                    Vertex(2f, 5f),
                    Vertex(2f, 15f),
                    Vertex(0f, 15f)
                ).allSides
            )
        )

        assertTrue(
            sides.containsAll(
                convexPolygonPeriodicShape(
                    Vertex(5f, 5f),
                    Vertex(13f, 5f),
                    Vertex(13f, 15f),
                    Vertex(5f, 15f)
                ).allSides
            )
        )

        val wrapped = splitVertically.parts.first { it.contains(1f, 6f) }
        val cut = splitVertically.parts.first { it != wrapped }

        assertEquals(3, cut.chains.size)

        assertEquals(
            PolygonalChain(
                listOf(
                    Vertex(5f, 5f),
                    Vertex(13f, 5f)
                ),
                true
            ), cut.chains[0]
        )

        assertEquals(
            PolygonalChain(
                listOf(
                    Vertex(13f, 5f),
                    Vertex(13f, 15f)
                ),
                false
            ), cut.chains[1]
        )

        assertEquals(
            PolygonalChain(
                listOf(
                    Vertex(13f, 15f),
                    Vertex(5f, 15f),
                    Vertex(5f, 5f)
                ),
                true
            ), cut.chains[2]
        )


        assertEquals(2, wrapped.chains.size)

        assertEquals(
            PolygonalChain(
                listOf(
                    Vertex(0f, 5f),
                    Vertex(2f, 5f),
                    Vertex(2f, 15f),
                    Vertex(0f, 15f)
                ),
                true
            ), wrapped.chains[0]
        )

        assertEquals(
            PolygonalChain(
                listOf(
                    Vertex(0f, 15f),
                    Vertex(0f, 5f)
                ),
                false
            ), wrapped.chains[1]
        )

    }

    @Test
    fun square_horizontalConstraint() {
        val splitHorizontally = square_5_5_15_15()
        splitHorizontally.horizontalConstraint(13f)

        assertTrue(splitHorizontally.contains(6f, 1f))
        assertTrue(splitHorizontally.contains(6f, 12f))

        assertEquals(2, splitHorizontally.parts.size)

        val sides = splitHorizontally.allSides
        assertEquals(8, sides.size) // two rectangles

        assertTrue(
            sides.containsAll(
                convexPolygonPeriodicShape(
                    Vertex(5f, 5f),
                    Vertex(15f, 5f),
                    Vertex(15f, 13f),
                    Vertex(5f, 13f)
                ).allSides
            )
        )

        assertTrue(
            sides.containsAll(
                convexPolygonPeriodicShape(
                    Vertex(5f, 0f),
                    Vertex(15f, 0f),
                    Vertex(15f, 2f),
                    Vertex(5f, 2f)
                ).allSides
            )
        )

        val wrapped = splitHorizontally.parts.first { it.contains(6f, 2f) }
        val cut = splitHorizontally.parts.first { it != wrapped }

        assertEquals(3, cut.chains.size)

        assertEquals(
            PolygonalChain(
                listOf(
                    Vertex(5f, 5f),
                    Vertex(15f, 5f),
                    Vertex(15f, 13f)
                ),
                true
            ), cut.chains[0]
        )

        assertEquals(
            PolygonalChain(
                listOf(
                    Vertex(15f, 13f),
                    Vertex(5f, 13f)
                ),
                false
            ), cut.chains[1]
        )

        assertEquals(
            PolygonalChain(
                listOf(
                    Vertex(5f, 13f),
                    Vertex(5f, 5f)
                ),
                true
            ), cut.chains[2]
        )


        assertEquals(2, wrapped.chains.size)

        assertEquals(
            PolygonalChain(
                listOf(
                    Vertex(15f, 0f),
                    Vertex(15f, 2f),
                    Vertex(5f, 2f),
                    Vertex(5f, 0f)
                ),
                true
            ), wrapped.chains[0]
        )

        assertEquals(
            PolygonalChain(
                listOf(
                    Vertex(5f, 0f),
                    Vertex(15f, 0f),
                ),
                false
            ), wrapped.chains[1]
        )

    }

    @Test
    fun square_vhConstraints() {
        val s = square_5_5_15_15()

        s.verticalConstraint(10f)
        s.horizontalConstraint(10f)

        assertEquals(4, s.parts.size)
    }

    @Test
    fun triangle_verticalConstraint() {
        val uut = convexPolygonPeriodicShape(
            Vertex(5f, 1f),
            Vertex(9f, 3f),
            Vertex(9f, 1f)
        ) as MultiPartPeriodicShape
        uut.verticalConstraint(7f)

        assertEquals(2, uut.parts.size)

        assertEquals(
            ConvexPolygon(
                listOf(
                    PolygonalChain(
                        listOf(
                            Vertex(5f, 1f),
                            Vertex(7f, 2f)
                        ),
                        visible = true
                    ),
                    PolygonalChain(
                        listOf(
                            Vertex(7f, 2f),
                            Vertex(7f, 1f)
                        ),
                        visible = false
                    ),
                    PolygonalChain(
                        listOf(
                            Vertex(7f, 1f),
                            Vertex(5f, 1f)
                        ),
                        visible = true
                    )
                )
            ),
            uut.parts[0]
        )

        assertEquals(
            ConvexPolygon(
                listOf(
                    PolygonalChain(listOf(
                        Vertex(0f, 2f),
                        Vertex(2f, 3f),
                        Vertex(2f, 1f),
                        Vertex(0f, 1f),
                    ), true),
                    PolygonalChain(listOf(
                        Vertex(0f, 1f),
                        Vertex(0f, 2f)
                    ), false),
                )
            ),
            uut.parts[1]
        )
    }

    @Test
    fun rect_minus77_0_77_73_verticalConstraint() {
        val shape = rect_minus77_0_75_73()

        shape.verticalConstraint(1080f)

        assertEquals(2, shape.parts.size)

        val cut = shape.parts.first { it.contains(2f, 2f) }
        val wrapped = shape.parts.first { it != cut }

        assertEquals(2, cut.chains.size)
        assertEquals(
            PolygonalChain(
                listOf(
                    Vertex(x = 0f, y = 0f),
                    Vertex(x = 75.0f, y = 0f),
                    Vertex(x = 75.0f, y = 73f),
                    Vertex(x = 0f, y = 73f)
                ), visible = true
            ), cut.chains[0]
        )

        assertEquals(
            PolygonalChain(
                listOf(
                    Vertex(x = 0f, y = 73f),
                    Vertex(x = 0f, y = 0f)
                ), visible = false
            ), cut.chains[1]
        )

        assertEquals(3, wrapped.chains.size)

        assertEquals(
            PolygonalChain(
                listOf(
                    Vertex(x = 1003f, y = 0f),
                    Vertex(x = 1080f, y = 0f),
                ), visible = true
            ), wrapped.chains[0]
        )

        assertEquals(
            PolygonalChain(
                listOf(
                    Vertex(x = 1080f, y = 0f),
                    Vertex(x = 1080f, y = 73f)
                ), visible = false
            ), wrapped.chains[1]
        )

        assertEquals(
            PolygonalChain(
                listOf(
                    Vertex(x = 1080f, y = 73f),
                    Vertex(x = 1003f, y = 73f),
                    Vertex(x = 1003f, y = 0f)
                ), visible = true
            ), wrapped.chains[2]
        )
    }

    @Test
    fun rect_degenereCut_verticalCut() {
        val mpps = MultiPartPeriodicShape(
            listOf(
                ConvexPolygon(
                    chains = listOf(
                        PolygonalChain(
                            vertices = listOf(
                                Vertex(x = 1080.0f, y = 2016.8096f),
                                Vertex(x = 1152.0f, y = 2016.8096f)
                            ), visible = false
                        ),
                        PolygonalChain(
                            vertices = listOf(
                                Vertex(x = 1152.0f, y = 2016.8096f),
                                Vertex(x = 1152.0f, y = 2066.0f)
                            ), visible = true
                        ),
                        PolygonalChain(
                            vertices = listOf(
                                Vertex(x = 1152.0f, y = 2066.0f),
                                Vertex(x = 1080.0f, y = 2066.0f),
                                Vertex(x = 1080.0f, y = 2016.8096f)
                            ), visible = false
                        )
                    )
                )
            )
        )
        mpps.verticalConstraint(1080.0f)

        assertEquals(1, mpps.parts.size)
        val wrapped = mpps.parts.first()
        assertEquals(3, wrapped.chains.size)

        assertEquals(
            PolygonalChain(
                listOf(
                    Vertex(x = 0f, y = 2016.8096f),
                    Vertex(x = 72.0f, y = 2016.8096f)
                ), visible = false
            ), wrapped.chains[0]
        )

        assertEquals(
            PolygonalChain(
                listOf(
                    Vertex(x = 72.0f, y = 2016.8096f),
                    Vertex(x = 72.0f, y = 2066.0f)
                ), visible = true
            ), wrapped.chains[1]
        )

        assertEquals(
            PolygonalChain(
                listOf(
                    Vertex(x = 72.0f, y = 2066.0f),
                    Vertex(x = 0.0f, y = 2066.0f),
                    Vertex(x = 0.0f, y = 2016.8096f)
                ), visible = false
            ), wrapped.chains[2]
        )
    }
}