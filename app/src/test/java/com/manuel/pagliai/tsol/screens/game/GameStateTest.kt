package com.manuel.pagliai.tsol.screens.game

import androidx.compose.ui.graphics.Color
import org.junit.Assert.*
import org.junit.Test

class GameStateTest {

    @Test
    fun testAutomatonColors() {
        val color1 = Color(1)
        val color2 = Color(2)
        val color3 = Color(3)
        val color4 = Color(4)

        assertNotEquals(color1, color2)
        assertNotEquals(color1, color3)
        assertNotEquals(color1, color4)
        assertNotEquals(color2, color3)
        assertNotEquals(color2, color4)
        assertNotEquals(color3, color4)

        val systemColor = AutomatonColors(null, null, null)
        assertEquals(color1, systemColor.cellsColorOr(color1))
        assertEquals(color1, systemColor.gridColorOr(color1))
        assertEquals(color1, systemColor.backgroundColorOr(color1))
        assertTrue(systemColor.usingSystemThemeForCellsColor())


        val customColors = AutomatonColors(cellsColor = color1, gridColor = color2, backgroundColor = color3)
        assertEquals(color1, customColors.cellsColorOr(color4))
        assertEquals(color2, customColors.gridColorOr(color4))
        assertEquals(color3, customColors.backgroundColorOr(color4))
        assertFalse(customColors.usingSystemThemeForCellsColor())
    }
}