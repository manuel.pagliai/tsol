package com.manuel.pagliai.tsol.domain.cells

import com.manuel.pagliai.tsol.TestCanvasConfiguration
import com.manuel.pagliai.tsol.di.DaggerTestComponent
import com.manuel.pagliai.tsol.domain.CellType
import com.manuel.pagliai.tsol.domain.Grid
import com.manuel.pagliai.tsol.screens.game.GameState
import org.junit.Assert.*
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import javax.inject.Inject
import kotlin.random.Random

@RunWith(RobolectricTestRunner::class)
class CellTypeTest {

    private lateinit var random: Random

    @Inject
    lateinit var cellTypes: CellTypes

    @Before
    fun setup() {
        DaggerTestComponent.create().inject(this)
        random = Random(0)
    }

    private fun eachCellType(
        debug: Boolean = false,
        debugInfo: String = "",
        test: (CellType) -> Unit
    ) {
        for (uut in cellTypes.list) {
            if (debug) {
                println("Testing $debugInfo ${uut.id}")
            }
            test(uut)
        }
    }

    private fun forEachCellTypeAndCanvasConfigCellSize(
        debug: Boolean = false,
        debugInfo: String = "",
        action: (CellType, TestCanvasConfiguration, Int) -> Unit
    ) {
        for (cellType in cellTypes.list) {
            for (canvasConfig in TestCanvasConfiguration.all()) {
                for (cellSize in GameState.MIN_CELL_SIZE..GameState.MAX_CELL_SIZE) {
                    if (debug) {
                        println("Testing $debugInfo ${cellType.id} with canvas $canvasConfig and cell size $cellSize")
                    }
                    action(cellType, canvasConfig, cellSize)
                }
            }
        }
    }

    private inline fun forEachCellTypeAndEachGrid(
        debug: Boolean = false,
        debugInfo: String = "",
        crossinline action: (CellType, Grid) -> Unit
    ) {
        forEachCellTypeAndCanvasConfigCellSize(
            debug = debug,
            debugInfo = debugInfo
        ) { cellType, canvasConfig, cellSize ->
            action(
                cellType,
                cellType.gridFactory.createGrid(
                    canvasWidth = canvasConfig.canvasWidth,
                    canvasHeight = canvasConfig.canvasHeight,
                    displayDensity = canvasConfig.displayDensity,
                    cellSize
                )
            )
        }
    }

    @Test
    fun setNeighboursCoordinates() = forEachCellTypeAndEachGrid { cellType, grid ->
        setNeighboursCoordinatesImpl(cellType, grid)
    }

    private fun setNeighboursCoordinatesImpl(
        cellType: CellType,
        grid: Grid
    ) {
        val neighboursCoordinates = IntArray(cellType.neighboursCount) { -1 }
        val neighboursNeighboursCoordinates = IntArray(cellType.neighboursCount) { -1 }

        for (cellPosition in 0 until grid.size) {

            neighboursCoordinates.reset()
            val (cellX, cellY) = grid.toXY(cellPosition)
            cellType.setNeighboursCoordinates(grid, cellX, cellY, neighboursCoordinates, 0)

            // neighbours are all distinct
            assertEquals(cellType.neighboursCount, neighboursCoordinates.toSet().size)

            // neighbours are always replaced and valid coordinates
            neighboursCoordinates.forEach {
                assertTrue(it >= 0)
                assertTrue(it < grid.size)
            }

            // this cell is a neighbour of each of its neighbours
            for (neighbour in neighboursCoordinates) {
                neighboursNeighboursCoordinates.reset()
                val (neighbourX, neighbourY) = grid.toXY(neighbour)
                cellType.setNeighboursCoordinates(
                    grid,
                    neighbourX,
                    neighbourY,
                    neighboursNeighboursCoordinates,
                    0
                )
                assertTrue(neighboursNeighboursCoordinates.contains(cellPosition))
            }

        }
    }

    @Test
    fun defaultGameRules() = eachCellType { uut ->
        val gameRules = uut.defaultGameRules
        assertTrue(gameRules.birthWith.isNotEmpty())
        assertTrue(gameRules.surviveWith.isNotEmpty())
        gameRules.birthWith.forEach {
            assertTrue(it >= 0)
            assertTrue(it <= uut.neighboursCount)
        }
        gameRules.surviveWith.forEach {
            assertTrue(it >= 0)
            assertTrue(it <= uut.neighboursCount)
        }
    }

    @Test
    fun gridFactory() = forEachCellTypeAndEachGrid { _, grid ->
        assertTrue(grid.columns > 0)
        assertTrue(grid.rows > 0)
    }


    @Test
    fun tilingCanvasAdapterFactory() =
        forEachCellTypeAndCanvasConfigCellSize { cellType, canvasConfig, cellSize ->

            val grid = cellType.gridFactory.createGrid(
                canvasWidth = canvasConfig.canvasWidth,
                canvasHeight = canvasConfig.canvasHeight,
                displayDensity = canvasConfig.displayDensity,
                cellSize
            )

            val canvasAdapter = cellType.tilingOnCanvasFactory.create(
                canvasWidth = canvasConfig.canvasWidth,
                canvasHeight = canvasConfig.canvasHeight,
                grid = grid
            )

            // just check no crash happens when querying, and if the method returns then it's the coordinates are valid
            repeat(10_000) {
                val x = random.nextFloat() * canvasConfig.canvasWidth
                val y = random.nextFloat() * canvasConfig.canvasHeight
                val coordinates = canvasAdapter.coordinates(x = x, y = y)
                assertTrue(
                    coordinates == null ||
                            (coordinates >= 0 && coordinates < grid.size)
                )
            }
        }

    @Test
    fun setNeighboursCoordinatesOffset() {
        fun checkCoordinatesSet(a: IntArray, nonNullStart: Int, nonNullCount: Int, alsoNot: Int) {
            for (i in a.indices) {
                if (i >= nonNullStart && i < nonNullCount + nonNullStart) {
                    assertNotEquals(-1, a[i])
                    assertNotEquals(alsoNot, a[i])
                } else {
                    assertEquals(-1, a[i])
                }
            }
        }

        val array = IntArray(150) { -1 }
        forEachCellTypeAndEachGrid { cellType, grid ->
            for (cellIndex in 0 until grid.size) {
                array.reset()
                val (x, y) = grid.toXY(cellIndex)
                cellType.setNeighboursCoordinates(
                    grid = grid,
                    x = x,
                    y = y,
                    dstArray = array,
                    dstOffset = 100
                )
                checkCoordinatesSet(array, 100, cellType.neighboursCount, cellIndex)
                repeat(10) {
                    array.reset()
                    val dstOffset = random.nextInt(array.size - cellType.neighboursCount)
                    cellType.setNeighboursCoordinates(
                        grid = grid,
                        x = x,
                        y = y,
                        dstArray = array,
                        dstOffset = dstOffset
                    )
                    checkCoordinatesSet(array, dstOffset, cellType.neighboursCount, cellIndex)
                }
            }
        }
    }


    @Ignore("Useful during development. Outputs to console, but it does not actually test anything")
    @Test
    fun gridFactory_debug() {
        val cc = TestCanvasConfiguration.PIXEL3_PORTRAIT
        for (cell in cellTypes.list) {
            print("${cell.id} ")
            repeat(20 - cell.id.length) {
                print(" ")
            }
            for (cellSize in GameState.MIN_CELL_SIZE until GameState.MAX_CELL_SIZE) {

                val grid = cell.gridFactory.createGrid(
                    cc.canvasWidth,
                    cc.canvasHeight,
                    cc.displayDensity,
                    cellSize
                )

                print("(${String.format("%03d", grid.rows)}, ${String.format("%03d", grid.columns)}, -> ${String.format("%04d", grid.size)}) ")

            }
            println("")
        }
    }
}

private fun IntArray.reset() {
    for (i in 0 until size) {
        this[i] = -1
    }
}

