package com.manuel.pagliai.tsol.domain.engine

import com.manuel.pagliai.tsol.domain.*
import com.manuel.pagliai.tsol.domain.cells.SquareCellType
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import kotlin.random.Random

class AutomatonNextGenCalculatorTest {

    private lateinit var uut: AutomatonNextGenCalculator
    private lateinit var cells20x10: Cells
    private lateinit var random: Random
    private lateinit var squareCellType: SquareCellType

    @Before
    fun setup() {
        cells20x10 = Cells(grid = Grid(rows = 20, columns = 10))
        squareCellType = SquareCellType()
        random = Random(0)
        uut = AutomatonNextGenCalculator(random)
    }

    @Test
    fun testNextGenerationCellStillLife() {
        fun testBlock(grid: Grid, tlColumn: Int, tlRow: Int) {


            // set up a block
            /*
             ___.._________.._
             ___.._________.._
             ...
             ___.._________.._
             ___..__XX_____.._
             ___..__XX_____.._
             ___.._________.._
             ___.._________.._
             ___.._________.._
             ...
             ___.._________.._
            */
            val blockCoordinates = setOf(
                grid.toLinear(x = tlColumn, y = tlRow),
                grid.toLinear(x = tlColumn + 1, y = tlRow),
                grid.toLinear(x = tlColumn, y = tlRow + 1),
                grid.toLinear(x = tlColumn + 1, y = tlRow + 1)
            )

            val block0 = aliveWithCoordinates(grid, blockCoordinates)
            Assert.assertEquals(4, block0.aliveCount)
            assertOnlyOnesAlive(
                block0,
                tlColumn, tlRow,
                tlColumn + 1, tlRow,
                tlColumn, tlRow + 1,
                tlColumn + 1, tlRow + 1
            )

            /*
             ___.._________.._       ___.._________.._
             ___.._________.._       ___.._________.._
             ...                     ...
             ___.._________.._       ___.._________.._
             ___..__XX_____.._       ___..__XX____.._
             ___..__XX_____.._   ->  ___..__XX____.._
             ___.._________.._       ___..________.._
             ___.._________.._       ___.._________.._
             ___.._________.._       ___.._________.._
             ...                     ...
             ___.._________.._       ___.._________.._
            */
            val block1 =
                uut.nextGen(cells = block0, squareCellType, squareCellType.defaultGameRules)

            Assert.assertEquals(4, block1.aliveCount)
            Assert.assertEquals(block0, block1)
            Assert.assertNotSame(block0, block1)

            assertOnlyOnesAlive(
                block1,
                tlColumn, tlRow,
                tlColumn + 1, tlRow,
                tlColumn, tlRow + 1,
                tlColumn + 1, tlRow + 1
            )

            assertOnlyOnesAlive(
                block0,
                tlColumn, tlRow,
                tlColumn + 1, tlRow,
                tlColumn, tlRow + 1,
                tlColumn + 1, tlRow + 1
            )
        }


        testBlock(cells20x10.grid, 0, 0)
        testBlock(cells20x10.grid, 19, 9)
        testBlock(cells20x10.grid, 3, 4)
        testBlock(cells20x10.grid, 4, 1)

        repeat(1_000) {
            val grid = Grid(
                rows = random.nextInt(5, 200),
                columns = random.nextInt(5, 200)
            )

            testBlock(
                grid,
                tlColumn = random.nextInt(grid.columns),
                tlRow = random.nextInt(grid.rows)
            )
        }
    }

    @Test
    fun testNextGenerationCellGlider() {
        fun testGlider(grid: Grid) {
            val gCells = Cells(grid = grid)
            assertOnlyOnesAlive(gCells) // none alive

            // set up a glider
            /*
             _X______..._
             __X______.._
             XXX______.._
             _________.._
             _________.._
             ...
             _________.._
            */

            val glider0 = gCells.setCell(1, 0, true)
                .setCell(2, 1, true)
                .setCell(0, 2, true)
                .setCell(1, 2, true)
                .setCell(2, 2, true)

            assertOnlyOnesAlive(
                glider0,
                1, 0,
                2, 1,
                0, 2,
                1, 2,
                2, 2
            )

            // next gen ( glider 1)

            /*
             _X______..._       _________.._
             __X______.._       X_X______.._
             XXX______.._   ->  _XX______.._
             _________.._       _X_______.._
             _________.._       _________.._
             ...                ...
             _________.._       _________.._
            */

            val glider1 = this.uut.nextGen(glider0, squareCellType, squareCellType.defaultGameRules)
            assertOnlyOnesAlive(
                glider1,
                0, 1,
                2, 1,
                1, 2,
                2, 2,
                1, 3
            )

            /*
             _________.._       _________.._
             X_X______.._       __X______.._
             _XX______.._   ->  X_X______.._
             _X_______.._       _XX______.._
             _________.._       _________.._
             ...
             _________.._       _________.._
            */
            val glider2 = this.uut.nextGen(glider1, squareCellType, squareCellType.defaultGameRules)
            assertOnlyOnesAlive(
                glider2,
                2, 1,
                0, 2,
                2, 2,
                1, 3,
                2, 3
            )

            /*
             _________.._       _________.._
             __X______.._       _X_______.._
             X_X______.._   ->  __XX_____.._
             _XX______.._       _XX______.._
             _________.._       _________.._
             _________.._       _________.._
             ...                ...
             _________.._       _________.._
            */
            val glider3 = this.uut.nextGen(glider2, squareCellType, squareCellType.defaultGameRules)
            assertOnlyOnesAlive(
                glider3,
                1, 1,
                2, 2,
                3, 2,
                1, 3,
                2, 3
            )

            /*
             _________.._       _________.._
             _X_______.._       __X______.._
             __XX_____.._   ->  ___X_____.._
             _XX______.._       _XXX_____.._
             _________.._       _________.._
             _________.._       _________.._
             ...                ...
             _________.._       _________.._
            */
            val glider4 = this.uut.nextGen(glider3, squareCellType, squareCellType.defaultGameRules)
            assertOnlyOnesAlive(
                glider4,
                2, 1,
                3, 2,
                1, 3,
                2, 3,
                3, 3
            )

            // A glider will never die, and it will always have 5 cells alive

            var g = glider4
            repeat(100) {
                g = this.uut.nextGen(g, squareCellType, squareCellType.defaultGameRules)
                Assert.assertEquals(5, g.aliveCount)
            }
        }


        testGlider(cells20x10.grid)

        repeat(1_000) {
            val grid = Grid(
                rows = random.nextInt(5, 200),
                columns = random.nextInt(5, 200)
            )

            testGlider(grid)
        }
    }

    @Test
    fun testNextGenerationCellBlinker() {



        fun testBlinker(grid: Grid, tlColumn: Int, tlRow: Int) {


            // set up a blinker

            /*
             ___.._________.._
             ___.._________.._
             ...
             ___.._________.._
             ___.._________.._
             ___..__XXX____.._
             ___.._________.._
             ___.._________.._
             ...
             ___.._________.._
            */

            val blinker0Coordinates = setOf(
                grid.toLinear(x = tlColumn, y = tlRow),
                grid.toLinear(x = tlColumn + 1, y = tlRow),
                grid.toLinear(x = tlColumn + 2, y = tlRow),
            )

            val blinker0 = aliveWithCoordinates(grid, blinker0Coordinates)
            Assert.assertEquals(3, blinker0.aliveCount)
            assertOnlyOnesAlive(
                blinker0,
                tlColumn, tlRow,
                tlColumn + 1, tlRow,
                tlColumn + 2, tlRow,
            )


            /*
             ___.._________.._       ___.._________.._
             ___.._________.._       ___.._________.._
             ...                     ...
             ___.._________.._       ___.._________.._
             ___.._________.._       ___..__X______.._
             ___..__XXX____.._   ->  ___..__X_____.._
             ___.._________.._       ___..__X_____.._
             ___.._________.._       ___.._________.._
             ___.._________.._       ___.._________.._
             ...                     ...
             ___.._________.._       ___.._________.._
            */

            val blinker1 = uut.nextGen(blinker0, squareCellType, squareCellType.defaultGameRules)

            Assert.assertEquals(3, blinker1.aliveCount)
            Assert.assertNotEquals(blinker0, blinker1)

            assertOnlyOnesAlive(
                blinker1,
                tlColumn + 1, tlRow - 1,
                tlColumn + 1, tlRow,
                tlColumn + 1, tlRow + 1
            )

            assertOnlyOnesAlive(
                blinker0,
                tlColumn, tlRow,
                tlColumn + 1, tlRow,
                tlColumn + 2, tlRow
            )

            /*
             ___.._________.._       ___.._________.._
             ___.._________.._       ___.._________.._
             ...                     ...
             ___.._________.._       ___.._________.._
             ___..___X_____.._       ___.._________.._
             ___..___X_____.._   ->  ___..__XXX____.._
             ___..___X_____.._       ___..________.._
             ___.._________.._       ___.._________.._
             ___.._________.._       ___.._________.._
             ...                     ...
             ___.._________.._       ___.._________.._
            */

            val blinker2 = uut.nextGen(blinker1, squareCellType, squareCellType.defaultGameRules)

            Assert.assertEquals(3, blinker2.aliveCount)
            Assert.assertNotEquals(blinker1, blinker2)
            Assert.assertEquals(blinker0, blinker2)
            Assert.assertNotSame(blinker0, blinker2)

            assertOnlyOnesAlive(
                blinker2,
                tlColumn, tlRow,
                tlColumn + 1, tlRow,
                tlColumn + 2, tlRow
            )

            var b: Cells = blinker2
            repeat(100) {
                if (it % 2 == 0) {
                    Assert.assertEquals(blinker0, b)
                    Assert.assertNotSame(blinker0, b)
                } else {
                    Assert.assertEquals(blinker1, b)
                    Assert.assertNotSame(blinker1, b)
                }
                b = uut.nextGen(b, squareCellType, squareCellType.defaultGameRules)
                Assert.assertEquals(3, b.aliveCount)
            }
        }


        testBlinker(cells20x10.grid, 0, 0)
        testBlinker(cells20x10.grid, 19, 9)
        testBlinker(cells20x10.grid, 3, 4)
        testBlinker(cells20x10.grid, 4, 1)

        repeat(1_000) {
            val grid = Grid(
                rows = random.nextInt(5, 200),
                columns = random.nextInt(5, 200)
            )

            testBlinker(
                grid,
                tlColumn = random.nextInt(grid.columns),
                tlRow = random.nextInt(grid.rows)
            )
        }

    }

    private fun assertOnlyOnesAlive(cells: Cells, vararg xys: Int) {
        Assert.assertEquals(0, xys.size % 2)
        val alive: Set<Int> =
            xys.asList().chunked(2).map { cells.grid.toLinear(x = it[0], y = it[1]) }.toSet()
        cells.forEachIndexed { index, cellAlive ->
            if (cellAlive) {
                Assert.assertTrue(alive.contains(index))
            } else {
                Assert.assertFalse(alive.contains(index))
            }
        }
    }


}