package com.manuel.pagliai.tsol.domain

import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import kotlin.random.Random

class SavedGameRulesTest {

    private lateinit var random: Random

    @Before
    fun setup() {
        random = Random(0)
    }

    @Test
    fun isAliveInNextGen() {
        GameRules(birthWith = listOf(2, 3), surviveWith = listOf(4)).let {

            // birth
            assertTrue(it.aliveInNextGen(false, 2, random))
            assertTrue(it.aliveInNextGen(false, 3, random))
            assertFalse(it.aliveInNextGen(false, 4, random))
            assertFalse(it.aliveInNextGen(false, 5, random))

            // survival
            assertFalse(it.aliveInNextGen(true, 2, random))
            assertFalse(it.aliveInNextGen(true, 3, random))
            assertTrue(it.aliveInNextGen(true, 4, random))
            assertFalse(it.aliveInNextGen(true, 5, random))
        }
    }

    @Test
    fun encoding() {
        fun testEncodeDecode(rules : GameRules) {
            assertEquals(
                rules,
                GameRules.Encoding.decode(GameRules.Encoding.encode(rules))
            )
        }


        testEncodeDecode(GameRules(birthWith = listOf(2, 3), surviveWith = listOf(4)))
        testEncodeDecode(GameRules(birthWith = listOf(2, 3), surviveWith = listOf()))
        testEncodeDecode(GameRules(birthWith = listOf(), surviveWith = listOf(4)))
        testEncodeDecode(GameRules(birthWith = listOf(), surviveWith = listOf()))
    }

}