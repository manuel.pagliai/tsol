package com.manuel.pagliai.tsol.com.manuel.pagliai.tsol.domain.tiling

import com.manuel.pagliai.tsol.domain.Grid
import com.manuel.pagliai.tsol.domain.tiling.*
import org.junit.Assert.assertArrayEquals
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.util.*

class PeriodicShapesTilingOnCanvasTest {

    lateinit var random: Random

    @Before
    fun setup() {
        random = Random(0)
    }

    @Test
    fun create() {
        val grid = Grid(rows = 20, columns = 10)
        val uut = PeriodicShapesTilingOnCanvas.create(
            grid,
            canvasWidth = 100f,
            canvasHeight = 200f,
            object : PeriodicShapesTilingOnCanvas.PeriodicShapeBuilder {
                override fun createShape(x: Int, y: Int): PeriodicShape {
                    val tl = Vertex(x * 10f, y * 10f)
                    val tr = tl.xShift(10f)
                    val br = tr.yShift(10f)
                    val bl = br.xShift(-10f)
                    return convexPolygonPeriodicShape(tl, tr, br, bl)
                }
            }
        )

        // make assertions on the tiling that has been created.
        // The implementation of the class itself is all delegated to the tiling (tested separately)
        val expectedShapes = mutableListOf<PeriodicShape>()
        repeat(grid.size) { linear ->
            val (x,y) = grid.toXY(linear)
            val tl = Vertex(x * 10f, y * 10f)
            val tr = tl.xShift(10f)
            val br = tr.yShift(10f)
            val bl = br.xShift(-10f)
            expectedShapes.add(convexPolygonPeriodicShape(tl, tr, br, bl))
        }

        assertEquals(expectedShapes.size, uut.tiling.shapes.size)
        repeat(expectedShapes.size) { index ->
            assertArrayEquals(
                (expectedShapes[index] as MultiPartPeriodicShape).parts,
                (uut.tiling.shapes[index] as MultiPartPeriodicShape).parts
            )
        }


        // test coordinates also (easy to do).
        assertEquals(0, uut.coordinates(6f, 7f))

        repeat(1000) {
            val x = 100f * random.nextFloat()
            val y = 100f * random.nextFloat()
            assertEquals(
                grid.toLinear((x/10f).toInt(), (y/10f).toInt()),
                uut.coordinates(x,y)
            )
        }
    }
}