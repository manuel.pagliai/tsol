package com.manuel.pagliai.tsol.domain

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import kotlin.random.Random

class GridTest {

    private lateinit var random: Random

    @Before
    fun setup() {
        random = Random(0)
    }

    @Test
    fun testCoordinateConversion() {

        fun testCoordinateConversion(grid: Grid) {
            for (c in 0 until grid.size) {
                val (x, y) = grid.toXY(c)
                assertEquals(c, grid.toLinear(x, y))

                // periodic conditions
                assertEquals(c, grid.toLinear(x + grid.columns, y + grid.rows))
            }

            for (y in 0 until grid.rows) {
                for (x in 0 until grid.columns) {
                    val c = grid.toLinear(x, y)
                    assertEquals(Pair(x, y), grid.toXY(c))
                }
            }

            assertEquals(0, grid.toLinear(x = grid.columns, y = grid.rows))
        }

        testCoordinateConversion(Grid(rows = 10, columns = 20))
        testCoordinateConversion(Grid(rows = 20, columns = 10))
        repeat(1_000) {
            testCoordinateConversion(
                Grid(
                    rows = random.nextInt(1, 1000),
                    columns = random.nextInt(1, 1000)
                )
            )
        }
    }
}