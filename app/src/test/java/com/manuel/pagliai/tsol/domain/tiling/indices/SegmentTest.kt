package com.manuel.pagliai.tsol.domain.tiling.indices

import org.junit.Assert
import org.junit.Test

class SegmentTest {

    @Test
    fun containsOther() {

        fun assertContains(s1: Segment<*>, s2: Segment<*>) {
            Assert.assertTrue(s1.contains(s1))
            Assert.assertTrue(s2.contains(s2))

            Assert.assertTrue(s1.contains(s2))

            // contains => intersect
            Assert.assertTrue(s1.intersect(s2))
            Assert.assertTrue(s2.intersect(s1))
        }

        fun assertNotContains(s1: Segment<*>, s2: Segment<*>) {
            Assert.assertTrue(s1.contains(s1))
            Assert.assertTrue(s2.contains(s2))

            Assert.assertFalse(s1.contains(s2))
        }

        assertContains(
            Segment(1f, 6f, infClosed = true, supClosed = true, payload = Unit),
            Segment(2f, 4f, infClosed = true, supClosed = true, payload = Unit)

        )

        assertContains(
            Segment(1f, 6f, infClosed = true, supClosed = true, payload = "something"),
            Segment(2f, 4f, infClosed = true, supClosed = true, payload = 42)

        )

        assertContains(
            Segment(1f, 6f, infClosed = false, supClosed = false, payload = Unit),
            Segment(2f, 4f, infClosed = true, supClosed = true, payload = Unit)

        )


        assertContains(
            Segment(1f, 6f, infClosed = true, supClosed = true, payload = Unit),
            Segment(1f, 6f, infClosed = true, supClosed = false, payload = Unit)

        )

        assertContains(
            Segment(1f, 6f, infClosed = false, supClosed = false, payload = Unit),
            Segment(1f, 6f, infClosed = false, supClosed = false, payload = Unit)

        )

        assertContains(
            Segment(1f, 6f, infClosed = false, supClosed = true, payload = Unit),
            Segment(1f, 6f, infClosed = false, supClosed = true, payload = Unit)

        )

        assertContains(
            Segment(1f, 6f, infClosed = true, supClosed = false, payload = Unit),
            Segment(1f, 6f, infClosed = true, supClosed = false, payload = Unit)
        )

        assertContains(
            Segment(1f, 6f, infClosed = true, supClosed = true, payload = Unit),
            Segment(1f, 6f, infClosed = true, supClosed = true, payload = Unit)
        )

        assertNotContains(
            Segment(1f, 6f, infClosed = true, supClosed = true, payload = Unit),
            Segment(0f, 2f, infClosed = true, supClosed = true, payload = Unit)

        )

        assertNotContains(
            Segment(1f, 6f, infClosed = true, supClosed = true, payload = Unit),
            Segment(2f, 7f, infClosed = true, supClosed = true, payload = Unit)
        )


        assertNotContains(
            Segment(1f, 6f, infClosed = true, supClosed = true, payload = Unit),
            Segment(0f, 7f, infClosed = true, supClosed = true, payload = Unit)
        )


        assertNotContains(
            Segment(1f, 6f, infClosed = false, supClosed = true, payload = Unit),
            Segment(1f, 6f, infClosed = true, supClosed = true, payload = Unit)
        )


        assertNotContains(
            Segment(1f, 6f, infClosed = true, supClosed = false, payload = Unit),
            Segment(1f, 6f, infClosed = true, supClosed = true, payload = Unit)
        )

    }

    @Test
    fun containsPoint() {
        Segment(1f, 6f, infClosed = true, supClosed = true, payload = Unit).let {
            Assert.assertTrue(it.contains(1f))
            Assert.assertTrue(it.contains(2f))
            Assert.assertTrue(it.contains(6f))
            Assert.assertFalse(it.contains(0f))
            Assert.assertFalse(it.contains(7f))
        }

        Segment(1f, 6f, infClosed = false, supClosed = true, payload = Unit).let {
            Assert.assertFalse(it.contains(1f))
            Assert.assertTrue(it.contains(2f))
            Assert.assertTrue(it.contains(6f))
            Assert.assertFalse(it.contains(0f))
            Assert.assertFalse(it.contains(7f))
        }

        Segment(1f, 6f, infClosed = true, supClosed = false, payload = Unit).let {
            Assert.assertTrue(it.contains(1f))
            Assert.assertTrue(it.contains(2f))
            Assert.assertFalse(it.contains(6f))
            Assert.assertFalse(it.contains(0f))
            Assert.assertFalse(it.contains(7f))
        }

        Segment(1f, 6f, infClosed = false, supClosed = false, payload = Unit).let {
            Assert.assertFalse(it.contains(1f))
            Assert.assertTrue(it.contains(2f))
            Assert.assertFalse(it.contains(6f))
            Assert.assertFalse(it.contains(0f))
            Assert.assertFalse(it.contains(7f))
        }
    }

    @Test
    fun intersect() {
        fun assertIntersect(s1: Segment<*>, s2: Segment<*>) {
            Assert.assertTrue(s1.intersect(s1))
            Assert.assertTrue(s2.intersect(s2))
            Assert.assertTrue(s1.intersect(s2))
            Assert.assertTrue(s2.intersect(s1))
        }

        fun assertNotIntersect(s1: Segment<*>, s2: Segment<*>) {
            Assert.assertTrue(s1.intersect(s1))
            Assert.assertTrue(s2.intersect(s2))
            Assert.assertFalse(s1.intersect(s2))
            Assert.assertFalse(s2.intersect(s1))
        }

        Segment(1f, 6f, infClosed = true, supClosed = true, payload = Unit).let {
            assertIntersect(it, it)
            assertIntersect(it, Segment(0f, 8f, infClosed = true, supClosed = true, payload = Unit))
            assertIntersect(
                it,
                Segment(0f, 8f, infClosed = false, supClosed = false, payload = Unit)
            )
            assertIntersect(it, Segment(0f, 2f, infClosed = true, supClosed = true, payload = Unit))
            assertIntersect(
                it,
                Segment(0f, 3f, infClosed = true, supClosed = false, payload = Unit)
            )
            assertIntersect(it, Segment(2f, 3f, infClosed = true, supClosed = true, payload = Unit))
            assertIntersect(
                it,
                Segment(2f, 3f, infClosed = true, supClosed = false, payload = Unit)
            )
            assertIntersect(
                it,
                Segment(2f, 3f, infClosed = false, supClosed = false, payload = Unit)
            )
            assertIntersect(
                it,
                Segment(2f, 8f, infClosed = false, supClosed = false, payload = Unit)
            )
            assertIntersect(it, Segment(0f, 1f, infClosed = true, supClosed = true, payload = Unit))
            assertIntersect(it, Segment(6f, 8f, infClosed = true, supClosed = true, payload = Unit))
            assertNotIntersect(
                it,
                Segment(0f, 1f, infClosed = true, supClosed = false, payload = Unit)
            )
            assertNotIntersect(
                it,
                Segment(0f, 0.5f, infClosed = true, supClosed = true, payload = Unit)
            )
            assertNotIntersect(
                it,
                Segment(6f, 8f, infClosed = false, supClosed = true, payload = Unit)
            )
            assertNotIntersect(
                it,
                Segment(7f, 8f, infClosed = false, supClosed = true, payload = Unit)
            )
            assertNotIntersect(
                it,
                Segment(7f, 8f, infClosed = true, supClosed = true, payload = Unit)
            )
        }

        Segment(1f, 6f, infClosed = false, supClosed = true, payload = Unit).let {
            assertIntersect(it, Segment(5f, 8f, infClosed = true, supClosed = true, payload = Unit))
            assertIntersect(
                it,
                Segment(5f, 8f, infClosed = false, supClosed = false, payload = Unit)
            )
            assertIntersect(it, Segment(0f, 2f, infClosed = true, supClosed = true, payload = Unit))
            assertIntersect(
                it,
                Segment(0f, 2f, infClosed = true, supClosed = false, payload = Unit)
            )

            assertNotIntersect(
                it,
                Segment(0f, 1f, infClosed = true, supClosed = true, payload = Unit)
            )
            assertNotIntersect(
                it,
                Segment(0f, 1f, infClosed = true, supClosed = false, payload = Unit)
            )

            assertNotIntersect(
                it,
                Segment(-1f, 0f, infClosed = true, supClosed = true, payload = Unit)
            )
            assertNotIntersect(
                it,
                Segment(-1f, 0f, infClosed = true, supClosed = false, payload = Unit)
            )
        }

        Segment(1f, 6f, infClosed = true, supClosed = false, payload = Unit).let {
            assertIntersect(it, Segment(5f, 8f, infClosed = true, supClosed = true, payload = Unit))
            assertIntersect(it, Segment(0f, 1f, infClosed = true, supClosed = true, payload = Unit))


            assertNotIntersect(
                it,
                Segment(6f, 8f, infClosed = true, supClosed = true, payload = Unit)
            )
            assertNotIntersect(
                it,
                Segment(6f, 8f, infClosed = false, supClosed = true, payload = Unit)
            )

            assertNotIntersect(
                it,
                Segment(7f, 8f, infClosed = true, supClosed = true, payload = Unit)
            )
            assertNotIntersect(
                it,
                Segment(7f, 8f, infClosed = false, supClosed = true, payload = Unit)
            )

        }
    }
}