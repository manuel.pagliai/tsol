package com.manuel.pagliai.tsol.domain.tiling

import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class PolygonalChainTest {

    private lateinit var v0 : Vertex
    private lateinit var v1 : Vertex
    private lateinit var v2 : Vertex
    private lateinit var v3 : Vertex

    lateinit var uut : PolygonalChain

    @Before
    fun setup() {
        v0 = Vertex(0f,1f)
        v1 = Vertex(2f,3f)
        v2 = Vertex(4f,5f)
        v3 = Vertex(6f,7f)

        uut = PolygonalChain(listOf(v0, v1, v2, v3), true)
    }

    @Test
    fun outOfBoundariesX() {
        Assert.assertFalse(uut.outsideXRange(0f, 6f))
        Assert.assertFalse(uut.outsideXRange(0f, 7f))
        Assert.assertTrue(uut.outsideXRange(1f, 7f))
        Assert.assertTrue(uut.outsideXRange(0f, 5f))
    }

    @Test
    fun outOfBoundariesY() {
        Assert.assertFalse(uut.outsideYRange(1f, 7f))
        Assert.assertFalse(uut.outsideYRange(0f, 8f))
        Assert.assertTrue(uut.outsideYRange(2f, 8f))
        Assert.assertTrue(uut.outsideYRange(0f, 6f))
    }

    @Test
    fun swapXY() {
        assertEquals(
            PolygonalChain(listOf(
                Vertex(1f,0f),
                Vertex(3f,2f),
                Vertex(5f,4f),
                Vertex(7f,6f)
            ),true),
            uut.swapXY()
        )
    }

    @Test
    fun get() {
        assertEquals(v0, uut[0])
        assertEquals(v1, uut[1])
        assertEquals(v2, uut[2])
    }

    @Test
    fun forEach() {
        val vertices = mutableListOf<Vertex>()
        uut.forEach { vertices.add(it) }
        assertEquals(listOf(v0, v1, v2, v3), vertices)
    }

    @Test
    fun getEmpty() {
        Assert.assertFalse(uut.empty)
        Assert.assertTrue(PolygonalChain(listOf(), true).empty)
        Assert.assertTrue(PolygonalChain(listOf(v0), true).empty)
        Assert.assertFalse(PolygonalChain(listOf(v0, v1), true).empty)
    }

    @Test
    fun getLast() {
        assertEquals(v3, uut.last)
    }

    @Test
    fun getFirst() {
        assertEquals(v0, uut.first)
    }

    @Test
    fun getSides() {
        assertEquals(
            listOf(
                Side(v0, v1),
                Side(v1, v2),
                Side(v2, v3),
            ),
            uut.sides
        )

        assertEquals(
            listOf<Side>(),
            PolygonalChain(listOf(), true).sides
        )

        assertEquals(
            listOf<Side>(),
            PolygonalChain(listOf(v0), true).sides
        )

        assertEquals(
            listOf(Side(v0, v1)),
            PolygonalChain(listOf(v0, v1), true).sides
        )
    }
}