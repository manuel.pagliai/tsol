package com.manuel.pagliai.tsol.domain.tiling

import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.util.*


class ConvexPolygonTest {

    private lateinit var uutSquare : ConvexPolygon
    private lateinit var uutSquare2 : ConvexPolygon
    private lateinit var uutTriangle1 : ConvexPolygon
    private lateinit var uutTriangle2 : ConvexPolygon
    private lateinit var uutRectangle : ConvexPolygon
    private lateinit var random : Random

    private val v0 = Vertex(0f,0f)
    private val v1 = Vertex(0f,1f)
    private val v2 = Vertex(1f,1f)
    private val v3 = Vertex(1f,0f)

    private val v4 = Vertex(1f, 2f)
    private val v5 = Vertex(3f, 2f)
    private val v6 = Vertex(3f, 8f)
    private val v7 = Vertex(1f, 8f)

    @Before
    fun setup() {
        uutSquare = ConvexPolygon.fromSingleChain(listOf(v0, v1, v2, v3))
        uutSquare2 = ConvexPolygon(
            listOf(
                PolygonalChain(listOf(v0, v1, v2), true),
                PolygonalChain(listOf(v2, v3, v0), false)
            )
        )
        uutTriangle1 = ConvexPolygon.fromSingleChain(listOf(v0, v1, v3))
        uutTriangle2 = ConvexPolygon.fromSingleChain(listOf(v1, v2, v3))


        uutRectangle = ConvexPolygon.fromSingleChain(listOf(v4, v5, v6, v7))

        random = Random(0)
    }



    @Test
    fun vertices() {
        assertEquals(listOf(v0, v1, v2, v3), uutSquare.vertices)
        assertEquals(listOf(v0, v1, v2, v3), uutSquare2.vertices)

        assertEquals(listOf(v0, v1, v3), uutTriangle1.vertices)
        assertEquals(listOf(v1, v2, v3), uutTriangle2.vertices)
    }

    @Test
    fun getSides() {
        assertEquals(
            listOf(Side(v0, v1), Side(v1, v2), Side(v2, v3), Side(v3, v0)),
            uutSquare.sides
        )

        assertEquals(
            uutSquare.sides,
            uutSquare2.sides
        )

        assertEquals(
            listOf(Side(v0, v1), Side(v1, v3), Side(v3, v0)),
            uutTriangle1.sides
        )
    }

    @Test
    fun outOfBoundariesY() {
        Assert.assertTrue(uutSquare.outOfBoundariesY(0.5f, 5f))
        Assert.assertFalse(uutSquare.outOfBoundariesY(-0.5f, 5f))
        Assert.assertFalse(uutSquare.outOfBoundariesY(0f, 1f))

        Assert.assertTrue(uutTriangle1.outOfBoundariesY(0.5f, 5f))
        Assert.assertFalse(uutTriangle1.outOfBoundariesY(-0.5f, 5f))
        Assert.assertFalse(uutTriangle1.outOfBoundariesY(0f, 1f))

        Assert.assertTrue(uutTriangle2.outOfBoundariesY(0.5f, 1f))
        Assert.assertFalse(uutTriangle2.outOfBoundariesY(-0.5f, 5f))
        Assert.assertFalse(uutTriangle2.outOfBoundariesY(0f, 1f))

        Assert.assertFalse(uutRectangle.outOfBoundariesY(0.5f, 10f))
        Assert.assertTrue(uutRectangle.outOfBoundariesY(3f, 10f))
        Assert.assertTrue(uutRectangle.outOfBoundariesY(3f, 7f))
        Assert.assertFalse(uutRectangle.outOfBoundariesY(2f, 8f))
    }

    @Test
    fun outOfBoundariesX() {
        Assert.assertTrue(uutSquare.outOfBoundariesX(0.5f, 5f))
        Assert.assertFalse(uutSquare.outOfBoundariesX(-0.5f, 5f))
        Assert.assertFalse(uutSquare.outOfBoundariesX(0f, 1f))

        Assert.assertTrue(uutTriangle1.outOfBoundariesX(0.5f, 5f))
        Assert.assertFalse(uutTriangle1.outOfBoundariesX(-0.5f, 5f))
        Assert.assertFalse(uutTriangle1.outOfBoundariesX(0f, 1f))

        Assert.assertTrue(uutTriangle2.outOfBoundariesX(0.5f, 1f))
        Assert.assertFalse(uutTriangle2.outOfBoundariesX(-0.5f, 5f))
        Assert.assertFalse(uutTriangle2.outOfBoundariesX(0f, 1f))

        Assert.assertTrue(uutRectangle.outOfBoundariesX(1.5f, 10f))
        Assert.assertTrue(uutRectangle.outOfBoundariesX(2f, 10f))
        Assert.assertTrue(uutRectangle.outOfBoundariesX(13f, 16f))
        Assert.assertFalse(uutRectangle.outOfBoundariesX(1f, 3f))
    }

    @Test
    fun swapXY() {
        assertEquals(listOf(v4, v5, v6, v7), uutRectangle.vertices)
        assertEquals(
            listOf(v4, v5, v6, v7).map { it.swapXY() }.toList(),
            uutRectangle.swapXY().vertices
        )
    }

    @Test
    fun contains() {
        Assert.assertTrue(uutSquare.contains(1f, 1f))
        Assert.assertFalse(uutSquare.contains(2f, 1f))
        Assert.assertFalse(uutSquare.contains(1f, 2f))
        Assert.assertTrue(uutSquare.contains(0f, 1f))
        Assert.assertTrue(uutSquare.contains(0f, 0f))
        Assert.assertTrue(uutSquare.contains(0f, 0.5f))
        Assert.assertTrue(uutSquare.contains(0.5f, 0.5f))
        Assert.assertFalse(uutSquare.contains(1.5f, 0.5f))
        Assert.assertFalse(uutSquare.contains(0.5f, 1.5f))
        Assert.assertFalse(uutSquare.contains(1.5f, 1.5f))

        Assert.assertTrue(uutTriangle1.contains(0.5f, 0.5f))
        Assert.assertTrue(uutTriangle1.contains(0.5f, 0.2f))
        Assert.assertTrue(uutTriangle1.contains(0.5f, 0f))
        Assert.assertFalse(uutTriangle1.contains(1f, 1f))
        Assert.assertTrue(uutTriangle1.contains(1f, 0f))
        Assert.assertTrue(uutTriangle1.contains(0f, 0f))
        Assert.assertTrue(uutTriangle1.contains(0f, 1f))
        Assert.assertTrue(uutTriangle1.contains(0f, 0.5f))
        Assert.assertTrue(uutTriangle1.contains(0f, 0f))

        Assert.assertFalse(uutTriangle1.contains(0f, 1.5f))
        Assert.assertFalse(uutTriangle1.contains(1.5f, 1.5f))
        Assert.assertFalse(uutTriangle1.contains(-1.5f, 0.5f))
        Assert.assertFalse(uutTriangle1.contains(-1.5f, 1.5f))
    }

    @Test
    fun containsRandom() {

        val n = 100_000

        containsRandom(n,
            -2f, 2f,
            -2f, 2f,
            uutSquare)
        {
            it.x in 0.0..1.0 &&  it.y in 0.0..1.0
        }

        containsRandom(n,
            -2f, 2f,
            -2f, 2f,
            uutSquare2)
        {
            it.x in 0.0..1.0 &&  it.y in 0.0..1.0
        }

        containsRandom(n,
            -2f, 2f,
            -2f, 2f,
            uutTriangle1)
        {
            it.x in 0.0..1.0 &&  it.y in 0.0..1.0
                    && it.y <= 1 -it.x
        }

        containsRandom(n,
            -2f, 2f,
            -2f, 2f,
            uutTriangle2)
        {
            it.x in 0.0..1.0 &&  it.y in 0.0..1.0
                    && it.y >= 1 - it.x
        }

        containsRandom(n,
            0f, 10f,
            0f, 10f,
            uutRectangle)
        {
            it.x in 1.0..3.0 &&  it.y in 2.0..8.0
        }
    }

    private fun containsRandom(
        n : Int,
        minX : Float,
        maxX : Float,
        minY : Float,
        maxY : Float,
        uut: ConvexPolygon,
        onContainsInv : (Vertex) -> Boolean
    ) {
        repeat(n) {
            val v = Vertex(
                random.nextFloat() * (maxX - minX) + minX,
                random.nextFloat() * (maxY - minY) + minY,
            )
            if (uut.contains(v.x, v.y))
                Assert.assertTrue(onContainsInv(v))
            else
                Assert.assertFalse(onContainsInv(v))
        }

    }
}