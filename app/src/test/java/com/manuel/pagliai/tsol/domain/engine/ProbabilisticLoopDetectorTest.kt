package com.manuel.pagliai.tsol.domain.engine

import com.manuel.pagliai.tsol.domain.Cells
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import org.junit.Assert
import org.junit.Before
import org.junit.Test


class ProbabilisticLoopDetectorTest {

    lateinit var uut: ProbabilisticLoopDetector

    @MockK
    lateinit var cells: Cells

    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        uut = ProbabilisticLoopDetector(100)
    }

    @Test
    fun falsePositiveDetection() {
        val c1: Cells = mockk()
        val c2: Cells = mockk()
        val c3: Cells = mockk()
        every { c1.hashCode() } returns 10
        every { c2.hashCode() } returns 110
        every { c3.hashCode() } returns 11

        Assert.assertFalse(uut.looping)
        uut.addGen(c1)
        Assert.assertFalse(uut.looping)
        uut.addGen(c2)
        Assert.assertTrue(uut.looping)
        uut.addGen(c3)
        Assert.assertFalse(uut.looping)
    }

    @Test
    fun reset() {
        Assert.assertFalse(uut.looping)
        uut.addGen(cells)
        Assert.assertFalse(uut.looping)
        uut.addGen(cells)
        Assert.assertTrue(uut.looping)
        uut.reset(cells, enabled = true)
        Assert.assertFalse(uut.looping)

    }

    @Test
    fun disabled() {

        // enabled
        Assert.assertFalse(uut.looping)
        uut.addGen(cells)
        Assert.assertFalse(uut.looping)

        uut.addGen(cells)
        Assert.assertTrue(uut.looping)

        // reset + disable
        uut.reset(cells, enabled = false)

        Assert.assertFalse(uut.looping)
        uut.addGen(cells)
        Assert.assertFalse(uut.looping)

        uut.addGen(cells)
        Assert.assertFalse(uut.looping)

        // re-enable
        uut.reset(cells, enabled = true)

        Assert.assertFalse(uut.looping)
        uut.addGen(cells)
        Assert.assertTrue(uut.looping)
    }

}

