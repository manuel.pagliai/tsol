package com.manuel.pagliai.tsol.domain

fun Cells.forEachIndexed(action: (Int, Boolean) -> Unit) {
    for (c in 0 until size) {
        action(c, this[c])
    }
}

fun aliveWithCoordinates(grid: Grid, aliveCoordinates: Set<Int>) = Cells(grid = grid) {
    aliveCoordinates.contains(it)
}

val Cells.aliveCount: Int
    get() {
        var count = 0
        for (i in 0 until size) {
            if (this[i]) {
                count++
            }
        }
        return count
    }
