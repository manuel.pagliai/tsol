package com.manuel.pagliai.tsol.domain


import org.junit.Assert.*
import org.junit.Test

class SavedGameTest {

    @Test
    fun fromGameState() {
        val state1 = Stubs.gameState(gameName = "state#1")
        val state2 = Stubs.gameState(gameName = "state#2")

        assertEquals(
            SavedGame.from(state1),
            SavedGame.from(state1)
        )

        assertNotSame(
            SavedGame.from(state1),
            SavedGame.from(state1)
        )

        assertNotEquals(
            SavedGame.from(state1),
            SavedGame.from(state2)
        )
    }
}