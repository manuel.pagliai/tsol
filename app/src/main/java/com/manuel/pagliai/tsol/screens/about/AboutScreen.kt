package com.manuel.pagliai.tsol.screens.about

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.TopAppBar
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.TestTags
import com.manuel.pagliai.tsol.screens.common.TopAppBarBackButton
import com.manuel.pagliai.tsol.screens.common.TopBarAppTitle
import kotlinx.coroutines.flow.collectLatest

/**
 * A screen showing the information about the app and the game of life in general.
 *
 * Also provides links to source code and to send feedback.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AboutScreen(
    navController: NavController,
    viewModel: AboutViewModel = hiltViewModel()
) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    TopBarAppTitle(title = stringResource(id = R.string.about))
                },
                backgroundColor = MaterialTheme.colorScheme.background,
                navigationIcon = {
                    TopAppBarBackButton { navController.navigateUp() }
                }
            )
        }
    ) { padding ->
        val context = LocalContext.current
        val sendEmailChooserTitle = stringResource(id = R.string.send_email_chooser_title)
        LaunchedEffect(key1 = context) {
            viewModel.eventFlow.collectLatest { event ->
                when (event) {
                    is AboutScreenUIEvent.SendFeedback -> {
                        val intent = Intent().apply {
                            action = Intent.ACTION_SENDTO
                            setData(Uri.parse("mailto:${event.to}?subject=${event.subject}"))
                            putExtra(Intent.EXTRA_EMAIL, arrayOf(event.to))
                            putExtra(Intent.EXTRA_SUBJECT, event.subject)
                        }
                        // send an email
                        try {
                            context.startActivity(intent)
                        } catch (e: Exception) {
                            e.printStackTrace()
                            context.startActivity(
                                Intent.createChooser(intent, sendEmailChooserTitle)
                            )
                        }
                    }
                }
            }
        }

        AboutScreenContent(
            appVersion = viewModel.appVersion,
            sendFeedback = { viewModel.sendFeedback() },
            links = viewModel.links,
            modifier = Modifier.padding(padding)
        )
    }
}

@Composable
fun AboutScreenContent(
    appVersion: String,
    links: List<Link>,
    sendFeedback: () -> Unit,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier
            .fillMaxWidth()
            .verticalScroll(rememberScrollState())
            .padding(16.dp)
            .testTag(TestTags.ABOUT_SCREEN_COLUMN)
    ) {

        Header(appVersion = appVersion, modifier = Modifier.fillMaxWidth())

        Spacer(modifier = Modifier.height(32.dp))

        AboutSection(stringResource(id = R.string.about)) {
            Spacer(modifier = Modifier.height(16.dp))
            AboutContent()
        }

        AboutSection(title = stringResource(id = R.string.links)) {
            LinksContent(
                links = links,
                sendFeedback = sendFeedback
            )
        }
    }
}

@SuppressLint("ResourceType")
@Composable
fun Header(appVersion: String, modifier: Modifier = Modifier) {
    Column(
        horizontalAlignment = CenterHorizontally,
        modifier = modifier
    ) {
        Image(
            painter = painterResource(id = R.raw.application_icon),
            contentDescription = stringResource(id = R.string.application_icon_cd),
            Modifier
                .clip(CircleShape)
                .width(150.dp)
                .height(150.dp)
        )
        Text(
            text = stringResource(id = R.string.app_name_long),
            style = MaterialTheme.typography.titleMedium
        )
        Text(
            text = stringResource(id = R.string.version, appVersion),
            style = MaterialTheme.typography.titleSmall,
            modifier = Modifier.alpha(0.5f),
        )
    }
}

@Composable
private fun AboutSection(title: String, content: @Composable () -> Unit) {
    Text(
        text = title,
        style = MaterialTheme.typography.titleMedium
    )
    content()
    Spacer(modifier = Modifier.height(32.dp))
}

@Composable
private fun AboutContent() {
    Text(
        text = stringResource(id = R.string.about_content),
        style = MaterialTheme.typography.bodySmall
    )
}

@Composable
fun LinksContent(
    links: List<Link>,
    sendFeedback: () -> Unit
) {
    val uriHandler = LocalUriHandler.current

    links.forEach {
        val uriString = stringResource(id = it.url)
        TextButton(onClick = { uriHandler.openUri(uriString) }) {
            Text(text = stringResource(id = it.label))
        }
    }

    TextButton(onClick = sendFeedback) {
        Text(text = stringResource(id = R.string.send_feedback))
    }
}