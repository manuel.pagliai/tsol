package com.manuel.pagliai.tsol.domain.cells

import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.domain.CellType
import com.manuel.pagliai.tsol.domain.GameRules.Companion.B3S235
import com.manuel.pagliai.tsol.domain.Grid
import com.manuel.pagliai.tsol.domain.GridFactory
import com.manuel.pagliai.tsol.domain.GridFactory.Companion.stdGridFactory
import com.manuel.pagliai.tsol.domain.TilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShape
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShapesTilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.Vertex
import com.manuel.pagliai.tsol.domain.tiling.unionOfConvexPolygons
import javax.inject.Inject

class LShapeCellType @Inject constructor(
    override val gridFactory: LShapeGridFactory,
    override val tilingOnCanvasFactory: LShapeTilingOnCanvasFactory
) : CellType {

    override val name = R.string.l_cell_type
    override val icon = R.drawable.ic_l_shape_cell_type
    override val id = "L_SHAPE"
    override val neighboursCount = 6
    override val defaultGameRules = B3S235

    override fun setNeighboursCoordinates(
        grid: Grid,
        x: Int,
        y: Int,
        dstArray: IntArray,
        dstOffset: Int
    ) {
        var offset = dstOffset

        when (y % 3) {
            0 -> {
                dstArray[offset++] = grid.toLinear(x, y - 2)
                dstArray[offset++] = grid.toLinear(x, y - 1)
                dstArray[offset++] = grid.toLinear(x, y + 1)
                dstArray[offset++] = grid.toLinear(x + 1, y + 1)
                dstArray[offset++] = grid.toLinear(x + 1, y + 2)
                dstArray[offset++] = grid.toLinear(x + 1, y - 1)
            }
            1 -> {
                dstArray[offset++] = grid.toLinear(x, y + 1)
                dstArray[offset++] = grid.toLinear(x, y - 1)
                dstArray[offset++] = grid.toLinear(x, y - 2)
                dstArray[offset++] = grid.toLinear(x - 1, y - 1)
                dstArray[offset++] = grid.toLinear(x + 1, y + 1)
                dstArray[offset++] = grid.toLinear(x, y + 2)
            }
            2 -> {
                dstArray[offset++] = grid.toLinear(x - 1, y - 2)
                dstArray[offset++] = grid.toLinear(x - 1, y - 1)
                dstArray[offset++] = grid.toLinear(x - 1, y + 1)
                dstArray[offset++] = grid.toLinear(x, y + 1)
                dstArray[offset++] = grid.toLinear(x, y + 2)
                dstArray[offset++] = grid.toLinear(x, y - 1)
            }
        }

        assert(offset == neighboursCount + dstOffset)
    }
}

class LShapeGridFactory @Inject constructor() : GridFactory {
    override fun createGrid(
        canvasWidth: Float,
        canvasHeight: Float,
        displayDensity: Float,
        cellSize: Int
    ) = stdGridFactory(
        canvasWidth = canvasWidth,
        canvasHeight = canvasHeight,
        displayDensity = displayDensity,
        cellSize = cellSize,
        rowGroupCount = 3,
        correctionFactorX = 0.5f,
        correctionFactorY = 1.5f
    )
}


class LShapeTilingOnCanvasFactory @Inject constructor() :
    TilingOnCanvas.Factory {
    override fun create(canvasWidth: Float, canvasHeight: Float, grid: Grid): TilingOnCanvas {
        return PeriodicShapesTilingOnCanvas.create(
            grid,
            canvasWidth = canvasWidth,
            canvasHeight = canvasHeight,
            shapeBuilder = LShapePeriodicShapeBuilder(grid, canvasWidth, canvasHeight)
        )
    }

    private class LShapePeriodicShapeBuilder(grid: Grid, canvasWidth: Float, canvasHeight: Float) :
        PeriodicShapesTilingOnCanvas.PeriodicShapeBuilder {

        private val a = canvasWidth / (3 * grid.columns)
        private val b = canvasHeight / grid.rows

        override fun createShape(x: Int, y: Int): PeriodicShape {

            // shape is not convex, and represented as the union of two rectangles.
            // check the assets folder for more info.

            val p0X = x * 3 * a - (y % 3) * a
            val p0Y = y * b

            val p0 = Vertex(p0X, p0Y)
            val p1 = p0.xShift(a)
            val p2 = p1.yShift(b).roundY()
            val p3 = p2.xShift(a)
            val p4 = p3.yShift(b)
            val p5 = Vertex(p0.x, p4.y)
            val p6 = Vertex(p0.x, p2.y)

            return unionOfConvexPolygons {
                polygon(p0) {
                    to(p1, true)
                    to(p2, true)
                    to(p6, false)
                    close(true)
                }

                polygon(p6) {
                    to(p2, false)
                    to(p3, true)
                    to(p4, true)
                    to(p5, true)
                    close(true)
                }
            }
        }
    }
}