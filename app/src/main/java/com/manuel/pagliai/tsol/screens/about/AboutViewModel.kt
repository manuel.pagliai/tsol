package com.manuel.pagliai.tsol.screens.about

import android.app.Application
import android.content.res.Resources


import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.manuel.pagliai.tsol.BuildConfig
import com.manuel.pagliai.tsol.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject


sealed class AboutScreenUIEvent {
    data class SendFeedback(val to: String, val subject: String) : AboutScreenUIEvent()
}

/**
 * A link to be displayed in the 'links' section
 */
data class Link(
    val label: Int,
    val url: Int
)

@HiltViewModel
class AboutViewModel @Inject constructor(
    application: Application,
) : AndroidViewModel(application) {

    /**
     * Emit ui events to send feedback or submit error reports...
     */
    private val _eventFlow = MutableSharedFlow<AboutScreenUIEvent>()
    val eventFlow: SharedFlow<AboutScreenUIEvent>
        get() = _eventFlow

    private val resources: Resources
        get() = getApplication<Application>().resources

    /**
     * A collection of useful links to be shown in the ui
     */
    val links: List<Link>
        get() = listOf(
            Link(label = R.string.source_code, url = R.string.source_code_url),
            Link(label = R.string.wikipedia, url = R.string.wikipedia_url),
        )

    val appVersion: String
        get() = BuildConfig.VERSION_NAME


    /**
     * Generates a new event that should be handle as to ask the user to his/her feedback
     * with the developer (via email)
     */
    fun sendFeedback() {
        viewModelScope.launch {
            _eventFlow.emit(
                AboutScreenUIEvent.SendFeedback(
                    to = resources.getString(R.string.support_email),
                    subject = resources.getString(R.string.feedback_email_subject)
                )
            )
        }
    }
}