package com.manuel.pagliai.tsol.domain

/**
 * A cell type identifies the geometry of the tesselation, as well as properties
 * of the game such as how to evaluate the size of the automaton, the number of neighbours of each cell
 * or the default game rules.
 *
 * The conventional game of life uses square cell types.
 * In The shape of life we do explore other variations of the game using non square cell types.
 *
 * Check the assets folder for documentation of each cell type.
 */
interface CellType {

    /**
     * True if hidden from UI (e.g., used only when debugging/testing)
     */
    val hidden: Boolean
        get() = false

    /**
     * Resource if of the name of the cell geometry for display purposes.
     */
    val name: Int

    /**
     * Resource id of the icon representing the cell
     */
    val icon: Int

    /**
     * Internal name of the cell geometry. Untranslated.
     * Must be unique for each implementation and it can be used to identify the cell.
     */
    val id: String

    /**
     * Identifies the number of neighbours of each cell.
     *
     * For example 8, in the classic square tiling, or
     * 6 in an hexagonal tiling.
     *
     * This must be the same for every cell.
     */
    val neighboursCount: Int

    /**
     * Game rules used in a new game with this geometry.
     */
    val defaultGameRules: GameRules

    /**
     * Algorithm used to generate a [Grid] for the [CellType] based on the canvas properties and user preferences.
     * Every time (i) the user changes cell size or cell type, or (ii) the canvas properties change (e.g., after the user has rotated the device)
     * A new [Grid] to be used for the automaton must be evaluated.
     */
    val gridFactory: GridFactory

    /**
     * Used to generate tiling on canvas ultimately used to render and interact with the automaton on canvas.
     */
    val tilingOnCanvasFactory: TilingOnCanvas.Factory

    /**
     * Sets the coordinates of the neighbours of cell in position
     *  @param [x], @param [y]
     *
     *  Coordinates should be in linear form (@see [Grid.toLinear]) and set
     *  in @param dstArray starting from position [dstOffset]. [dstOffset] is the index of the first position
     *  in the array to be written.
     *
     *  Each implementation is responsible to set exactly @see [neighboursCount] coordinates in the array
     *  (that is each implementation must set the cells @param dstOffset (included) to @param dstOffset + [neighboursCount] (excluded)
     *
     *  This code can be quite 'hot' so make sure implementations are efficient
     */
    fun setNeighboursCoordinates(
        grid: Grid,
        x: Int,
        y: Int,
        dstArray: IntArray,
        dstOffset: Int
    )
}