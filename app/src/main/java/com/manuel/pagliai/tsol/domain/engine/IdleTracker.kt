package com.manuel.pagliai.tsol.domain.engine

import android.os.CountDownTimer

/**
 * Simple class responsible to call a configurable lambda
 *      if it has not been reset in [idleAfterMs] milliseconds.
 *
 * Used to trigger events when the user has not done any
 * activity in a while, and the system is in an 'idle' state
 */
class IdleTracker(
    private val idleAfterMs: Long
) {

    /**
     * The action to trigger when the system reaches the idle state
     */
    private var action: (() -> Unit)? = null

    private val countDownTimer = object : CountDownTimer(idleAfterMs, idleAfterMs) {
        override fun onTick(millisUntilFinished: Long) {}
        override fun onFinish() {
            action?.invoke()
        }
    }

    fun onTimeout(action: () -> Unit) {
        this.action = action
        countDownTimer.start()
    }

    fun reset() {
        countDownTimer.cancel()
        countDownTimer.start()
    }

}
