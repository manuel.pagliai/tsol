package com.manuel.pagliai.tsol.screens.game

import android.annotation.SuppressLint
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Slider
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.TextButton
import androidx.compose.material3.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.Screens
import com.manuel.pagliai.tsol.TestTags
import com.manuel.pagliai.tsol.domain.engine.ScreenOrientation
import com.manuel.pagliai.tsol.screens.common.ThreeDotsMenu
import com.manuel.pagliai.tsol.screens.common.ThreeDotsMenuItem
import com.manuel.pagliai.tsol.screens.common.TopAppBarBackButton
import kotlinx.coroutines.flow.collectLatest
import kotlin.math.roundToInt
import androidx.compose.material3.Text as Text1


/**
 * Screen displaying the automaton and the controls to interact with it.
 */
@OptIn(ExperimentalMaterial3Api::class)
@ExperimentalAnimationApi
@Composable
fun GameScreen(
    navController: NavController,
    viewModel: GameViewModel = hiltViewModel()
) {

    val gameState = viewModel.gameState.value
    var changeSpeedDialog by remember { mutableStateOf(false) }
    var changeCellSizeDialog by remember { mutableStateOf(false) }
    var changeCellTypeDialog by remember { mutableStateOf(false) }
    var makeCopyDialog by remember { mutableStateOf(false) }
    var renameDialog by remember { mutableStateOf(false) }
    val resources = LocalContext.current.resources
    val snackbarHostState = remember { SnackbarHostState() }
    var promptFirstGameName: String? by remember { mutableStateOf(null) }

    LaunchedEffect(key1 = Unit) {
        viewModel.gameScreenLaunched()
        viewModel.uiEventFlow.collectLatest { uiEvent ->
            when (uiEvent) {
                is GameScreenUIEvent.CopyCreated -> {
                    snackbarHostState.showSnackbar(
                        message = resources.getString(
                            R.string.copy_created_snackbar,
                            uiEvent.newName
                        )
                    )
                    Unit
                }
                is GameScreenUIEvent.GameRenamed -> {
                    snackbarHostState.showSnackbar(
                        message = resources.getString(
                            R.string.game_renamed_snackbar,
                            uiEvent.newName
                        )
                    )
                    Unit
                }
                is GameScreenUIEvent.PromptFirstGameName -> {
                    promptFirstGameName = uiEvent.currentGameName
                }
            }.javaClass // force exhaustive when statement. This ugly thing will no longer be needed in kotlin >= 1.7

        }

    }

    Scaffold(
        snackbarHost = { SnackbarHost(snackbarHostState) },
        topBar = {
            GameScreenTopBar(
                gameName = gameState?.gameName ?: "",
                generation = gameState?.generation ?: 0,
                looping = gameState?.isLooping == true,
                onChangeSpeed = { changeSpeedDialog = true },
                onChangeCellSize = { changeCellSizeDialog = true },
                onCreateCopy = { makeCopyDialog = true },
                onRename = { renameDialog = true },
                onBackPressed = { navController.navigateUp() },
                onSettings = { navController.navigate(Screens.Settings.route) }
            )
        },
        bottomBar = {
            GameScreenBottomBar(
                viewModel,
                gameState?.controls ?: GameControls.DEFAULT,
                onChangeCellType = { changeCellTypeDialog = true })
        }
    ) { innerPadding ->
        Box(modifier = Modifier.padding(innerPadding)) {
            GameScreenContent(viewModel, gameState)
            if (changeSpeedDialog) {
                gameState?.let {
                    IntSliderDialog(
                        title = stringResource(id = R.string.change_speed_dialog_title),
                        currentValue = it.speed,
                        minValue = GameState.MIN_SPEED,
                        maxValue = GameState.MAX_SPEED,
                        testTag = TestTags.CHANGE_SPEED_SCROLLBAR,
                        onDismissRequest = { changeSpeedDialog = false },
                        onNewValue = { newSpeed -> viewModel.changeSpeed(newSpeed) }
                    )
                }
            }
            if (changeCellSizeDialog) {
                gameState?.let {
                    IntSliderDialog(
                        title = stringResource(id = R.string.change_cell_size_dialog_title),
                        currentValue = it.cellSize,
                        minValue = GameState.MIN_CELL_SIZE,
                        maxValue = GameState.MAX_CELL_SIZE,
                        testTag = TestTags.CHANGE_CELL_SIZE_SCROLLBAR,
                        onDismissRequest = { changeCellSizeDialog = false },
                        onNewValue = { newCellSize -> viewModel.changeCellSize(newCellSize) }
                    )
                }
            }

            if (makeCopyDialog) {
                gameState?.let {
                    TextInputDialog(
                        onDismissRequest = { makeCopyDialog = false },
                        title = stringResource(id = R.string.create_copy_dialog_title),
                        placeHolderText = null,
                        affirmativeButtonLabel = stringResource(id = R.string.create_game_copy_label),
                        cancelButtonLabel = stringResource(id = R.string.cancel_label),
                        textFieldTag = TestTags.CREATE_GAME_COPY_TEXT_FIELD
                    ) { newName ->
                        makeCopyDialog = false
                        viewModel.makeCopy(newName)
                    }
                }
            }

            if (renameDialog) {
                gameState?.let {
                    TextInputDialog(
                        onDismissRequest = { renameDialog = false },
                        title = stringResource(id = R.string.rename_dialog_title),
                        placeHolderText = null,
                        affirmativeButtonLabel = stringResource(id = R.string.rename_game_label),
                        cancelButtonLabel = stringResource(id = R.string.cancel_label),
                        textFieldTag = TestTags.RENAME_GAME_TEXT_FIELD
                    ) { newName ->
                        renameDialog = false
                        viewModel.rename(newName, firstGameName = false)
                    }
                }
            }

            if (promptFirstGameName != null) {
                gameState?.let {
                    TextInputDialog(
                        onDismissRequest = { promptFirstGameName = null },
                        title = stringResource(id = R.string.game_name_title),
                        placeHolderText = stringResource(id = R.string.untitled_game),
                        affirmativeButtonLabel = stringResource(id = R.string.ok),
                        cancelButtonLabel = stringResource(id = R.string.cancel_label),
                        textFieldTag = TestTags.FIRST_GAME_NAME_TEXT_FIELD
                    ) { newName ->
                        promptFirstGameName = null
                        viewModel.rename(newName, firstGameName = true)
                    }
                }
            }

            if (changeCellTypeDialog) {
                gameState?.let { game ->
                    ChangeCellTypeDialog(
                        onDismissRequest = { changeCellTypeDialog = false },
                        onNewCellType = { newCellType, newRules ->
                            changeCellTypeDialog = false
                            viewModel.updateCells(newCellType = newCellType, newRules = newRules)
                        },
                        viewModel.probabilityValues,
                        cellTypes = viewModel.allCellTypes,
                        currentCellType = game.cellType,
                        currentGameRules = game.rules
                    )
                }
            }
        }
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun TextInputDialog(
    focusRequester: FocusRequester = remember { FocusRequester() },
    onDismissRequest: () -> Unit,
    title: String,
    placeHolderText: String?,
    affirmativeButtonLabel: String,
    cancelButtonLabel: String,
    textFieldTag: String,
    onValueConfirmed: (String) -> Unit
) {
    var enteredValue by remember { mutableStateOf("") }

    Dialog(onDismissRequest = onDismissRequest) {
        Column(
            modifier = Modifier
                .background(MaterialTheme.colorScheme.surface)
                .padding(16.dp)
        ) {
            Text1(
                title,
                style = MaterialTheme.typography.titleMedium
            )
            Spacer(modifier = Modifier.height(8.dp))
            val keyboardController = LocalSoftwareKeyboardController.current
            TextField(
                value = enteredValue,
                singleLine = true,
                onValueChange = {
                    enteredValue = it
                },
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                keyboardActions = KeyboardActions(onDone = { keyboardController?.hide() }),
                modifier = Modifier
                    .focusRequester(focusRequester)
                    .testTag(textFieldTag),
                placeholder = placeHolderText?.let { { Text1(text = placeHolderText) } }
            )
            Spacer(modifier = Modifier.height(8.dp))
            Box(
                Modifier
                    .padding(horizontal = 8.dp, vertical = 2.dp)
                    .align(Alignment.End)
            ) {
                Row {
                    TextButton(onClick = onDismissRequest) {
                        Text1(text = cancelButtonLabel)
                    }

                    Spacer(modifier = Modifier.width(2.dp))

                    TextButton(
                        onClick = { onValueConfirmed(enteredValue) },
                        enabled = enteredValue.isNotBlank()
                    ) {
                        Text1(text = affirmativeButtonLabel)
                    }
                }

            }

        }

        DisposableEffect(Unit) {
            focusRequester.requestFocus()
            onDispose { }
        }
    }

}

@SuppressLint("UnusedBoxWithConstraintsScope")
@Composable
fun GameScreenContent(
    viewModel: GameViewModel,
    gameState: GameState?
) {
    val screenOrientation =
        ScreenOrientation.fromConfigurationOrientation(LocalConfiguration.current.orientation)
    var size by remember { mutableStateOf(IntSize.Zero) }
    var lastMeasuredCanvasSize: IntSize by remember { mutableStateOf(IntSize(0, 0)) }
    BoxWithConstraints(
        Modifier
            .fillMaxSize()
            .onGloballyPositioned { layoutCoordinates ->
                size = layoutCoordinates.size
                if (lastMeasuredCanvasSize != size) {
                    viewModel.onCanvasMeasured(
                        size.width.toFloat(),
                        size.height.toFloat(),
                        displayDensity = 1.dp.value,
                        orientation = screenOrientation
                    )
                    lastMeasuredCanvasSize = size
                }
            }) {
        if (size.height > 0 && size.width > 0) {
            GameComponent(
                gameState,
                size,
                onCellSelected = { coordinates -> viewModel.onCellSelected(coordinates) },
                onAutomatonRenderCompleted = { viewModel.onAutomatonRenderCompleted() }
            )
        }
    }
}

@Composable
fun IntSliderDialog(
    title: String,
    currentValue: Int,
    minValue: Int,
    maxValue: Int,
    testTag: String,
    onDismissRequest: () -> Unit,
    onNewValue: (Int) -> Unit
) {
    var value by remember {
        mutableStateOf(currentValue.toFloat())
    }

    Dialog(onDismissRequest = onDismissRequest) {
        Column(
            modifier = Modifier
                .background(color = MaterialTheme.colorScheme.background)
                .padding(16.dp)
        ) {
            Text1(text = title, style = MaterialTheme.typography.headlineMedium)
            Row {
                Slider(
                    value = value,
                    valueRange = minValue.toFloat()..maxValue.toFloat(),
                    onValueChange = { value = it },
                    onValueChangeFinished = { onNewValue(value.roundToInt()) },
                    modifier = Modifier.testTag(testTag)
                )
            }
        }
    }

}

@Composable
fun GameScreenTopBar(
    gameName: String,
    generation: Int,
    looping: Boolean,
    onChangeSpeed: (() -> Unit),
    onChangeCellSize: (() -> Unit),
    onCreateCopy: (() -> Unit),
    onRename: (() -> Unit),
    onSettings: (() -> Unit),
    onBackPressed: (() -> Unit)
) {
    TopAppBar(
        title = {
            Text1(
                text = gameName,
                style = MaterialTheme.typography.titleLarge,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier.clickable { onRename() })
        },
        backgroundColor = MaterialTheme.colorScheme.background,
        navigationIcon = {
            TopAppBarBackButton(onBackPressed)
        },
        actions = {
            Text1(
                text = generation.toString(),
                style = MaterialTheme.typography.titleMedium,
                modifier = Modifier.testTag(TestTags.AUTOMATON_GENERATION_LABEL)
            )
            Spacer(modifier = Modifier.width(8.dp))

            if (looping) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_loop_black_24dp),
                    contentDescription = stringResource(
                        id = R.string.looping_cd
                    )
                )
                Spacer(modifier = Modifier.width(8.dp))
            }

            ThreeDotsMenu(
                items = listOf(
                    ThreeDotsMenuItem.ClickableLabel(
                        label = stringResource(id = R.string.open_change_speed_dialog),
                        onChangeSpeed
                    ),
                    ThreeDotsMenuItem.ClickableLabel(
                        label = stringResource(id = R.string.open_change_cell_size_dialog),
                        onChangeCellSize
                    ),
                    ThreeDotsMenuItem.ClickableLabel(
                        label = stringResource(id = R.string.open_create_copy_dialog),
                        onCreateCopy
                    ),
                    ThreeDotsMenuItem.ClickableLabel(
                        label = stringResource(id = R.string.open_rename_dialog),
                        onRename
                    ),
                    ThreeDotsMenuItem.Divider,
                    ThreeDotsMenuItem.ClickableLabel(
                        label = stringResource(id = R.string.open_settings),
                        onSettings
                    )
                )
            )
        }
        //modifier = Modifier.height(64.dp).align
    )
}

@Composable
fun GameScreenBottomBar(
    viewModel: GameViewModel,
    controls: GameControls,
    onChangeCellType: () -> Unit
) {
    BottomAppBar(modifier = Modifier.height(64.dp)) {
        // add/remove cells on selection
        if (controls.addCellsOnSelection) {
            IconButton(
                onClick = { viewModel.setRemoveCellsOnSelection() }) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_baseline_create_24px),
                    contentDescription = stringResource(
                        R.string.add_cells_cd
                    )
                )
            }
        } else {
            IconButton(
                onClick = { viewModel.setAddCellsOnSelection() }) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_eraser),
                    contentDescription = stringResource(
                        R.string.remove_cells_cd
                    ),
                    tint = colorResource(
                        id = R.color.delete
                    )
                )
            }

        }

        IconButton(
            enabled = controls.canUndo,
            onClick = { viewModel.undo() }) {
            Icon(
                painter = painterResource(id = R.drawable.ic_undo_24px),
                contentDescription = stringResource(
                    R.string.undo_cd
                )
            )
        }

        IconButton(
            enabled = controls.canRedo,
            onClick = { viewModel.redo() }) {
            Icon(
                painter = painterResource(id = R.drawable.ic_redo_24px),
                contentDescription = stringResource(
                    R.string.redo_cd
                )
            )
        }


        // play/pause
        if (controls.isPlaying) {
            IconButton(onClick = { viewModel.pause() }) {
                Icon(
                    painterResource(R.drawable.ic_pause_24px),
                    contentDescription = stringResource(
                        R.string.pause_cd
                    )
                )
            }
        } else {
            IconButton(onClick = { viewModel.play() }) {
                Icon(
                    imageVector = Icons.Default.PlayArrow,
                    contentDescription = stringResource(R.string.play_cd)
                )
            }
        }

        IconButton(
            enabled = controls.canNextGen,
            onClick = { viewModel.nextGen() }) {
            Icon(
                painter = painterResource(id = R.drawable.ic_next_gen),
                contentDescription = stringResource(
                    R.string.next_generation_cd
                )
            )
        }

        IconButton(
            enabled = controls.canPrevGen,
            onClick = { viewModel.prevGen() }) {
            Icon(
                painter = painterResource(id = R.drawable.ic_previous_gen),
                contentDescription = stringResource(
                    R.string.previous_generation_cd
                )
            )
        }

        IconButton(
            enabled = controls.canChangeCellType,
            onClick = onChangeCellType
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_change_cell_type),
                contentDescription = stringResource(
                    R.string.change_cell_type_cd
                )
            )
        }

        // clear/restore
        if (controls.canClear) {
            IconButton(
                onClick = { viewModel.clearCells() }) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_baseline_clear_24px),
                    contentDescription = stringResource(
                        R.string.clear_cd
                    )
                )
            }
        } else {
            IconButton(
                onClick = { viewModel.restore() }) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_restart_alt_black_24dp),
                    contentDescription = stringResource(
                        R.string.restore_cd
                    )
                )
            }
        }
    }
}


