package com.manuel.pagliai.tsol.domain.tiling


import android.graphics.Path
import androidx.annotation.VisibleForTesting


import kotlin.math.max
import kotlin.math.min

/**
 * The portion of a line between two points
 */
data class Side(val from: Vertex, val to: Vertex) {
    init {
        assert(from != to)
    }
}

/**
 *  A polygonal chain is a connected series of line segments.
 *  We do enrich it with a visibility flag representing whether or not the chain should be drawn on screen.
 *
 *  A chain is not necessarily close, and it does not necessarily identify a polygon. Rather a [ConvexPolygon] is made from one or more [PolygonalChain]
 *
 *  Invisible chains are useful as a way to
 *  - model a non convex polygon as a list of convex polygons: some of the internal chains should not be drawn:
 *      for example the arrow cell (=>) is drawn as the union of a rectangle (=) and a triangle (>)
 *      portions of the perimeter of the two shapes should not be rendered
 *  - when a shape encounters a periodic boundary and split in two portion of the outline should not be drawn as
 *    it is an artifact of the periodic boundary rather than part of the shape.
 */
data class PolygonalChain(val vertices: List<Vertex>, val visible: Boolean) {

    fun outsideXRange(minX: Float, maxX: Float) =
        vertices.any { it.outsideXRange(minX, maxX) }

    fun outsideYRange(minY: Float, maxY: Float) =
        vertices.any { it.outsideYRange(minY, maxY) }

    /**
     * Creates a new chain where x and y coordinates have been swapped.
     */
    fun swapXY() = copy(vertices = this.vertices.map { it.swapXY() }.toList())

    operator fun get(position: Int) = vertices[position]

    inline fun forEach(action: (Vertex) -> Unit) = vertices.forEach(action)

    val empty: Boolean
        get() = vertices.size < 2

    val last: Vertex
        get() = vertices.last()

    val first: Vertex
        get() = vertices.first()

    /**
     * An Android's [Path] identified by this chain.
     */
    val path: Path
        get() = Path().apply {
            val vertex = vertices.first()
            moveTo(vertex.x.toInt().toFloat(), vertex.y.toInt().toFloat())
            (1 until vertices.size).forEach {
                val vertex1 = vertices[it]
                lineTo(vertex1.x.toInt().toFloat(), vertex1.y.toInt().toFloat())
            }
        }

    val lines: FloatArray
        get() {
            val lines = FloatArray(4 * vertices.size - 2)

            vertices.forEachIndexed { i, v ->
                if (i == 0) {
                    lines[0] = v.x
                    lines[1] = v.y
                } else {
                    lines[4 * i - 2] = v.x
                    lines[4 * i - 1] = v.y
                    lines[4 * i] = v.x
                    lines[4 * i + 1] = v.y

                }
            }
            return lines
        }

    /**
     * The chain as list of [Side]s.
     */
    val sides: List<Side>
        get() = vertices.zipWithNext().map {
            Side(
                it.first,
                it.second
            )
        }.toList()
}

/**
 * A convex polygon represented as a list of [PolygonalChain]s
 *
 * Each chain defines part of the polygon border.
 *
 * The polygon is assumed to be convex.
 */
data class ConvexPolygon(val chains: List<PolygonalChain>) {

    /**
     * All the vertices associated to the polygon
     */
    @VisibleForTesting
    val vertices: List<Vertex>

    private val first: Vertex
        get() = vertices.first()

    private val last: Vertex
        get() = vertices.last()

    /**
     * Portion of the polygon border that should be drawn
     */
    private val visibleBorderLines: Array<FloatArray>

    init {
        assert(chains.all { !it.empty })
        assert(chains.indices.all { i -> chains[i].last == chains[(i + 1) % chains.size].first })

        val vertices = mutableListOf<Vertex>()
        chains.forEach { chain ->
            chain.forEach { vertex ->
                if (vertices.isEmpty() ||
                    (vertices.last() != vertex && vertices.first() != vertex)
                ) {
                    vertices.add(vertex)
                }
            }
        }

        assert(vertices.size == vertices.toSet().size)
        assert(vertices.size > 2)
        this.vertices = vertices

        visibleBorderLines = chains.filter { it.visible }.map { it.lines }.toTypedArray()
    }

    /**
     * The path as in the shape of the polygon itself. When drawing the polygon this represents
     * the area within the polygon.
     */
    private val shapePath by lazy(LazyThreadSafetyMode.NONE) {
        Path().apply {
            moveTo(first.x, first.y)
            (1 until vertices.size).map {
                val vertex = vertices[it]
                lineTo(vertex.x, vertex.y)
            }
            close()
        }
    }

    val sides: List<Side>
        get() = vertices.zipWithNext().map {
            Side(
                it.first,
                it.second
            )
        }.toMutableList().apply {
            add(
                Side(
                    this@ConvexPolygon.last,
                    this@ConvexPolygon.first
                )
            )
        }

    /**
     * True if the polygon is not contained between [minY] and [maxY]
     */
    fun outOfBoundariesY(minY: Float, maxY: Float) = chains.any { it.outsideYRange(minY, maxY) }

    /**
     * True if the polygon is not contained between [minX] and [maxX]
     */
    fun outOfBoundariesX(minX: Float, maxX: Float) = chains.any { it.outsideXRange(minX, maxX) }

    /**
     * Creates a new polygon where x and y coordinates have been swapped on all polygons.
     */
    fun swapXY() =
        ConvexPolygon(chains.map { it.swapXY() }
            .toList())

    /**
     * True if the point [xq], [yq] is contained in the polygon.
     */
    fun contains(xq: Float, yq: Float): Boolean {
        /*
         * Implemented via the ray casting algorithm
         */
        var hitCount = 0

        for (i in vertices.indices) {

            var v0 = vertices[i]
            if (v0.x == xq && v0.y == yq) return true
            var v1 = vertices[(i + 1) % vertices.size]

            if (v0 == v1)
                continue

            // v0 -> v1 is an edge

            val minX = min(v0.x, v1.x)
            val maxX = max(v0.x, v1.x)

            val minY = min(v0.y, v1.y)
            val maxY = max(v0.y, v1.y)

            if (yq < minY || yq > maxY) {
                continue
            }

            // yq overlap for the segment
            if (xq < minX) {
                hitCount++ // xq is on the left of the segment. surely it will intersect as yq overlaps the segment
            } else {

                // edge cases

                // vertical line
                if (v0.x == v1.x) {
                    // vertical line, y is bound between minY and maxY
                    if (xq < minX) {
                        hitCount++
                        continue
                    } else if (xq == minX) {
                        return true
                    }
                }

                // horizontal line
                if (v0.y == v1.y) {
                    if (xq in minX..maxX)
                        return true

                    return false
                }

                // order v1 and v0 wrt x
                if (v0.x > v1.x) {
                    val tmp = v0
                    v0 = v1
                    v1 = tmp
                }

                // we need to check if the vertex (xq, yq) is on the left of the segment S from (v1.x, v1.y)
                //    to (v2.x, v2.y).
                // the segment can be parameterised as v1 + (v2 -v1)t with t in [0,1]
                //  we can do that by comparing xq with the xIntersect defined as where the line y = yq
                //  intersects the segment S
                // by solving the y equation:
                // v0.y + (v1.y - v0.y) * t = yq
                // t = (yq - v0.y)/(v1.y - v0.y)
                // now xIntersect is
                val xIntersect = v0.x + (v1.x - v0.x) * ((yq - v0.y) / (v1.y - v0.y))

                // finally

                if (xIntersect == xq) // (on the line itself)
                    return true

                if (xIntersect > xq) // on the left
                    hitCount++
            }
        }

        return hitCount == 1
    }

    fun onBorderLines(onLines: (FloatArray) -> Unit) {
        visibleBorderLines.forEach(onLines)
    }

    fun onCellPaths(onPath: (Path) -> Unit) {
        onPath(shapePath)
    }

    companion object {

        /**
         * Creates a new polygon as a single chain out of [vertices]
         */
        fun fromSingleChain(
            vertices: List<Vertex>,
            autoClose: Boolean = true,
            visible: Boolean = true
        ): ConvexPolygon {
            if (!autoClose || vertices.first() == vertices.last())
                return ConvexPolygon(
                    listOf(
                        PolygonalChain(
                            vertices,
                            visible
                        )
                    )
                )
            val closed = mutableListOf<Vertex>().apply {
                addAll(vertices)
                add(vertices.first())
            }
            return ConvexPolygon(
                listOf(
                    PolygonalChain(
                        closed,
                        visible
                    )
                )
            )
        }
    }
}