package com.manuel.pagliai.tsol.screens.game

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LifecycleOwner
import com.manuel.pagliai.tsol.R

@Composable
fun GameComponent(
    gameState: GameState?,
    size: IntSize,
    onCellSelected: (Int) -> Unit,
    onAutomatonRenderCompleted: () -> Unit,
    lifecycleOwner: LifecycleOwner = LocalLifecycleOwner.current,
) {

    val automatonRenderer = remember {
        AutomatonRenderer(
            width = size.width,
            height = size.height,
            gridStrokeWidth = 3.dp.value
        )
    }
    DisposableEffect(Unit) {
        onDispose { automatonRenderer.recycle() }
    }

    val automatonRendererReady = remember { automatonRenderer.ready }
    if (gameState == null) {
        Loading(modifier = Modifier.fillMaxSize())
    } else {
        automatonRenderer.prepareRendererAsync(
            rememberCoroutineScope(),
            gameState.cellType,
            gameState.currentGenCells.grid
        )
        if (automatonRendererReady.value) {
            val (cellsColor, gridColor, backgroundColor) = gameState.automatonColors.valuesOr(
                MaterialTheme.colorScheme
            )
            val rippleColor =
                if (gameState.controls.addCellsOnSelection) cellsColor else colorResource(
                    id = R.color.delete
                )
            val newBornCellColor = if (gameState.automatonColors.usingSystemThemeForCellsColor()) {
                MaterialTheme.colorScheme.secondary
            } else {
                colorResource(id = R.color.non_theme_new_born_cells_color)
            }
            AutomatonComponent(
                size,
                currentGenCells = gameState.currentGenCells,
                nextGenCells = gameState.nextGenCells,
                cellType = gameState.cellType,
                onCellSelected = onCellSelected,
                renderer = automatonRenderer,
                showNextGenCellsAnimation = gameState.showNextGenPreview,
                onAutomatonRenderCompleted = onAutomatonRenderCompleted,
                rippleColor = rippleColor,
                cellsColor = cellsColor,
                gridColor = gridColor,
                backgroundColor = backgroundColor,
                newBornCellColor = newBornCellColor
            )
        } else {
            Loading(modifier = Modifier.fillMaxSize())
        }
    }

    DisposableEffect(lifecycleOwner) {
        onDispose { automatonRenderer.recycle() }
    }
}

@Composable
fun Loading(modifier: Modifier = Modifier) {
    Image(
        painter = painterResource(id = R.drawable.ic_loading_icon_a),
        contentDescription = stringResource(
            id = R.string.loading_cd
        ),
        modifier = modifier
    )
}
