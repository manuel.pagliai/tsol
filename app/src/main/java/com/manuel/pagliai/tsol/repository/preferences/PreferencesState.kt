package com.manuel.pagliai.tsol.repository.preferences

import androidx.compose.ui.graphics.Color

/**
 * Represents the preferences as saved on disk.
 *
 * @see [com.manuel.pagliai.tsol.domain.GameSettings] for the effective settings that should be used in the game.
 */
data class PreferencesState(

    /**
     * If true, when the user hasn't done anything in a while an animation will be displayed
     * showing the transition from the current automaton generation to the next generation
     */
    val showNextGenAnimationIdle: Boolean,

    /**
     * If true the automaton will be rendered using the system theme colours.
     * If false, [cellsColor], [gridColor] and [backgroundColor] will be used instead.
     */
    val useSystemColors: Boolean,

    /**
     * Color of the (alive) cells in the rendered automata as configured by the user.
     * Ignored if [useSystemColors] is set to true
     */
    val cellsColor: Color,

    /**
     * Color of the grid (i.e., the cell border) in the rendered automata as configured by the user.
     * Ignored if [useSystemColors] is set to true
     */
    val gridColor: Color,

    /**
     * Color of the background (i.e., the cell that are not alive) in the rendered automata as configured by the user.
     * Ignored if [useSystemColors] is set to true
     */
    val backgroundColor: Color,

    /**
     * Game speed (when the automaton is running) of a new game
     */
    val newGamesDefaultSpeed: Int,

    /**
     * Cells size of a new game. Not affected by [randomiseNewGamesCells]
     */
    val newGamesDefaultCellSize: Int,

    /**
     * If true, new games will be setup with a random cell type and cell alive values.
     * Does not affect the cell size. see [newGamesDefaultCellSize]
     */
    val randomiseNewGamesCells: Boolean
)