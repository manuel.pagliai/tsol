package com.manuel.pagliai.tsol.repository.games

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.manuel.pagliai.tsol.domain.SavedGame
import kotlinx.coroutines.flow.Flow


@Dao
interface GameDAO {

    @Query("SELECT * FROM games ORDER BY modifiedAt  DESC")
    fun getAllGames(): Flow<List<SavedGame>>

    @Query("SELECT * FROM games WHERE id = :id")
    suspend fun getGameById(id: Long): SavedGame?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveGame(game: SavedGame): Long

    @Delete
    suspend fun delete(savedGame: SavedGame)

}