package com.manuel.pagliai.tsol.domain.cells

import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.domain.CellType
import com.manuel.pagliai.tsol.domain.GameRules.Companion.B2S34
import com.manuel.pagliai.tsol.domain.Grid
import com.manuel.pagliai.tsol.domain.GridFactory
import com.manuel.pagliai.tsol.domain.GridFactory.Companion.stdGridFactory
import com.manuel.pagliai.tsol.domain.TilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShape
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShapesTilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.Vertex
import com.manuel.pagliai.tsol.domain.tiling.convexPolygonPeriodicShape
import javax.inject.Inject

class DualLavesCellType @Inject constructor(
    override val gridFactory: DualLavesGridFactory,
    override val tilingOnCanvasFactory: DualLavesTilingOnCanvasFactory
) : CellType {

    override val name = R.string.dual_laves_cell_type
    override val icon = R.drawable.ic_dual_laves_cell_type
    override val id = "DUAL_LAVES"
    override val neighboursCount = 6
    override val defaultGameRules = B2S34

    override fun setNeighboursCoordinates(
        grid: Grid,
        x: Int,
        y: Int,
        dstArray: IntArray,
        dstOffset: Int
    ) {
        var offset = dstOffset

        dstArray[offset++] = grid.toLinear(x - 1, y)
        dstArray[offset++] = grid.toLinear(x + 1, y)
        dstArray[offset++] = grid.toLinear(x, y - 1)
        dstArray[offset++] = grid.toLinear(x, y + 1)
        if (y % 2 == 0) {
            dstArray[offset++] = grid.toLinear(x + 1, y - 1)
            dstArray[offset++] = grid.toLinear(x + 1, y + 1)
        } else {
            dstArray[offset++] = grid.toLinear(x - 1, y - 1)
            dstArray[offset++] = grid.toLinear(x - 1, y + 1)
        }

        assert(offset == neighboursCount + dstOffset)
    }
}

class DualLavesGridFactory @Inject constructor() : GridFactory {
    override fun createGrid(
        canvasWidth: Float,
        canvasHeight: Float,
        displayDensity: Float,
        cellSize: Int
    ) = stdGridFactory(
        canvasWidth = canvasWidth,
        canvasHeight = canvasHeight,
        displayDensity = displayDensity,
        cellSize = cellSize,
        colGroupCount = 2,
        rowGroupCount = 2,
        correctionFactorX = 0.7f,
        correctionFactorY = 0.7f
    )

}


class DualLavesTilingOnCanvasFactory @Inject constructor() :
    TilingOnCanvas.Factory {
    override fun create(canvasWidth: Float, canvasHeight: Float, grid: Grid): TilingOnCanvas {
        return PeriodicShapesTilingOnCanvas.create(
            grid,
            canvasWidth = canvasWidth,
            canvasHeight = canvasHeight,
            shapeBuilder = DualLavesShapeBuilder(grid, canvasWidth, canvasHeight)
        )
    }

    private class DualLavesShapeBuilder(grid: Grid, canvasWidth: Float, canvasHeight: Float) :
        PeriodicShapesTilingOnCanvas.PeriodicShapeBuilder {

        private val a = canvasWidth / grid.columns.toFloat()
        private val b = canvasHeight / grid.rows.toFloat()
        private val b1: Float = b * (1 - F)
        private val b2: Float = 2 * F * b


        override fun createShape(x: Int, y: Int): PeriodicShape {
            val p0 = Vertex(x * a, 2 * (y / 2) * b)
            val p1 = p0.xShift(a)
            val p2 = p1.yShift(b1)
            val p3 = p2.add(-a / 2, b2)
            val p4 = p0.copy(y = p2.y)
            val p5 = p3.xShift(-a)
            val p6 = p5.yShift(b1)
            val p7 = p6.copy(x = p3.x)

            return if (y % 2 == 0) { // pointing down
                convexPolygonPeriodicShape(p0, p1, p2, p3, p4)
            } else { // pointing up
                convexPolygonPeriodicShape(p6, p5, p4, p3, p7)
            }
        }

        companion object {
            const val F = 0.2f // controls how "spiky" the cells are.
        }
    }
}
