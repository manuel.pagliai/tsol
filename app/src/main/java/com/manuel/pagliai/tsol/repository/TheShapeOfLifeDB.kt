package com.manuel.pagliai.tsol.repository

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.manuel.pagliai.tsol.domain.CellType
import com.manuel.pagliai.tsol.domain.Cells
import com.manuel.pagliai.tsol.domain.GameRules
import com.manuel.pagliai.tsol.domain.SavedGame
import com.manuel.pagliai.tsol.domain.cells.CellTypes
import com.manuel.pagliai.tsol.repository.games.GameDAO

/**
 * Application database.
 *
 * Used to save/update games.
 * @see [SavedGame] for the model of persisted game
 *
 * Settings/Preferences are not stored in the database.
 * @see [com.manuel.pagliai.tsol.repository.preferences.PreferencesRepository]
 */
@Database(entities = [SavedGame::class], version = 1, exportSchema = false)
@TypeConverters(value = [TheShapeOfLifeDBConverters::class, CellTypes::class])
abstract class TheShapeOfLifeDB : RoomDatabase() {

    abstract fun gameDAO(): GameDAO

    companion object {
        const val DATABASE_NAME = "TheShapeOfLifeDB"
    }

}

class TheShapeOfLifeDBConverters {

    @TypeConverter
    fun fromGameRulesToText(gameRules: GameRules) = GameRules.Encoding.encode(gameRules)

    @TypeConverter
    fun fromTextToGameRules(byteArray: ByteArray) = GameRules.Encoding.decode(byteArray)

    @TypeConverter
    fun fromCellsToBytes(cells: Cells) = Cells.Encoding.encode(cells)

    @TypeConverter
    fun fromBytesToCells(byteArray: ByteArray) = Cells.Encoding.decode(byteArray)

    @TypeConverter
    fun fromCellToId(cellType: CellType) = cellType.id

}