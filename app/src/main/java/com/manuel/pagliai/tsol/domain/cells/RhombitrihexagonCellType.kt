package com.manuel.pagliai.tsol.domain.cells

import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.domain.CellType
import com.manuel.pagliai.tsol.domain.GameRules.Companion.B345S34
import com.manuel.pagliai.tsol.domain.Grid
import com.manuel.pagliai.tsol.domain.GridFactory
import com.manuel.pagliai.tsol.domain.TilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShape
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShapesTilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.convexPolygonPeriodicShape
import javax.inject.Inject


private val RhombitrihexagonCellsGrouping = HorizontalCellsGrouping(6)

class RhombitrihexagonCellType @Inject constructor(
    override val gridFactory: RhombitrihexagonGridFactory,
    override val tilingOnCanvasFactory: RhombitrihexagonTilingOnCanvasFactory
) : CellType {

    override val name = R.string.rhombitrihexagon_cell_type
    override val icon = R.drawable.ic_rhombitrihexagon_cell_type
    override val id = "RHOMBITRIHEXAGON"
    override val neighboursCount = 9
    override val defaultGameRules = B345S34

    override fun setNeighboursCoordinates(
        grid: Grid,
        x: Int,
        y: Int,
        dstArray: IntArray,
        dstOffset: Int
    ) {


        val (groupX, groupY) = RhombitrihexagonCellsGrouping.groupXY(x, y)
        val cellGroupIndex = RhombitrihexagonCellsGrouping.cellGroupIndex(x)

        var offset = dstOffset

        for (i in 0..5) {
            if (i != cellGroupIndex) {
                dstArray[offset++] =
                    RhombitrihexagonCellsGrouping.toCellLinear(
                        grid,
                        groupX,
                        groupY,
                        cellGroupIndex = i
                    )
            }
        }

        val ccf = groupX % 2 // correction factor

        when (cellGroupIndex) {
            0 -> {
                dstArray[offset++] =
                    RhombitrihexagonCellsGrouping.toCellLinear(
                        grid, groupX - 1, groupY - ccf, 3
                    )
                dstArray[offset++] =
                    RhombitrihexagonCellsGrouping.toCellLinear(
                        grid, groupX - 1, groupY - ccf, 4
                    )
                dstArray[offset++] =
                    RhombitrihexagonCellsGrouping.toCellLinear(
                        grid, groupX - 1, groupY + 1 - ccf, 2
                    )
                dstArray[offset++] =
                    RhombitrihexagonCellsGrouping.toCellLinear(
                        grid, groupX - 1, groupY + 1 - ccf, 3
                    )
            }
            1 -> {
                dstArray[offset++] =
                    RhombitrihexagonCellsGrouping.toCellLinear(
                        grid, groupX, groupY - 1, 4
                    )
                dstArray[offset++] =
                    RhombitrihexagonCellsGrouping.toCellLinear(
                        grid, groupX, groupY - 1, 5
                    )
                dstArray[offset++] =
                    RhombitrihexagonCellsGrouping.toCellLinear(
                        grid, groupX - 1, groupY - ccf, 3
                    )
                dstArray[offset++] =
                    RhombitrihexagonCellsGrouping.toCellLinear(
                        grid, groupX - 1, groupY - ccf, 4
                    )
            }
            2 -> {
                dstArray[offset++] =
                    RhombitrihexagonCellsGrouping.toCellLinear(
                        grid, groupX, groupY - 1, 4
                    )
                dstArray[offset++] =
                    RhombitrihexagonCellsGrouping.toCellLinear(
                        grid, groupX, groupY - 1, 5
                    )
                dstArray[offset++] =
                    RhombitrihexagonCellsGrouping.toCellLinear(
                        grid, groupX + 1, groupY - ccf, 0
                    )
                dstArray[offset++] =
                    RhombitrihexagonCellsGrouping.toCellLinear(
                        grid, groupX + 1, groupY - ccf, 5
                    )
            }
            3 -> {
                dstArray[offset++] =
                    RhombitrihexagonCellsGrouping.toCellLinear(grid, groupX + 1, groupY - ccf, 0)
                dstArray[offset++] =
                    RhombitrihexagonCellsGrouping.toCellLinear(
                        grid, groupX + 1, groupY - ccf, 5
                    )
                dstArray[offset++] =
                    RhombitrihexagonCellsGrouping.toCellLinear(
                        grid, groupX + 1, groupY + 1 - ccf, 1
                    )
                dstArray[offset++] =
                    RhombitrihexagonCellsGrouping.toCellLinear(
                        grid, groupX + 1, groupY + 1 - ccf, 0
                    )
            }
            4 -> {
                dstArray[offset++] =
                    RhombitrihexagonCellsGrouping.toCellLinear(
                        grid, groupX, groupY + 1, 2
                    )
                dstArray[offset++] =
                    RhombitrihexagonCellsGrouping.toCellLinear(
                        grid, groupX, groupY + 1, 1
                    )
                dstArray[offset++] =
                    RhombitrihexagonCellsGrouping.toCellLinear(
                        grid, groupX + 1, groupY + 1 - ccf, 1
                    )
                dstArray[offset++] =
                    RhombitrihexagonCellsGrouping.toCellLinear(
                        grid, groupX + 1, groupY + 1 - ccf, 0
                    )
            }
            5 -> {
                dstArray[offset++] =
                    RhombitrihexagonCellsGrouping.toCellLinear(
                        grid, groupX, groupY + 1, 2
                    )
                dstArray[offset++] =
                    RhombitrihexagonCellsGrouping.toCellLinear(
                        grid, groupX, groupY + 1, 1
                    )
                dstArray[offset++] =
                    RhombitrihexagonCellsGrouping.toCellLinear(
                        grid, groupX - 1, groupY + 1 - ccf, 2
                    )
                dstArray[offset++] =
                    RhombitrihexagonCellsGrouping.toCellLinear(
                        grid, groupX - 1, groupY + 1 - ccf, 3
                    )
            }
        }

        assert(offset == neighboursCount + dstOffset)
    }

}

class RhombitrihexagonGridFactory @Inject constructor() : GridFactory {
    override fun createGrid(
        canvasWidth: Float,
        canvasHeight: Float,
        displayDensity: Float,
        cellSize: Int
    ) = HexagonBasedGridFactoryHelper.createGrid(
        polygonCount = 6,
        canvasWidth = canvasWidth,
        canvasHeight = canvasHeight,
        displayDensity = displayDensity,
        cellSize = cellSize,
        correctionFactorX = 0.8f,
        correctionFactorY = 0.8f,
    )
}


class RhombitrihexagonTilingOnCanvasFactory @Inject constructor() :
    TilingOnCanvas.Factory {
    override fun create(canvasWidth: Float, canvasHeight: Float, grid: Grid): TilingOnCanvas {
        return PeriodicShapesTilingOnCanvas.create(
            grid,
            canvasWidth = canvasWidth,
            canvasHeight = canvasHeight,
            shapeBuilder = RhombitrihexagonPeriodicShapeBuilder(grid, canvasWidth, canvasHeight)
        )
    }

    private class RhombitrihexagonPeriodicShapeBuilder(
        grid: Grid,
        canvasWidth: Float,
        canvasHeight: Float
    ) :
        PeriodicShapesTilingOnCanvas.PeriodicShapeBuilder {

        private val hexagonBasedPeriodicShapeBuilderHelper = HexagonBasedPeriodicShapeBuilderHelper(
            grid = grid,
            canvasWidth = canvasWidth,
            canvasHeight = canvasHeight,
            polygonCount = 6
        )

        override fun createShape(x: Int, y: Int): PeriodicShape {
            val (p0, p1, p2, p3, p4, p5, p6) = hexagonBasedPeriodicShapeBuilderHelper.vertices(x, y)

            val p7 = (p0 + p1) / 2f
            val p8 = (p1 + p2) / 2f
            val p9 = (p2 + p3) / 2f
            val p10 = (p3 + p4) / 2f
            val p11 = (p4 + p5) / 2f
            val p12 = (p5 + p0) / 2f

            return when (RhombitrihexagonCellsGrouping.cellGroupIndex(x)) {
                0 -> convexPolygonPeriodicShape(p12, p0, p7, p6)
                1 -> convexPolygonPeriodicShape(p7, p1, p8, p6)
                2 -> convexPolygonPeriodicShape(p8, p2, p9, p6)
                3 -> convexPolygonPeriodicShape(p9, p3, p10, p6)
                4 -> convexPolygonPeriodicShape(p10, p4, p11, p6)
                5 -> convexPolygonPeriodicShape(p11, p5, p12, p6)
                else -> throw IllegalStateException()
            }
        }
    }
}
