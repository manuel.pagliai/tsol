package com.manuel.pagliai.tsol.domain


import android.graphics.Path
import timber.log.Timber

/**
 * This class is an interface to handle rendering and interaction of an automaton being rendered on a canvas.
 *
 * Implementations are dependent on the [CellType] and the number/arrangement of the cells in the automaton (i.e., the [Grid]).
 */
interface TilingOnCanvas {

    /**
     * Invokes @param [action] on all the paths associated to the border of the cell at
     * coordinates @param [coordinates]. The argument passed to the lambda has the same semantics as pts in [android.graphics.Canvas.drawLines]
     * Used to draw the grid/tiling on the canvas
     * @see [android.graphics.Canvas.drawLines]
     */
    fun onGridLines(coordinates: Int, action: (FloatArray) -> Unit)

    /**
     * Invokes @param [action] on all the paths associated to the shapes of the cell at
     * coordinates @param [coordinates].
     *
     * Used to draw alive/dead cells on the canvas
     *
     * Because of periodic boundaries a cell might be associated with multiple shapes.
     */
    fun onCellPaths(coordinates: Int, action: (Path) -> Unit)

    /**
     * @returns the coordinates of the cell covering the point @param [x] @param [y] of the canvas
     * or null if the point is not covered by any cell (e.g., because of numerical errors).
     */
    fun coordinates(x: Float, y: Float): Int?

    /**
     * Abstraction over a factory to create a new instance of [TilingOnCanvas].
     * Implementations are dependent upon the [CellType].
     *
     * This method is usually computationally expensive.
     */
    interface Factory {
        fun create(
            canvasWidth: Float,
            canvasHeight: Float,
            grid: Grid
        ): TilingOnCanvas
    }

}

/**
 * Helpful extension function
 * @see [TilingOnCanvas.Factory]
 */
fun CellType.createTilingOnCanvas(
    canvasWidth: Float, canvasHeight: Float,
    grid: Grid
): TilingOnCanvas {
    Timber.d("Creating tiling canvas adapter")
    return this.tilingOnCanvasFactory.create(canvasWidth, canvasHeight, grid)
}