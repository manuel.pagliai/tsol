package com.manuel.pagliai.tsol.domain.tiling

import androidx.annotation.VisibleForTesting
import com.manuel.pagliai.tsol.domain.tiling.indices.Segment
import com.manuel.pagliai.tsol.domain.tiling.indices.SegmentTree

private const val ENABLE_INDEX = true

/**
 * A list of non overlapping [PeriodicShape]s covering the canvas
 * Each [PeriodicShape] is associated to an index.
 * The shapes and the index associated to each shape are known at constructor time.
 *
 * Offers modelling of a tiling of the plane and an optimised way of retrieving which
 * shape is covering a canvas point.
 *
 * In practice each [PeriodicShape] corresponds to an automaton cell, and
 * its index, is the coordinates of the cell.
 *
 */
class PeriodicShapeTiling private constructor(
    /**
     * The list of [PeriodicShape]s tiling the plane.
     */
    @VisibleForTesting val shapes: Array<PeriodicShape>,

    /**
     * A [SegmentTree] built on the y-axis used to accelerate queries.
     * In practice this is used when the user touches a point (x,y) in the canvas
     * and we need to detect which cell has been touched.
     *
     * While each [PeriodicShape] is capable of detecting whether a point (x,y) falls inside the shape
     * (see [PeriodicShape.contains]), this is used to reduce the number of [PeriodicShape]s we enquiry
     * (i.e., exclude the [PeriodicShape]s that surely do not contain the coordinate (x,y)).
     *
     * Usually the device has a portrait aspect ratio, meaning that the height is larger than the width of the canvas.
     * By indexing which shapes are capable of containing the y coordinate corresponding to the y touched
     * by the user we do need to call the much slower [PeriodicShape.contains] only on those shapes
     * that have a chance to contain the query coordinates.
     *
     * It is possible to extend the Segment tree to be multi-dimensional. In practice,
     * even on slower devices, indexing one dimension seems to perform just fine, and it is quite simpler.
     *
     * associates to each interval in the tree the index of the shape in [shapes].
     *
     * @see [SegmentTree]
     */
    private val yIndex: SegmentTree<Int>
) {

    companion object {
        /**
         * Creates a new Tiling object from shapes.
         *
         * see class docs
         *
         * @param size the number of shapes for the tiling
         * @param shapesInit a function mapping each integer in [0, size) to the corresponding [PeriodicShape]
         * the shapes returned by the function will constitute the tiling.
         */
        fun create(size: Int, shapesInit: (Int) -> PeriodicShape): PeriodicShapeTiling {

            val shapes = Array(size, shapesInit)

            // non horizontal
            val segments = ArrayList<Segment<Int>>()

            // build the index (y axis only).
            shapes.forEachIndexed { shapeId, shape ->
                shape.allSides.forEach { side ->
                    val fromY = side.from.y
                    val toY = side.to.y
                    if (fromY != toY) {
                        segments.add(closedSegmentOf(fromY, toY, shapeId))
                    }
                }
            }
            val yIndex = SegmentTree.create(segments)

            return PeriodicShapeTiling(shapes, yIndex)
        }

        private fun closedSegmentOf(y0: Float, y1: Float, shapeId: Int): Segment<Int> {
            return if (y0 > y1) {
                Segment(y1, y0, infClosed = true, supClosed = true, payload = shapeId)
            } else {
                Segment(y0, y1, infClosed = true, supClosed = true, payload = shapeId)
            }
        }
    }

    val size: Int
        get() = shapes.size

    /**
     * The shape associated to [index].
     */
    operator fun get(index: Int) = shapes.let {
        if (index < 0 || index >= it.size)
            null
        else
            it[index]
    }

    /**
     * @return the index of the [PeriodicShape] covering
     * the coordinates [x], [y].
     *
     * or null if no [PeriodicShape] covers such coordinates (e.g., because of a numerical error).
     */
    fun positionOf(x: Float, y: Float): Int? {
        if (ENABLE_INDEX) {
            yIndex.onQueryResult(y) {
                if (shapes[it.payload].contains(x, y))
                    return it.payload
            }
        } else {
            for (ii in shapes.indices)
                if (shapes[ii].contains(x, y))
                    return ii
        }

        return null
    }

}