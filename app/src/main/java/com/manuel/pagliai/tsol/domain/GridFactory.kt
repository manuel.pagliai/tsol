package com.manuel.pagliai.tsol.domain

import kotlin.math.ceil
import kotlin.math.max


/**
 * Abstraction over an algorithm to create a [Grid] identifying the size of the automaton for a given
 * canvas and cell size.
 *
 * The algorithm implementation will be dependent from the cell type (
 * @see [CellType.createGrid] and [CellType.gridFactory].
 */
interface GridFactory {

    /**
     * @return the [Grid] for a given canvas configuration and cell size.
     * The implementation is dependent on the [CellType]
     */
    fun createGrid(
        canvasWidth: Float,
        canvasHeight: Float,
        displayDensity: Float,
        cellSize: Int
    ): Grid

    companion object {

        val MAX_CELL_SIZE: Int
            get() = CELL_SIZE_TO_DP.size - 1


        /**
         * Constants associated to cell sizes to adjust for the automaton size.
         *  Useful, and extracted here as they are used in several implementations
         */
        val CELL_SIZE_TO_DP = List(60) { 24 + it }

        /**
         * Offers a 'standard' algorithm to evaluate a grid. Useful to share common logic
         * and make sure the number of cells is more or less the same for each [CellType]
         *
         *
         * @param colGroupCount how many columns constitutes a 'group' (i.e., a periodic pattern).
         * the number of columns in the grid is guaranteed to be a multiple of this value.
         * @see [com.manuel.pagliai.tsol.domain.cells.HorizontalCellsGrouping]
         *
         * @param rowGroupCount same as per [colGroupCount] but for rows.
         */
        fun stdGridFactory(
            canvasWidth: Float,
            canvasHeight: Float,
            displayDensity: Float,
            cellSize: Int,
            colGroupCount: Int = 1,
            rowGroupCount: Int = 1,
            correctionFactorX: Float = 1f,
            correctionFactorY: Float = 1f,
            minColumns: Int = 4,
            minRows: Int = 4
        ): Grid {

            val u = CELL_SIZE_TO_DP[cellSize] * displayDensity

            var columns = max(
                correctionFactorX * canvasWidth / (u * colGroupCount),
                1f
            ).toInt() * colGroupCount
            var rows = max(
                correctionFactorY * canvasHeight / (u * rowGroupCount),
                1f
            ).toInt() * rowGroupCount

            if (columns < minColumns) {
                columns = ceil(minColumns / colGroupCount.toFloat()).toInt() * colGroupCount
            }

            if (rows < minRows) {
                rows = ceil(minRows / rowGroupCount.toFloat()).toInt() * rowGroupCount
            }

            return Grid(rows = rows, columns = columns)
        }
    }
}

fun CellType.createGrid(
    canvasWidth: Float,
    canvasHeight: Float,
    displayDensity: Float,
    cellSize: Int
): Grid {
    return this.gridFactory.createGrid(canvasWidth, canvasHeight, displayDensity, cellSize)
}