package com.manuel.pagliai.tsol.domain.cells


import com.manuel.pagliai.tsol.domain.Grid

/**
 * In some [CellTypes] not all cells are the same. There is however
 * a repeating pattern where after a certain number of cells the same pattern repeats, albeit shifted in space.
 *
 * For example there are 3 shapes of [RhombusCellType], and every 3 rhombus makes an hexagon.
 * The space is tiled in hexagons, each subdivided in 3 sub-shapes.
 *
 * This class represents a standard way of representing a 'grouping' of cells.
 * This is helpful whn thinking about how the plane is tiled (It's easier to think about hexagons than rhombus).
 *
 * In practice however, this grouping is entirely transparent to the user. As such it is still
 * necessary to identify individual cells for rendering or neighbours evaluation purposes.
 *
 * As such we need to convert from 'this cell in this group' to 'the cell at coordinates x,y'
 *
 * Here, each group is represented by cells on the same row, and adjacent columns.
 * Check the assets folder for helps understanding how cells are organised in space and grouped together.
 */
class HorizontalCellsGrouping(private val groupSize: Int) {

    /**
     * Return the [x,y] coordinate of the "group" shape.
     * This assumes the space to be tiled in the group shape rather than cell shape.
     *
     * Useful when drawing cells: For example, when drawing [RhombusCellType], it's easier to start
     * by understanding where the hexagon is located in space, then drawing the cell within the hexagon.
     * This will return the hexagon x,y which later can be translated in space.
     * Once the hexagon has been located on the canvas, the individual cell can be drawn.
     */
    fun groupXY(cellX: Int, cellY: Int) = Pair(
        cellX / groupSize,
        cellY
    )

    /**
     * @return the index within the group of the cell with x coordinate @param [x]
     * Because cells in a group are mapped horizontally, this is simply a modulo operation (e.g., every 3 [RhombusCellType] cells it's an hexagon)
     */
    fun cellGroupIndex(x: Int) = x % groupSize

    /**
     * Given the coordinates of the group shape and the cell index within the group
     * returns the linear coordinates of the cell.
     */
    fun toCellLinear(grid: Grid, groupX: Int, groupY: Int, cellGroupIndex: Int): Int {
        assert(cellGroupIndex >= 0)
        assert(cellGroupIndex < groupSize)
        return grid.toLinear(x = groupX * groupSize + cellGroupIndex, y = groupY)
    }
}