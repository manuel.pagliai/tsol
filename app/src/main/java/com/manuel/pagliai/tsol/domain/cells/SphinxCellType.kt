package com.manuel.pagliai.tsol.domain.cells

import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.domain.CellType
import com.manuel.pagliai.tsol.domain.GameRules.Companion.B3S23
import com.manuel.pagliai.tsol.domain.Grid
import com.manuel.pagliai.tsol.domain.GridFactory
import com.manuel.pagliai.tsol.domain.GridFactory.Companion.stdGridFactory
import com.manuel.pagliai.tsol.domain.TilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShape
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShapesTilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.Vertex
import com.manuel.pagliai.tsol.domain.tiling.unionOfConvexPolygons
import javax.inject.Inject

private val SphinxCellsGrouping = HorizontalCellsGrouping(2)

class SphinxCellType @Inject constructor(
    override val gridFactory: SphinxGridFactory,
    override val tilingOnCanvasFactory: SphinxTilingOnCanvasFactory
) : CellType {

    override val name = R.string.sphinx_cell_type
    override val icon = R.drawable.ic_sphinx_cell_type
    override val id = "SPHINX"
    override val neighboursCount = 12
    override val defaultGameRules = B3S23

    override fun setNeighboursCoordinates(
        grid: Grid,
        x: Int,
        y: Int,
        dstArray: IntArray,
        dstOffset: Int
    ) {
        var offset = dstOffset
        val (groupX, groupY) = SphinxCellsGrouping.groupXY(x, y)

        if (SphinxCellsGrouping.cellGroupIndex(x) == 0) {
            dstArray[offset++] = SphinxCellsGrouping.toCellLinear(grid, groupX, groupY, 1)
            dstArray[offset++] = SphinxCellsGrouping.toCellLinear(grid, groupX - 1, groupY, 0)
            dstArray[offset++] = SphinxCellsGrouping.toCellLinear(grid, groupX - 1, groupY, 1)
            dstArray[offset++] = SphinxCellsGrouping.toCellLinear(grid, groupX - 1, groupY - 1, 1)
            dstArray[offset++] = SphinxCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 0)
            dstArray[offset++] = SphinxCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 1)
            dstArray[offset++] = SphinxCellsGrouping.toCellLinear(grid, groupX + 1, groupY - 1, 0)
            dstArray[offset++] = SphinxCellsGrouping.toCellLinear(grid, groupX + 1, groupY - 1, 1)
            dstArray[offset++] = SphinxCellsGrouping.toCellLinear(grid, groupX + 1, groupY, 0)
            dstArray[offset++] = SphinxCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 0)
            dstArray[offset++] = SphinxCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1, 0)
            dstArray[offset++] = SphinxCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1, 1)
        } else {
            dstArray[offset++] = SphinxCellsGrouping.toCellLinear(grid, groupX, groupY, 0)
            dstArray[offset++] = SphinxCellsGrouping.toCellLinear(grid, groupX - 1, groupY, 1)
            dstArray[offset++] = SphinxCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1, 0)
            dstArray[offset++] = SphinxCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1, 1)
            dstArray[offset++] = SphinxCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 1)
            dstArray[offset++] = SphinxCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 0)
            dstArray[offset++] = SphinxCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1, 0)
            dstArray[offset++] = SphinxCellsGrouping.toCellLinear(grid, groupX + 1, groupY, 0)
            dstArray[offset++] = SphinxCellsGrouping.toCellLinear(grid, groupX + 1, groupY, 1)
            dstArray[offset++] = SphinxCellsGrouping.toCellLinear(grid, groupX + 1, groupY - 1, 1)
            dstArray[offset++] = SphinxCellsGrouping.toCellLinear(grid, groupX + 1, groupY - 1, 0)
            dstArray[offset++] = SphinxCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 1)
        }
        assert(offset == neighboursCount + dstOffset)
    }

}

class SphinxGridFactory @Inject constructor() : GridFactory {
    override fun createGrid(
        canvasWidth: Float,
        canvasHeight: Float,
        displayDensity: Float,
        cellSize: Int
    ) = stdGridFactory(
        canvasWidth = canvasWidth,
        canvasHeight = canvasHeight,
        displayDensity = displayDensity,
        cellSize = cellSize,
        colGroupCount = 2,
        rowGroupCount = 2
    )
}

class SphinxTilingOnCanvasFactory @Inject constructor() :
    TilingOnCanvas.Factory {
    override fun create(canvasWidth: Float, canvasHeight: Float, grid: Grid): TilingOnCanvas {
        return PeriodicShapesTilingOnCanvas.create(
            grid,
            canvasWidth = canvasWidth,
            canvasHeight = canvasHeight,
            shapeBuilder = SphinxPeriodicShapeBuilder(grid, canvasWidth, canvasHeight)
        )
    }

    private class SphinxPeriodicShapeBuilder(
        grid: Grid,
        val canvasWidth: Float,
        canvasHeight: Float
    ) :
        PeriodicShapesTilingOnCanvas.PeriodicShapeBuilder {

        private val a: Float = 2 * canvasWidth / grid.columns
        private val b: Float = canvasHeight / grid.rows


        override fun createShape(x: Int, y: Int): PeriodicShape {

            // N.B. The shape is not convex: it is represented as the union of a parallelogram and a triangle.
            // check the assets folder for more info.

            val p0xAbs = a * (x / 2) + a / 4 * y
            val p0x =
                p0xAbs - ((p0xAbs / canvasWidth).toInt() * canvasWidth) // workaround for cutting algorithm not handling coordinates larger than 2 the canvas size.

            val p0 = Vertex(p0x, y * b)
            val p1 = p0.xShift(a)
            val p2 = p0.add(a / 4f, b)
            val p3 = p2.xShift(a)
            val p4 = ((p0 + p2) / 2f).roundY()
            val p5 = p4.xShift(a)
            val p6 = p4.copy(x = p0.x + a / 2)
            val p7 = p6.xShift(a / 4f)


            return if (SphinxCellsGrouping.cellGroupIndex(x) == 0) {
                unionOfConvexPolygons {
                    polygon(p0) {
                        to(p1, visible = true)
                        to(p7, visible = false)
                        to(p6, visible = true)
                        to(p4, visible = false)
                        close(true)
                    }

                    polygon(p4) {
                        to(p6, visible = false)
                        to(p2, visible = true)
                        close(false)
                    }

                }
            } else {
                unionOfConvexPolygons {
                    polygon(p2) {
                        to(p6, visible = true)
                        to(p7, visible = true)
                        to(p5, visible = false)
                        to(p3, visible = true)
                        close(true)
                    }

                    polygon(p7) {
                        to(p5, visible = false)
                        to(p1, visible = true)
                        close(true)
                    }
                }
            }
        }
    }
}