package com.manuel.pagliai.tsol.domain.cells

import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.domain.CellType
import com.manuel.pagliai.tsol.domain.GameRules.Companion.B2S34
import com.manuel.pagliai.tsol.domain.Grid
import com.manuel.pagliai.tsol.domain.GridFactory
import com.manuel.pagliai.tsol.domain.GridFactory.Companion.stdGridFactory
import com.manuel.pagliai.tsol.domain.TilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShape
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShapesTilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.Vertex
import com.manuel.pagliai.tsol.domain.tiling.convexPolygonPeriodicShape
import javax.inject.Inject

private const val ONE_OVER_ROOT_2 = 0.70710678118f

private val HerringboneCellsGrouping = HorizontalCellsGrouping(2)

class HerringboneCellType @Inject constructor(
    override val gridFactory: HerringboneGridFactory,
    override val tilingOnCanvasFactory: HerringboneTilingOnCanvasFactory
) : CellType {

    override val name = R.string.herringbone_cell_type
    override val icon = R.drawable.ic_herringbone_cell_type
    override val id = "HERRINGBONE"
    override val neighboursCount = 6
    override val defaultGameRules = B2S34

    override fun setNeighboursCoordinates(
        grid: Grid,
        x: Int,
        y: Int,
        dstArray: IntArray,
        dstOffset: Int
    ) {
        var offset = dstOffset

        dstArray[offset++] = grid.toLinear(x + 1, y)
        dstArray[offset++] = grid.toLinear(x - 1, y)
        dstArray[offset++] = grid.toLinear(x + 2, y)
        dstArray[offset++] = grid.toLinear(x - 2, y)

        if (HerringboneCellsGrouping.cellGroupIndex(x) == 0) {
            dstArray[offset++] = grid.toLinear(x + 1, y - 1)
            dstArray[offset++] = grid.toLinear(x - 1, y - 1)
        } else {
            dstArray[offset++] = grid.toLinear(x + 1, y + 1)
            dstArray[offset++] = grid.toLinear(x - 1, y + 1)
        }

        require(offset == neighboursCount + dstOffset)
    }
}

class HerringboneGridFactory @Inject constructor() : GridFactory {
    override fun createGrid(
        canvasWidth: Float,
        canvasHeight: Float,
        displayDensity: Float,
        cellSize: Int
    ) = stdGridFactory(
        canvasWidth = canvasWidth,
        canvasHeight = canvasHeight,
        displayDensity = displayDensity,
        cellSize = cellSize,
        colGroupCount = 2,
        rowGroupCount = 1,
        correctionFactorX = 2f,
        correctionFactorY = 0.5f
    )
}

class HerringboneTilingOnCanvasFactory @Inject constructor() :
    TilingOnCanvas.Factory {
    override fun create(canvasWidth: Float, canvasHeight: Float, grid: Grid): TilingOnCanvas {
        return PeriodicShapesTilingOnCanvas.create(
            grid,
            canvasWidth = canvasWidth,
            canvasHeight = canvasHeight,
            shapeBuilder = HerringboneShapeBuilder(grid, canvasWidth, canvasHeight)
        )
    }

    private class HerringboneShapeBuilder(grid: Grid, canvasWidth: Float, canvasHeight: Float) :
        PeriodicShapesTilingOnCanvas.PeriodicShapeBuilder {

        private val ax: Float
        private val ay: Float
        private val bx: Float
        private val by: Float

        init {
            val a = canvasWidth / (grid.columns * ONE_OVER_ROOT_2)
            val b = canvasHeight / (2 * grid.rows * ONE_OVER_ROOT_2)


            ax = a * ONE_OVER_ROOT_2
            ay = ax

            bx = b * ONE_OVER_ROOT_2
            by = bx
        }

        override fun createShape(x: Int, y: Int): PeriodicShape {
            val p0 = Vertex(x = (x / 2) * (2 * ax), y = y * 2 * by)
            val p1 = p0.add(ax, ay)
            val p2 = p1.add(bx, -by)
            val p3 = p0.add(bx, -by)

            val p4 = p1.add(bx, by)
            val p5 = p4.add(ax, -ay)
            val p6 = p5.add(-bx, -by)


            return if (HerringboneCellsGrouping.cellGroupIndex(x) == 0) {
                convexPolygonPeriodicShape(
                    p0,
                    p1,
                    p2,
                    p3
                )
            } else {
                convexPolygonPeriodicShape(
                    p1,
                    p4,
                    p5,
                    p6
                )
            }
        }
    }
}