package com.manuel.pagliai.tsol.domain.cells

import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.domain.CellType
import com.manuel.pagliai.tsol.domain.GameRules.Companion.B34S235
import com.manuel.pagliai.tsol.domain.Grid
import com.manuel.pagliai.tsol.domain.GridFactory
import com.manuel.pagliai.tsol.domain.TilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShape
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShapesTilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.convexPolygonPeriodicShape
import javax.inject.Inject


private val PentagonalCellsGrouping = HorizontalCellsGrouping(3)

class PentagonalCellType @Inject constructor(
    override val gridFactory: PentagonalGridFactory,
    override val tilingOnCanvasFactory: PentagonalTilingOnCanvasFactory
) : CellType {

    override val name = R.string.pentagonal_cell_type
    override val icon = R.drawable.ic_pentagonal_cell_type
    override val id: String = "PENTAGONAL"
    override val neighboursCount = 6
    override val defaultGameRules = B34S235

    override fun setNeighboursCoordinates(
        grid: Grid,
        x: Int,
        y: Int,
        dstArray: IntArray,
        dstOffset: Int
    ) {
        val (groupX, groupY) = PentagonalCellsGrouping.groupXY(x, y)
        val evenGroupColumn = groupX % 2 == 0
        var offset = dstOffset

        when (PentagonalCellsGrouping.cellGroupIndex(x)) {
            0 -> {
                dstArray[offset++] =
                    PentagonalCellsGrouping.toCellLinear(grid, groupX, groupY, 1)
                dstArray[offset++] =
                    PentagonalCellsGrouping.toCellLinear(grid, groupX, groupY, 2)
                dstArray[offset++] =
                    PentagonalCellsGrouping.toCellLinear(grid, groupX - 1, groupY, 2)
                dstArray[offset++] =
                    PentagonalCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 1)
                if (evenGroupColumn) {
                    dstArray[offset++] =
                        PentagonalCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1, 1)
                    dstArray[offset++] =
                        PentagonalCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1, 2)
                } else {
                    dstArray[offset++] =
                        PentagonalCellsGrouping.toCellLinear(grid, groupX - 1, groupY, 1)
                    dstArray[offset++] =
                        PentagonalCellsGrouping.toCellLinear(grid, groupX - 1, groupY - 1, 2)
                }
            }
            1 -> {
                dstArray[offset++] =
                    PentagonalCellsGrouping.toCellLinear(grid, groupX, groupY, 0)
                dstArray[offset++] =
                    PentagonalCellsGrouping.toCellLinear(grid, groupX, groupY, 2)
                dstArray[offset++] =
                    PentagonalCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 0)
                dstArray[offset++] =
                    PentagonalCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 2)
                if (evenGroupColumn) {
                    dstArray[offset++] =
                        PentagonalCellsGrouping.toCellLinear(grid, groupX + 1, groupY, 0)
                    dstArray[offset++] =
                        PentagonalCellsGrouping.toCellLinear(grid, groupX - 1, groupY, 2)
                } else {
                    dstArray[offset++] =
                        PentagonalCellsGrouping.toCellLinear(grid, groupX + 1, groupY - 1, 0)
                    dstArray[offset++] =
                        PentagonalCellsGrouping.toCellLinear(grid, groupX - 1, groupY - 1, 2)
                }
            }
            2 -> {
                dstArray[offset++] =
                    PentagonalCellsGrouping.toCellLinear(grid, groupX, groupY, 0)
                dstArray[offset++] =
                    PentagonalCellsGrouping.toCellLinear(grid, groupX, groupY, 1)
                dstArray[offset++] =
                    PentagonalCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 1)
                dstArray[offset++] =
                    PentagonalCellsGrouping.toCellLinear(grid, groupX + 1, groupY, 0)
                if (evenGroupColumn) {
                    dstArray[offset++] =
                        PentagonalCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1, 0)
                    dstArray[offset++] =
                        PentagonalCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1, 1)
                } else {
                    dstArray[offset++] =
                        PentagonalCellsGrouping.toCellLinear(grid, groupX + 1, groupY - 1, 0)
                    dstArray[offset++] =
                        PentagonalCellsGrouping.toCellLinear(grid, groupX + 1, groupY, 1)
                }
            }
        }
        assert(offset == neighboursCount + dstOffset)
    }

}

class PentagonalGridFactory @Inject constructor() : GridFactory {
    override fun createGrid(
        canvasWidth: Float,
        canvasHeight: Float,
        displayDensity: Float,
        cellSize: Int
    ) = HexagonBasedGridFactoryHelper.createGrid(
        polygonCount = 3,
        canvasWidth = canvasWidth,
        canvasHeight = canvasHeight,
        displayDensity = displayDensity,
        cellSize = cellSize,
        correctionFactorX = 0.5f,
        correctionFactorY = 0.5f
    )
}

class PentagonalTilingOnCanvasFactory @Inject constructor() :
    TilingOnCanvas.Factory {
    override fun create(canvasWidth: Float, canvasHeight: Float, grid: Grid): TilingOnCanvas {
        return PeriodicShapesTilingOnCanvas.create(
            grid,
            canvasWidth = canvasWidth,
            canvasHeight = canvasHeight,
            shapeBuilder = PentagonalPeriodicShapeBuilder(grid, canvasWidth, canvasHeight)
        )
    }

    private class PentagonalPeriodicShapeBuilder(
        grid: Grid,
        canvasWidth: Float,
        canvasHeight: Float
    ) : PeriodicShapesTilingOnCanvas.PeriodicShapeBuilder {
        private val helper = HexagonBasedPeriodicShapeBuilderHelper(
            grid = grid,
            canvasWidth = canvasWidth,
            canvasHeight = canvasHeight,
            polygonCount = 3
        )

        override fun createShape(x: Int, y: Int): PeriodicShape {
            val (p0, p1, p2, p3, p4, p5, p6) = helper.vertices(x, y)
            val p7 = (p0 + p1) / 2f
            val p8 = (p2 + p3) / 2f
            val p9 = (p4 + p5) / 2f

            return when (PentagonalCellsGrouping.cellGroupIndex(x)) {
                0 -> {
                    convexPolygonPeriodicShape(p7, p6, p9, p5, p0)
                }
                1 -> {
                    convexPolygonPeriodicShape(p7, p1, p2, p8, p6)
                }
                2 -> {
                    convexPolygonPeriodicShape(p8, p3, p4, p9, p6)
                }
                else -> throw IllegalStateException()
            }
        }
    }
}

