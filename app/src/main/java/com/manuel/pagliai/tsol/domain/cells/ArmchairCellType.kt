package com.manuel.pagliai.tsol.domain.cells

import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.domain.CellType
import com.manuel.pagliai.tsol.domain.GameRules.Companion.B2S34
import com.manuel.pagliai.tsol.domain.Grid
import com.manuel.pagliai.tsol.domain.GridFactory
import com.manuel.pagliai.tsol.domain.GridFactory.Companion.stdGridFactory
import com.manuel.pagliai.tsol.domain.TilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShape
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShapesTilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.Vertex
import com.manuel.pagliai.tsol.domain.tiling.unionOfConvexPolygons
import javax.inject.Inject


private val ArmchairCellsGrouping = HorizontalCellsGrouping(2)

class ArmchairCellType @Inject constructor(
    override val gridFactory: ArmchairGridFactory,
    override val tilingOnCanvasFactory: ArmchairTilingOnCanvasFactory
) : CellType {

    override val name = R.string.armchair_cell_type
    override val icon = R.drawable.ic_armchair_cell_type
    override val id = "ARMCHAIR"
    override val neighboursCount = 7
    override val defaultGameRules = B2S34

    override fun setNeighboursCoordinates(
        grid: Grid,
        x: Int,
        y: Int,
        dstArray: IntArray,
        dstOffset: Int
    ) {
        var offset = dstOffset

        dstArray[offset++] = grid.toLinear(x + 1, y)
        dstArray[offset++] = grid.toLinear(x - 1, y)
        dstArray[offset++] = grid.toLinear(x, y - 1)
        dstArray[offset++] = grid.toLinear(x, y + 1)
        dstArray[offset++] = grid.toLinear(x + 1, y + 1)
        dstArray[offset++] = grid.toLinear(x - 1, y - 1)
        dstArray[offset++] =
            if (ArmchairCellsGrouping.cellGroupIndex(x) == 0) {
                grid.toLinear(x - 1, y + 1)
            } else {
                grid.toLinear(x + 1, y - 1)
            }
        assert(offset == neighboursCount + dstOffset)
    }
}

class ArmchairGridFactory @Inject constructor() : GridFactory {
    override fun createGrid(
        canvasWidth: Float,
        canvasHeight: Float,
        displayDensity: Float,
        cellSize: Int
    ) = stdGridFactory(
        canvasWidth = canvasWidth,
        canvasHeight = canvasHeight,
        displayDensity = displayDensity,
        cellSize = cellSize,
        colGroupCount = 2,
        rowGroupCount = 2
    )
}

class ArmchairTilingOnCanvasFactory @Inject constructor() :
    TilingOnCanvas.Factory {
    override fun create(canvasWidth: Float, canvasHeight: Float, grid: Grid): TilingOnCanvas {
        return PeriodicShapesTilingOnCanvas.create(
            grid,
            canvasWidth = canvasWidth,
            canvasHeight = canvasHeight,
            shapeBuilder = ArmchairShapeBuilder(grid, canvasWidth, canvasHeight)
        )
    }

    private class ArmchairShapeBuilder(grid: Grid, canvasWidth: Float, canvasHeight: Float) :
        PeriodicShapesTilingOnCanvas.PeriodicShapeBuilder {

        private val a = 2 * canvasWidth / (grid.columns)
        private val b = canvasHeight / grid.rows

        override fun createShape(x: Int, y: Int): PeriodicShape {
            // N.B. The shape is not convex: it is represented as the union of two convex rectangles.
            // check the assets folder for more info.

            val p0 = Vertex((x / 2) * a, y * b)
            val p1 = p0.xShift(a / 4)
            val p2 = p0.xShift(a)
            val p3 = p2.yShift(b / 2)
            val p4 = p2.yShift(b)
            val p5 = p4.xShift(-a / 4)
            val p6 = p0.copy(y = p4.y)
            val p7 = p0.copy(y = p3.y)
            val p8 = p1.copy(y = p3.y)
            val p9 = p5.yShift(-b / 2)

            return when (ArmchairCellsGrouping.cellGroupIndex(x)) {
                0 -> unionOfConvexPolygons {
                    polygon(p0) {
                        to(p1, true)
                        to(p8, true)
                        to(p7, false)
                        close(true)
                    }

                    polygon(p9) {
                        to(p5, true)
                        to(p6, true)
                        to(p7, true)
                        to(p8, false)
                        close(true)
                    }
                }
                1 -> {
                    unionOfConvexPolygons {
                        polygon(p1) {
                            to(p2, true)
                            to(p3, true)
                            to(p9, false)
                            to(p8, false)
                            close(true)
                        }

                        polygon(p5) {
                            to(p9, true)
                            to(p3, false)
                            to(p4, true)
                            close(true)
                        }
                    }
                }
                else -> throw IllegalStateException()
            }
        }
    }
}