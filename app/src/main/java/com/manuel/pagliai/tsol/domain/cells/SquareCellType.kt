package com.manuel.pagliai.tsol.domain.cells


import android.graphics.Path
import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.domain.*
import com.manuel.pagliai.tsol.domain.GridFactory.Companion.stdGridFactory
import javax.inject.Inject

class SquareCellType @Inject constructor(
    override val gridFactory: SquareGridFactory,
    override val tilingOnCanvasFactory: SquareTilingOnCanvas.Factory
) : CellType {

    constructor() : this(SquareGridFactory(), SquareTilingOnCanvas.Factory())

    override val name: Int = R.string.square_cell_type
    override val icon: Int = R.drawable.ic_square_cell_type
    override val id: String = "SQUARE"
    override val neighboursCount: Int = 8
    override val defaultGameRules: GameRules = GameRules.B3S23

    override fun setNeighboursCoordinates(
        grid: Grid,
        x: Int,
        y: Int,
        dstArray: IntArray,
        dstOffset: Int
    ) {
        var offset = dstOffset

        dstArray[offset++] = grid.toLinear(x, y - 1)
        dstArray[offset++] = grid.toLinear(x + 1, y - 1)
        dstArray[offset++] = grid.toLinear(x + 1, y)
        dstArray[offset++] = grid.toLinear(x + 1, y + 1)
        dstArray[offset++] = grid.toLinear(x, y + 1)
        dstArray[offset++] = grid.toLinear(x - 1, y + 1)
        dstArray[offset++] = grid.toLinear(x - 1, y)
        dstArray[offset++] = grid.toLinear(x - 1, y - 1)

        assert(offset == neighboursCount + dstOffset)
    }
}

class SquareGridFactory @Inject constructor() : GridFactory {
    override fun createGrid(
        canvasWidth: Float,
        canvasHeight: Float,
        displayDensity: Float,
        cellSize: Int
    ) = stdGridFactory(
        canvasWidth = canvasWidth,
        canvasHeight = canvasHeight,
        displayDensity = displayDensity,
        cellSize = cellSize
    )
}

class SquareTilingOnCanvas(
    private val horizontalSide: Float,
    private val verticalSide: Float,
    private val grid: Grid,
    /**
     * The path associated to each cell. Indexed according to linear coordinates
     * @see [onCellPaths]
     */
    private val paths: List<Path>,

    /**
     * The grid lines associated to each cell. Indexed according to linear coordinates
     * @see [onGridLines]
     */
    private val gridLines: List<FloatArray>
) : TilingOnCanvas {

    class Factory @Inject constructor() : TilingOnCanvas.Factory {
        override fun create(
            canvasWidth: Float,
            canvasHeight: Float,
            grid: Grid
        ): TilingOnCanvas {

            // N.B. in theory horizontal side is the same as the vertical side.
            // In practice however this is a best effort: the numbers of rows and columns has to be an integer,
            // and the canvas aspect ratio might not allow for perfectly square cells.

            val horizontalSide = canvasWidth / grid.columns
            val verticalSide = canvasHeight / grid.rows
            val cellsCount = grid.size
            val paths = ArrayList<Path>(cellsCount)
            val lines = ArrayList<FloatArray>(cellsCount)

            for (linear in 0 until cellsCount) {
                val (x, y) = grid.toXY(linear)


                val leftX = x * horizontalSide
                val topY = y * verticalSide

                val rightX = leftX + horizontalSide
                val bottomY = topY + verticalSide

                paths.add(Path().apply {
                    moveTo(leftX, topY)
                    lineTo(rightX, topY)
                    lineTo(rightX, bottomY)
                    lineTo(leftX, bottomY)
                    close()
                })

                lines.add(
                    floatArrayOf(
                        leftX,
                        topY,

                        rightX,
                        topY,

                        rightX,
                        topY,

                        rightX,
                        bottomY,

                        rightX,
                        bottomY,

                        leftX,
                        bottomY,


                        leftX,
                        bottomY,

                        leftX,
                        topY
                    )
                )
            }

            return SquareTilingOnCanvas(
                horizontalSide = horizontalSide,
                verticalSide = verticalSide,
                grid = grid,
                paths = paths,
                gridLines = lines
            )
        }
    }

    override fun onGridLines(coordinates: Int, action: (FloatArray) -> Unit) {
        action(gridLines[coordinates])
    }

    override fun onCellPaths(coordinates: Int, action: (Path) -> Unit) {
        action(paths[coordinates])
    }

    override fun coordinates(x: Float, y: Float): Int? {
        val col = (x / horizontalSide).toInt()
        val row = (y / verticalSide).toInt()
        if (row >= grid.rows || col >= grid.columns)
            return null
        return grid.toLinear(col, row)
    }
}