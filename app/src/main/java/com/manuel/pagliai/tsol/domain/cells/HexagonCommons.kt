package com.manuel.pagliai.tsol.domain.cells

import com.manuel.pagliai.tsol.domain.Grid
import com.manuel.pagliai.tsol.domain.GridFactory
import com.manuel.pagliai.tsol.domain.tiling.Vertex
import kotlin.math.ceil


/**
 * Common logic to help rendering shapes whose tiling results in a subdivision of an hexagon.
 *      The logic here is for an hexagon with two horizontal side. i.e., an hexagon like this:
 *              p1      p2
 *               ########          -|
 *             ############         |
 *           ################       |
 *      p0 ######## p6 ####### p3  |  hexHeight
 *           ################       |
 *             ############         |
 *           p5 ######### p4      _|
 *        |____________________|
 *                 hexWidth
 *
 * [polygonCount] represents the number of polygons the hexagon is sub divided into.
 */
class HexagonBasedPeriodicShapeBuilderHelper(
    grid: Grid,
    canvasWidth: Float,
    canvasHeight: Float,
    private val polygonCount: Int
) {

    data class HexagonVertices(
        val p0: Vertex,
        val p1: Vertex,
        val p2: Vertex,
        val p3: Vertex,
        val p4: Vertex,
        val p5: Vertex,
        val p6: Vertex = (p0 + p3) / 2f
    ) {
        fun toArray(): Array<Vertex> {
            return arrayOf(p0, p1, p2, p3, p4, p5, p6)
        }
    }

    /**
     * max width of the polygon
     */
    private val hexWidth = canvasWidth / ((grid.columns / polygonCount) * 3 / 2f) * 2f

    /**
     * max height of the polygon (intended as max deltaY between two points in the polygon border).
     */
    private val hexHeight = canvasHeight / (grid.rows)

    /**
     * provided the hexagon as represented in the class doc, returns the coordinates of the point furthest to the left.
     *  this point is, by convention, denoted as p0 in this project. Usually other points (p1, p2,..) are then assigned clockwise staring from p0
     */
    fun vertices(x: Int, y: Int): HexagonVertices {
        val hexX = x / polygonCount

        val p0X = if (hexX % 2 != 0) {
            3 * (hexX / 2) * (hexWidth / 2f) + hexWidth
        } else {
            3 * (hexX / 2) * (hexWidth / 2f) + (hexWidth / 4f)
        }

        val p0Y = if (hexX % 2 != 0) {
            hexHeight / 2f + y * hexHeight
        } else {
            hexHeight + y * hexHeight
        }

        val p0 = Vertex(p0X, p0Y)
        val p1 = p0.add(hexWidth / 4f, -hexHeight / 2f)
        val p2 = p1.xShift(hexWidth / 2f)
        val p3 = p0.xShift(hexWidth)
        val p4 = p2.yShift(hexHeight)
        val p5 = p1.yShift(hexHeight)

        return HexagonVertices(
            p0 = p0,
            p1 = p1,
            p2 = p2,
            p3 = p3,
            p4 = p4,
            p5 = p5
        )
    }
}


/**
 * Helper class for [GridFactory] for tilings that are a subdivision of the hexagon tiling.
 */
object HexagonBasedGridFactoryHelper {
    fun createGrid(
        polygonCount: Int,
        canvasWidth: Float,
        canvasHeight: Float,
        displayDensity: Float,
        cellSize: Int,
        correctionFactorX: Float = 1f,
        correctionFactorY: Float = 1f
    ): Grid {
        val ux = ceil(polygonCount / 3f) * GridFactory.CELL_SIZE_TO_DP[cellSize] * displayDensity
        val uy = ceil(polygonCount / 3f) * GridFactory.CELL_SIZE_TO_DP[cellSize] * displayDensity

        val cols = canvasWidth / ux * correctionFactorX
        val rows = canvasHeight / uy * correctionFactorY

        // make sure we have an even number of "columns", and "rows" as to wrap up nicely
        // also make sure the rows and cols are both > 2, as to guarantee that x + 1 is not the same as x -1
        return Grid(
            columns = 4 * ceil(cols / 4).toInt() * polygonCount,
            rows = 4 * ceil(rows / 4).toInt()
        )
    }

}
