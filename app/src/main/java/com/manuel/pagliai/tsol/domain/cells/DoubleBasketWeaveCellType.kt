package com.manuel.pagliai.tsol.domain.cells

import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.domain.*
import com.manuel.pagliai.tsol.domain.GridFactory.Companion.stdGridFactory
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShape
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShapesTilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.Vertex
import com.manuel.pagliai.tsol.domain.tiling.convexPolygonPeriodicShape
import javax.inject.Inject


private val DoubleBasketWeaveCellsGrouping = HorizontalCellsGrouping(4)

class DoubleBasketweaveCellType @Inject constructor(
    override val gridFactory: DoubleBasketweaveGridFactory,
    override val tilingOnCanvasFactory: DoubleBasketWaveTilingOnCanvasFactory
) : CellType {

    override val name = R.string.double_basketweave_cell_type
    override val icon = R.drawable.ic_double_basketwave_celltype
    override val id = "DOUBLE_BASKETWEAVE"
    override val neighboursCount = 7
    override val defaultGameRules = GameRules.B3S235

    override fun setNeighboursCoordinates(
        grid: Grid,
        x: Int,
        y: Int,
        dstArray: IntArray,
        dstOffset: Int
    ) {

        var offset = dstOffset
        val (groupX, groupY) = DoubleBasketWeaveCellsGrouping.groupXY(x, y)
        val evenRow = groupY % 2 == 0

        when (DoubleBasketWeaveCellsGrouping.cellGroupIndex(x)) {
            0 -> {
                dstArray[offset++] =
                    DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX - 1, groupY, 3)
                dstArray[offset++] =
                    DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX, groupY, 1)
                dstArray[offset++] =
                    DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX, groupY, 2)
                dstArray[offset++] =
                    DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 1)
                if (evenRow) {
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX - 1, groupY - 1, 1)
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX - 1, groupY - 1, 2)
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX - 1, groupY - 1, 3)
                } else {
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX + 1, groupY - 1, 1)
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 2)
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 3)
                }
            }
            1 -> {
                dstArray[offset++] =
                    DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX - 1, groupY, 3)
                dstArray[offset++] =
                    DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX, groupY, 0)
                dstArray[offset++] =
                    DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX, groupY, 2)
                dstArray[offset++] =
                    DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 0)
                if (evenRow) {
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1, 0)
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1, 2)
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1, 3)
                } else {
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 2)
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 3)
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1, 0)
                }
            }
            2 -> {
                dstArray[offset++] =
                    DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX, groupY, 0)
                dstArray[offset++] =
                    DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX, groupY, 1)
                dstArray[offset++] =
                    DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX, groupY, 3)
                if (evenRow) {
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 0)
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1, 3)
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 1)
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX - 1, groupY - 1, 3)
                } else {
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1, 0)
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 3)
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX + 1, groupY - 1, 1)
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 3)
                }
            }
            3 -> {
                dstArray[offset++] =
                    DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX, groupY, 2)
                dstArray[offset++] =
                    DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX + 1, groupY, 0)
                dstArray[offset++] =
                    DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX + 1, groupY, 1)
                if (evenRow) {
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 0)
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 2)
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 1)
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 2)
                } else {
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1, 0)
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1, 2)
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX + 1, groupY - 1, 1)
                    dstArray[offset++] =
                        DoubleBasketWeaveCellsGrouping.toCellLinear(grid, groupX + 1, groupY - 1, 2)
                }
            }
            else -> throw IllegalStateException()
        }
        assert(offset == neighboursCount + dstOffset)
    }

}

class DoubleBasketweaveGridFactory @Inject constructor() : GridFactory {
    override fun createGrid(
        canvasWidth: Float, canvasHeight: Float, displayDensity: Float, cellSize: Int
    ) = stdGridFactory(
        canvasWidth = canvasWidth,
        canvasHeight = canvasHeight,
        displayDensity = displayDensity,
        cellSize = cellSize,
        colGroupCount = 4,
        rowGroupCount = 2,
        correctionFactorX = 1.6f,
        correctionFactorY = 0.6f
    )
}

class DoubleBasketWaveTilingOnCanvasFactory @Inject constructor() :
    TilingOnCanvas.Factory {
    override fun create(canvasWidth: Float, canvasHeight: Float, grid: Grid): TilingOnCanvas {
        return PeriodicShapesTilingOnCanvas.create(
            grid,
            canvasWidth = canvasWidth,
            canvasHeight = canvasHeight,
            shapeBuilder = DoubleBasketWaveShapeBuilder(grid, canvasWidth, canvasHeight)
        )
    }

    private class DoubleBasketWaveShapeBuilder(
        grid: Grid, canvasWidth: Float, canvasHeight: Float
    ) : PeriodicShapesTilingOnCanvas.PeriodicShapeBuilder {

        private val a = 4 * canvasWidth / grid.columns
        private val b = canvasHeight / grid.rows

        override fun createShape(x: Int, y: Int): PeriodicShape {

            val p0 = Vertex(x / 4 * a + (y % 2) * a / 2f, y * b)
            val p1 = p0.xShift(a / 2f)
            val p2 = p1.yShift(b / 2f)
            val p3 = p0.copy(y = p2.y)
            val p4 = p0.yShift(b)
            val p5 = p4.copy(x = p1.x)
            val p6 = p1.xShift(a / 4f)
            val p7 = p6.copy(y = p4.y)
            val p8 = p0.xShift(a)
            val p9 = p8.copy(y = p4.y)

            return when (DoubleBasketWeaveCellsGrouping.cellGroupIndex(x = x)) {
                0 -> {
                    convexPolygonPeriodicShape(
                        p0, p1, p2, p3
                    )
                }
                1 -> {
                    convexPolygonPeriodicShape(
                        p2, p3, p4, p5
                    )
                }
                2 -> {
                    convexPolygonPeriodicShape(
                        p1, p6, p7, p5
                    )
                }
                3 -> {
                    convexPolygonPeriodicShape(
                        p6, p8, p9, p7
                    )
                }
                else -> {
                    throw IllegalStateException()
                }
            }

        }
    }
}