package com.manuel.pagliai.tsol.domain.cells

import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.domain.CellType
import com.manuel.pagliai.tsol.domain.GameRules.Companion.B346S23
import com.manuel.pagliai.tsol.domain.Grid
import com.manuel.pagliai.tsol.domain.GridFactory
import com.manuel.pagliai.tsol.domain.GridFactory.Companion.stdGridFactory
import com.manuel.pagliai.tsol.domain.TilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShape
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShapesTilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.Vertex
import com.manuel.pagliai.tsol.domain.tiling.convexPolygonPeriodicShape
import javax.inject.Inject

private val CairoCellsGrouping = HorizontalCellsGrouping(4)

class CairoCellType @Inject constructor(
    override val gridFactory: CairoGridFactory,
    override val tilingOnCanvasFactory: CairoTilingOnCanvasFactory
) : CellType {

    override val name = R.string.pentagonal_cairo_cell_type
    override val icon = R.drawable.ic_cairo_cell_type
    override val id = "CAIRO"
    override val neighboursCount = 7
    override val defaultGameRules = B346S23

    override fun setNeighboursCoordinates(
        grid: Grid,
        x: Int,
        y: Int,
        dstArray: IntArray,
        dstOffset: Int
    ) {
        val (groupX, groupY) = CairoCellsGrouping.groupXY(x, y)
        val rcf = groupY % 2 // correction factor
        var offset = dstOffset

        when (CairoCellsGrouping.cellGroupIndex(x)) {
            0 -> {
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX, groupY, 1)
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX, groupY, 2)
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX - 1, groupY, 3)
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX - 1 + rcf, groupY - 1, 2)
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX - 1 + rcf, groupY - 1, 3)
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX - 1 + rcf, groupY + 1, 1)
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX - 1 + rcf, groupY + 1, 3)
            }
            1 -> {
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX, groupY, 0)
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX, groupY, 2)
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX, groupY, 3)
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX - 1 + rcf, groupY - 1, 2)
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX - 1 + rcf, groupY - 1, 3)
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX + rcf, groupY - 1, 0)
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX + rcf, groupY - 1, 2)
            }
            2 -> {
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX, groupY, 0)
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX, groupY, 1)
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX, groupY, 3)
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX - 1 + rcf, groupY + 1, 1)
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX - 1 + rcf, groupY + 1, 3)
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX + rcf, groupY + 1, 0)
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX + rcf, groupY + 1, 1)
            }
            3 -> {
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX, groupY, 1)
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX, groupY, 2)
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX + 1, groupY, 0)
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX + rcf, groupY - 1, 0)
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX + rcf, groupY - 1, 2)
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX + rcf, groupY + 1, 0)
                dstArray[offset++] =
                    CairoCellsGrouping.toCellLinear(grid, groupX + rcf, groupY + 1, 1)
            }
            else -> throw IllegalStateException()
        }
        assert(offset == neighboursCount + dstOffset)
    }

}

class CairoGridFactory @Inject constructor() : GridFactory {
    override fun createGrid(
        canvasWidth: Float,
        canvasHeight: Float,
        displayDensity: Float,
        cellSize: Int
    ) = stdGridFactory(
        canvasWidth = canvasWidth,
        canvasHeight = canvasHeight,
        displayDensity = displayDensity,
        cellSize = cellSize,
        colGroupCount = 4,
        rowGroupCount = 4,
        correctionFactorY = 0.8f,
        correctionFactorX = 1.4f
    )
}

class CairoTilingOnCanvasFactory @Inject constructor() :
    TilingOnCanvas.Factory {
    override fun create(canvasWidth: Float, canvasHeight: Float, grid: Grid): TilingOnCanvas {
        return PeriodicShapesTilingOnCanvas.create(
            grid,
            canvasWidth = canvasWidth,
            canvasHeight = canvasHeight,
            shapeBuilder = CairoPeriodicShapeBuilder(grid, canvasWidth, canvasHeight)
        )
    }

    private class CairoPeriodicShapeBuilder(grid: Grid, canvasWidth: Float, canvasHeight: Float) :
        PeriodicShapesTilingOnCanvas.PeriodicShapeBuilder {

        // check assets folder for more info.
        // also, check "A Note on the Game of Life in Hexagonal and Pentagonal Tessellations" by Carter Bays

        private val a: Float = canvasWidth / (grid.columns / 4)
        private val b: Float = 3 * canvasHeight / (2f * (grid.rows))

        override fun createShape(x: Int, y: Int): PeriodicShape {

            val hexagonStartX = (x / 4) * a + ((y % 2) * a / 2f)
            val hexagonStartY = y * (b / 3f + b / 3f)

            val p0 = Vertex(hexagonStartX, hexagonStartY)
            val p1 = p0.add(a / 4f, -b / 6f)
            val p2 = p0 + Vertex(a / 2f, -b / 3f)
            val p3 = p1.xShift(a / 2f)
            val p4 = p0.xShift(a)
            val p5 = p4.yShift(b / 3f)
            val p6 = p3.yShift(2f / 3f * b)
            val p7 = p2.yShift(b)
            val p8 = p1.copy(y = p6.y)
            val p9 = p0.copy(y = p5.y)
            val p10 = p0 + Vertex((a * 3f / 4) / 2, b / 6f)
            val p11 = p10.xShift(a / 4f)

            return when (CairoCellsGrouping.cellGroupIndex(x)) {
                0 -> convexPolygonPeriodicShape(
                    p0,
                    p1,
                    p10,
                    p8,
                    p9
                )
                1 -> convexPolygonPeriodicShape(
                    p1,
                    p2,
                    p3,
                    p11,
                    p10
                )
                2 -> convexPolygonPeriodicShape(
                    p10,
                    p11,
                    p6,
                    p7,
                    p8
                )
                3 -> convexPolygonPeriodicShape(
                    p11,
                    p3,
                    p4,
                    p5,
                    p6
                )
                else -> throw IllegalStateException()
            }
        }

    }
}
