package com.manuel.pagliai.tsol.repository.preferences


import androidx.compose.ui.graphics.Color
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.*
import com.manuel.pagliai.tsol.domain.GameSettings
import com.manuel.pagliai.tsol.screens.game.GameState
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.io.IOException
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

/**
 * Handles persistence of user preferences on disk.
 *
 * Preferences are modelled as [PreferencesState] and [GameSettings]
 *
 * Note there is a difference between the the preferences saved on disk, and
 * the settings that are applied to the games: When system [PreferencesState.useSystemColors] is enabled
 * [PreferencesState.cellsColor], [PreferencesState.backgroundColor] and [PreferencesState.gridColor] are persisted
 * but not applied (the system colors are applied instead).
 * [GameSettings] represents the settings applied at runtime.
 *
 * Persistence is handled via [DataStore]
 *
 * @see [PreferencesState]
 * @see [GameSettings]
 */
@Singleton
class PreferencesRepository @Inject constructor(
    @Named("SettingsDataStore")
    private val settingsDataStore: DataStore<Preferences>
) {

    /**
     * Represents a preference, its key, default value and get/update methods.
     */
    private sealed class TSOLPreference<T>(
        private val key: Preferences.Key<T>,
        private val defaultValue: T
    ) {

        fun getOrDefault(preferences: Preferences) = preferences[key] ?: defaultValue
        fun update(preferences: MutablePreferences, newValue: T) {
            preferences[key] = newValue
        }

        object ShowNextGenWhnIdle :
            TSOLPreference<Boolean>(booleanPreferencesKey("show_next_gen_when_idle"), true)

        object NewGamesRandomCells :
            TSOLPreference<Boolean>(booleanPreferencesKey("randomise_new_games"), false)

        object UseSystemColors :
            TSOLPreference<Boolean>(booleanPreferencesKey("use_system_colors"), true)

        object CellsColor : TSOLPreference<Long>(
            longPreferencesKey("cells_color"),
            Color(0xFF3700B3).value.toLong()
        )

        object GridColor :
            TSOLPreference<Long>(longPreferencesKey("grid_color"), Color(0xFF03DAC5).value.toLong())

        object BackgroundColor : TSOLPreference<Long>(
            longPreferencesKey("background_color"),
            Color(0xFFFFFFFF).value.toLong()
        )

        object NewGamesDefaultSpeed : TSOLPreference<Int>(
            intPreferencesKey("new_games_default_speed"),
            GameState.NEW_GAMES_DEFAULT_SPEED_PREFERENCE_DEFAULT
        )

        object NewGamesDefaultCellSize : TSOLPreference<Int>(
            intPreferencesKey("new_games_default_cell_size"),
            GameState.NEW_GAMES_DEFAULT_CELL_SIZE_PREFERENCE_DEFAULT
        )
    }

    private operator fun <T> Preferences.get(tsolPreference: TSOLPreference<T>): T =
        tsolPreference.getOrDefault(this)

    private operator fun <T> MutablePreferences.set(
        tsolPreference: TSOLPreference<T>,
        newValue: T
    ) = tsolPreference.update(this, newValue)

    /**
     * Flow emitting preferences as saved on disk.
     * @see [PreferencesState]
     */
    val preferencesState = settingsDataStore.data
        .catch { exception ->
            // dataStore.data throws an IOException when an error is encountered when reading data
            if (exception is IOException) {
                emit(emptyPreferences())
            } else {
                throw exception
            }
        }.map { preferences ->
            PreferencesState(
                showNextGenAnimationIdle = preferences[TSOLPreference.ShowNextGenWhnIdle],
                useSystemColors = preferences[TSOLPreference.UseSystemColors],
                cellsColor = Color(preferences[TSOLPreference.CellsColor].toULong()),
                gridColor = Color(preferences[TSOLPreference.GridColor].toULong()),
                backgroundColor = Color(preferences[TSOLPreference.BackgroundColor].toULong()),
                newGamesDefaultSpeed = preferences[TSOLPreference.NewGamesDefaultSpeed],
                newGamesDefaultCellSize = preferences[TSOLPreference.NewGamesDefaultCellSize],
                randomiseNewGamesCells = preferences[TSOLPreference.NewGamesRandomCells]
            )
        }

    /**
     * Flow emitting the [GameSettings] as derived from the [preferencesState]
     */
    val gameSettings = preferencesState.map { prefs ->
        GameSettings(
            showNextGenAnimationIdle = prefs.showNextGenAnimationIdle,
            randomiseNewGamesCells = prefs.randomiseNewGamesCells,
            cellsColor = if (prefs.useSystemColors) null else prefs.cellsColor,
            gridColor = if (prefs.useSystemColors) null else prefs.gridColor,
            backgroundColor = if (prefs.useSystemColors) null else prefs.backgroundColor,
            newGamesDefaultSpeed = prefs.newGamesDefaultSpeed,
            newGamesDefaultCellSize = prefs.newGamesDefaultCellSize
        )
    }

    suspend fun updateShowNexGenWhenIdle(newValue: Boolean) {
        settingsDataStore.edit { preferences ->
            preferences[TSOLPreference.ShowNextGenWhnIdle] = newValue
        }
    }

    suspend fun updateRandomiseNewGamesCells(newValue: Boolean) {
        settingsDataStore.edit { preferences ->
            preferences[TSOLPreference.NewGamesRandomCells] = newValue
        }
    }

    suspend fun updateUseSystemColors(newValue: Boolean) {
        settingsDataStore.edit { preferences ->
            preferences[TSOLPreference.UseSystemColors] = newValue
        }
    }

    suspend fun updateCellsColor(newValue: Color) {
        settingsDataStore.edit { preferences ->
            preferences[TSOLPreference.CellsColor] = newValue.value.toLong()
        }
    }

    suspend fun updateGridColor(newValue: Color) {
        settingsDataStore.edit { preferences ->
            preferences[TSOLPreference.GridColor] = newValue.value.toLong()
        }
    }

    suspend fun updateBackgroundColor(newValue: Color) {
        settingsDataStore.edit { preferences ->
            preferences[TSOLPreference.BackgroundColor] = newValue.value.toLong()
        }
    }

    suspend fun updateNewGamesDefaultSpeed(newValue: Int) {
        settingsDataStore.edit { preferences ->
            preferences[TSOLPreference.NewGamesDefaultSpeed] = newValue
        }
    }

    suspend fun updateNewGamesDefaultCellSize(newValue: Int) {
        settingsDataStore.edit { preferences ->
            preferences[TSOLPreference.NewGamesDefaultCellSize] = newValue
        }
    }
}