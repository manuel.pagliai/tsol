package com.manuel.pagliai.tsol.domain.cells

import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.domain.*
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShape
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShapesTilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.convexPolygonPeriodicShape
import javax.inject.Inject

private val KisrhombilleCellsGrouping = HorizontalCellsGrouping(12)

class KisrhombilleCellType @Inject constructor(
    override val gridFactory: KisrhombilleGridFactory,
    override val tilingOnCanvasFactory: KisrhombilleTilingCanvasAdapterFactory,
) : CellType {

    override val hidden: Boolean
        get() = true // does not look that good yet. Also, rules are cumbersome

    override val name = R.string.kisrhombille_cell_type
    override val icon = R.drawable.ic_kisrhombille
    override val id: String = "KISRHOMBILLE"
    override val neighboursCount = 16
    override val defaultGameRules = GameRules.B45S2345
    override fun setNeighboursCoordinates(
        grid: Grid,
        x: Int,
        y: Int,
        dstArray: IntArray,
        dstOffset: Int
    ) {
        val (groupX, groupY) = KisrhombilleCellsGrouping.groupXY(x, y)
        val cellGroupIndex = KisrhombilleCellsGrouping.cellGroupIndex(x)

        var offset = dstOffset
        (0..11).forEach {
            if (it != cellGroupIndex) {
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX, groupY, it)
            }
        }
        val ccf = groupX % 2

        when (cellGroupIndex) {
            0 -> {
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1 - ccf, 3)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1 - ccf, 4)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX - 1, groupY - ccf, 6)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX - 1, groupY - ccf, 7)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX - 1, groupY - ccf, 8)
            }
            1 -> {
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX - 1, groupY - ccf, 5)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX - 1, groupY - ccf, 6)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX - 1, groupY - ccf, 7)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 9)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 10)
            }
            2 -> {
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX - 1, groupY - ccf, 5)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX - 1, groupY - ccf, 6)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 8)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 9)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 10)
            }
            3 -> {
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX + 1, groupY - ccf, 0)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX + 1, groupY - ccf, 11)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 7)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 8)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 9)
            }
            4 -> {
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX + 1, groupY - ccf, 0)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX + 1, groupY - ccf, 10)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX + 1, groupY - ccf, 11)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 7)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 8)

            }
            5 -> {
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX + 1, groupY - ccf, 9)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX + 1, groupY - ccf, 10)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX + 1, groupY - ccf, 11)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1 - ccf, 1)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1 - ccf, 2)
            }
            6 -> {
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX + 1, groupY - ccf, 9)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX + 1, groupY - ccf, 10)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1 - ccf, 0)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1 - ccf, 1)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1 - ccf, 2)
            }
            7 -> {
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 3)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 4)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1 - ccf, 0)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1 - ccf, 1)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1 - ccf, 11)
            }
            8 -> {
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 2)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 3)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 4)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1 - ccf, 0)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1 - ccf, 11)
            }
            9 -> {
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 1)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 2)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 3)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1 - ccf, 5)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1 - ccf, 6)
            }
            10 -> {
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 1)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 2)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1 - ccf, 4)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1 - ccf, 5)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1 - ccf, 6)
            }
            11 -> {
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX - 1, groupY - ccf, 8)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX - 1, groupY - ccf, 7)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1 - ccf, 3)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1 - ccf, 4)
                dstArray[offset++] =
                    KisrhombilleCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1 - ccf, 5)
            }
            else -> throw IllegalStateException()
        }

        assert(offset == neighboursCount + dstOffset)
    }

}

class KisrhombilleGridFactory @Inject constructor() : GridFactory {
    override fun createGrid(
        canvasWidth: Float,
        canvasHeight: Float,
        displayDensity: Float,
        cellSize: Int
    ) = HexagonBasedGridFactoryHelper.createGrid(
        polygonCount = 12,
        canvasWidth = canvasWidth,
        canvasHeight = canvasHeight,
        displayDensity = displayDensity,
        cellSize = cellSize,
        correctionFactorX = 0.8f,
        correctionFactorY = 0.8f
    )
}

class KisrhombilleTilingCanvasAdapterFactory @Inject constructor() :
    TilingOnCanvas.Factory {
    override fun create(canvasWidth: Float, canvasHeight: Float, grid: Grid): TilingOnCanvas {
        return PeriodicShapesTilingOnCanvas.create(
            grid,
            canvasWidth = canvasWidth,
            canvasHeight = canvasHeight,
            shapeBuilder = KisrhombilleShapeBuilder(grid, canvasWidth, canvasHeight)
        )
    }

    private class KisrhombilleShapeBuilder(grid: Grid, canvasWidth: Float, canvasHeight: Float) :
        PeriodicShapesTilingOnCanvas.PeriodicShapeBuilder {

        private val helper = HexagonBasedPeriodicShapeBuilderHelper(
            grid = grid,
            canvasWidth = canvasWidth,
            canvasHeight = canvasHeight,
            polygonCount = 12
        )

        override fun createShape(x: Int, y: Int): PeriodicShape {
            val (p0, p1, p2, p3, p4, p5, p6) = helper.vertices(x, y)

            // midpoints
            val p7 = (p0 + p1) / 2f
            val p8 = (p1 + p2) / 2f
            val p9 = (p2 + p3) / 2f
            val p10 = (p3 + p4) / 2f
            val p11 = (p4 + p5) / 2f
            val p12 = (p5 + p0) / 2f

            // arrange points clockwise starting from p0 (i.e., the left-most vertex)
            val array = arrayOf(p0, p7, p1, p8, p2, p9, p3, p10, p4, p11, p5, p12)

            val t = KisrhombilleCellsGrouping.cellGroupIndex(x)

            return convexPolygonPeriodicShape(array[t], array[(t + 1) % 12], p6)
        }

    }
}
