package com.manuel.pagliai.tsol.screens.game


import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.view.MotionEvent
import androidx.compose.animation.animateColor
import androidx.compose.animation.core.*
import androidx.compose.foundation.layout.size
import androidx.compose.material3.ColorScheme
import androidx.compose.runtime.*
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.input.pointer.pointerInteropFilter
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import com.manuel.pagliai.tsol.TestTags
import com.manuel.pagliai.tsol.domain.*
import kotlinx.coroutines.*


fun AutomatonColors.valuesOr(colorScheme: ColorScheme): Triple<Color, Color, Color> = Triple(
    cellsColorOr(colorScheme.primary),
    gridColorOr(colorScheme.onPrimaryContainer),
    backgroundColorOr(colorScheme.primaryContainer)
)

/**
 * Class responsible to render an automaton on canvas.
 *
 * Before the automaton can be rendered, a preparation step is necessary, which must be executed in a background thread.
 *
 * Internally uses [TilingOnCanvas] instances instantiated on demand as needed.
 * Because instantiation of [TilingOnCanvas] instances is expensive [backgroundThreadDispatcher] is used
 * to do the work in background.
 *
 * Rendering the automaton is a 3 steps process.
 *
 * (i) Instantiate and configure the renderer instance. This is a one off operation
 *
 * (ii) Render preparation, either via [prepareRenderer] or [prepareRendererAsync]. This will make sure all the expensive
 *  operation are done off the main thread
 *
 * (iii) [render] where the automaton is rendered according to the params, and the render made available as a [Bitmap]
 */
class AutomatonRenderer(
    val width: Int,
    val height: Int,
    val gridStrokeWidth: Float,
    private val backgroundThreadDispatcher: CoroutineDispatcher = Dispatchers.Default
) {

    /**
     * Represents the "signature" for the tiling: if the signature changes a new [TilingOnCanvas] must be created.
     * @see [prepareRendererAsync] and [prepareRenderer]
     */
    data class TilingSignature(val grid: Grid, val cellType: CellType)

    /**
     * Set to true once the last background job for [prepareRenderer] or [prepareRendererAsync] has completed
     * and the renderer is ready to render the automaton (s.t., the [TilingSignature] being the same).
     */
    val ready: State<Boolean>
        get() = _ready

    private val _ready = mutableStateOf(false)
    private var cellsColor: Color? = null

    @Suppress("JoinDeclarationAndAssignment")
    private val cellsPaint: Paint
    private val dyingCellPaint: Paint
    private val newBornCellPaint: Paint

    private var backgroundColor: Color? = null

    private var gridColor: Color? = null
    private val gridPaint: Paint

    private var lastRenderedCells: Cells? = null
    private var lastShowNextGenCellsAnimation: Boolean? = null

    /**
     * Used to draw the grid (i.e., just the border of each cell).
     * redrawn when necessary
     */
    private val gridBitmap: Bitmap
    private val gridCanvas: Canvas

    /**
     * Used to draw the automaton.
     */
    private val buffBitmap: Bitmap
    private val buffCanvas: Canvas

    var tilingOnCanvas: TilingOnCanvas? = null
    private var tilingCanvasAdapterTilingSignature: TilingSignature? = null
    private var lastTilingCanvasAdapterSignature: TilingSignature? = null
    private var tilingCanvasAdapterJob: Job? = null

    init {
        this.cellsPaint = Paint().apply {
            style = Paint.Style.FILL
            strokeCap = Paint.Cap.BUTT
        }

        this.dyingCellPaint = Paint().apply {
            style = Paint.Style.FILL
            strokeCap = Paint.Cap.BUTT
        }

        this.newBornCellPaint = Paint().apply {
            style = Paint.Style.FILL
            strokeCap = Paint.Cap.BUTT
        }

        this.gridPaint = Paint().apply {
            style = Paint.Style.STROKE
            strokeCap = Paint.Cap.BUTT
            strokeJoin = Paint.Join.MITER
            strokeWidth = gridStrokeWidth
            isAntiAlias = true
        }

        buffBitmap = Bitmap.createBitmap(
            width,
            height,
            Bitmap.Config.ARGB_8888
        )
        buffCanvas = Canvas(buffBitmap)
        gridBitmap = Bitmap.createBitmap(
            width,
            height,
            Bitmap.Config.ARGB_8888
        )
        gridCanvas = Canvas(gridBitmap)
    }


    /**
     * Preliminary step necessary before rendering.
     * [ready] is set when the job has completed]
     *
     * Unlike [prepareRenderer], the method returns immediately, and a coroutine is launched in [coroutineScope]
     */
    fun prepareRendererAsync(coroutineScope: CoroutineScope, cellType: CellType, grid: Grid): Job? {
        val tilingSignature = TilingSignature(grid = grid, cellType = cellType)
        if (tilingSignature != tilingCanvasAdapterTilingSignature) {
            _ready.value = false
            // the signature has changed: we need to create a new tiling canvas adapter
            // this could be an expensive operation, and it needs to be executed on a background thread
            val prevJob = tilingCanvasAdapterJob
            val job = coroutineScope.launch {
                prevJob?.cancelAndJoin()
                val newTilingCanvasAdapter = withContext(backgroundThreadDispatcher) {
                    tilingSignature.cellType.createTilingOnCanvas(
                        canvasWidth = width.toFloat(),
                        canvasHeight = height.toFloat(),
                        tilingSignature.grid
                    )
                }
                tilingOnCanvas = newTilingCanvasAdapter
                tilingCanvasAdapterTilingSignature = tilingSignature
                withContext(Dispatchers.Main) {
                    _ready.value = true
                }
            }
            tilingCanvasAdapterJob = job
            return job
        }
        return null
    }

    /**
     * Similar to [prepareRendererAsync], but uses an existing coroutine to suspend instead of spawning an asynchronous jobs.
     */
    suspend fun prepareRenderer(cellType: CellType, grid: Grid) {
        val tilingSignature = TilingSignature(grid = grid, cellType = cellType)
        if (tilingSignature != tilingCanvasAdapterTilingSignature) {
            withContext(Dispatchers.Main) {
                _ready.value = false
            }
            val newTilingCanvasAdapter = withContext(backgroundThreadDispatcher) {
                tilingSignature.cellType.createTilingOnCanvas(
                    canvasWidth = width.toFloat(),
                    canvasHeight = height.toFloat(),
                    tilingSignature.grid
                )
            }
            withContext(Dispatchers.Main) {
                tilingOnCanvas = newTilingCanvasAdapter
                tilingCanvasAdapterTilingSignature = tilingSignature
                _ready.value = true
            }
        }
    }

    /**
     * Method used to render the automaton. Once the automaton has been rendered, the render is made available as a bitmap.
     *
     * Internally a [tilingOnCanvas] is used. [tilingOnCanvas] must be kept in sync with [Grid] and [cellType] of the game.
     * If either [Grid] or [cellType] changes, a new [tilingOnCanvas] must be created. Because this operation is expensive
     * it must be executed off the main thread. @see [prepareRendererAsync], [prepareRenderer], and [ready]
     *
     * Assumes either [prepareRenderer] or [prepareRendererAsync] was called and [ready] to be true
     * evey time either [Cells.grid] or [cellType] changes.
     */
    fun render(
        currentGenCells: Cells,
        nextGenCells: Cells,
        cellType: CellType,
        showNextGenCellsAnimation: Boolean,
        cellsColor: Color,
        backgroundColor: Color,
        gridColor: Color,
        newBornCellsColor: Color?,
        dyingCellsColor: Color?,
        onRenderBitmap: (Bitmap) -> Unit
    ) {
        assert(nextGenCells.grid == currentGenCells.grid)
        assert(ready.value)

        var redrawGrid = false
        var redrawCells = showNextGenCellsAnimation ||
                lastRenderedCells !== currentGenCells ||
                lastShowNextGenCellsAnimation != showNextGenCellsAnimation

        val tilingSignature = TilingSignature(
            currentGenCells.grid,
            cellType = cellType
        )

        if (lastTilingCanvasAdapterSignature != tilingSignature) {
            redrawGrid = true
            redrawCells = true
        }

        if (cellsColor != this.cellsColor) {
            redrawCells = true
            this.cellsColor = cellsColor
            this.cellsPaint.color = cellsColor.toArgb()
        }

        if (backgroundColor != this.backgroundColor
            || gridColor != this.gridColor
        ) {

            this.backgroundColor = backgroundColor
            this.gridColor = gridColor
            this.gridPaint.color = gridColor.toArgb()
            redrawCells = true
            redrawGrid = true

        }

        if (redrawGrid) {
            gridBitmap.eraseColor(android.graphics.Color.TRANSPARENT)
            (0 until currentGenCells.size).forEach { cellCoordinates ->
                tilingOnCanvas!!.onGridLines(cellCoordinates) { borderLines ->
                    gridCanvas.drawLines(borderLines, gridPaint)
                }
            }
        }


        if (redrawCells) {
            buffCanvas.drawColor(backgroundColor.toArgb())
            if (!showNextGenCellsAnimation) {
                (0 until currentGenCells.size).forEach { cellCoordinates ->
                    if (currentGenCells[cellCoordinates]) {
                        tilingOnCanvas!!.onCellPaths(coordinates = cellCoordinates) { cellPath ->
                            buffCanvas.drawPath(cellPath, cellsPaint)
                        }
                    }
                }
            } else {
                newBornCellPaint.color = newBornCellsColor!!.toArgb()
                dyingCellPaint.color = dyingCellsColor!!.toArgb()
                (0 until currentGenCells.size).forEach { cellCoordinates ->
                    val current = currentGenCells[cellCoordinates]
                    val next = nextGenCells[cellCoordinates]
                    if (current && next) { // alive here and in the next gen
                        tilingOnCanvas!!.onCellPaths(coordinates = cellCoordinates) { cellPath ->
                            buffCanvas.drawPath(cellPath, cellsPaint)
                        }
                    } else if (current && !next) { // alive, but will die next
                        tilingOnCanvas!!.onCellPaths(coordinates = cellCoordinates) { cellPath ->
                            buffCanvas.drawPath(cellPath, dyingCellPaint)
                        }
                    } else if (!current && next) { // dead but will become alive
                        tilingOnCanvas!!.onCellPaths(coordinates = cellCoordinates) { cellPath ->
                            buffCanvas.drawPath(cellPath, newBornCellPaint)
                        }
                    }
                }
            }
            buffCanvas.drawBitmap(gridBitmap, 0f, 0f, null)
        }

        onRenderBitmap(buffBitmap)
        lastRenderedCells = currentGenCells
        lastShowNextGenCellsAnimation = showNextGenCellsAnimation
        lastTilingCanvasAdapterSignature = tilingSignature
    }

    fun recycle() {
        buffBitmap.recycle()
        gridBitmap.recycle()
    }

}


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun AutomatonComponent(
    size: IntSize,
    currentGenCells: Cells,
    nextGenCells: Cells,
    cellType: CellType,
    showNextGenCellsAnimation: Boolean,
    onCellSelected: (Int) -> Unit,
    renderer: AutomatonRenderer,
    onAutomatonRenderCompleted: () -> Unit,
    modifier: Modifier = Modifier,
    cellsColor: Color,
    backgroundColor: Color,
    gridColor: Color,
    rippleColor: Color,
    newBornCellColor: Color
) {

    // ripple.
    // To help with UX a "ripple" effect is shown to reflect user input.
    // Implemented from scratch as the indication framework does not do the job.
    // Not perfect, but it does the job IMO.

    val rippleAnimDurationMs = 300
    val rippleAnimDelayMs = 30
    var rippleCenter: Offset? by remember { mutableStateOf(null) }
    var showRipple: Boolean by remember { mutableStateOf(false) }
    val t = updateTransition(targetState = showRipple, label = "ripple_transition")

    val rippleRadius by t.animateDp(label = "ripple_radius", transitionSpec = {
        if (targetState) {
            tween(
                durationMillis = rippleAnimDurationMs,
                delayMillis = rippleAnimDelayMs,
                easing = LinearOutSlowInEasing
            )
        } else {
            snap()
        }
    }) {
        if (it) 90.dp else 0.dp
    }

    val rippleAlpha by t.animateFloat(label = "ripple_alpha", transitionSpec = {
        if (targetState) {
            tween(
                durationMillis = rippleAnimDurationMs,
                delayMillis = rippleAnimDelayMs,
                easing = LinearEasing
            )
        } else {
            snap()
        }
    }) {
        if (it) 0f else 1f
    }

    if (rippleAlpha == 0f) {
        showRipple = false
    }

    fun onNewTouchOffset(offset: Offset) {
        if (!showRipple) {
            rippleCenter = offset
            showRipple = true
        }
    }

    // nex gen animation

    val animationDurationMs = 4000
    var dyingCellColorState: State<Color>? = null
    var newBornCellColorState: State<Color>? = null

    if (showNextGenCellsAnimation) {
        val infiniteTransition = rememberInfiniteTransition()

        dyingCellColorState = infiniteTransition.animateColor(
            initialValue = cellsColor,
            targetValue = backgroundColor,
            animationSpec = infiniteRepeatable(
                animation = tween(durationMillis = animationDurationMs),
                repeatMode = RepeatMode.Reverse
            )
        )

        newBornCellColorState = infiniteTransition.animateColor(
            initialValue = backgroundColor,
            targetValue = newBornCellColor,
            animationSpec = infiniteRepeatable(
                animation = tween(durationMillis = animationDurationMs),
                repeatMode = RepeatMode.Reverse
            )
        )
    }

    // draw automaton + any animation

    androidx.compose.foundation.Canvas(
        modifier = modifier
            .size(width = size.width.dp, height = size.height.dp)
            .pointerInteropFilter {
                return@pointerInteropFilter when (it.action) {
                    MotionEvent.ACTION_DOWN -> {
                        onNewTouchOffset(Offset(it.x, it.y))
                        onTouchEvent(renderer.tilingOnCanvas, it.x, it.y, onCellSelected)
                    }
                    MotionEvent.ACTION_MOVE -> {
                        onNewTouchOffset(Offset(it.x, it.y))
                        onTouchEvent(renderer.tilingOnCanvas, it.x, it.y, onCellSelected)
                    }
                    MotionEvent.ACTION_UP -> {
                        false
                    }
                    else -> false
                }
            }
            .testTag(TestTags.AUTOMATON_CANVAS),
        onDraw = {
            renderer.render(
                backgroundColor = backgroundColor,
                cellsColor = cellsColor,
                cellType = cellType,
                currentGenCells = currentGenCells,
                gridColor = gridColor,
                nextGenCells = nextGenCells,
                showNextGenCellsAnimation = showNextGenCellsAnimation,
                newBornCellsColor = newBornCellColorState?.value,
                dyingCellsColor = dyingCellColorState?.value
            ) { bitmap ->
                drawImage(bitmap.asImageBitmap())
                if (showRipple && rippleCenter != null) {
                    drawCircle(
                        color = rippleColor,
                        center = rippleCenter!!,
                        radius = rippleRadius.value,
                        alpha = rippleAlpha
                    )
                }
                onAutomatonRenderCompleted()
            }
        })
}

private fun onTouchEvent(
    tilingOnCanvas: TilingOnCanvas?,
    x: Float,
    y: Float,
    onCellSelected: (Int) -> Unit
): Boolean {
    return tilingOnCanvas?.coordinates(x, y)?.let { coordinates ->
        onCellSelected(coordinates)
        true
    } ?: false
}

