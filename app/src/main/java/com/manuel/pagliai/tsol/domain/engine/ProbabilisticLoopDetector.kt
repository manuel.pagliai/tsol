package com.manuel.pagliai.tsol.domain.engine

import com.manuel.pagliai.tsol.domain.Cells
import java.util.*

/**
 * Detects loops in an automaton run.
 *
 * A loop is defined as a sequence of configuration of the automaton cells C0 ... CK-1, CK
 * with C0 = CK i.e., the automaton ends up repeating itself indefinitely.
 *
 * Detection should be disabled for non deterministic games: [com.manuel.pagliai.tsol.domain.GameRules.isDeterministicGame]
 *
 * True negative are unlikely but possible for large values of [nbits]
 * False positive are not possible
 *
 * N.B. Because of periodic boundaries, loops are  inevitable, because
 * the number of configuration is finite (< 2^(grid.size)). In practice however, it makes sense
 * to detect loops and signal to the user as soon as a looping config has been reached.
 *
 * Instances are not thread safe.
 */
class ProbabilisticLoopDetector(private val nbits: Int) {
    private var enabled: Boolean = true
    private val encountered = BitSet(nbits)
    var looping = false
        private set

    fun reset(cells: Cells, enabled: Boolean) {
        this.enabled = enabled
        encountered.clear()
        looping = false
        addGen(cells)
    }

    fun addGen(cells: Cells) {
        if (!enabled) return
        val i = kotlin.math.abs(cells.hashCode() % nbits)
        // keep assigning the looping field: in case of a false positive, at least we will (most likely)
        // fix it in the next generation.
        looping = encountered[i]
        encountered[i] = true
    }
}
