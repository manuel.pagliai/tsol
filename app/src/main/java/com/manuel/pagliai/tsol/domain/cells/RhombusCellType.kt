package com.manuel.pagliai.tsol.domain.cells

import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.domain.CellType
import com.manuel.pagliai.tsol.domain.GameRules.Companion.B3S24
import com.manuel.pagliai.tsol.domain.Grid
import com.manuel.pagliai.tsol.domain.GridFactory
import com.manuel.pagliai.tsol.domain.TilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShape
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShapesTilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.convexPolygonPeriodicShape
import javax.inject.Inject


private val RhombusCellsGrouping = HorizontalCellsGrouping(3)

class RhombusCellType @Inject constructor(
    override val gridFactory: RhombusGridFactory,
    override val tilingOnCanvasFactory: RhombusTilingOnCanvasFactory
) : CellType {

    override val name = R.string.rhombus_cell_type
    override val icon = R.drawable.ic_rhombus_cell_type
    override val id = "RHOMBUS"
    override val neighboursCount = 10
    override val defaultGameRules = B3S24

    override fun setNeighboursCoordinates(
        grid: Grid,
        x: Int,
        y: Int,
        dstArray: IntArray,
        dstOffset: Int
    ) {
        val (groupX, groupY) = RhombusCellsGrouping.groupXY(x, y)
        val cellGroupIndex = RhombusCellsGrouping.cellGroupIndex(x)
        var offset = dstOffset

        (0..2).forEach {
            if (it != cellGroupIndex) {
                dstArray[offset++] = grid.toLinear(x - cellGroupIndex + it, groupY)
            }
        }

        val ccf = groupX % 2 // correction factor

        when (cellGroupIndex) {
            0 -> {
                dstArray[offset++] =
                    RhombusCellsGrouping.toCellLinear(grid, groupX - 1, groupY - ccf, 1)
                dstArray[offset++] =
                    RhombusCellsGrouping.toCellLinear(grid, groupX - 1, groupY - ccf, 2)
                dstArray[offset++] =
                    RhombusCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1 - ccf, 0)
                dstArray[offset++] =
                    RhombusCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1 - ccf, 1)
                dstArray[offset++] =
                    RhombusCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 1)
                dstArray[offset++] =
                    RhombusCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 2)
                dstArray[offset++] =
                    RhombusCellsGrouping.toCellLinear(grid, groupX + 1, groupY - ccf, 0)
                dstArray[offset++] =
                    RhombusCellsGrouping.toCellLinear(grid, groupX + 1, groupY - ccf, 2)
            }
            1 -> {
                dstArray[offset++] =
                    RhombusCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 2)
                dstArray[offset++] =
                    RhombusCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 1)
                dstArray[offset++] =
                    RhombusCellsGrouping.toCellLinear(grid, groupX + 1, groupY - ccf, 0)
                dstArray[offset++] =
                    RhombusCellsGrouping.toCellLinear(grid, groupX + 1, groupY - ccf, 2)
                dstArray[offset++] =
                    RhombusCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1 - ccf, 0)
                dstArray[offset++] =
                    RhombusCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1 - ccf, 2)
                dstArray[offset++] =
                    RhombusCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 0)
                dstArray[offset++] =
                    RhombusCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 1)
            }
            2 -> {
                dstArray[offset++] =
                    RhombusCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1 - ccf, 0)
                dstArray[offset++] =
                    RhombusCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1 - ccf, 2)
                dstArray[offset++] =
                    RhombusCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 0)
                dstArray[offset++] =
                    RhombusCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 1)
                dstArray[offset++] =
                    RhombusCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1 - ccf, 0)
                dstArray[offset++] =
                    RhombusCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1 - ccf, 1)
                dstArray[offset++] =
                    RhombusCellsGrouping.toCellLinear(grid, groupX - 1, groupY - ccf, 1)
                dstArray[offset++] =
                    RhombusCellsGrouping.toCellLinear(grid, groupX - 1, groupY - ccf, 2)
            }
            else -> throw IllegalStateException()
        }
        assert(offset == neighboursCount + dstOffset)
    }

}

class RhombusGridFactory @Inject constructor() : GridFactory {
    override fun createGrid(
        canvasWidth: Float,
        canvasHeight: Float,
        displayDensity: Float,
        cellSize: Int
    ) = HexagonBasedGridFactoryHelper.createGrid(
        polygonCount = 3,
        canvasWidth = canvasWidth,
        canvasHeight = canvasHeight,
        displayDensity = displayDensity,
        cellSize = cellSize,
        correctionFactorX = 0.5f,
        correctionFactorY = 0.5f
    )
}

class RhombusTilingOnCanvasFactory @Inject constructor() :
    TilingOnCanvas.Factory {
    override fun create(canvasWidth: Float, canvasHeight: Float, grid: Grid): TilingOnCanvas {
        return PeriodicShapesTilingOnCanvas.create(
            grid,
            canvasWidth = canvasWidth,
            canvasHeight = canvasHeight,
            shapeBuilder = RhombusPeriodicShapeBuilder(grid, canvasWidth, canvasHeight)
        )
    }

    private class RhombusPeriodicShapeBuilder(grid: Grid, canvasWidth: Float, canvasHeight: Float) :
        PeriodicShapesTilingOnCanvas.PeriodicShapeBuilder {

        private val hexagonBasedPeriodicShapeBuilderHelper = HexagonBasedPeriodicShapeBuilderHelper(
            polygonCount = 3,
            grid = grid,
            canvasWidth = canvasWidth,
            canvasHeight = canvasHeight
        )

        override fun createShape(x: Int, y: Int): PeriodicShape {
            val (p0, p1, p2, p3, p4, p5, p6) = hexagonBasedPeriodicShapeBuilderHelper.vertices(x, y)

            return when (RhombusCellsGrouping.cellGroupIndex(x)) {
                0 -> convexPolygonPeriodicShape(p0, p1, p2, p6)
                1 -> convexPolygonPeriodicShape(p2, p3, p4, p6)
                2 -> convexPolygonPeriodicShape(p4, p5, p0, p6)
                else -> throw java.lang.IllegalStateException()
            }
        }
    }
}
