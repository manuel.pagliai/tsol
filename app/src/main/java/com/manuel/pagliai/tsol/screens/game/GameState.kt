package com.manuel.pagliai.tsol.screens.game

import androidx.compose.ui.graphics.Color
import com.manuel.pagliai.tsol.domain.*
import com.manuel.pagliai.tsol.domain.engine.GameEngine
import com.manuel.pagliai.tsol.domain.engine.ScreenOrientation
import com.manuel.pagliai.tsol.screens.game.GameState.Companion.MAX_CELL_SIZE
import com.manuel.pagliai.tsol.screens.game.GameState.Companion.MAX_SPEED
import com.manuel.pagliai.tsol.screens.game.GameState.Companion.MIN_CELL_SIZE
import com.manuel.pagliai.tsol.screens.game.GameState.Companion.MIN_SPEED


/**
 * Controls enabled status and effect of buttons in the control toolbar
 */
data class GameControls(

    /**
     * True -> add alive cells
     * False -> kill cells
     */
    val addCellsOnSelection: Boolean,

    /**
     * True -> the user can 'undo' last action
     */
    val canUndo: Boolean,

    /**
     * True -> the user can 'redo' last action
     */
    val canRedo: Boolean,

    /**
     * True if the game is currently running
     */
    val isPlaying: Boolean,

    /**
     * True if can advance to the next generation of the automaton via a button press
     */
    val canNextGen: Boolean,

    /**
     * True if, via the button press, the automaton can be restored to the previous generation
     */
    val canPrevGen: Boolean,

    /**
     * If True, when the user presses this button a dialog to change the [CellType] will be shown
     */
    val canChangeCellType: Boolean,

    /**
     * True -> clear the automaton, killing each alive cell
     * False -> restore the automaton to the last edited configuration
     */
    val canClear: Boolean
) {
    companion object {
        val DEFAULT = GameControls(
            isPlaying = false,
            canPrevGen = false,
            canNextGen = false,
            canClear = false,
            canChangeCellType = false,
            canRedo = false,
            canUndo = false,
            addCellsOnSelection = true
        )
    }
}

/**
 * Represents the game and its control as they should be displayed on the screen.
 */
data class GameState(

    /**
     * The name of the game, as inserted by the user.
     */
    val gameName: String,

    /**
     * Identifier of the cell type used in the game.
     * A cell type identifies how cells are drawn on the screen, as well as the neighbours of each cell.
     */
    val cellType: CellType,

    /**
     * The cells grid, as last configured by the user. This
     * will only take into account user's edit such as add cells/remove cells ...
     *
     * It will not change when the automaton is running.
     */
    val lastEditedCells: Cells,

    /**
     * Cells as currently displayed on the screen. This is the state of the automaton at the current generation.
     */
    val currentGenCells: Cells,

    /**
     * The cells for the next generation of the automaton. Null if the automaton has not yet initialised.
     */
    val nextGenCells: Cells,

    /**
     * The generation of this automaton.
     */
    val generation: Int,

    /**
     * The rules of the game: Identifies how the next generation of the automaton is evaluated.
     */
    val rules: GameRules,

    /**
     * The ratio of width and height of the canvas used to display the game.
     * Used when displaying a preview of the game.
     *
     * If we do not maintain this aspect ratio, the automaton will appear "squashed" when displaying
     * the same game in contexts that are different from the one the automaton was originally created in.
     */
    val previewAspectRatio: Float,

    /**
     * see [com.manuel.pagliai.tsol.domain.engine.ScreenOrientation]
     */
    val screenOrientation: ScreenOrientation,

    /**
     * Identifies the user preference on how big the cells are with respect to the screen.
     * Lower the value the more the number of cells drawn on the screen.
     * Must be within [MIN_CELL_SIZE] and [MAX_CELL_SIZE].
     */
    val cellSize: Int,

    /**
     * Identifies how fast the automaton reaches the next generation when playing.
     * Must be within [MIN_SPEED] and [MAX_SPEED].
     */
    val speed: Int,

    /**
     * Timestamp of when the automaton was first created.
     */
    val createdAt: Long,

    /**
     * Timestamp of when the automaton was last edited.
     */
    val modifiedAt: Long,

    /**
     * Controls enabled status and effect of buttons in the control toolbar.
     */
    val controls: GameControls,

    /**
     * True if it has been detected that the automaton is running in a loop.
     */
    val isLooping: Boolean,

    /**
     * True if a preview of the next gen should be shown. This is usually the case when
     * the user does not do anything for a while, and an animation showing a transition from the [currentGenCells] to [nextGenCells]
     */
    val showNextGenPreview: Boolean,

    /**
     * Assigned once the game has been saved.
     */
    val gameId: Long? = null,

    val automatonColors: AutomatonColors
) {

    init {
        assert(currentGenCells.grid == nextGenCells.grid)
        assert(currentGenCells.grid == lastEditedCells.grid)
        assert(modifiedAt >= createdAt)
        assert(!isLooping || rules.isDeterministicGame)
    }


    companion object {
        const val MIN_CELL_SIZE = 0
        val MAX_CELL_SIZE
            get() = GridFactory.MAX_CELL_SIZE

        /**
         * @see [com.manuel.pagliai.tsol.repository.preferences.PreferencesState.newGamesDefaultCellSize]
         */
        val NEW_GAMES_DEFAULT_CELL_SIZE_PREFERENCE_DEFAULT: Int
            get() = MAX_CELL_SIZE / 2

        const val MIN_SPEED = 0
        val MAX_SPEED: Int
            get() = GameEngine.MAX_SPEED

        /**
         * @see [com.manuel.pagliai.tsol.repository.preferences.PreferencesState.newGamesDefaultSpeed]
         */
        val NEW_GAMES_DEFAULT_SPEED_PREFERENCE_DEFAULT: Int
            get() = MAX_SPEED / 2
    }
}

/**
 * Colors that should be used to render the automaton.
 *
 * Because colors in the settings might not be applied (see [GameSettings], colors to be used
 * are exposed via method receiving the theme colors.
 *
 * @see [GameSettings]
 * @see [AutomatonColors.from]
 * @see [cellsColorOr], [gridColorOr], [backgroundColorOr]
 */
data class AutomatonColors(
    private val cellsColor: Color?,
    private val gridColor: Color?,
    private val backgroundColor: Color?
) {
    init {
        if (cellsColor == null) {
            assert(gridColor == null)
            assert(backgroundColor == null)
        } else {
            requireNotNull(gridColor)
            requireNotNull(backgroundColor)
        }
    }

    /**
     * @return the colors to be used to render the cells shapes
     * [systemThemeColor] the color to be used to render the cell in case system colors should be used
     * (as opposed to the color set in the preference)
     */
    fun cellsColorOr(systemThemeColor: Color) = cellsColor ?: systemThemeColor

    /**
     * Same as [cellsColorOr] but for the grid color
     */
    fun gridColorOr(systemThemeColor: Color) = gridColor ?: systemThemeColor

    /**
     * Same as [cellsColorOr] but for the background color
     */
    fun backgroundColorOr(systemThemeColor: Color) = backgroundColor ?: systemThemeColor

    /**
     * @return true of the system color should be used to render cells, grid and background.
     * False if the colors set in the preferences should be used instead.
     */
    fun usingSystemThemeForCellsColor() = cellsColor == null

    companion object {
        fun from(gameSettings: GameSettings) = AutomatonColors(
            cellsColor = gameSettings.cellsColor,
            gridColor = gameSettings.gridColor,
            backgroundColor = gameSettings.backgroundColor,
        )
    }
}
