package com.manuel.pagliai.tsol.domain.cells

import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.domain.*
import com.manuel.pagliai.tsol.domain.GameRules.Companion.B4S234
import com.manuel.pagliai.tsol.domain.GridFactory.Companion.stdGridFactory
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShape
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShapesTilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.Vertex
import com.manuel.pagliai.tsol.domain.tiling.convexPolygonPeriodicShape
import javax.inject.Inject

private val Triangular14CellsGrouping = HorizontalCellsGrouping(4)

class Triangle14CellType @Inject constructor(
    override val gridFactory: Triangular14GridFactory,
    override val tilingOnCanvasFactory: Triangular14TilingOnCanvasFactory
) : CellType {

    override val name = R.string.triangular_14_cell_type
    override val icon = R.drawable.ic_triangular_14_cell_type
    override val id = "TRIANGULAR_14"
    override val neighboursCount = 14
    override val defaultGameRules: GameRules = B4S234

    override fun setNeighboursCoordinates(
        grid: Grid,
        x: Int,
        y: Int,
        dstArray: IntArray,
        dstOffset: Int
    ) {
        val (groupX, groupY) = Triangular14CellsGrouping.groupXY(x, y)
        var offset = dstOffset
        when (Triangular14CellsGrouping.cellGroupIndex(x)) {
            0 -> {
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX, groupY - 1, 3)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX, groupY - 1, 1)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX - 1, groupY - 1, 2)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX - 1, groupY - 1, 3)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX - 1, groupY, 0)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX - 1, groupY, 2)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX, groupY, 1)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX, groupY, 3)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX, groupY, 2)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX + 1, groupY, 1)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX + 1, groupY, 0)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX + 1, groupY - 1, 3)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX + 1, groupY - 1, 1)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX, groupY - 1, 2)
            }
            1 -> {
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX, groupY - 1, 3)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX, groupY - 1, 1)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX - 1, groupY - 1, 2)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX - 1, groupY - 1, 3)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX - 1, groupY, 0)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX - 1, groupY, 2)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX - 1, groupY, 3)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1, 0)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1, 2)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX, groupY + 1, 1)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX, groupY + 1, 0)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX, groupY, 3)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX, groupY, 2)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX, groupY, 0)
            }
            2 -> {
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX, groupY - 1, 3)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX, groupY - 1, 2)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX + 1, groupY - 1, 1)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX + 1, groupY - 1, 3)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX + 1, groupY, 0)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX + 1, groupY, 1)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX + 1, groupY, 3)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1, 0)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1, 1)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX, groupY + 1, 2)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX, groupY + 1, 0)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX, groupY, 3)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX, groupY, 1)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX, groupY, 0)
            }
            3 -> {
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX - 1, groupY, 2)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX - 1, groupY, 3)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1, 0)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1, 2)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX, groupY + 1, 0)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX, groupY + 1, 2)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX, groupY + 1, 1)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1, 0)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1, 1)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX + 1, groupY, 1)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX + 1, groupY, 3)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX, groupY, 2)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX, groupY, 1)
                dstArray[offset++] =
                    Triangular14CellsGrouping.toCellLinear(grid, groupX, groupY, 0)
            }
        }
        assert(offset == dstOffset + neighboursCount)
    }
}

class Triangular14GridFactory @Inject constructor() : GridFactory {
    override fun createGrid(
        canvasWidth: Float,
        canvasHeight: Float,
        displayDensity: Float,
        cellSize: Int
    ) = stdGridFactory(
        canvasWidth = canvasWidth,
        canvasHeight = canvasHeight,
        displayDensity = displayDensity,
        cellSize = cellSize,
        colGroupCount = 4,
        rowGroupCount = 1,
        correctionFactorX = 2f,
        correctionFactorY = 0.5f
    )
}


class Triangular14TilingOnCanvasFactory @Inject constructor() :
    TilingOnCanvas.Factory {
    override fun create(canvasWidth: Float, canvasHeight: Float, grid: Grid): TilingOnCanvas {
        return PeriodicShapesTilingOnCanvas.create(
            grid,
            canvasWidth = canvasWidth,
            canvasHeight = canvasHeight,
            shapeBuilder = Triangle14PeriodicShapeBuilder(grid, canvasWidth, canvasHeight)
        )
    }

    private class Triangle14PeriodicShapeBuilder(
        grid: Grid,
        canvasWidth: Float,
        canvasHeight: Float
    ) : PeriodicShapesTilingOnCanvas.PeriodicShapeBuilder {

        private val squareWidth: Float = 4 * (canvasWidth / grid.columns)
        private val squareHeight: Float = canvasHeight / grid.rows

        override fun createShape(x: Int, y: Int): PeriodicShape {

            val (groupX, groupY) = Triangular14CellsGrouping.groupXY(x, y)

            val p0 = Vertex(groupX * squareWidth, groupY * squareHeight) // top left
            val p1 = Vertex((groupX + 1) * squareWidth, groupY * squareHeight) // top right
            val p2 = Vertex((groupX + 1) * squareWidth, (groupY + 1) * squareHeight) // bottom right
            val p3 = Vertex(groupX * squareWidth, (groupY + 1) * squareHeight) // bottom left
            val p4 = Vertex((groupX + 0.5f) * squareWidth, (groupY + 0.5f) * squareHeight) // center

            return when (Triangular14CellsGrouping.cellGroupIndex(x)) {
                0 -> convexPolygonPeriodicShape(
                    p0,
                    p1,
                    p4
                )
                1 -> convexPolygonPeriodicShape(
                    p0,
                    p3,
                    p4
                )
                2 -> convexPolygonPeriodicShape(
                    p1,
                    p2,
                    p4
                )
                3 -> convexPolygonPeriodicShape(
                    p3,
                    p2,
                    p4
                )
                else -> throw IllegalStateException()
            }
        }
    }
}