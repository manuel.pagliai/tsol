package com.manuel.pagliai.tsol.domain.engine

import com.manuel.pagliai.tsol.domain.SavedGame
import com.manuel.pagliai.tsol.repository.games.GamesRepository
import com.manuel.pagliai.tsol.screens.game.GameState
import javax.inject.Inject

/**
 * Save/Updates a game
 */
class LifeSaver @Inject constructor(
    private val gamesRepository: GamesRepository
) {
    /**
     * @return the id of the game saved. That is the same as
     * [GameState.gameId] if present, or a new id to be associated to the game.
     */
    suspend fun save(gameState: GameState): Long {
        val savedGame = SavedGame.from(gameState)
        val id = gamesRepository.saveGame(savedGame)
        assert(savedGame.id == null || id == savedGame.id)
        return id
    }
}
