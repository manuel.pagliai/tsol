package com.manuel.pagliai.tsol.domain.engine

import androidx.annotation.VisibleForTesting
import com.manuel.pagliai.tsol.domain.*
import com.manuel.pagliai.tsol.domain.cells.CellTypes
import com.manuel.pagliai.tsol.domain.engine.GameEngine.Companion.DELAYS
import com.manuel.pagliai.tsol.screens.game.GameState
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.util.concurrent.ExecutorService
import javax.inject.Inject
import javax.inject.Qualifier
import kotlin.random.Random


@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class GameEngineThread

/**
 * Controls the updating and evolution of the game.
 *
 * The game itself is represented by [GameModel]
 *
 * The game can be observed via [gameState]
 *
 * The engine needs to be initialised before it can be used.
 *
 * @see [initialiseNewGame]
 * @see [initialiseFromSavedGame]
 *
 * The automaton being represented in dependent from the canvas used to display the automaton:
 * @see GridFactory
 *
 * When the automaton is playing, the engine represent 'half' of the game loop:
 * The 'other half' is implemented in [com.manuel.pagliai.tsol.screens.game.AutomatonComponent]
 *
 *  (i)   The engine emits a new automaton via [gameState]
 *  (ii)  The [gameState] updated is detected and rendered on the screen
 *  (iii) Once the automaton has been rendered, [onAutomatonRenderCompleted] is called
 *  (iv)  The engine will 'sleep' according to the [GameModel.speed], [GameModel.extraDelay] and [DELAYS]
 *  (v)   The next generation of the automaton is evaluated
 *  (vi)  The new automaton is emitted via [gameState]
 *
 */
class GameEngine @Inject constructor(
    /**
     * Used to run long running/background operations.
     */
    @GameEngineThread
    private val engineExecutorService: ExecutorService,
    private val cellTypes: CellTypes,
    private val lifeSaver: LifeSaver,

    /**
     * The only source of truth of what the game is.
     * Always accessed (both reading and updating) while
     * holding [modelMutex]
     *
     * Not visible outside the engine, but exposed via [gameState]
     */
    private val _model: GameModel,

    /**
     * Used to animate the next gen transition when idling (s.t. user preference)
     */
    private val idleTracker: IdleTracker,
    private val random: Random
) {

    companion object {
        /**
         * Maximum allowed value for the speed of the game.
         * @see [GameState.speed]
         */
        val MAX_SPEED: Int
            get() = DELAYS.size - 1

        /**
         * These are the delays to schedule updates when the automaton is running.
         * A larger delay means a slower execution.
         *
         * @see [scheduleNextUpdate].
         *
         * N.B. this control the amount of time the engine waits
         * before start evaluating the next generation of the automaton
         * once [onAutomatonRenderCompleted] has been called. The amount of time
         * between two subsequent generation would be larger than the interval here.
         */
        @VisibleForTesting
        val DELAYS = arrayOf(
            750L,
            625L,
            550L,
            500L,
            450L,
            400L,
            350L,
            325L,
            300L,
            275L,
            250L,
            225L,
            200L,
            175L,
            150L,
            125,
            110L,
            100L,
            90L,
            80L,
            75L,
            70L,
            60L,
            50L,
            45L,
            40L
        )
    }

    /**
     * Used to guarantee mutual exclusion for reading/updating [_model]
     */
    private val modelMutex = Mutex()

    /**
     * If not null, this is the job for the coroutine for the next generation update
     */
    private var nextUpdateJob: Job? = null

    /**
     * Used to dispatch long running or background operations
     */
    private val engineDispatcher = engineExecutorService.asCoroutineDispatcher()

    /**
     * Exposes the game via so that it can be visualised.
     * [gameState] is what needs to be observed by the view/view model.
     */
    private val _gameState: MutableStateFlow<GameState?> = MutableStateFlow(null)
    val gameState: Flow<GameState?>
        get() = _gameState

    /**
     * Initialises the engine for a new game.
     * A new automaton will be created and saved according to the parameters
     *
     * Either this method or [initialiseFromSavedGame] needs to be called
     * before the engine can be used.
     */
    suspend fun initialiseNewGame(
        name: String,
        canvasWidth: Float,
        canvasHeight: Float,
        displayDensity: Float,
        orientation: ScreenOrientation,
        gameSettings: GameSettings
    ) {
        val cellType =
            if (gameSettings.randomiseNewGamesCells) cellTypes.list.random(random) else cellTypes.defaultCellType
        val cellSize = gameSettings.newGamesDefaultCellSize
        val speed = gameSettings.newGamesDefaultSpeed

        val grid = cellType.createGrid(
            canvasWidth,
            canvasHeight,
            displayDensity,
            cellSize
        )
        val cells = if (gameSettings.randomiseNewGamesCells) {
            Cells(grid) { random.nextBoolean() }
        } else {
            Cells(grid = grid)
        }

        // randomise ?
        val gameRules = cellType.defaultGameRules

        modelUpdate {
            it.initialiseNewGame(
                name,
                cells,
                gameRules = gameRules,
                cellSize = cellSize,
                previewAspectRatio = canvasWidth / canvasHeight,
                orientation = orientation,
                cellType = cellType,
                gameSettings = gameSettings,
                speed = speed
            )
        }
    }

    /**
     * Initialises the engine with a previously saved game.
     * When the game is updated, the changes will be reflected in the game persisted on disk
     * with the same id.
     *
     * Either this method or [initialiseFromSavedGame] needs to be called
     * before the engine can be used.
     */
    suspend fun initialiseFromSavedGame(savedGame: SavedGame, gameSettings: GameSettings) {
        modelUpdate {
            it.initialiseFromSavedGame(savedGame, gameSettings)
        }
    }

    suspend fun cellSelected(coordinates: Int) {
        modelUpdate {
            it.cellSelected(coordinates)
        }
    }

    /**
     * Makes the automaton advance to the next generation (if possible).
     * This method is associated to the user action
     *
     * @see [GameModel.nextGen]
     * @see [GameState.controls]
     * @see [com.manuel.pagliai.tsol.screens.game.GameControls.canNextGen]
     *
     * @see [scheduleNextUpdate] for advancing to the next gen when the automaton is playing.
     */
    suspend fun nextGen() {
        modelUpdate {
            it.nextGen()
        }
    }

    /**
     * Makes the automaton go back to the previous generation (if possible).
     * This method is associated to the user action
     *
     * @see [GameModel.prevGen]
     * @see [GameState.controls]
     * @see [com.manuel.pagliai.tsol.screens.game.GameControls.canPrevGen]
     */
    suspend fun prevGen() {
        modelUpdate {
            it.prevGen()
        }
    }

    /**
     * Update cells properties of the automaton.
     *
     * The user wants to update the [CellType], [GameRules] or both to use in the game.
     * Cell size will stay the same. @see [changeCellSize] for that
     */
    suspend fun changeCellTypeOrRules(
        newCellType: CellType,
        newRules: GameRules,
        canvasWidth: Float,
        canvasHeight: Float,
        displayDensity: Float
    ) {
        modelUpdate { model ->
            val newCells =
                if (newCellType != model.cellType) {
                    val grid = newCellType.createGrid(
                        canvasWidth,
                        canvasHeight,
                        displayDensity,
                        model.cellSize
                    )
                    Cells(grid = grid)
                } else {
                    model.currentGenCells
                }
            model.updateCells(newCellType = newCellType, newCells = newCells, newRules = newRules)
        }
    }

    /**
     * User wants to update the cell size. [CellType] and [GameRules] will stay the same.
     * @see [changeCellTypeOrRules]
     */
    suspend fun changeCellSize(
        newSize: Int,
        canvasWidth: Float,
        canvasHeight: Float,
        displayDensity: Float
    ) {
        modelUpdate { model ->
            if (newSize != model.cellSize) {
                val newGrid =
                    model.cellType.createGrid(canvasWidth, canvasHeight, displayDensity, newSize)
                model.projectCellsToNewGrid(newGrid, newSize)
            }
        }
    }

    /**
     * Used to re-render the current automaton on a new canvas. Depending
     * on the change, the new automaton might be smaller and some cells being lost.
     *
     * An example use case for this method is the user rotating the device.
     */
    suspend fun projectToNewCanvas(
        canvasWidth: Float,
        canvasHeight: Float,
        displayDensity: Float,
        orientation: ScreenOrientation
    ) {
        modelUpdate { model ->
            val newGrid =
                model.cellType.createGrid(canvasWidth, canvasHeight, displayDensity, model.cellSize)
            model.projectCellsToNewGrid(newGrid, model.cellSize)
            model.updateCanvasProperties(orientation, canvasWidth / canvasHeight)
        }
    }

    /**
     * Updates the speed at which the automaton is playing: i.e., how fast the automaton
     * advances to the next generation (or how small the delay between two subsequent next generation evaluation is)
     * while the automaton is running.
     */
    suspend fun changeSpeed(newSpeed: Int) {
        // always update: if new speed is the same we still need to be aware of user activity
        modelUpdate {
            it.speed = newSpeed
        }
    }

    /**
     * If @param [addCellsOnSelection] is true, when [cellSelected] is invoked, the cell will be become alive
     * (if not already alive), if false when [cellSelected] is invoked, the cell will be killed (if not already dead).
     */
    suspend fun setAddCellsOnSelection(addCellsOnSelection: Boolean) {
        modelUpdate {
            it.addCellsOnSelection = addCellsOnSelection
        }
    }

    /**
     * Undo the last user action if possible.
     *
     * @see [redo]
     * @see [GameModel.canUndo]
     * @see [com.manuel.pagliai.tsol.screens.game.GameControls.canUndo]
     */
    suspend fun undo() {
        modelUpdate {
            it.undo()
        }
    }

    /**
     * Re-do of the last undone user action if possible.
     *
     * @see [undo]
     * @see [GameModel.canRedo]
     * @see [com.manuel.pagliai.tsol.screens.game.GameControls.canRedo]
     */
    suspend fun redo() {
        modelUpdate {
            it.redo()
        }
    }

    /**
     * Starts playing the automaton. The automaton will automatically advance to the next generation
     * according to [GameModel.speed].
     *
     * Some updates with the automaton are still possible. For example, the user is still allowed to give life
     * or kill cells.
     *
     * In order for the game loop to work correctly, [onAutomatonRenderCompleted] must be called in
     * order for the next update to be scheduled (@see class docs).
     *
     * @see [pause]
     * @see [scheduleNextUpdate]
     */
    suspend fun play() {
        val started = modelMutex.withLock {
            val started = !_model.isPlaying
            _model.isPlaying = true
            _model.notifyUserActivity()
            _gameState.emit(_model.gameState)
            started
        }

        if (started) {
            scheduleNextUpdate()
        }

        idleTracker.reset()
    }

    /**
     * The automaton will stop updating. The most recent generation of the automaton is still visible on screen
     *
     * @see [play]
     * @see [scheduleNextUpdate]
     * @see [cancelNextUpdate]
     */
    suspend fun pause() {
        val stopped = modelMutex.withLock {
            val stopped = _model.isPlaying
            _model.isPlaying = false
            _model.notifyUserActivity()
            _gameState.emit(_model.gameState)
            stopped
        }

        if (stopped) {
            cancelNextUpdate()
        }

        idleTracker.reset()
    }

    /**
     * Stores the new settings in the model. This method needs to be called every time the user updates the preferences
     * in the settings page.
     */
    suspend fun updateSettings(gameSettings: GameSettings) {
        modelUpdate { it.updateSettings(gameSettings) }
    }

    /**
     * Creates a copy of the current game. The new game will be saved with a new [GameModel.gameId] and
     * @param [newName]. The new game will be the one visible on screen.
     * The current game (i.e., the game displayed when this method is called is left unchanged on disk).
     *
     * @see [rename]
     */
    suspend fun makeCopy(newName: String) {
        modelUpdate {
            it.gameId = null
            it.name = newName
        }
    }

    /**
     * Changes the name of the game. Unlike [makeCopy] no new game will be created ([GameModel.gameId] will stay the same).
     */
    suspend fun rename(newName: String) {
        modelUpdate {
            it.name = newName
        }
    }

    /**
     * If enabled, kills all alive cells on screen.
     * @see [GameModel.canClear]
     * @see [restoreLastEdited]
     */
    suspend fun clearCells() {
        modelUpdate {
            it.clearCells()
        }
    }

    /**
     * If enabled, restore the last automaton edited by the user: any change in the cells that is due
     * to the automaton advancing generation will be reverted.
     *
     * @see [GameModel.canClear]
     * @see [GameModel.canRestoreLastEdited]
     */
    suspend fun restoreLastEdited() {
        modelUpdate {
            it.restoreLastEdited()
        }
    }

    /**
     * Notifies the engine that the current automaton rendering cycle is completed, and if the automaton is playing
     * the next cycle should start.
     *
     * @see [GameEngine] class docs
     * @see [scheduleNextUpdate]
     */
    suspend fun onAutomatonRenderCompleted() {
        if (isPlaying()) {
            scheduleNextUpdate()
        }
    }

    private suspend fun cancelNextUpdate() {
        nextUpdateJob?.cancel()
        nextUpdateJob?.join()
    }

    /**
     * Notifies the engine about the user not performing any action in a while (and the game is to be considered
     * in an idle state).
     *
     * @see [GameModel.showNextGenPreview]
     */
    suspend fun notifyIdle() {
        if (_gameState.value == null) {
            idleTracker.reset()
        } else {
            modelUpdate(userActivity = false) { gameModel ->
                gameModel.notifyIdle()
            }
        }
    }

    /**
     * We need to be notified when the game screen gets launched, and consider the navigation
     * to the game screen as a user activity. Otherwise if the user idles the game, navigate to
     * another screen (e.g., settings), and then navigate back to the game screen the game will still
     * be mistakenly be rendered as idle (when clearly there is user activity going on).
     */
    suspend fun gameScreenLaunched() {
        modelUpdate {
            it.notifyUserActivity()
        }
        idleTracker.reset()
    }

    /**
     * If the next generation is not already scheduled, starts a coroutine to evaluate the next generation of the automaton
     * after a delay. This is part of the game loop while the game is playing.
     *
     * @see [GameEngine] class docs.
     */
    private suspend fun scheduleNextUpdate() {

        if (nextUpdateJob?.isCompleted == false) {
            return
        }

        nextUpdateJob = withContext(engineDispatcher) {
            launch {
                delay(nextGenTime() - System.currentTimeMillis())
                if (extraDelay()) {
                    delay(nextGenDelay() / 2)
                }
                modelUpdate { model ->
                    model.extraDelay = false
                    if (model.isPlaying) {
                        model.nextGen()
                        model.nextGenTime = System.currentTimeMillis() + DELAYS[model.speed]
                    }
                }
            }
        }
    }

    private suspend fun isPlaying(): Boolean = retrieveFromModel { it.isPlaying }

    private suspend fun extraDelay(): Boolean = retrieveFromModel { it.extraDelay }

    private suspend fun nextGenTime() = retrieveFromModel { it.nextGenTime }

    private suspend fun nextGenDelay() = DELAYS[retrieveFromModel { it.speed }]

    private suspend inline fun <T> retrieveFromModel(retrieveFunction: (GameModel) -> T): T =
        modelMutex.withLock {
            retrieveFunction(_model)
        }

    /**
     * Performs an update of [_model] while holding the mutes.
     * Be aware that the [modelMutex] is not reentrant
     *
     * Also takes care of triggering the model to be saved on persisted storage.
     *
     * @param [update] the update to be performed on the model.
     * @param [userActivity] true if the model update is due to some kind of user input. False
     *      otherwise (e.g., the device is left idle, and we need to update the model to start displaying
     *      the transition to the next generation].
     */
    private suspend fun modelUpdate(
        userActivity: Boolean = true,
        update: (GameModel) -> Unit
    ) {
        withContext(engineDispatcher) {
            modelMutex.withLock {
                val oldState = _model.gameState
                update(_model)
                val newState = _model.gameState
                if (userActivity) {
                    _model.notifyUserActivity()
                }

                if (newState != oldState) {
                    _model.updateTimestamp()

                    val deferredSaveAction = async {
                        return@async lifeSaver.save(newState!!)
                    }
                    if (_model.gameId == null) {
                        _model.gameId = deferredSaveAction.await()
                    }
                }
                _gameState.emit(newState)
            }
        }

        if (userActivity) {
            idleTracker.reset()
        }
    }

    /**
     * True if the engine has been initialised and there is a game set.
     *
     * @see [initialiseNewGame]
     * @see [initialiseFromSavedGame]
     */
    fun isInitialised() = _model.gameState != null
}