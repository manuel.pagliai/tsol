package com.manuel.pagliai.tsol.domain.cells

import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.domain.CellType
import com.manuel.pagliai.tsol.domain.GameRules.Companion.B3S23
import com.manuel.pagliai.tsol.domain.Grid
import com.manuel.pagliai.tsol.domain.GridFactory
import com.manuel.pagliai.tsol.domain.GridFactory.Companion.stdGridFactory
import com.manuel.pagliai.tsol.domain.TilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShape
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShapesTilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.Vertex
import com.manuel.pagliai.tsol.domain.tiling.unionOfConvexPolygons
import javax.inject.Inject

class ArrowCellType @Inject constructor(
    override val gridFactory: ArrowGridFactory,
    override val tilingOnCanvasFactory: ArrowTilingOnCanvasFactory
) : CellType {

    override val name = R.string.arrow_cell_type
    override val icon = R.drawable.ic_arrow_cell_type_24
    override val id = "ARROW"
    override val neighboursCount = 8
    override val defaultGameRules = B3S23

    override fun setNeighboursCoordinates(
        grid: Grid,
        x: Int,
        y: Int,
        dstArray: IntArray,
        dstOffset: Int
    ) {
        var offset = dstOffset

        dstArray[offset++] = grid.toLinear(x + 1, y)
        dstArray[offset++] = grid.toLinear(x + 2, y)
        dstArray[offset++] = grid.toLinear(x - 1, y)
        dstArray[offset++] = grid.toLinear(x - 2, y)
        dstArray[offset++] = grid.toLinear(x, y - 1)
        dstArray[offset++] = grid.toLinear(x, y + 1)

        if (x % 2 == 0) { // ltr
            dstArray[offset++] = grid.toLinear(x + 1, y - 1)
            dstArray[offset++] = grid.toLinear(x - 1, y - 1)
        } else { // rtl
            dstArray[offset++] = grid.toLinear(x + 1, y + 1)
            dstArray[offset++] = grid.toLinear(x - 1, y + 1)
        }

        assert(offset == neighboursCount + dstOffset)
    }
}

class ArrowGridFactory @Inject constructor() : GridFactory {
    override fun createGrid(
        canvasWidth: Float,
        canvasHeight: Float,
        displayDensity: Float,
        cellSize: Int
    ) = stdGridFactory(
        canvasWidth = canvasWidth,
        canvasHeight = canvasHeight,
        displayDensity = displayDensity,
        cellSize = cellSize,
        rowGroupCount = 3,
        colGroupCount = 2,
        correctionFactorX = 1.4f,
        correctionFactorY = 0.8f
    )
}

class ArrowTilingOnCanvasFactory @Inject constructor() :
    TilingOnCanvas.Factory {
    override fun create(canvasWidth: Float, canvasHeight: Float, grid: Grid): TilingOnCanvas {
        return PeriodicShapesTilingOnCanvas.create(
            grid,
            canvasWidth = canvasWidth,
            canvasHeight = canvasHeight,
            shapeBuilder = ArrowPeriodicShapeBuilder(grid, canvasWidth, canvasHeight)
        )
    }

    private class ArrowPeriodicShapeBuilder(grid: Grid, canvasWidth: Float, canvasHeight: Float) :
        PeriodicShapesTilingOnCanvas.PeriodicShapeBuilder {
        // N.B. The shape is not convex: it is represented as the union of a rectangle and a triangle.
        // check the assets folder for more info.

        private val a = canvasWidth / grid.columns
        private val b = canvasHeight / (2 * grid.rows)

        override fun createShape(x: Int, y: Int): PeriodicShape {

            val p0 = Vertex(x = (x / 2) * 2 * a, y = y * 2 * b)
            val p1 = p0.xShift(a).roundX()
            val p2 = p1.yShift(b)
            val p3 = p0.copy(y = p2.y)
            val p4 = p1.yShift(-b / 2)
            val p5 = Vertex(p0.x + 2 * a, p0.y + b / 2).roundX()
            val p6 = p2.yShift(b / 2)

            val p7 = p5.copy(y = p2.y)
            val p8 = p7.yShift(b)
            val p9 = p8.yShift(b / 2)

            val p10 = p7.xShift(a)
            val p11 = p10.copy(y = p8.y)


            if (x % 2 == 0) { // left to right
                return unionOfConvexPolygons {
                    polygon(p0) {
                        to(p1, true)
                        to(p2, false)
                        to(p3, true)
                        close(true)
                    }

                    polygon(p1) {
                        to(p4, true)
                        to(p5, true)
                        to(p6, true)
                        to(p2, true)
                        close(false)
                    }
                }
            } else { // right to left
                return unionOfConvexPolygons {
                    polygon(p6) {
                        to(p5, true)
                        to(p7, true)
                        to(p8, false)
                        to(p9, true)
                        close(true)
                    }

                    polygon(p7) {
                        to(p10, true)
                        to(p11, true)
                        to(p8, true)
                        close(false)
                    }
                }
            }
        }
    }
}
