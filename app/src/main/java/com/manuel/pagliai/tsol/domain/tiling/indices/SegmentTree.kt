package com.manuel.pagliai.tsol.domain.tiling.indices

import com.manuel.pagliai.tsol.BuildConfig

import java.util.*

/**
 * An interval, intended as the portion of a line between an [inf] and a [sup],
 * augmented with a [payload].
 *
 * [inf] and [sup] are included in the interval according to [infClosed] and [supClosed] respectively
 *
 * For example:
 *      [3,5] is Segment(inf=3, sup=5, infClosed = true, supClosed = true, payload = Unit)
 *      [3,5) is Segment(inf=3, sup=5, infClosed = true, supClosed = false, payload = Unit)
 *      (3,5) is Segment(inf=3, sup=5, infClosed = false, supClosed = false, payload = Unit)
 *
 * A [payload] is used to associate use-case dependent information to segments in the [SegmentTree]
 */
data class Segment<T>(
    val inf: Float,
    val sup: Float,
    val infClosed: Boolean,
    val supClosed: Boolean,
    val payload: T
) {
    init {
        assert(inf <= sup)
    }

    fun contains(x: Float) = ((infClosed && x >= inf) || x > inf) &&
            ((supClosed && x <= sup) || x < sup)


    fun contains(other: Segment<*>) =
        (((this.infClosed || !other.infClosed) && this.inf <= other.inf) || this.inf < other.inf) &&
                (((this.supClosed || !other.supClosed) && this.sup >= other.sup) || this.sup > other.sup)


    fun intersect(other: Segment<*>): Boolean {
        return ((this.infClosed && other.supClosed && this.inf <= other.sup) || this.inf < other.sup) &&
                ((this.supClosed && other.infClosed && this.sup >= other.inf) || this.sup > other.inf)
    }
}

/**
 * The payload associated to a Segments used to build the [SegmentTree]: Nodes in the tree are represented
 * as [Segment] using instances of this class as [Segment.payload].
 */
class SegmentTreeNodePayload<T>(
    val leftChild: Segment<SegmentTreeNodePayload<T>>? = null,
    val rightChild: Segment<SegmentTreeNodePayload<T>>? = null
) {

    /**
     * The set of [Segment]s to be associated to this node. The segments will be a subset of the [Segment]s used
     * to build the tree (as opposed to the [Segment]s representing the tree).
     */
    val canonicalSet: MutableList<Segment<T>> = arrayListOf()

    var parent: Segment<SegmentTreeNodePayload<T>>? = null
        set(value) {
            assert(field == null) // parent can only be set once.
            field = value
        }
}

typealias SegmentTreeNode<T> = Segment<SegmentTreeNodePayload<T>>

/**
 * A [SegmentTree] is a data structure optimised to retrieve all stored intervals containing
 * a given point
 *
 * The tree should not be changed after it has been created.
 *
 * This is a vanilla implementation of a Segment tree as described in the Wikipedia article.
 * @see <a href="https://en.wikipedia.org/wiki/Segment_tree"/>
 *
 * A payload can be associated to the intervals of the tree: @see [Segment]
 */
class SegmentTree<T> private constructor(val root: SegmentTreeNode<T>) {

    /**
     * Invokes @param [cons] on all intervals containing @param [point]
     */
    inline fun onQueryResult(point: Float, cons: (Segment<T>) -> Unit) {
        var n: SegmentTreeNode<T> = root

        while_loop@ while (true) {
            if (n.contains(point)) {
                n.payload.canonicalSet.forEach(cons)
            }
            n = if (n.payload.leftChild?.contains(point) == true) {
                n.payload.leftChild!!
            } else if (n.payload.rightChild?.contains(point) == true) {
                n.payload.rightChild!!
            } else {
                break@while_loop
            }
        }
    }

    companion object {

        /**
         * Builds a new [SegmentTree] from a list of [Segment]. It will not be possible to add other
         * [Segment] once the tree has been built.
         */
        fun <T> create(segments: List<Segment<T>>, levelsChecks: Boolean = false): SegmentTree<T> {

            assert(segments.isNotEmpty()) { "segments cannot be empty" }

            // collecting points

            val points = TreeSet<Float>()

            segments.forEach {
                points.add(it.inf)
                points.add(it.sup)
            }

            var level = LinkedList<SegmentTreeNode<T>>()

            // build elementary intervals (leaves)

            val pointsIterator = points.iterator()

            var p0: Float = pointsIterator.next()

            level.add(leafForPoint(p0))

            while (pointsIterator.hasNext()) {
                val p1 = pointsIterator.next()
                level.add(
                    openLeaf(
                        p0,
                        p1
                    )
                )
                level.add(
                    leafForPoint(
                        p1
                    )
                )
                p0 = p1
            }

            // build the tree

            do {

                if (BuildConfig.DEBUG && levelsChecks) {
                    for (n0 in level) {
                        for (n1 in level) {
                            if (n1 != n0) {
                                if (n1.intersect(n0)) {
                                    error("Assertion failed")
                                }
                            }
                        }
                    }
                }

                val upperLevel = LinkedList<SegmentTreeNode<T>>()
                val it = level.iterator()
                while (it.hasNext()) {
                    val left = it.next()
                    if (it.hasNext()) {
                        val right = it.next()
                        val parent = union(left, right)
                        left.payload.parent = parent
                        right.payload.parent = parent
                        upperLevel.add(parent)
                    } else {
                        upperLevel.add(level.pollLast()!!)
                    }
                }
                level = upperLevel
            } while (level.size != 1)

            // add canonical intervals

            val root = level[0]

            segments.forEach { s ->
                val q = LinkedList<SegmentTreeNode<T>>()
                q.add(root)
                do {
                    val n = q.pop()

                    if (s.contains(n)) {
                        n.payload.canonicalSet.add(s)
                        continue
                    }

                    if (n.payload.leftChild?.intersect(s) == true) {
                        q.add(n.payload.leftChild)
                    }
                    if (n.payload.rightChild?.intersect(s) == true) {
                        q.add(n.payload.rightChild)
                    }

                } while (q.isNotEmpty())
            }

            return SegmentTree(root)
        }

        /**
         * @return a leaf [SegmentTreeNode] with associated interval [[point], [point]]
         */
        private fun <T> leafForPoint(point: Float): SegmentTreeNode<T> =
            Segment(
                inf = point,
                sup = point,
                infClosed = true,
                supClosed = true,
                payload = SegmentTreeNodePayload(
                    leftChild = null,
                    rightChild = null
                )
            )

        /**
         * @return a leaf [SegmentTreeNode] associated to the interval ([inf], [sup])
         */
        private fun <T> openLeaf(inf: Float, sup: Float) = SegmentTreeNode<T>(
            inf = inf,
            sup = sup,
            infClosed = false,
            supClosed = false,
            payload = SegmentTreeNodePayload(
                leftChild = null,
                rightChild = null
            )
        )

        /**
         * @return the union between [left] and [right] nodes.
         * Assumes the two intervals are consecutive and disjoint
         */
        private fun <T> union(
            left: SegmentTreeNode<T>,
            right: SegmentTreeNode<T>
        ): SegmentTreeNode<T> {

            assert(left.sup == right.inf)
            assert(!left.intersect(right))

            return Segment(
                inf = left.inf,
                sup = right.sup,
                infClosed = left.infClosed,
                supClosed = right.supClosed,
                payload = SegmentTreeNodePayload(
                    leftChild = left,
                    rightChild = right
                )
            )
        }
    }
}


