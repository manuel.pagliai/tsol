package com.manuel.pagliai.tsol.screens.welcome

import android.annotation.SuppressLint
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.Screens
import com.manuel.pagliai.tsol.TestTags
import com.manuel.pagliai.tsol.domain.SavedGame
import com.manuel.pagliai.tsol.domain.engine.ScreenOrientation
import com.manuel.pagliai.tsol.screens.common.ThreeDotsMenu
import com.manuel.pagliai.tsol.screens.common.ThreeDotsMenuItem
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.text.DateFormat
import java.text.DecimalFormat
import java.util.*

/**
 * This is the first screen the user will see when launching the app.
 *
 * If there are no games saved, a message showing 'tap on the + button to create a new game' should be shown.
 * If there are saved games, a galley of games should be shown.
 *
 * Regardless, the user is able to create a new game by clicking on the + button.
 *
 */
@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@ExperimentalFoundationApi
@Composable
fun WelcomeScreen(
    navController: NavController,
    viewModel: WelcomeScreenViewModel = hiltViewModel()
) {
    val snackbarHostState = remember { SnackbarHostState() }
    val scope = rememberCoroutineScope()
    val games = viewModel.allGames.value
    var createNewGameDialog by remember { mutableStateOf(false) }

    Scaffold(
        topBar = {
            WelcomeScreenTopBar(
                onSettings = { navController.navigate(Screens.Settings.route) },
                onAbout = { navController.navigate(Screens.About.route) }
            )
        },
        snackbarHost = { SnackbarHost(snackbarHostState) },
        floatingActionButton = { // the + button to create new games.
            FloatingActionButton(
                onClick = { createNewGameDialog = true },
                modifier = Modifier.testTag(TestTags.CREATE_NEW_GAME_BUTTON)
            ) {
                Icon(
                    imageVector = Icons.Default.Add,
                    contentDescription = stringResource(R.string.new_game_cd)
                )
            }
        },
    ) {
        if (games.isEmpty()) {
            TapPlusToCreateANewGame(modifier = Modifier.fillMaxWidth())
        } else {
            GameGallery(
                viewModel,
                scope,
                games,
                navController,
                showSnackbar = { msg -> snackbarHostState.showSnackbar(msg) }
            )
        }
        if (createNewGameDialog) {
            navController.navigate(Screens.Game.route)
            createNewGameDialog = false
        }
    }
}

@Composable
fun TapPlusToCreateANewGame(modifier: Modifier = Modifier) {
    Column(modifier = modifier) {
        Spacer(modifier = Modifier.weight(1f))
        Image(
            painterResource(id = R.drawable.ic_loading_icon_a),
            contentDescription = stringResource(id = R.string.app_logo_icon_cd),
            contentScale = ContentScale.FillHeight,
            colorFilter = ColorFilter.tint(color = MaterialTheme.colorScheme.primary.copy(alpha = 0.7f)),
            modifier = Modifier
                .weight(4f)
                .align(CenterHorizontally)
        )
        Spacer(modifier = Modifier.weight(0.5f))
        Column(
            modifier = modifier
                .weight(1f)
                .align(CenterHorizontally)
        ) {
            Text(
                text = stringResource(id = R.string.you_dont_have_any_games), modifier = Modifier
                    .align(CenterHorizontally)
            )
            Text(
                text = stringResource(id = R.string.tap_plus_to_create_one), modifier = Modifier
                    .align(CenterHorizontally)
            )
        }
        Spacer(modifier = Modifier.weight(1f))

    }
}

/**
 * The grid of [SavedGame]s. The user will be able to open/inspect/delete any of the game in the repository.
 *
 * @see [SavedGameDetailsItem]
 */
@OptIn(
    ExperimentalMaterial3Api::class,
    ExperimentalFoundationApi::class
)
@Composable
private fun GameGallery(
    viewModel: WelcomeScreenViewModel,
    scope: CoroutineScope,
    games: List<SavedGame>,
    navController: NavController,
    showSnackbar: suspend (String) -> Unit
) {

    /**
     * Used when deleting a game (asking for confirmation in a dialog)
     */
    var gameBeingDeleted: SavedGame? by remember { mutableStateOf(null) }

    /**
     * Used when showing more information about a game
     */
    var showDetailsForGame: SavedGame? by remember { mutableStateOf(null) }

    /**
     * Used when the user tries to load a game in the wrong orientation.
     *
     * @see [com.manuel.pagliai.tsol.domain.engine.ScreenOrientation]
     */
    var wrongOrientationDialog: ScreenOrientation? by remember { mutableStateOf(null) }

    val screenOrientation =
        ScreenOrientation.fromConfigurationOrientation(LocalConfiguration.current.orientation)

    // dialogs:
    if (wrongOrientationDialog != null) {
        AlertDialog(
            title = {
                Text(
                    stringResource(id = R.string.rotate_your_device_to_open_game_dialog_title),
                    style = MaterialTheme.typography.headlineMedium
                )
            },
            text = {
                val titleId = if (wrongOrientationDialog == ScreenOrientation.LANDSCAPE) {
                    R.string.rotate_your_device_to_open_game_dialog_text_landscape
                } else {
                    R.string.rotate_your_device_to_open_game_dialog_text_portrait
                }
                Text(stringResource(titleId), style = MaterialTheme.typography.bodyMedium)
            },
            onDismissRequest = { wrongOrientationDialog = null },
            confirmButton = {
                Row(
                    horizontalArrangement = Arrangement.Center,
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Button(onClick = { wrongOrientationDialog = null }) {
                        Text(stringResource(id = R.string.got_it))
                    }
                }
            }
        )
    }

    showDetailsForGame?.let {
        SavedGameDetailsDialog(
            game = it,
            onDismiss = { showDetailsForGame = null }
        )
    }

    gameBeingDeleted?.let {
        val msg = stringResource(
            id = R.string.game_deleted_snackbar,
            it.name
        )

        ConfirmGameDeletionDialog(
            it.name,
            { gameBeingDeleted = null },
            {
                viewModel.onDelete(it)
                gameBeingDeleted = null

                scope.launch {
                    showSnackbar(msg)
                }
            }
        )
    }

    // Game gallery grid:
    LazyVerticalGrid(
        columns = GridCells.Fixed(2), // 2 columns grid `
        contentPadding = PaddingValues(8.dp),
        modifier = Modifier.padding(top = 56.dp)
    ) {
        this.items(games.size) { gameIndex ->
            Box(Modifier.padding(8.dp)) {
                SavedGameItem(
                    savedGame = games[gameIndex],
                    animatedPreviewsCache = viewModel.animatedPreviewsCache,
                    automatonColors = viewModel.automatonColors.value,
                    onDelete = {
                        gameBeingDeleted = games[gameIndex]
                    },
                    onSelect = {
                        if (screenOrientation == games[gameIndex].orientation) {
                            // load the game.
                            navController.navigate(Screens.Game.route + "?gameId=${games[gameIndex].id}")
                        } else {
                            // cannot load the game because we are in the wrong orientation. Show a dialog instead.
                            wrongOrientationDialog = games[gameIndex].orientation
                        }
                    },
                    onDetails = {
                        showDetailsForGame = games[gameIndex]
                    }
                )
            }
        }
    }
}

/**
 * Displays the game details, time it was created, cell rules ....
 */
@OptIn(ExperimentalFoundationApi::class)
@ExperimentalMaterial3Api
@Composable
fun SavedGameDetailsDialog(
    game: SavedGame,
    probabilityFormat: DecimalFormat = DecimalFormat("#.###"),
    onDismiss: () -> Unit
) {

    Dialog(onDismissRequest = onDismiss) {
        Card(modifier = Modifier.fillMaxWidth()) {
            Column(modifier = Modifier.padding(8.dp)) {
                Text(text = game.name, style = MaterialTheme.typography.titleMedium)

                LazyVerticalGrid(
                    columns = GridCells.Fixed(2),
                    verticalArrangement = Arrangement.spacedBy(16.dp),
                    horizontalArrangement = Arrangement.spacedBy(16.dp),
                    contentPadding = PaddingValues(16.dp)
                ) {
                    item {
                        SavedGameDetailsItem(
                            stringResource(id = R.string.cell_type_label),
                            stringResource(id = game.cellType.name),
                        )
                    }

                    item {
                        SavedGameDetailsItem(
                            stringResource(id = R.string.neighbours_count_label),
                            game.cellType.neighboursCount.toString(),
                        )
                    }

                    item {
                        SavedGameDetailsItem(
                            stringResource(id = R.string.born_label),
                            game.rules.birthWith.joinToString(", ")
                        )
                    }

                    item {
                        SavedGameDetailsItem(
                            stringResource(id = R.string.survive_label),
                            game.rules.surviveWith.joinToString(", ")
                        )
                    }

                    if (game.rules.pRandomBirth > 0 || game.rules.pRandomDeath > 0) {
                        item {
                            SavedGameDetailsItem(
                                stringResource(id = R.string.random_birth_probability),
                                probabilityFormat.format(game.rules.pRandomBirth)
                            )
                        }

                        item {
                            SavedGameDetailsItem(
                                stringResource(id = R.string.random_death_probability),
                                probabilityFormat.format(game.rules.pRandomDeath)
                            )
                        }

                    }

                    item {
                        SavedGameDetailsItem(
                            stringResource(id = R.string.created),
                            formatDate(game.createdAt)
                        )
                    }

                    item {
                        SavedGameDetailsItem(
                            stringResource(id = R.string.modified),
                            formatDate(game.modifiedAt)
                        )
                    }
                }
            }
        }
    }
}

private fun formatDate(date: Long): String =
    DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(Date(date))

/**
 * An item shown in the game details dialog
 * @see [SavedGameDetailsDialog]
 */
@Composable
fun SavedGameDetailsItem(
    label: String,
    value: String,
    modifier: Modifier = Modifier,
    labelStyle: TextStyle = MaterialTheme.typography.labelSmall,
    valueStyle: TextStyle = MaterialTheme.typography.bodySmall
) {
    Column(modifier = modifier) {
        Text(
            text = label,
            style = labelStyle
        )
        Spacer(modifier = Modifier.height(4.dp))
        Text(text = value, style = valueStyle)
    }
}

@Composable
fun WelcomeScreenTopBar(
    onSettings: () -> Unit,
    onAbout: () -> Unit
) {
    TopAppBar(
        title = {
            Text(
                stringResource(id = R.string.app_name_long),
                style = MaterialTheme.typography.titleLarge,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis
            )
        },
        backgroundColor = MaterialTheme.colorScheme.background,
        actions = {
            ThreeDotsMenu(
                items = listOf(
                    ThreeDotsMenuItem.ClickableLabel(
                        stringResource(id = R.string.settings),
                        onSettings
                    ),
                    ThreeDotsMenuItem.ClickableLabel(stringResource(id = R.string.about), onAbout)
                )
            )
        }
    )
}

@Composable
fun ConfirmGameDeletionDialog(
    gameName: String,
    onDismissRequest: () -> Unit,
    onConfirm: () -> Unit,
    onCancel: () -> Unit = onDismissRequest
) {
    AlertDialog(
        onDismissRequest = onDismissRequest,
        title = {
            Text(text = stringResource(id = R.string.delete_game_confirmation, gameName))
        },
        confirmButton = {
            Button(
                onClick = onConfirm
            ) {
                Text(stringResource(id = R.string.delete))
            }
        },
        dismissButton = {
            Button(
                onClick = onCancel
            ) {
                Text(stringResource(id = R.string.cancel_label))
            }
        }
    )
}


