package com.manuel.pagliai.tsol.screens.game

import android.app.Application
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.Screens
import com.manuel.pagliai.tsol.domain.CellType
import com.manuel.pagliai.tsol.domain.GameRules
import com.manuel.pagliai.tsol.domain.SavedGame
import com.manuel.pagliai.tsol.domain.cells.CellTypes
import com.manuel.pagliai.tsol.domain.engine.GameEngine
import com.manuel.pagliai.tsol.domain.engine.IdleTracker
import com.manuel.pagliai.tsol.domain.engine.ScreenOrientation
import com.manuel.pagliai.tsol.repository.games.GamesRepository
import com.manuel.pagliai.tsol.repository.preferences.PreferencesRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

/**
 * An event for the UI to be handled
 */
sealed class GameScreenUIEvent {

    /**
     * Signals to the ui a new game has been created
     */
    data class CopyCreated(val newName: String) : GameScreenUIEvent()

    /**
     * Signals to the ui a new game has been renamed
     */
    data class GameRenamed(val newName: String) : GameScreenUIEvent()

    /**
     * When the game is first created, we should prompt the user to set its name.
     */
    data class PromptFirstGameName(val currentGameName: String) : GameScreenUIEvent()
}


/**
 * View model for the game screen. Handles user interaction with the automaton and
 * game management (copy, rename ...).
 *
 * Also handles state/controls for all the dialogs that can be opened from the game screen.
 *
 * Most methods wraps [GameEngine] functions by running them in [viewModelScope].
 *
 * @see [GameEngine].
 */
@HiltViewModel
class GameViewModel @Inject constructor(
    /**
     * Used to load the game selected from the welcome screen if any.
     */
    private val gamesRepository: GamesRepository,

    /**
     * Used to update the engine every time the settings are updated
     */
    private val preferencesRepository: PreferencesRepository,

    /**
     * The engine itself, does most of the work of updating/handling/modelling the game.
     */
    private val engine: GameEngine,
    savedStateHandle: SavedStateHandle,
    private val cellTypes: CellTypes,

    /**
     * Instance must be shared with [engine].
     * Used to launch the 'on idle' functionality of the engine (requiring [viewModelScope])
     *
     * @see [GameEngine.notifyIdle]
     */
    idleTracker: IdleTracker,
    application: Application
) : AndroidViewModel(application) {

    val probabilityValues: FloatArray
        get() = GameRules.PROBABILITY_VALUES

    val allCellTypes: List<CellType>
        get() = cellTypes.list

    private val _gameState: MutableState<GameState?> = mutableStateOf(null)
    val gameState: State<GameState?> = _gameState

    private val _uiEventFlow = MutableSharedFlow<GameScreenUIEvent>()
    val uiEventFlow: SharedFlow<GameScreenUIEvent>
        get() = _uiEventFlow

    /**
     * @see [onCanvasMeasured]
     */
    private var canvasWidth: Float? = null
    private var canvasHeight: Float? = null
    private var displayDensity: Float? = null
    private var orientation: ScreenOrientation? = null

    /**
     * If a game should be loaded from the repository, this is the task loading the game.
     */
    private var loadGame: Deferred<SavedGame?>? = null

    init {
        val gameId =
            savedStateHandle.get<Long>(Screens.Game.GAME_ID_PARAM_NAME) ?: Screens.Game.NEW_GAME_ID
        if (gameId == Screens.Game.NEW_GAME_ID) {
            // setup new game
        } else {
            // load the game with the specified id.
            loadGame = viewModelScope.async {
                val savedGame = gamesRepository.getGameById(gameId)
                initialize(fromSavedGame = savedGame)
                savedGame
            }
        }

        idleTracker.onTimeout {
            viewModelScope.launch {
                engine.notifyIdle()
            }
        }
    }

    fun gameScreenLaunched() {
        viewModelScope.launch {
            engine.gameScreenLaunched()
        }
    }

    /**
     * The automaton characteristics are dependent on properties of the canvas used
     * to render the automaton.
     *
     * For example, with a fixed [GameState.cellSize], on a larger screen we are going to display
     * more cells than a smaller screen.
     *
     * This method should be called as soon as the canvas configuration is known and every time it changes
     */
    fun onCanvasMeasured(
        canvasWidth: Float,
        canvasHeight: Float,
        displayDensity: Float,
        orientation: ScreenOrientation
    ) {
        if (this.canvasWidth != canvasWidth || this.canvasHeight != canvasHeight || this.displayDensity != displayDensity || this.orientation != orientation) {
            this.canvasWidth = canvasWidth
            this.canvasHeight = canvasHeight
            this.displayDensity = displayDensity
            this.orientation = orientation

            if (!engine.isInitialised()) {
                // initialise the engine with the canvas properties
                initialize()
            } else {
                // the game is already initialised, project it to the new canvas
                viewModelScope.launch {
                    engine.projectToNewCanvas(
                        canvasWidth,
                        canvasHeight,
                        displayDensity,
                        orientation
                    )
                }
            }
        }
    }

    /**
     * Initialises the engine with a game.
     * Canvas properties needs to be set before this method does anything.
     *
     * N.B. When loading an existing game from the repository two conditions must be met to initialise the engine:
     * (i) canvas properties to be known and (ii) [loadGame] to be completed.
     */
    private fun initialize(fromSavedGame: SavedGame? = null) {
        if (this.canvasWidth == null) {
            return // canvas not yet ready
        }

        viewModelScope.launch {

            preferencesRepository.gameSettings.onEach {
                engine.updateSettings(it)
            }.launchIn(viewModelScope)

            val gameSettings = preferencesRepository.gameSettings.first()

            val savedGame =
                if (loadGame != null && fromSavedGame == null) {
                    loadGame?.await()
                } else {
                    fromSavedGame
                }

            loadGame = null

            if (savedGame == null) {
                val gameName = getApplication<Application>().getString(R.string.untitled_game)
                engine.initialiseNewGame(
                    gameName,
                    canvasWidth!!,
                    canvasHeight!!,
                    displayDensity!!,
                    orientation!!,
                    gameSettings
                )
                _uiEventFlow.emit(GameScreenUIEvent.PromptFirstGameName(gameName))

            } else {
                Timber.d("Initialising from saved game")
                engine.initialiseFromSavedGame(savedGame, gameSettings)
            }

            engine.gameState.onEach {
                _gameState.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun setAddCellsOnSelection() {
        viewModelScope.launch {
            engine.setAddCellsOnSelection(true)
        }
    }

    fun setRemoveCellsOnSelection() {
        viewModelScope.launch {
            engine.setAddCellsOnSelection(false)
        }
    }

    fun onCellSelected(coordinates: Int) {
        viewModelScope.launch {
            engine.cellSelected(coordinates)
        }
    }

    fun undo() {
        viewModelScope.launch {
            engine.undo()
        }
    }

    fun redo() {
        viewModelScope.launch {
            engine.redo()
        }
    }

    fun play() {
        viewModelScope.launch {
            engine.play()
        }
    }

    fun pause() {
        viewModelScope.launch {
            engine.pause()
        }
    }

    fun nextGen() {
        viewModelScope.launch {
            engine.nextGen()
        }
    }

    fun prevGen() {
        viewModelScope.launch {
            engine.prevGen()
        }
    }

    fun changeCellSize(newSize: Int) {
        val canvasWidth = this.canvasWidth
        val canvasHeight = this.canvasHeight
        val displayDensity = this.displayDensity

        if (canvasWidth == null || canvasHeight == null || displayDensity == null) {
            Timber.w("Ignoring change cell size request as canvas is not yet ready")
            return // canvas not yet ready
        }

        viewModelScope.launch {
            engine.changeCellSize(
                newSize,
                canvasWidth,
                canvasHeight,
                displayDensity
            )
        }
    }

    fun changeSpeed(newSpeed: Int) {
        viewModelScope.launch {
            engine.changeSpeed(newSpeed)
        }
    }

    fun updateCells(newCellType: CellType, newRules: GameRules) {

        val canvasWidth = this.canvasWidth
        val canvasHeight = this.canvasHeight
        val displayDensity = this.displayDensity

        if (canvasWidth == null || canvasHeight == null || displayDensity == null) {
            Timber.w("Ignoring change update cells request as canvas is not yet ready")
            return // canvas not yet ready
        }

        viewModelScope.launch {
            engine.changeCellTypeOrRules(
                newCellType,
                newRules,
                canvasWidth,
                canvasHeight,
                displayDensity
            )
        }
    }

    fun clearCells() {
        viewModelScope.launch {
            engine.clearCells()
        }
    }

    fun restore() {
        viewModelScope.launch {
            engine.restoreLastEdited()
        }
    }

    fun onAutomatonRenderCompleted() {
        viewModelScope.launch {
            engine.onAutomatonRenderCompleted()
        }
    }

    fun makeCopy(newName: String) {
        viewModelScope.launch {
            engine.makeCopy(newName)
            _uiEventFlow.emit(GameScreenUIEvent.CopyCreated(newName))
        }
    }

    fun rename(newName: String, firstGameName: Boolean = false) {
        viewModelScope.launch {
            engine.rename(newName)
            if (!firstGameName) {
                _uiEventFlow.emit(GameScreenUIEvent.GameRenamed(newName))
            }
        }
    }

}