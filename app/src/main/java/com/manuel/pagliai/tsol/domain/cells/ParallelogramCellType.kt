package com.manuel.pagliai.tsol.domain.cells

import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.domain.CellType
import com.manuel.pagliai.tsol.domain.GameRules
import com.manuel.pagliai.tsol.domain.Grid
import com.manuel.pagliai.tsol.domain.TilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShape
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShapesTilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.Vertex
import com.manuel.pagliai.tsol.domain.tiling.convexPolygonPeriodicShape
import javax.inject.Inject

/**
 * Effectively the same as [SquareCellType] but rendered differently.
 */
class ParallelogramCellType @Inject constructor(
    override val gridFactory: SquareGridFactory, // reuse existing square impl.
    override val tilingOnCanvasFactory: ParallelogramTilingOnCanvasFactory
) : CellType {

    override val name: Int = R.string.parallelogram_cell_type
    override val icon: Int = R.drawable.ic_parallelogram_cell_type
    override val id: String = "PARALLELOGRAM"
    override val neighboursCount: Int = 8
    override val defaultGameRules: GameRules = GameRules.B3S23
    override fun setNeighboursCoordinates(
        grid: Grid,
        x: Int,
        y: Int,
        dstArray: IntArray,
        dstOffset: Int
    ) {
        var offset = dstOffset

        dstArray[offset++] = grid.toLinear(x, y - 1)
        dstArray[offset++] = grid.toLinear(x + 1, y - 1)
        dstArray[offset++] = grid.toLinear(x + 1, y)
        dstArray[offset++] = grid.toLinear(x + 1, y + 1)
        dstArray[offset++] = grid.toLinear(x, y + 1)
        dstArray[offset++] = grid.toLinear(x - 1, y + 1)
        dstArray[offset++] = grid.toLinear(x - 1, y)
        dstArray[offset++] = grid.toLinear(x - 1, y - 1)

        assert(offset == neighboursCount + dstOffset)
    }
}

class ParallelogramTilingOnCanvasFactory @Inject constructor() :
    TilingOnCanvas.Factory {
    override fun create(canvasWidth: Float, canvasHeight: Float, grid: Grid): TilingOnCanvas {
        return PeriodicShapesTilingOnCanvas.create(
            grid,
            canvasWidth = canvasWidth,
            canvasHeight = canvasHeight,
            shapeBuilder = ParallelogramPeriodicShapeBuilder(grid, canvasWidth, canvasHeight)
        )
    }

    private class ParallelogramPeriodicShapeBuilder(
        grid: Grid,
        val canvasWidth: Float,
        canvasHeight: Float
    ) : PeriodicShapesTilingOnCanvas.PeriodicShapeBuilder {

        private val base: Float = canvasWidth / grid.columns
        private val height: Float = canvasHeight / grid.rows

        override fun createShape(x: Int, y: Int): PeriodicShape {

            val p0XAbs = base * x + base * 0.25f * y
            // adjust for the shape to appear within the canvas to adjust for limitations of the cutting algorithm
            val p0X = p0XAbs - ((p0XAbs / canvasWidth).toInt() * canvasWidth)

            val p0 = Vertex(p0X, y * height) // top left
            val p1 = p0.xShift(base) // top right
            val p2 = Vertex(p0X + 1.25f * base, (y + 1) * height) // bottom right
            val p3 = p2.xShift(-base) // bottom left

            return convexPolygonPeriodicShape(
                p0,
                p1,
                p2,
                p3
            )
        }
    }
}
