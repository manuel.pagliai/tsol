package com.manuel.pagliai.tsol.domain.tiling

/**
 * Helper class with DSL like syntax to help create a shape representable as the union of one or more
 * convex polygons. Allows to define invisible [PolygonalChain]s as to allow
 * for composition of convex polygon to represent a convex shape.
 *
 * For example, the arrow cell type shape (=>) is obtained as a rectangle (arrow body) and
 * a triangle (arrow tip) with sections of their perimeters invisible.
 */
class UnionOfConvexPolygonsBuilder {

    private val polygons = mutableListOf<ConvexPolygon>()

    /**
     * Start building a polygon starting from [x], [y]
     */
    fun polygon(x: Float, y: Float, init: ConvexPolygonBuilder.() -> Unit) {
        return polygon(Vertex(x, y), init)
    }

    /**
     * Start building a polygon starting from [start]
     */
    fun polygon(start: Vertex, init: ConvexPolygonBuilder.() -> Unit) {
        polygons.add(ConvexPolygonBuilder(start).apply(init).build())
    }

    fun build(): PeriodicShape =
        MultiPartPeriodicShape(polygons)

    /**
     * Helper class to build [ConvexPolygon] with DSL like syntax
     */
    class ConvexPolygonBuilder(private val start: Vertex) {

        private var buildingChainVisible = false
        private var buildingChain: MutableList<Vertex> = ArrayList()
        private val chains = mutableListOf<PolygonalChain>()

        init {
            buildingChain.add(start)
        }

        /**
         * Create the last segment of the polygon.
         * This method must be called for each polygon
         */
        fun close(visible: Boolean): ConvexPolygonBuilder {
            return to(start, visible)
        }

        /**
         * Creates a new side of the polygon
         */
        fun to(
            to: Vertex,
            visible: Boolean
        ): ConvexPolygonBuilder {
            chain(visible).add(to)
            return this
        }

        private fun chain(visible: Boolean): MutableList<Vertex> {
            if (buildingChainVisible != visible) {
                val last = buildingChain.last()
                if (buildingChain.size > 1)
                    chains.add(
                        PolygonalChain(
                            buildingChain,
                            buildingChainVisible
                        )
                    )
                buildingChain = ArrayList()
                buildingChain.add(last)
                buildingChainVisible = visible
            }
            return buildingChain
        }

        fun build(): ConvexPolygon {
            if (buildingChain.size > 1) {
                chains.add(
                    PolygonalChain(
                        buildingChain,
                        buildingChainVisible
                    )
                )
                buildingChain = mutableListOf()
            }
            return ConvexPolygon(chains)
        }

    }
}

fun unionOfConvexPolygons(init: UnionOfConvexPolygonsBuilder.() -> Unit) =
    UnionOfConvexPolygonsBuilder()
        .apply(init).build()