package com.manuel.pagliai.tsol.domain.tiling

import android.graphics.Path
import androidx.annotation.VisibleForTesting
import timber.log.Timber


/**
 * Abstraction over a periodic shape: a geometric shape capable of respecting periodic boundaries
 *      (i.e., the pacman-like effect where something disappears from one edge of the screen and
 *      reappears on the opposite side).
 *
 * Instances of this class are meant to be mutable.
 */
interface PeriodicShape {

    /**
     * The list of ALL sides of the shape.
     *
     * [Side]s do not appear in any particular order, and may be associated to one or more polygons.
     *
     * These must not be used use for rendering: it is not possible to build each polygons from [allSides]
     * as:
     * (i)   Sides are not in order
     * (ii)  Some of the sides must not be rendered (e.g., when a periodic boundary is encountered, that creates a side in the polygon, but it should not be drawn).
     * (iii) A periodic shape could be represented as more than one polygon
     *
     * The list is intended for indexing, or detect if a point falls within the shape.
     *
     * For rendering purposes see [onBorderLines] and [onCellPaths]
     */
    val allSides: List<Side>

    /**
     * True if the shape contains the point (x, y)
     */
    fun contains(x: Float, y: Float): Boolean

    /**
     * Imposes a maximum Y on the shape (minimum is assumed to be zero).
     *      If the shape has side intercepting the line y = boundaryY
     *      then it will be split accordingly, and the wrapped shape will appear on the other side of the screen
     *
     *  Warning: the shape is must not have vertices with y coordinate outside [- [boundaryY], 2 * [boundaryY]]
     */
    fun horizontalConstraint(boundaryY: Float)

    /**
     * Imposes a maximum X on the shape (minimum is assumed to be zero).
     *      If the shape has side intercepting the line x = boundaryX
     *      then it will be split accordingly, and the wrapped shape will appear on the other side of the screen
     *
     * Warning: the shape is must not have vertices with x coordinate outside [- [boundaryX], 2 * [boundaryX]]
     */
    fun verticalConstraint(boundaryX: Float)

    /**
     * Invokes [onLines] on every set of lines representing the visible border of the periodic shape.
     *
     * The argument passed to the lambda has the same semantics as pts in [android.graphics.Canvas.drawLines]
     * Used to draw the grid/tiling on the canvas
     * @see [android.graphics.Canvas.drawLines]
     *
     * @see [com.manuel.pagliai.tsol.domain.TilingOnCanvas]
     */
    fun onBorderLines(onLines: (FloatArray) -> Unit)

    /**
     * Invokes [onPath] on every path representing a polygon part of the periodic shape.
     * @see [com.manuel.pagliai.tsol.domain.TilingOnCanvas]
     */
    fun onCellPaths(onPath: (Path) -> Unit)
}

/**
 * A [PeriodicShape] consisting of a single convex polygon
 */
fun convexPolygonPeriodicShape(vararg vertices: Vertex): PeriodicShape = MultiPartPeriodicShape(
    listOf(
        ConvexPolygon.fromSingleChain(
            vertices.asList()
        )
    )
)

/**
 * [PeriodicShape] implementation based on a set of [ConvexPolygon]
 *
 * This is a mutable class
 */
class MultiPartPeriodicShape(convexPolygons: List<ConvexPolygon>) : PeriodicShape {

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    var parts: Array<ConvexPolygon> = convexPolygons.toTypedArray()

    override val allSides: List<Side>
        get() = parts.asSequence().map { it.sides }.flatten().distinct().toList()

    override fun contains(x: Float, y: Float) = parts.any { it.contains(x, y) }

    override fun horizontalConstraint(boundaryY: Float) {
        if (anyVertex { it.y < -boundaryY || it.y > 2f * boundaryY }) {
            throw IllegalStateException("$this contains points with y outside [-$boundaryY, 2* $boundaryY]")
        }
        val newPolygons = parts.toMutableList()
        val polygonsNeedsCutting =
            parts.filter { it.outOfBoundariesY(0f, boundaryY) }.toMutableList()

        if (polygonsNeedsCutting.isEmpty())
            return

        polygonsNeedsCutting.forEach {
            newPolygons.remove(it)
        }

        polygonsNeedsCutting.forEach {
            doHorizontalCut(it, boundaryY).forEach { newPolygon ->
                newPolygons.add(newPolygon)
            }
        }

        parts = newPolygons.toTypedArray()
    }

    override fun verticalConstraint(boundaryX: Float) {
        if (anyVertex { it.x < -boundaryX || it.x > 2f * boundaryX }) {
            throw IllegalStateException("$this contains points with x outside [-$boundaryX, 2*-$boundaryX]")
        }
        val newPolygons = parts.toMutableList()

        val polygonsNeedsCutting =
            parts.filter { it.outOfBoundariesX(0f, boundaryX) }.toMutableSet()

        if (polygonsNeedsCutting.isEmpty())
            return

        polygonsNeedsCutting.forEach {
            newPolygons.remove(it)
        }

        polygonsNeedsCutting.forEach {
            doVerticalCut(it, boundaryX).forEach { newPolygon ->
                newPolygons.add(newPolygon)
            }
        }

        parts = newPolygons.toTypedArray()
    }

    private inline fun anyVertex(vertexPredicate: (Vertex) -> Boolean): Boolean {
        return parts.any { p ->
            p.vertices.any(vertexPredicate)
        }
    }

    override fun onBorderLines(onLines: (FloatArray) -> Unit) {
        parts.forEach {
            it.onBorderLines(onLines)
        }
    }

    override fun onCellPaths(onPath: (Path) -> Unit) {
        parts.forEach {
            it.onCellPaths(onPath)
        }
    }

    private fun doHorizontalCut(polygon: ConvexPolygon, hBoundaryY: Float): Set<ConvexPolygon> {
        return doVerticalCut(polygon.swapXY(), hBoundaryY).map { it.swapXY() }.toSet()
    }

    /**
     * Splits a polygon in two via a vertical cut. The result will be two different (convex) polygons
     * one representing the intersection between [polygon] and 0 < x < [boundaryX], the other
     * obtained as intersection between [polygon] and x > [boundaryX] (or < 0) translated by -[boundaryX] ([boundaryX]).
     *
     * In practice this translate to the effect of the polygon wrapping trough the boundary and re-appearing
     * on the other side of the screen pacman-style.
     *
     *
     * For sake of documentation we will refer to convex_polygon_wrapping.svg in the docs folder.
     * The algorithm described here for a rectangle is extensible for any convex polygon.
     *
     * The rectangle A, C, D, F needs to be cut on a vertical boundary.
     * The intersection between the rectangle and the vertical boundary are the points B and E
     *
     * This cut splits the convex polygon into two convex polygons (in this case two rectangles, but it is true in general):
     * [A,B,E,F] and [B,C,D,E]
     *
     * [A,B,E,F] is called the 'cut polygon' and it will remain where it is, whereas [B,C,D,E] will 'wrap'
     * around the screen and become the 'wrapped polygon' [B',C',D',E']
     *
     * The cut operates on [PolygonalChain]s. In the example, there is a single visible chain
     *
     * A -> C -> D -> F
     *
     * We start iterating the chain: while iterating the chain(s) we will be building the wrapped and cut polygons
     *
     * Because the first vertex (A) is not beyond the boundary we add it to the cut polygon: (in fact any side in the chain
     * that does not intersect the boundary can be added as is), and we start building a (visible) chain,
     * next point is C which is beyond the vertical constraint. By mean of [PeriodicBoundaryIntersection]
     * we know that the current chain will stop at B [PeriodicBoundaryIntersection.boundaryHit] for the cut polygon, and a new chain
     * starting with B' -> C' ([PeriodicBoundaryIntersection.periodicBoundaryHit] and [PeriodicBoundaryIntersection.vertex] resp.)
     * will start on the cut polygon.
     *
     * Note that on the intersection we start an invisible chain in B for the cut polygon.
     * this chain will be completed in E once the side (D -> F) of the chain is analysed.
     * This allows for a correct filling of the rectangle, but without drawing a 'spurious side'
     * B -> E which should not be drawn (as it is the boundary of the canvas, not a side of the original shape).
     *
     * The algorithm proceed similarly on the wrapped polygon, with the only difference that coordinates
     * need to be readjusted to account for the wrapping.
     */
    private fun doVerticalCut(polygon: ConvexPolygon, boundaryX: Float): Set<ConvexPolygon> {
        try {
            return doVerticalCutImpl(polygon, boundaryX)
        } catch (e: Exception) {
            Timber.e(e, "PeriodicShape $polygon | $boundaryX")
            throw e
        }
    }

    private fun doVerticalCutImpl(polygon: ConvexPolygon, boundaryX: Float): Set<ConvexPolygon> {

        /**
         * the part of the polygon unaffected by the cut (x < vBoundaryX)
         */
        val cutPolygon = MultiChainPolygonBuilder()

        /**
         * The polygon that will wrap and appear other side of the screen
         */
        val wrappedPolygon = MultiChainPolygonBuilder()

        /**
         * True if the first vertex encountered for the polygon needed to be wrap.
         * False if it belongs to the cut polygon.
         *
         * Used to identify if a polygon (wrapped or cut) should be closed once we encounter the segment leaving
         *  the (wrapped or cut) polygon. See class docs.
         */
        var startedWrapped: Boolean? = null

        polygon.chains.forEach { polygonChain ->

            val chainVisible = polygonChain.visible

            // true if the first vertex needs wrapping, false otherwise

            polygonChain.sides.forEach { side ->

                // It is assumed that we do not have sides larger/longer than the screen width/height

                val fromNeedsWrapping = side.from.outsideXRange(0f, boundaryX)
                val toNeedsWrapping = side.to.outsideXRange(0f, boundaryX)

                if (fromNeedsWrapping) {
                    wrappedPolygon.ensureOpenChain(chainVisible)
                } else {
                    cutPolygon.ensureOpenChain(chainVisible)
                }

                startedWrapped = if (startedWrapped == null) {
                    fromNeedsWrapping
                } else {
                    startedWrapped
                }

                if (!fromNeedsWrapping && !toNeedsWrapping) {
                    cutPolygon.next(side)
                } else if (fromNeedsWrapping && toNeedsWrapping) {
                    if (side.from.x >= 0) {
                        wrappedPolygon.next(
                            side.copy(
                                from = side.from.xShift(-boundaryX),
                                to = side.to.xShift(-boundaryX)
                            )
                        )
                    } else {
                        wrappedPolygon.next(
                            side.copy(
                                from = side.from.xShift(boundaryX),
                                to = side.to.xShift(boundaryX)
                            )
                        ) // v1.x is < 0
                    }
                } else if (!fromNeedsWrapping && toNeedsWrapping) {
                    val periodicIntersection =
                        PeriodicBoundaryIntersection.fromNotWrappedToWrapped(
                            side,
                            boundaryX
                        )

                    if (cutPolygon.currentChainEmpty()) {
                        cutPolygon.next(side.from)
                    }

                    cutPolygon.next(periodicIntersection.boundaryHit)
                        .ensureOpenChain(false)
                        .next(periodicIntersection.boundaryHit)


                    if (wrappedPolygon.buildingChain) {
                        wrappedPolygon.next(periodicIntersection.periodicBoundaryHit)
                    }
                    wrappedPolygon
                        .ensureOpenChain(chainVisible)
                        .next(periodicIntersection.periodicBoundaryHit)
                        .next(periodicIntersection.vertex)

                    if (startedWrapped == true) {
                        /*
                         * we started as wrapped, and we are leaving the cut polygon
                         * we will never return cross the boundary again: we need to close the polygon
                         */
                        cutPolygon.next(cutPolygon.firstPolygonVertex()).endChain()
                    }


                } else if (fromNeedsWrapping && !toNeedsWrapping) {

                    val periodicIntersection =
                        PeriodicBoundaryIntersection.fromWrappedToNotWrapped(
                            side,
                            boundaryX
                        )

                    if (wrappedPolygon.currentChainEmpty()) {
                        if (side.from.x >= 0) {
                            wrappedPolygon.next(side.from.xShift(-boundaryX))
                        } else {
                            wrappedPolygon.next(side.from.xShift(boundaryX)) // v1.x is < 0
                        }
                    }

                    wrappedPolygon.next(periodicIntersection.boundaryHit)
                        .ensureOpenChain(false)
                        .next(periodicIntersection.boundaryHit)


                    if (cutPolygon.buildingChain) {
                        cutPolygon
                            .next(periodicIntersection.periodicBoundaryHit)
                    }

                    cutPolygon.ensureOpenChain(chainVisible)
                        .next(periodicIntersection.periodicBoundaryHit)
                        .next(periodicIntersection.vertex)

                    if (startedWrapped == false) {
                        /*
                         * we started in cut, and we are leaving the wrapped polygon
                         * we will never return cross the boundary again: we need to close the polygon
                         */
                        wrappedPolygon.next(wrappedPolygon.firstPolygonVertex()).endChain()
                    }
                }
            }

        }

        cutPolygon.endChain()
        wrappedPolygon.endChain()

        val polygons = mutableSetOf<ConvexPolygon>()
        cutPolygon.build()?.let { polygons.add(it) }
        wrappedPolygon.build()?.let { polygons.add(it) }
        return polygons
    }

    /**
     * Class to represent a segment intersection with a periodic boundary
     *
     * Used exclusively during he 'cutting' algorithm
     *
     * [boundaryHit] where the periodic boundary is hit
     * [periodicBoundaryHit] equivalent of [boundaryHit] but on the other side of the screen.
     * (think of it as [boundaryHit] modulo canvas width/height)
     *
     * [vertex] where the end of the segment is, once it has been wrapped around the period boundary
     *
     */
    private data class PeriodicBoundaryIntersection(
        val boundaryHit: Vertex,
        val periodicBoundaryHit: Vertex,
        val vertex: Vertex
    ) {

        companion object {

            fun fromNotWrappedToWrapped(
                side: Side,
                vBoundaryX: Float
            ): PeriodicBoundaryIntersection {

                val from = side.from
                val to = side.to
                val m = side.slope()

                return if (to.x >= 0) {
                    val hitBoundaryY = from.y + m * (vBoundaryX - from.x)
                    PeriodicBoundaryIntersection(
                        boundaryHit = Vertex(
                            vBoundaryX,
                            hitBoundaryY
                        ),
                        periodicBoundaryHit = Vertex(
                            0f,
                            hitBoundaryY
                        ),
                        vertex = to.xShift(-vBoundaryX)
                    )
                } else {
                    val hitZeroY = from.y + m * (0f - from.x)
                    PeriodicBoundaryIntersection(
                        boundaryHit = Vertex(
                            0f,
                            hitZeroY
                        ),
                        periodicBoundaryHit = Vertex(
                            vBoundaryX,
                            hitZeroY
                        ),
                        vertex = to.xShift(vBoundaryX) // v1.x is < 0
                    )
                }
            }

            fun fromWrappedToNotWrapped(
                side: Side,
                vBoundaryX: Float
            ): PeriodicBoundaryIntersection {

                val from = side.from
                val to = side.to
                val m = side.slope()

                return if (from.x < 0) {
                    val hitZeroY = from.y + m * (-from.x)
                    PeriodicBoundaryIntersection(
                        boundaryHit = Vertex(
                            vBoundaryX,
                            hitZeroY
                        ),
                        periodicBoundaryHit = Vertex(
                            0f,
                            hitZeroY
                        ),
                        vertex = to
                    )
                } else {
                    val hitBoundaryY = from.y + m * (vBoundaryX - from.x)
                    PeriodicBoundaryIntersection(
                        boundaryHit = Vertex(
                            0f,
                            hitBoundaryY
                        ),
                        periodicBoundaryHit = Vertex(
                            vBoundaryX,
                            hitBoundaryY
                        ),
                        vertex = to
                    )
                }
            }
        }
    }


    /**
     * Class helping to build a convex polygon out of one or more [PolygonalChain]s with different visibility
     *
     * offers more advanced methods than [UnionOfConvexPolygonsBuilder.ConvexPolygonBuilder] at the cost of
     *      readability/ease of use.
     *
     *  Designed to help with the cutting algorithm hence kept private.
     */
    private class MultiChainPolygonBuilder {
        private val chains = mutableListOf<PolygonalChain>()

        var buildingChain = false
            private set
        private var currentChainVisible: Boolean = false
        private lateinit var currentChainVertices: MutableList<Vertex>

        fun ensureOpenChain(chainVisible: Boolean): MultiChainPolygonBuilder {
            if (!buildingChain) {
                newChain(chainVisible)
            } else if (chainVisible != this.currentChainVisible) {
                endChain()
                newChain(chainVisible)
            }
            return this
        }

        private fun newChain(visible: Boolean): MultiChainPolygonBuilder {
            assert(!buildingChain)
            buildingChain = true
            currentChainVertices = mutableListOf()
            currentChainVisible = visible
            return this
        }

        fun currentChainEmpty() = currentChainVertices.isEmpty()

        fun endChain(): MultiChainPolygonBuilder {
            if (buildingChain && !currentChainEmpty()) {
                // add chains even with just one point. The point could be used to "close" the final polygon
                chains.add(
                    PolygonalChain(
                        currentChainVertices,
                        currentChainVisible
                    )
                )
            }
            buildingChain = false
            return this
        }

        fun firstPolygonVertex(): Vertex {
            if (chains.isNotEmpty()) {
                return chains[0].first
            } else if (!currentChainEmpty()) {
                return currentChainVertices.first()
            }
            throw IllegalStateException("No vertex added")
        }

        fun next(vertex: Vertex): MultiChainPolygonBuilder {
            assert(buildingChain)
            if (!currentChainEmpty() && currentChainVertices.last() == vertex) {
                return this
            }
            assert(
                !currentChainVertices.contains(vertex) ||
                        currentChainVertices.first() == vertex
            )
            currentChainVertices.add(vertex)
            return this
        }

        fun next(side: Side): MultiChainPolygonBuilder {
            assert(buildingChain)
            if (currentChainVertices.isNotEmpty()) {
                assert(currentChainVertices.last() == side.from)
                return next(side.to)
            }
            next(side.from)
            next(side.to)
            return this
        }

        fun build(): ConvexPolygon? {
            if (chains.asSequence()
                    .map { it.vertices }
                    .flatten()
                    .distinct()
                    .count() < 3
            ) {
                return null // <3 vertices, not a polygon
            }

            val polygonChains = mutableListOf<PolygonalChain>()
            for (i in 0 until chains.size - 1) {
                val c0 = chains[i]
                val c1 = chains[i + 1]
                assert(c0.last == c1.first) { "Chains to dot concatenate" }
            }

            chains.asSequence().filter { !it.empty }.forEach {
                polygonChains.add(it)
            }

            assert(chains.last().last == chains.first().first) { "Polygon not closed" }

            if (polygonChains.isEmpty())
                return null
            return ConvexPolygon(polygonChains)
        }
    }
}

/**
 * The slope of the line identified by this side.
 *
 * Assumes the side is not vertical (or close to vertical really).
 */
private fun Side.slope(): Float = (to.y - from.y) / (to.x - from.x)

