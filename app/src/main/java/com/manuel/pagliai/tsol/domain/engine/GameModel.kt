package com.manuel.pagliai.tsol.domain.engine

import android.content.res.Configuration
import androidx.annotation.VisibleForTesting
import com.manuel.pagliai.tsol.domain.*
import com.manuel.pagliai.tsol.screens.game.AutomatonColors
import com.manuel.pagliai.tsol.screens.game.GameControls
import com.manuel.pagliai.tsol.screens.game.GameState
import java.util.*
import javax.inject.Inject
import javax.inject.Qualifier

/**
 * Represents the screen orientation when the game was rendered.
 * Stored as part of the model, and visible to the ui.
 *
 * If, on the welcome screen, the user tries to load a game in a different orientation from the current display orientation
 * a dialog should be shown prompting the user to rotate the device before loading the game.
 *
 * The reason for this behaviour is that there is no good solution on rendering a game on a canvas different from
 * the one it was created in.
 * (i)   One option would be to project the game on the new canvas. This is an undoable operation with the possibility of losing information: not good
 * (ii)  Another option would be to force rendering the automaton on the wrong canvas. While this is possible the automaton
 * would look squished/stretched: simply put squares will not be squares as the number of rows and columns is the same, but the canvas would be different.
 * (iii) Finally, the one option would be to rotate the device for the user instead of asking. This is possible and almost good.
 * The only issue with this is that the app would be then responsible to manage the device orientation. The handy control asking the user
 * to press the button before rotating the ui would no longer be visible, and the app itself is then responsible to handle rotation. We would
 * need to avoid accidental/annoying rotations.
 */
enum class ScreenOrientation {
    PORTRAIT,
    LANDSCAPE;

    companion object {
        fun fromConfigurationOrientation(orientation: Int) =
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                LANDSCAPE
            } else {
                PORTRAIT
            }
    }
}

/**
 * Represents the automaton as last edited by the user.
 *
 * This might not be how the automaton is rendered on the screen as that might represent a
 * future generation of the edited automaton.
 */
data class Automaton(
    val name: String,
    val cellType: CellType,
    val cells: Cells,
    val rules: GameRules,
    val cellSize: Int,
    val previewAspectRatio: Float,
    val orientation: ScreenOrientation
)

/**
 * Used to generate timestamps
 */
class Clock @Inject constructor() {
    fun now(): Long = System.currentTimeMillis()
}

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class EditHistory

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class ExecutionHistory

/**
 * Internal representation of a game used as part of the game engine.
 *
 * Outside the game engine, the game should be observed via [GameState] emitted by [GameModel.gameState]
 *
 * Instances are mutable and not thread safe.
 *
 * Instances must be initialised with an initial automaton before they can be used
 * @see [initialiseNewGame]
 * @see [initialiseFromSavedGame]
 * @see [initialise]
 *
 *
 * This class models a game, and provide some logic to consistently update the model according
 * to user action.
 * Functionalities of events handling, updates scheduling and concurrency management, or game serialisation
 * are not provided by this class. @see [GameEngine] instead.
 *
 *
 * Related classes:
 *
 * @see GameState
 * @see GameEngine
 * @see GameSettings
 * @see LifeSaver
 * @see AutomatonNextGenCalculator
 */
class GameModel @Inject constructor
    (
    /**
     * Stores cells of each generation for each automaton run (either while playing, or while the user is
     * clicking the next gen button)
     *
     * This will always contain at least two elements: the cells for the current automaton (stored in [HistoryBuffer.peek]
     * and the cells of the next gen stored in [HistoryBuffer.peekNext]
     *
     * Because of [GameRules.pRandomDeath] and  [GameRules.pRandomBirth]
     * the next generation cannot be re-evaluated on demand as it is not deterministic. Instead
     * the next generation shown in the next-gen hints must be the same as the one shown once the user clicks the next gen button (or starts the automaton)
     * This means that every automaton that gets displayed is aware of what the next generation is.
     *
     * When going back in history we need to account for the current generation stored in the immediate past (
     * we need to discard the current immediate past returned by [HistoryBuffer.goBack] as that would return the current cells
     * and peek the cells in the immediate past, which are now the new current generation (i.e., the cells before the current ones)
     *
     * Current gen will be saved in the immediate past. And there will always be a next gen already populated
     */
    @ExecutionHistory
    private val executionHistory: HistoryBuffer<Cells>,

    /**
     * Stores the edit as performed by the user on the automaton. Provides undo/redo functionality.
     * For example, the user selects toggles the life state of a cell and then taps on undo
     * For example, the user "clears" the current automaton (i.e, kills all alive cells), then taps on undo.
     *
     * Not all user actions can are recorded (e.g., change cell type, change cell size ...).
     */
    @EditHistory
    private val editHistory: HistoryBuffer<CellsEdit>,

    /**
     * Used to detect loops in an automaton run (if rules are deterministic)
     */
    private val loopDetector: ProbabilisticLoopDetector,
    private val clock: Clock,
    private val automatonNextGenCalculator: AutomatonNextGenCalculator
) {

    val cellSize: Int
        get() = automaton.cellSize

    val cellType: CellType
        get() = automaton.cellType

    var name: String
        get() = automaton.name
        set(value) {
            automaton = automaton.copy(name = value)
        }

    val rules: GameRules
        get() = automaton.rules

    private lateinit var automaton: Automaton

    /**
     * Cells as they should be rendered on the screen
     */
    val currentGenCells: Cells
        get() = executionHistory.peek()!!

    /**
     * The cells of the next generations. Used, for example, when animation the transition
     * from the current to the next generation while the device is left idle.
     *
     * @see [showNextGenPreview]
     * @see [executionHistory]
     */
    private val nextGenCells: Cells
        get() = executionHistory.peekNext()!!

    var speed: Int = GameState.NEW_GAMES_DEFAULT_SPEED_PREFERENCE_DEFAULT
        set(value) {
            assert(speed in GameState.MIN_SPEED..GameState.MAX_SPEED)
            field = value
        }

    /**
     * It should be kept in sync with the latest game settings out of [com.manuel.pagliai.tsol.repository.preferences.PreferencesRepository]
     */
    private lateinit var gameSettings: GameSettings

    /**
     * 0-based generation of the automaton.
     *
     * Every time the user edits the automaton this should be reset.
     */
    private var generation: Int = 0

    /**
     * Timestamp of when the automaton was first created.
     */
    private var createdAt: Long = -1

    /**
     * Timestamp of when the automaton was last edited.
     */
    private var modifiedAt: Long = -1

    /**
     * True if a preview of the next gen should be shown. This is usually the case when
     * the user does not do anything for a while, and an animation showing a transition from the [currentGenCells] to [nextGenCells]
     */
    private var showNextGenPreview: Boolean = false

    /**
     * Assigned once the game has been saved.
     */
    var gameId: Long? = null

    /**
     * True if the automaton is running.
     * N.B. This is only for sake of modelling the state of the game: no update is handled directly by this class
     */
    var isPlaying = false

    /**
     * If, while running the automaton, the user touches a dead cell,
     *      the cell should become alive. If however the next gen happens right after that, the cell might die:
     *      the result is that the user never see the cell becoming alive and it looks like the action had no effect.
     *
     *      In this particular scenario (user touches a dead cell during the automaton run),
     *      allow for an extra delay (half the time of the standard delay) in order to allow the user to appreciate that
     *      the cell he/she has just created is alive, but maybe it is going to die in the next gen.
     */
    var extraDelay = false

    /**
     * The time the next generation update should take place. if negative not scheduled (i.e., the automaton is not running)
     * N.B. This is only for sake of modelling the state of the game: no update is handled directly by this class
     */
    var nextGenTime = -1L

    private val canPrevGen
        get() = !isPlaying && executionHistory.canGoBack(moreThan = 1) // the 'immediate past' is the current gen: we need one extra element i.e., the real past.

    private val canNextGenNow
        get() = !isPlaying && (!automaton.rules.isDeterministicGame || automaton.cells.alive)

    private val canUndo
        get() = !isPlaying && editHistory.canGoBack() && editHistoryMatchesGen

    private val canRedo
        get() = !isPlaying && editHistory.canGoForward() && editHistoryMatchesGen

    /**
     * If true, the button to kill all alive cells should be shown
     * If false, the button to reset to last edited automaton should be shown
     */
    private val canClear
        get() = !isPlaying && automaton.cells.alive && editHistoryMatchesGen

    private val canRestoreLastEdited
        get() = !canClear

    /**
     * If true [cellSelected] gives life else it kills a cell
     */
    var addCellsOnSelection = true

    /**
     * True if the edit history is compatible with the current automaton.
     *
     * For example:
     * (i) The user edits the automaton while it is not running. [editHistoryMatchesGen] will be true. Undo/Redo controls are enabled
     * (ii) The user taps on next generation: the edits in [editHistory] no longer apply to the new automaton. [editHistoryMatchesGen] will be false. Undo/Redo controls are both disabled. Notably this does not clear the edit history
     * (iii) The user taps on previous generation back to last edited automaton. [editHistoryMatchesGen] will be trie and Undo/Redo controls are enabled
     */
    @VisibleForTesting
    val editHistoryMatchesGen
        get() = currentGenCells === automaton.cells


    /**
     * The game as visible outside of the engine.
     */
    val gameState: GameState?
        get() {
            if (!this::automaton.isInitialized)
                return null
            return GameState(
                gameName = automaton.name,
                cellType = automaton.cellType,
                currentGenCells = currentGenCells,
                nextGenCells = nextGenCells,
                lastEditedCells = automaton.cells,
                showNextGenPreview = showNextGenPreview,
                isLooping = loopDetector.looping,
                generation = generation,
                modifiedAt = modifiedAt,
                createdAt = createdAt,
                speed = speed,
                cellSize = automaton.cellSize,
                rules = automaton.rules,
                previewAspectRatio = automaton.previewAspectRatio,
                screenOrientation = automaton.orientation,
                gameId = gameId,
                controls = GameControls(
                    addCellsOnSelection = addCellsOnSelection,
                    canUndo = canUndo,
                    canRedo = canRedo,
                    isPlaying = isPlaying,
                    canChangeCellType = !isPlaying,
                    canClear = canClear,
                    canNextGen = canNextGenNow,
                    canPrevGen = canPrevGen
                ),
                automatonColors = AutomatonColors.from(gameSettings = this.gameSettings)
            )
        }

    /**
     * Initialises the model for a new game.
     * Either this method or [initialiseFromSavedGame] must be called before the model can be used.
     * @see [isInitialized]
     */
    fun initialiseNewGame(
        name: String,
        cells: Cells,
        cellType: CellType,
        cellSize: Int,
        gameRules: GameRules,
        previewAspectRatio: Float,
        orientation: ScreenOrientation,
        speed: Int,
        gameSettings: GameSettings
    ) {
        val now = clock.now()

        initialise(
            Automaton(
                name = name,
                cellType = cellType,
                cells = cells,
                rules = gameRules,
                cellSize = cellSize,
                previewAspectRatio = previewAspectRatio,
                orientation = orientation,
            ),
            createdAt = now,
            modifiedAt = now,
            speed = speed,
            gameSettings = gameSettings
        )

    }

    /**
     * Initialises the model with a game that has been previously been saved.
     * Either this method or [initialiseNewGame] must be called before the model can be used.
     * @see [isInitialized]
     */
    fun initialiseFromSavedGame(savedGame: SavedGame, gameSettings: GameSettings) {
        this.gameId = savedGame.id
        initialise(
            Automaton(
                name = savedGame.name,
                cellType = savedGame.cellType,
                cells = savedGame.cells,
                rules = savedGame.rules,
                cellSize = savedGame.cellSize,
                previewAspectRatio = savedGame.previewAspectRatio,
                orientation = savedGame.orientation
            ),
            createdAt = savedGame.createdAt,
            modifiedAt = savedGame.modifiedAt,
            speed = savedGame.speed,
            gameSettings = gameSettings
        )
    }

    /**
     * N.B. make sure this method is called before using this model.
     */
    private fun initialise(
        automaton: Automaton,
        createdAt: Long,
        modifiedAt: Long,
        speed: Int,
        gameSettings: GameSettings
    ) {
        this.editHistory.reset()
        this.gameSettings = gameSettings
        this.automaton = automaton // initialise late init
        this.createdAt = createdAt
        this.modifiedAt = modifiedAt
        this.speed = speed
        updateAutomaton(automaton) // re-use logic when the automaton gets updated.
    }

    /**
     * Makes the automaton advance to the next generation (i.e., replacing the cells
     * with the new one evaluated according to the values of the [currentGenCells] and [GameRules])
     */
    fun nextGen() {
        val newCells = executionHistory.goForward()!!

        if (!executionHistory.canGoForward()) {
            executionHistory.add(
                automatonNextGenCalculator.nextGen(
                    cells = newCells,
                    cellType = cellType,
                    gameRules = automaton.rules
                )
            )
            executionHistory.goBack()!!
            loopDetector.addGen(newCells)
        }
        generation++
    }


    /**
     * If possible, the device is reverted back to its previous generation.
     * It does nothing if it is not possible to go back to the previous generation (either
     * it is no longer available in the history, or the current automaton is the most recent edited by the user).
     */
    fun prevGen() {
        if (canPrevGen) {
            executionHistory.goBack()!! // discard current gen, we know it is the same as automaton
            // peek now points to the previous element in history.
            // We could not have done goBack()!! as that would have returned the current generation
            generation--
        }
    }

    /**
     * The user has touched a cell. Depending on [addCellsOnSelection] a dead cell should come to life
     * or be a live cell should be killed.
     */
    fun cellSelected(coordinates: Int) {
        setCell(coordinates, addCellsOnSelection)
    }

    private fun setCell(coordinates: Int, alive: Boolean) {
        if (coordinates < 0 ||
            coordinates > automaton.cells.size
        ) {
            return
        }

        if (this.isPlaying) {
            editHistory.reset()
            updateAutomaton(
                automaton.copy(cells = currentGenCells.setCell(coordinates, alive))
            )
        } else {
            if (automaton.cells[coordinates] != alive) {
                if (!editHistoryMatchesGen) {
                    editHistory.reset()
                }
                val edit = SingleCellEdit(coordinates, alive)
                editHistory.add(edit)
                updateAutomaton(
                    automaton.copy(cells = edit.applyEdit(currentGenCells))
                )
            }
        }
    }

    fun undo() {
        if (canUndo) {
            applyEditFromHistory(editHistory.goBack()!!.createUndo())
        }
    }

    fun redo() {
        if (canRedo) {
            applyEditFromHistory(editHistory.goForward()!!)
        }
    }

    private fun applyEditFromHistory(edit: CellsEdit) {
        assert(!isPlaying)
        val newAutomaton = automaton.copy(cells = edit.applyEdit(automaton.cells))
        updateAutomaton(newAutomaton = newAutomaton)
    }

    /**
     * Starts a new execution history for the automaton returned by [updateAutomaton]
     * EditHistory is maintained subject to the automaton not changing its config. or
     * if [automaton] is not set this is a no-op
     */
    private fun updateAutomaton(newAutomaton: Automaton) {
        if (automaton.cellType != newAutomaton.cellType
            || automaton.cells.grid != newAutomaton.cells.grid
            || automaton.cellSize != newAutomaton.cellSize
        ) {
            // can't reuse old cells
            editHistory.reset()
        }
        executionHistory.reset()
        generation = 0
        loopDetector.reset(automaton.cells, enabled = newAutomaton.rules.isDeterministicGame)
        executionHistory.add(newAutomaton.cells)
        executionHistory.add(
            automatonNextGenCalculator.nextGen(
                cells = newAutomaton.cells,
                gameRules = newAutomaton.rules,
                cellType = cellType
            )
        )
        executionHistory.goBack()!!
        this.automaton = newAutomaton
    }

    /**
     * Kills all the cells. Does nothing if the control is not enabled when this is called.
     */
    fun clearCells() {
        if (canClear) {
            val grid = automaton.cells.grid
            val clearCells = Cells(grid = grid)
            if (automaton.cells.alive) {
                // there is something alive, create and record an edit
                val edit = CellsReplacement(from = automaton.cells, to = clearCells)
                if (!editHistoryMatchesGen) {
                    editHistory.reset()
                }
                editHistory.add(edit)
                updateAutomaton(automaton.copy(cells = edit.applyEdit(automaton.cells)))
            } else {
                // everything is already dead.
                generation = 0
                loopDetector.reset(automaton.cells, enabled = automaton.rules.isDeterministicGame)
            }
        }
    }

    /**
     * Reverts the automaton back to the last automaton edited by the user.
     */
    fun restoreLastEdited() {
        if (canRestoreLastEdited) {
            // N.B. when resetting is ok, and in fact desirable,
            // to lose history of random behaviour
            updateAutomaton(automaton)
        }
    }

    fun updateTimestamp() {
        this.modifiedAt = clock.now()
    }

    /**
     * Replaces the cells and rules with the one provided.
     */
    fun updateCells(newCellType: CellType, newCells: Cells, newRules: GameRules) {
        updateAutomaton(automaton.copy(cellType = newCellType, cells = newCells, rules = newRules))
    }

    /**
     * Edits the automaton migrating the current cells to the @param newGrid
     * cells alive state will be preserved where possible, but if newGrid is smaller than
     * the current grid some cells will be lost.
     */
    fun projectCellsToNewGrid(newGrid: Grid, newSize: Int) {
        val newCells = createProjectionCells(currentGenCells, newGrid)
        updateAutomaton(automaton.copy(cells = newCells, cellSize = newSize))
    }

    fun updateSettings(gameSettings: GameSettings) {
        this.gameSettings = gameSettings
    }

    fun notifyIdle() {
        showNextGenPreview = gameSettings.showNextGenAnimationIdle
    }

    fun notifyUserActivity() {
        showNextGenPreview = false
    }

    /**
     * To be called when canvas properties changes (e.g., a device rotation)
     */
    fun updateCanvasProperties(newOrientation: ScreenOrientation, newPreviewAspectRatio: Float) {
        automaton =
            automaton.copy(previewAspectRatio = newPreviewAspectRatio, orientation = newOrientation)
    }

    private fun createProjectionCells(cells: Cells, newGrid: Grid): Cells {
        if (newGrid == cells.grid)
            return cells
        val newBitSet = BitSet(newGrid.size)
        for (x in 0 until minOf(cells.grid.columns, newGrid.columns)) {
            for (y in 0 until minOf(cells.grid.rows, newGrid.rows)) {
                if (cells[x, y]) {
                    newBitSet[newGrid.toLinear(x, y)] = true
                }
            }
        }
        return Cells(bitSet = newBitSet, grid = newGrid)
    }

}
