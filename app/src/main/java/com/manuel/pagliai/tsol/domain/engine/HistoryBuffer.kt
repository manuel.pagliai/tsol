package com.manuel.pagliai.tsol.domain.engine

import kotlin.math.min


/**
 * Circular buffer used to manage a linear history of state changes
 */
class HistoryBuffer<T> constructor(size: Int) {

    @Suppress("UNCHECKED_CAST")
    private val buffer = arrayOfNulls<Any?>(size) as Array<T?>

    /**
     * Points to the next element in the buffer
     */
    private var next = 0
        private set(value) {
            field = checkInBufferRange(value)
        }

    /**
     * Points to the current element (i.e., the one before the next) if any
     */
    private val current
        get() = (next + buffer.size - 1) % buffer.size

    /**
     * The number of elements in the past stored in the buffer
     */
    private var past = 0
        private set(value) {
            field = checkInBufferRange(value)
        }

    /**
     * The number of elements in the future
     */
    private var future = 0
        private set(value) {
            field = checkInBufferRange(value)
        }

    /**
     * Add a new element to the buffer. All the future from this point will be lost
     */
    fun add(element: T?) {
        buffer[next] = element
        // adding an element will destroy all "future" history
        past = min(past + 1, buffer.size)
        future = 0
        incNext()
    }

    /**
     * @return true if the buffer can go back more than [moreThan] times
     * by default [moreThan] is zero meaning we can go back at least 1 time
     */
    fun canGoBack(moreThan: Int = 0): Boolean = past > moreThan

    /**
     * retrieve the current element of the buffer and go back 1 slots in the buffer
     */
    fun goBack(): T? {
        check(canGoBack())
        next = current
        past--
        future++
        return buffer[next]
    }

    /**
     * @return true if the buffer can go forward more than [moreThan] times
     * by default [moreThan] is zero meaning we can go forward at least 1 time
     */
    fun canGoForward(moreThan: Int = 0) = future > moreThan

    /**
     * provided that the future is available, updates the buffer head to point to the next element (also returned)
     */
    fun goForward(): T? {
        check(canGoForward())
        if (!canGoForward()) return null
        val element = buffer[next]
        incNext()
        past++
        future--
        return element
    }

    /**
     * @return the current element of the buffer (if any) without updating the pointer
     */
    fun peek(): T? {
        if (!canGoBack()) return null
        return buffer[current]
    }

    /**
     * @return the next element of the buffer (if any) without updating the pointer
     */
    fun peekNext(): T? {
        if (!canGoForward()) return null
        return buffer[next]
    }

    /**
     * resets the buffer
     */
    fun reset() {
        past = 0
        future = 0
        next = 0
        for (ii in buffer.indices)
            buffer[ii] = null
    }

    private fun incNext() {
        next = (next + 1) % buffer.size
    }

    private fun checkInBufferRange(value: Int): Int {
        assert(value in 0..buffer.size)
        return value
    }
}