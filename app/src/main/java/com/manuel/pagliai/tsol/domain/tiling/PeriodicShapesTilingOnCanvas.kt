package com.manuel.pagliai.tsol.domain.tiling


import android.graphics.Path
import androidx.annotation.VisibleForTesting
import com.manuel.pagliai.tsol.domain.Grid
import com.manuel.pagliai.tsol.domain.TilingOnCanvas

/**
 * Cell canvas adapter based on [PeriodicShapeTiling].
 *
 * Shapes are represented via [PeriodicShape] that would allow for wrapping a shape across the screen
 * pacman-style. see [PeriodicShape]
 *
 *  Warning while shapes coordinates are allowed to be negative or beyond the canvas size,
 *  they are not allowed to be beyond double the canvas size (or minus a canvas size).
 *  see [PeriodicShape.verticalConstraint] and [PeriodicShape.horizontalConstraint]
 *  (this limitation is due to computations in the tiling engine/cutting algorithms, in practice it is quite
 *  easy to overcome this issue by 'shifting' the polygon within the constraints).
 */
class PeriodicShapesTilingOnCanvas private constructor(@VisibleForTesting val tiling: PeriodicShapeTiling) :
    TilingOnCanvas {

    override fun onGridLines(coordinates: Int, action: (FloatArray) -> Unit) {
        tiling[coordinates]?.onBorderLines(action)
    }

    override fun onCellPaths(coordinates: Int, action: (Path) -> Unit) {
        tiling[coordinates]?.onCellPaths(action)
    }

    override fun coordinates(x: Float, y: Float) = tiling.positionOf(x, y)

    companion object {

        fun create(
            grid: Grid,
            canvasWidth: Float,
            canvasHeight: Float,
            shapeBuilder: PeriodicShapeBuilder
        ): PeriodicShapesTilingOnCanvas {

            return PeriodicShapesTilingOnCanvas(
                // create the tiling: build shapes and cut them according to the canvas dimensions.
                PeriodicShapeTiling.create(grid.size) { coordinates ->
                    val (x, y) = grid.toXY(coordinates)
                    shapeBuilder.createShape(
                        x,
                        y,
                    ).also {
                        it.horizontalConstraint(canvasHeight)
                        it.verticalConstraint(canvasWidth)
                    }
                }
            )
        }
    }

    /**
     * Abstraction over the construction of a shape at a given coordinates
     * Implementation of this method is of course dependent on the [com.manuel.pagliai.tsol.domain.CellType]
     *
     * Although there are some limitations (see class docs), the shape can be drawn as if an infinite canvas
     * (It will then be cut and split during construction of the tiling).
     *
     * Check the assets folder for helpful drawings for each cell type.
     */
    interface PeriodicShapeBuilder {
        fun createShape(x: Int, y: Int): PeriodicShape
    }
}