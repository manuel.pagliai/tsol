package com.manuel.pagliai.tsol.domain.cells

import androidx.room.ProvidedTypeConverter
import androidx.room.TypeConverter
import com.manuel.pagliai.tsol.domain.CellType
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.reflect.KClass

/**
 * Registry of all [CellType]s used in the application.
 */
@Singleton
@ProvidedTypeConverter
class CellTypes @Inject constructor(
    squareCellType: SquareCellType,
    hexagonalCellType: HexagonalCellType,
    runningBondCellType: RunningBondCellType,
    herringboneCellType: HerringboneCellType,
    triangular12CellType: Triangular12CellType,
    triangle14CellType: Triangle14CellType,
    parallelogramCellType: ParallelogramCellType,
    rhombusCellType: RhombusCellType,
    doubleBasketweaveCellType: DoubleBasketweaveCellType,
    rhombitrihexagonCellType: RhombitrihexagonCellType,
    dualLavesCellType: DualLavesCellType,
    pentagonalCellType: PentagonalCellType,
    cairoCellType: CairoCellType,
    lShapeCellType: LShapeCellType,
    arrowCellType: ArrowCellType,
    triakisCellType: TriakisCellType,
    sphinxCellType: SphinxCellType,
    armchairCellType: ArmchairCellType,
    trapezoidalCellType: TrapezoidalCellType,
    kisrhombilleCellType: KisrhombilleCellType
) {

    private val allCellTypes = listOf(
        squareCellType,
        hexagonalCellType,
        runningBondCellType,
        herringboneCellType,
        triangular12CellType,
        triangle14CellType,
        parallelogramCellType,
        rhombusCellType,
        doubleBasketweaveCellType,
        rhombitrihexagonCellType,
        dualLavesCellType,
        pentagonalCellType,
        cairoCellType,
        lShapeCellType,
        arrowCellType,
        triakisCellType,
        sphinxCellType,
        armchairCellType,
        trapezoidalCellType,
        kisrhombilleCellType
    )

    init {
        assert(!defaultCellType.hidden)
    }

    /**
     * All [CellType]s visible in the app
     */
    val list: List<CellType>
        get() = allCellTypes.filter { !it.hidden }.toList()

    val defaultCellType: CellType
        get() = get(SquareCellType::class)

    /**
     * Return the [CellType] instance in the registry of class [klass] (fails if not found)
     */
    @Suppress("UNCHECKED_CAST")
    operator fun <CT> get(klass: KClass<CT>): CT where CT : CellType {
        return list.first { it::class == klass } as CT
    }

    /**
     * Return the [CellType] with id [cellTypeId] (fails if none is found)
     * @see [CellType.id]
     */
    @TypeConverter
    fun getById(cellTypeId: String): CellType {
        return list.first { it.id == cellTypeId }
    }

}