package com.manuel.pagliai.tsol.domain.cells

import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.domain.*
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShape
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShapesTilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.convexPolygonPeriodicShape
import javax.inject.Inject

private val Triangular12CellsGrouping = HorizontalCellsGrouping(6)

class Triangular12CellType @Inject constructor(
    override val gridFactory: Triangular12GridFactory,
    override val tilingOnCanvasFactory: Triangular12TilingOnCanvasFactory
) : CellType {

    override val id: String = "TRIANGULAR_12"
    override val name: Int = R.string.triangular_12_cell_type
    override val icon: Int = R.drawable.ic_triangular_12_cell_type
    override val neighboursCount: Int = 12
    override val defaultGameRules = GameRules.B3S27

    override fun setNeighboursCoordinates(
        grid: Grid,
        x: Int,
        y: Int,
        dstArray: IntArray,
        dstOffset: Int
    ) {
        val (groupX, groupY) = Triangular12CellsGrouping.groupXY(x, y)
        val cellGroupIndex = Triangular12CellsGrouping.cellGroupIndex(x)

        var offset = dstOffset

        for (i in 0..5) {
            if (i != cellGroupIndex) {
                dstArray[offset++] =
                    Triangular12CellsGrouping.toCellLinear(grid, groupX, groupY, cellGroupIndex = i)
            }
        }
        val ccf = groupX % 2 // correction factor

        when (Triangular12CellsGrouping.cellGroupIndex(x)) {
            0 -> {
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX - 1,
                    groupY - ccf,
                    cellGroupIndex = 2
                )
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX - 1,
                    groupY - ccf,
                    cellGroupIndex = 3
                )
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX - 1,
                    groupY - ccf,
                    cellGroupIndex = 4
                )
                dstArray[offset++] =
                    Triangular12CellsGrouping.toCellLinear(
                        grid,
                        groupX,
                        groupY - 1,
                        cellGroupIndex = 4
                    )
                dstArray[offset++] =
                    Triangular12CellsGrouping.toCellLinear(
                        grid,
                        groupX,
                        groupY - 1,
                        cellGroupIndex = 5
                    )
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX - 1,
                    groupY + 1 - ccf,
                    cellGroupIndex = 1
                )
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX - 1,
                    groupY + 1 - ccf,
                    cellGroupIndex = 2
                )
            }
            1 -> {
                dstArray[offset++] =
                    Triangular12CellsGrouping.toCellLinear(
                        grid,
                        groupX,
                        groupY - 1,
                        cellGroupIndex = 3
                    )
                dstArray[offset++] =
                    Triangular12CellsGrouping.toCellLinear(
                        grid,
                        groupX,
                        groupY - 1,
                        cellGroupIndex = 4
                    )
                dstArray[offset++] =
                    Triangular12CellsGrouping.toCellLinear(
                        grid,
                        groupX,
                        groupY - 1,
                        cellGroupIndex = 5
                    )
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX - 1,
                    groupY - ccf,
                    cellGroupIndex = 2
                )
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX - 1,
                    groupY - ccf,
                    cellGroupIndex = 3
                )
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX + 1,
                    groupY - ccf,
                    cellGroupIndex = 0
                )
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX + 1,
                    groupY - ccf,
                    cellGroupIndex = 5
                )
            }
            2 -> {
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX + 1,
                    groupY - ccf,
                    cellGroupIndex = 0
                )
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX + 1,
                    groupY - ccf,
                    cellGroupIndex = 5
                )
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX + 1,
                    groupY - ccf,
                    cellGroupIndex = 4
                )
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX + 1,
                    groupY + 1 - ccf,
                    cellGroupIndex = 0
                )
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX + 1,
                    groupY + 1 - ccf,
                    cellGroupIndex = 1
                )
                dstArray[offset++] =
                    Triangular12CellsGrouping.toCellLinear(
                        grid,
                        groupX,
                        groupY - 1,
                        cellGroupIndex = 3
                    )
                dstArray[offset++] =
                    Triangular12CellsGrouping.toCellLinear(
                        grid,
                        groupX,
                        groupY - 1,
                        cellGroupIndex = 4
                    )
            }
            3 -> {
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX + 1,
                    groupY + 1 - ccf,
                    cellGroupIndex = 0
                )
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX + 1,
                    groupY + 1 - ccf,
                    cellGroupIndex = 1
                )
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX + 1,
                    groupY + 1 - ccf,
                    cellGroupIndex = 5
                )
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX + 1,
                    groupY - ccf,
                    cellGroupIndex = 4
                )
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX + 1,
                    groupY - ccf,
                    cellGroupIndex = 5
                )
                dstArray[offset++] =
                    Triangular12CellsGrouping.toCellLinear(
                        grid,
                        groupX,
                        groupY + 1,
                        cellGroupIndex = 1
                    )
                dstArray[offset++] =
                    Triangular12CellsGrouping.toCellLinear(
                        grid,
                        groupX,
                        groupY + 1,
                        cellGroupIndex = 2
                    )
            }
            4 -> {
                dstArray[offset++] =
                    Triangular12CellsGrouping.toCellLinear(
                        grid,
                        groupX,
                        groupY + 1,
                        cellGroupIndex = 0
                    )
                dstArray[offset++] =
                    Triangular12CellsGrouping.toCellLinear(
                        grid,
                        groupX,
                        groupY + 1,
                        cellGroupIndex = 1
                    )
                dstArray[offset++] =
                    Triangular12CellsGrouping.toCellLinear(
                        grid,
                        groupX,
                        groupY + 1,
                        cellGroupIndex = 2
                    )
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX - 1,
                    groupY + 1 - ccf,
                    cellGroupIndex = 2
                )
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX - 1,
                    groupY + 1 - ccf,
                    cellGroupIndex = 3
                )
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX + 1,
                    groupY + 1 - ccf,
                    cellGroupIndex = 0
                )
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX + 1,
                    groupY + 1 - ccf,
                    cellGroupIndex = 5
                )
            }
            5 -> {
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX - 1,
                    groupY + 1 - ccf,
                    cellGroupIndex = 1
                )
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX - 1,
                    groupY + 1 - ccf,
                    cellGroupIndex = 2
                )
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX - 1,
                    groupY + 1 - ccf,
                    cellGroupIndex = 3
                )
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX - 1,
                    groupY - ccf,
                    cellGroupIndex = 3
                )
                dstArray[offset++] = Triangular12CellsGrouping.toCellLinear(
                    grid,
                    groupX - 1,
                    groupY - ccf,
                    cellGroupIndex = 4
                )
                dstArray[offset++] =
                    Triangular12CellsGrouping.toCellLinear(
                        grid,
                        groupX,
                        groupY + 1,
                        cellGroupIndex = 0
                    )
                dstArray[offset++] =
                    Triangular12CellsGrouping.toCellLinear(
                        grid,
                        groupX,
                        groupY + 1,
                        cellGroupIndex = 1
                    )
            }
            else -> {
                throw IllegalStateException()
            }
        }
        assert(offset == neighboursCount + dstOffset)
    }
}

class Triangular12GridFactory @Inject constructor() : GridFactory {
    override fun createGrid(
        canvasWidth: Float,
        canvasHeight: Float,
        displayDensity: Float,
        cellSize: Int
    ) = HexagonBasedGridFactoryHelper.createGrid(
        polygonCount = 6,
        canvasWidth = canvasWidth,
        canvasHeight = canvasHeight,
        displayDensity = displayDensity,
        cellSize = cellSize,
        correctionFactorX = 0.6f,
        correctionFactorY = 0.6f
    )
}

class Triangular12TilingOnCanvasFactory @Inject constructor() :
    TilingOnCanvas.Factory {
    override fun create(
        canvasWidth: Float,
        canvasHeight: Float,
        grid: Grid
    ): TilingOnCanvas {
        return PeriodicShapesTilingOnCanvas.create(
            grid,
            canvasWidth = canvasWidth,
            canvasHeight = canvasHeight,
            shapeBuilder = Triangle12PeriodicShapeBuilder(grid, canvasWidth, canvasHeight)
        )
    }

    private class Triangle12PeriodicShapeBuilder(
        grid: Grid,
        canvasWidth: Float,
        canvasHeight: Float
    ) : PeriodicShapesTilingOnCanvas.PeriodicShapeBuilder {

        private val helper = HexagonBasedPeriodicShapeBuilderHelper(
            grid = grid,
            canvasWidth = canvasWidth,
            canvasHeight = canvasHeight,
            polygonCount = 6
        )

        override fun createShape(x: Int, y: Int): PeriodicShape {
            val vertices = helper.vertices(x, y).toArray()
            val centre = vertices[6]
            val cellGroupIndex = Triangular12CellsGrouping.cellGroupIndex(x)

            return convexPolygonPeriodicShape(
                vertices[cellGroupIndex],
                vertices[(cellGroupIndex + 1) % 6],
                centre
            )
        }

    }
}