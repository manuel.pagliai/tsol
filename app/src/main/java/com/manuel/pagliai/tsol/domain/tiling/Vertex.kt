package com.manuel.pagliai.tsol.domain.tiling

import kotlin.math.round

/**
 * A Point in a 2 dimensional space, usually representing a vertex in a polygon
 *
 * Instances are immutable and must be kept independent from the rendering framework
 */
data class Vertex(
    @JvmField
    val x: Float,
    @JvmField
    val y: Float
) {

    fun add(deltaX: Float, deltaY: Float) = copy(x = this.x + deltaX, y = this.y + deltaY)

    operator fun plus(augend: Vertex) =
        Vertex(x + augend.x, y + augend.y)

    operator fun minus(subtrahend: Vertex) =
        Vertex(
            x - subtrahend.x,
            y - subtrahend.y
        )

    operator fun div(divisor: Float) =
        Vertex(x / divisor, y / divisor)

    operator fun times(multiplier: Float) =
        Vertex(x * multiplier, y * multiplier)


    fun swapXY() = copy(x = this.y, y = this.x)

    fun xShift(delta: Float) = copy(x = this.x + delta)
    fun yShift(delta: Float) = copy(y = this.y + delta)
    fun roundY() = copy(y = round(y))
    fun roundX() = copy(x = round(x))

    fun outsideXRange(minX: Float, maxX: Float) = x < minX || x > maxX
    fun outsideYRange(minY: Float, maxY: Float) = y < minY || y > maxY
}