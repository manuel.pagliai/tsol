package com.manuel.pagliai.tsol.domain.engine

import com.manuel.pagliai.tsol.domain.CellType
import com.manuel.pagliai.tsol.domain.Cells
import com.manuel.pagliai.tsol.domain.GameRules
import com.manuel.pagliai.tsol.domain.Grid
import java.util.*
import javax.inject.Inject
import kotlin.random.Random

/**
 * Class responsible to evaluate the cells for the next generation of an automaton.
 *
 * @see [nextGen]
 *
 * This class is not thread safe.
 *
 * Internally wraps an 'impl' instance where neighbours coordinates of each cells are cached:
 * Profiling the app that evaluating the coordinates of neighbours of each cell was where the majority
 * of the time advancing to the next generation was spent: because that does not change for a given
 * [CellType]/[Grid], there is no reason to re-evaluate those at every gen, and they can be cached instead.
 */
class AutomatonNextGenCalculator @Inject constructor(val random: Random) {
    /**
     * Neighbours coordinates for each cell stored as a linear array (for every cell type, the number
     * of neighbours is independent from the cell coordinates).
     */
    @JvmInline
    private value class NeighboursCoordinates(val neighboursCoordinatesArray: IntArray) {
        inline fun forEachNeighbour(
            coordinates: Int,
            cellNeighboursCount: Int,
            cons: (Int) -> Unit
        ) {
            val startIndex = coordinates * cellNeighboursCount
            for (i in startIndex until startIndex + cellNeighboursCount) {
                cons(neighboursCoordinatesArray[i])
            }
        }

        companion object {
            /**
             * Builds a new [NeighboursCoordinates], reusing @param [previousArray] if possible.
             * This will map each cell coordinate to the coordinate of its neighbours
             */
            fun build(
                grid: Grid,
                cellType: CellType,
                previousArray: IntArray?
            ): NeighboursCoordinates {
                val gridSize = grid.size
                val arraySize = gridSize * cellType.neighboursCount
                val coordinatesArray =
                    if (previousArray != null && previousArray.size >= arraySize) {
                        previousArray
                    } else {
                        IntArray(arraySize)
                    }
                val neighboursCount = cellType.neighboursCount
                var offset = 0
                repeat(gridSize) { cell ->
                    val (x, y) = grid.toXY(cell)
                    cellType.setNeighboursCoordinates(
                        grid = grid,
                        x = x,
                        y = y,
                        dstArray = coordinatesArray,
                        dstOffset = offset
                    )
                    offset += neighboursCount
                }
                return NeighboursCoordinates(coordinatesArray)
            }
        }
    }

    private class AutomatonNextGenCalculatorImpl(
        private val cellType: CellType,
        private val grid: Grid,
        val neighboursCoordinates: NeighboursCoordinates
    ) {

        private val size = grid.size
        private val neighboursCount = cellType.neighboursCount

        fun nextGenCells(cells: Cells, gameRules: GameRules, random: Random): Cells {
            val newBitSet = BitSet(size)
            for (coordinates in 0 until size) {
                var aliveNeighboursCount = 0
                neighboursCoordinates.forEachNeighbour(
                    coordinates,
                    neighboursCount
                ) { neighbourCoordinates ->
                    if (cells[neighbourCoordinates]) {
                        aliveNeighboursCount += 1
                    }
                }
                val aliveInCurrentGen = cells[coordinates]
                val aliveInNextGen = gameRules.aliveInNextGen(
                    aliveInCurrentGen,
                    aliveNeighboursCount,
                    random
                )
                newBitSet[coordinates] = aliveInNextGen
            }
            return Cells(bitSet = newBitSet, grid = this.grid)
        }

        fun validFor(grid: Grid, cellType: CellType) =
            this.grid == grid && this.cellType == cellType
    }

    /**
     * Initialised (and re-initialised) on demand. Does the actual job of evaluating the next
     * generation, maintaining an instance of [NeighboursCoordinates] in memory.
     */
    private var impl: AutomatonNextGenCalculatorImpl? = null

    fun nextGen(cells: Cells, cellType: CellType, gameRules: GameRules): Cells {
        val grid = cells.grid

        if (impl?.validFor(
                grid,
                cellType
            ) != true
        ) { // can we use the existing impl, or we need to create a new one ?
            impl = AutomatonNextGenCalculatorImpl(
                grid = grid,
                cellType = cellType,
                neighboursCoordinates = NeighboursCoordinates.build(
                    grid,
                    cellType,
                    impl?.neighboursCoordinates?.neighboursCoordinatesArray
                )
            )
        }
        return impl!!.nextGenCells(cells, gameRules, random)
    }
}