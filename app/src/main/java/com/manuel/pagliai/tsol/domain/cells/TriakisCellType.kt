package com.manuel.pagliai.tsol.domain.cells

import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.domain.CellType
import com.manuel.pagliai.tsol.domain.GameRules.Companion.B56S567
import com.manuel.pagliai.tsol.domain.Grid
import com.manuel.pagliai.tsol.domain.GridFactory
import com.manuel.pagliai.tsol.domain.TilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShape
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShapesTilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.convexPolygonPeriodicShape
import javax.inject.Inject

private val TriakisCellsGrouping = HorizontalCellsGrouping(6)

class TriakisCellType @Inject constructor(
    override val gridFactory: TriakisGridFactory,
    override val tilingOnCanvasFactory: TriakisTilingOnCanvasFactory
) : CellType {

    override val name = R.string.triakis_cell_type
    override val icon = R.drawable.ic_triakis_cell_type
    override val id = "TRIAKIS"
    override val neighboursCount = 21
    override val defaultGameRules = B56S567
    override fun setNeighboursCoordinates(
        grid: Grid,
        x: Int,
        y: Int,
        dstArray: IntArray,
        dstOffset: Int
    ) {
        val (groupX, groupY) = TriakisCellsGrouping.groupXY(x, y)
        val cellGroupIndex = TriakisCellsGrouping.cellGroupIndex(x)

        var offset = dstOffset
        (0..5).forEach {
            if (it != cellGroupIndex) {
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX, groupY, it)
            }
        }

        val ccf = groupX % 2 // correction factor

        when (cellGroupIndex) {
            0, 1 -> {
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX - 1, groupY - ccf, 2)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX - 1, groupY - ccf, 3)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX - 1, groupY - ccf, 4)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX - 1, groupY - ccf, 5)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1 - ccf, 0)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1 - ccf, 1)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1 - ccf, 4)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1 - ccf, 5)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 3)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 2)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 4)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 5)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX + 1, groupY - ccf, 0)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX + 1, groupY - ccf, 1)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX + 1, groupY - ccf, 2)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX + 1, groupY - ccf, 3)
            }
            2, 3 -> {
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX - 1, groupY - ccf, 2)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX - 1, groupY - ccf, 3)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX - 1, groupY - ccf, 4)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX - 1, groupY - ccf, 5)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1 - ccf, 0)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1 - ccf, 1)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1 - ccf, 4)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX - 1, groupY + 1 - ccf, 5)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 0)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 1)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 4)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 5)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1 - ccf, 0)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1 - ccf, 1)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1 - ccf, 2)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1 - ccf, 3)
            }
            4, 5 -> {
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 3)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 2)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 4)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX, groupY - 1, 5)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX + 1, groupY - ccf, 0)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX + 1, groupY - ccf, 1)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX + 1, groupY - ccf, 2)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX + 1, groupY - ccf, 3)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 0)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 1)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 4)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX, groupY + 1, 5)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1 - ccf, 0)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1 - ccf, 1)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1 - ccf, 2)
                dstArray[offset++] =
                    TriakisCellsGrouping.toCellLinear(grid, groupX + 1, groupY + 1 - ccf, 3)
            }
        }

        assert(offset == neighboursCount + dstOffset)
    }

}

class TriakisGridFactory @Inject constructor() : GridFactory {
    override fun createGrid(
        canvasWidth: Float,
        canvasHeight: Float,
        displayDensity: Float,
        cellSize: Int
    ) = HexagonBasedGridFactoryHelper.createGrid(
        polygonCount = 6,
        canvasWidth = canvasWidth,
        canvasHeight = canvasHeight,
        displayDensity = displayDensity,
        cellSize = cellSize,
        correctionFactorX = 0.8f,
        correctionFactorY = 0.8f
    )
}


class TriakisTilingOnCanvasFactory @Inject constructor() :
    TilingOnCanvas.Factory {
    override fun create(canvasWidth: Float, canvasHeight: Float, grid: Grid): TilingOnCanvas {
        return PeriodicShapesTilingOnCanvas.create(
            grid,
            canvasWidth = canvasWidth,
            canvasHeight = canvasHeight,
            shapeBuilder = TriakisPeriodicShapeBuilder(grid, canvasWidth, canvasHeight)
        )
    }

    private class TriakisPeriodicShapeBuilder(grid: Grid, canvasWidth: Float, canvasHeight: Float) :
        PeriodicShapesTilingOnCanvas.PeriodicShapeBuilder {
        private val helper = HexagonBasedPeriodicShapeBuilderHelper(
            grid = grid,
            canvasWidth = canvasWidth,
            canvasHeight = canvasHeight,
            polygonCount = 6
        )

        override fun createShape(x: Int, y: Int): PeriodicShape {
            val (p0, p1, p2, p3, p4, p5, p6) = helper.vertices(x, y)

            return when (TriakisCellsGrouping.cellGroupIndex(x)) {
                0 -> convexPolygonPeriodicShape(p0, p1, p2)
                1 -> convexPolygonPeriodicShape(p0, p2, p6)
                2 -> convexPolygonPeriodicShape(p0, p4, p6)
                3 -> convexPolygonPeriodicShape(p0, p4, p5)
                4 -> convexPolygonPeriodicShape(p2, p4, p6)
                5 -> convexPolygonPeriodicShape(p2, p3, p4)
                else -> throw IllegalStateException()
            }
        }
    }
}