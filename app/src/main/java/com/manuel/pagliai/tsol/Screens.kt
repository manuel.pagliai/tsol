package com.manuel.pagliai.tsol

/**
 * Enumerates the various screens of the app
 */
sealed class Screens(val route: String) {

    /**
     * The welcome screen displays a list of saved games, or instructions on how to create a new game
     * if there are no saved games.
     *
     * From this screen the user can open a saved game, create a new one, or navigate to the settings/about page
     *
     */
    object Welcome : Screens("welcome")

    /**
     * A game of life is displayed. Here the user can run and edit the automaton, go back to the welcome screen
     * or navigate to the settings/about page
     */
    object Game : Screens("game") {

        /**
         * Id of the game to open.
         * @see [com.manuel.pagliai.tsol.domain.SavedGame.id]
         */
        const val GAME_ID_PARAM_NAME = "gameId"

        /**
         * value for the [GAME_ID_PARAM_NAME] parameter to indicate a new game should be created
         */
        const val NEW_GAME_ID = -1L
    }

    /**
     * Settings page, allowing the user customise the game appearance and behaviour.
     */
    object Settings : Screens("settings")

    /**
     * An about page is shown. The user can generate error report and share them with developers.
     */
    object About : Screens("about")
}

/**
 * Constants used to identify components in tests
 *
 * @see [androidx.compose.ui.platform.testTag]
 */
object TestTags {

    const val THREE_DOTS_MENU_TEST_TAG: String = "THREE_DOTS_MENU_TEST_TAG"
    const val SAVED_GAME_THREE_DOTS_MENU_TEST_TAG: String = "SAVED_GAME_THREE_DOTS_MENU_TEST_TAG"
    const val AUTOMATON_GENERATION_LABEL: String = "AUTOMATON_GENERATION_LABEL"
    const val AUTOMATON_CANVAS: String = "AUTOMATON_CANVAS"
    const val CHANGE_SPEED_SCROLLBAR: String = "CHANGE_SPEED_SCROLLBAR"
    const val CHANGE_CELL_TYPE_DIALOG: String = "CHANGE_CELL_TYPE_DIALOG"
    const val CHANGE_CELL_SIZE_SCROLLBAR: String = "CHANGE_CELL_SIZE_SCROLLBAR"
    const val CREATE_GAME_COPY_TEXT_FIELD: String = "CREATE_GAME_COPY_TEXT_FIELD"
    const val RENAME_GAME_TEXT_FIELD: String = "RENAME_GAME_TEXT_FIELD"
    const val FIRST_GAME_NAME_TEXT_FIELD: String = "FIRST_GAME_NAME_TEXT_FIELD"
    const val ABOUT_SCREEN_COLUMN: String = "ABOUT_SCREEN_COLUMN"
    const val CREATE_NEW_GAME_BUTTON: String = "CREATE_NEW_GAME_BUTTON"
    const val SAVED_GAME_ITEM: String = "SAVED_GAME_ITEM"
    const val USE_SYSTEM_COLOR_SWITCH: String = "USE_SYSTEM_COLOR_SWITCH"

}