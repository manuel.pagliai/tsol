package com.manuel.pagliai.tsol.domain.cells

import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.domain.*
import com.manuel.pagliai.tsol.domain.GridFactory.Companion.stdGridFactory
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShape
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShapesTilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.Vertex
import com.manuel.pagliai.tsol.domain.tiling.convexPolygonPeriodicShape
import javax.inject.Inject

class RunningBondCellType @Inject constructor(
    override val gridFactory: RunningBondGridFactory,
    override val tilingOnCanvasFactory: RunningBondTilingOnCanvasFactory
) : CellType {

    override val name: Int = R.string.running_bond_cell_type
    override val icon: Int = R.drawable.ic_running_bond_cell_type
    override val id: String = "RUNNING_BOND"
    override val neighboursCount: Int = 6
    override val defaultGameRules: GameRules = GameRules.B2S34

    override fun setNeighboursCoordinates(
        grid: Grid,
        x: Int,
        y: Int,
        dstArray: IntArray,
        dstOffset: Int
    ) {
        var offset = dstOffset

        dstArray[offset++] = grid.toLinear(x - 1, y)
        dstArray[offset++] = grid.toLinear(x + 1, y)
        dstArray[offset++] = grid.toLinear(x, y - 1)
        dstArray[offset++] = grid.toLinear(x, y + 1)

        if (y % 2 == 0) {
            dstArray[offset++] = grid.toLinear(x + 1, y - 1)
            dstArray[offset++] = grid.toLinear(x + 1, y + 1)
        } else {
            dstArray[offset++] = grid.toLinear(x - 1, y - 1)
            dstArray[offset++] = grid.toLinear(x - 1, y + 1)
        }

        assert(offset == neighboursCount + dstOffset)
    }
}

class RunningBondGridFactory @Inject constructor() : GridFactory {
    override fun createGrid(
        canvasWidth: Float,
        canvasHeight: Float,
        displayDensity: Float,
        cellSize: Int
    ) = stdGridFactory(
        canvasWidth = canvasWidth,
        canvasHeight = canvasHeight,
        displayDensity = displayDensity,
        cellSize = cellSize,
        colGroupCount = 1,
        rowGroupCount = 2,
        correctionFactorX = 0.6f,
        correctionFactorY = 1.5f
    )
}

class RunningBondTilingOnCanvasFactory @Inject constructor() :
    TilingOnCanvas.Factory {
    override fun create(
        canvasWidth: Float,
        canvasHeight: Float,
        grid: Grid
    ): TilingOnCanvas {
        return PeriodicShapesTilingOnCanvas.create(
            grid,
            canvasWidth = canvasWidth,
            canvasHeight = canvasHeight,
            shapeBuilder = RunningBondPeriodicShapeBuilder(grid, canvasWidth, canvasHeight)
        )
    }

    private class RunningBondPeriodicShapeBuilder(
        grid: Grid,
        canvasWidth: Float,
        canvasHeight: Float
    ) :
        PeriodicShapesTilingOnCanvas.PeriodicShapeBuilder {

        private val cellHeight = canvasHeight / grid.rows.toFloat()
        private val cellWidth = canvasWidth / grid.columns.toFloat()

        override fun createShape(x: Int, y: Int): PeriodicShape {
            val p0 = Vertex(
                cellWidth * x - ((y % 2) * 0.5f * cellWidth), // shift half a cell-width left cells on even rows
                y * cellHeight
            ) // top left
            val p1 = p0.xShift(cellWidth) // top right
            val p2 = p1.yShift(cellHeight)  // bottom right
            val p3 = p0.yShift(cellHeight) // bottom left
            return convexPolygonPeriodicShape(p0, p1, p2, p3)
        }
    }

}