package com.manuel.pagliai.tsol.screens.welcome

import android.graphics.drawable.Drawable
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.manuel.pagliai.tsol.domain.SavedGame
import com.manuel.pagliai.tsol.repository.games.GamesRepository
import com.manuel.pagliai.tsol.repository.preferences.PreferencesRepository
import com.manuel.pagliai.tsol.screens.game.AutomatonColors
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Named

data class SavedGameAnimatedPreviewKey(
    override val gameId : Long,
    val modifiedAt : Long,
    val cellsColor: ULong,
    val gridColor: ULong,
    val backgroundColor: ULong
) : ProcessingOnFutureGenerationsCacheKey

/**
 * View model backing the Welcome screen.
 *
 * Responsible to load the list of [SavedGame]s from the repository, as well as deleting games.
 */
@HiltViewModel
class WelcomeScreenViewModel @Inject constructor(
    private val gamesRepository: GamesRepository,
    /**
     * Evaluates and caches future generations of automaton animated previews
     */
    @Named("GamePreviews")
    val animatedPreviewsCache: ProcessingOnFutureGenerationsCache<SavedGameAnimatedPreviewKey, Drawable>,

    /**
     * Used to retrieve colors (used to render game previews),
     */
    preferencesRepository: PreferencesRepository
) : ViewModel() {

    /**
     * The list of all games stored as retrieved from the repository.
     * Updates as games are added/deleted.
     */
    private val _allGames = mutableStateOf<List<SavedGame>>(emptyList())
    val allGames: State<List<SavedGame>> = _allGames

    /**
     * Colors to be used to render game previews.
     */
    private val _automatonColors = mutableStateOf(
        AutomatonColors(null, null, null)
    )
    val automatonColors: State<AutomatonColors> = _automatonColors

    init {
        viewModelScope.launch {

            _automatonColors.value =
                AutomatonColors.from(preferencesRepository.gameSettings.first())

            preferencesRepository.gameSettings
                .onEach { _automatonColors.value = AutomatonColors.from(it) }
                .launchIn(viewModelScope)

            gamesRepository.getAllGames()
                .onEach { _allGames.value = it }
                .launchIn(viewModelScope)
        }
    }

    fun onDelete(savedGame: SavedGame) {
        viewModelScope.launch {
            animatedPreviewsCache.remove(savedGame)
            gamesRepository.delete(savedGame)
        }
    }
}