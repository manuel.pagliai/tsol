package com.manuel.pagliai.tsol.domain

import com.manuel.pagliai.tsol.domain.GameRules.Companion.PROBABILITY_VALUES
import java.nio.ByteBuffer
import kotlin.random.Random

/**
 * Determines when a dead cell comes to live,
 * or an alive cell survives in the next generation.
 *
 * This class, together with the cells of the automaton determines
 * the evolution of the automaton when running.
 */
data class GameRules(

    /**
     * A dead cell comes to life if the number of live surrounding cells in the current generation
     * appears in this list.
     *
     * The live status of the cell be overwritten by [pRandomDeath] and [pRandomDeath]
     */
    val birthWith: List<Int>,

    /**
     * A live cell will still be alive in the next generation if the number of alive
     * surrounding cells in the current generation appears in this list.
     *
     * The live status of the cell can be overwritten by [pRandomDeath] and [pRandomDeath]
     */
    val surviveWith: List<Int>,

    /**
     * Probability of cell that should have been dead according to [birthWith] and [surviveWith]
     * to be alive in the next generation
     *
     * @see [PROBABILITY_VALUES]
     */
    val pRandomBirth: Float = 0f,

    /**
     * Probability of cell that should be alive according to [birthWith] and [surviveWith]
     * to be dead in the next generation
     *
     * @see [PROBABILITY_VALUES]
     */
    val pRandomDeath: Float = 0f
) {

    /**
     * Evaluate weather a cell should be alive in the next generation according
     */
    fun aliveInNextGen(
        aliveInCurrentGen: Boolean,
        aliveNeighboursCount: Int,
        random: Random
    ): Boolean {
        val alive = if (aliveInCurrentGen) {
            surviveWith.contains(aliveNeighboursCount)
        } else {
            birthWith.contains(aliveNeighboursCount)
        }

        if (alive && pRandomDeath > 0) {
            return random.nextDouble() > pRandomDeath
        }

        if (!alive && pRandomBirth > 0) {
            return random.nextDouble() < pRandomBirth
        }

        return alive
    }

    /**
     * True if there is no randomic/probabilistic component in the game rules, and the game evolution
     * is entirely determined by the initial configuration of the automaton and the [GameRules]
     */
    val isDeterministicGame: Boolean
        get() = pRandomBirth == 0f && pRandomDeath == 0f

    companion object {


        /**
         * Allowed values of [pRandomBirth] and [pRandomDeath].
         */
        val PROBABILITY_VALUES: FloatArray = run {
            val pvs = ArrayList<Float>()
            for (i in 0..100) {
                pvs.add(i * 0.0001f)
            }
            for (i in 2..20) {
                pvs.add(i * 0.01f)
            }
            pvs.toFloatArray()
        }

        /**
         * Some rule presets
         */
        val B3S23 = GameRules(birthWith = listOf(3), surviveWith = listOf(2, 3))
        val B2S34 = GameRules(birthWith = listOf(2), surviveWith = listOf(3, 4))
        val B346S23 = GameRules(birthWith = listOf(3, 4, 6), surviveWith = listOf(2, 3))
        val B3S27 = GameRules(birthWith = listOf(3), surviveWith = listOf(2, 7))
        val B4S234 = GameRules(birthWith = listOf(4), surviveWith = listOf(2, 3, 4))
        val B34S356 = GameRules(birthWith = listOf(3, 4), surviveWith = listOf(3, 5, 6))
        val B3S235 = GameRules(birthWith = listOf(3), surviveWith = listOf(2, 3, 5))
        val B234S23 = GameRules(birthWith = listOf(2, 3, 4), surviveWith = listOf(2, 3))
        val B45S2345 = GameRules(birthWith = listOf(4, 5), surviveWith = listOf(2, 3, 4, 5))
        val B3S24 = GameRules(birthWith = listOf(3), surviveWith = listOf(2, 4))
        val B345S34 = GameRules(birthWith = listOf(3, 4, 5), surviveWith = listOf(3, 4))
        val B34S235 = GameRules(birthWith = listOf(3,4), surviveWith = listOf(2, 3, 5))
        val B56S567 = GameRules(birthWith = listOf(5, 6), surviveWith = listOf(5, 6, 7))

    }

    /**
     * Encode/Decode instances of [GameRules] to/from byte arrays
     * Used when persisting the game on disk.
     */
    object Encoding {

        private val magic: ByteArray = byteArrayOf(0x72, 0x75, 0x6c, 0x65, 0x73)
        private const val version = 1

        fun encode(gameRules: GameRules): ByteArray {
            val buffer = ByteBuffer.allocate(
                magic.size
                        + Int.SIZE_BYTES /*version*/
                        + Int.SIZE_BYTES /* birth size */
                        + Int.SIZE_BYTES * gameRules.birthWith.size
                        + Int.SIZE_BYTES /* survive size */
                        + Int.SIZE_BYTES * gameRules.surviveWith.size
                        + Float.SIZE_BYTES /*pRandomBirth*/
                        + Float.SIZE_BYTES /*pRandomDeath*/

            )
            magic.forEach { buffer.put(it) }
            buffer.putInt(version)

            buffer.putInt(gameRules.birthWith.size)
            gameRules.birthWith.forEach { buffer.putInt(it) }

            buffer.putInt(gameRules.surviveWith.size)
            gameRules.surviveWith.forEach { buffer.putInt(it) }

            buffer.putFloat(gameRules.pRandomBirth)
            buffer.putFloat(gameRules.pRandomDeath)

            return buffer.array()
        }

        fun decode(byteArray: ByteArray): GameRules {
            val byteBuffer = ByteBuffer.wrap(byteArray)
            val magic = ByteArray(magic.size) {
                byteBuffer.get()
            }

            if (!magic.contentEquals(this.magic)) {
                throw IllegalStateException("Unrecognised game rules format $magic")
            }

            val version = byteBuffer.int
            if (version != this.version) {
                throw IllegalStateException("Unknown version for game rules encoding: $version")
            }


            val birthWith = IntArray(byteBuffer.int) { byteBuffer.int }.asList()
            val surviveWith = IntArray(byteBuffer.int) { byteBuffer.int }.asList()
            val pRandomBirth = byteBuffer.float
            val pRandomDeath = byteBuffer.float

            return GameRules(
                birthWith = birthWith,
                surviveWith = surviveWith,
                pRandomBirth = pRandomBirth,
                pRandomDeath = pRandomDeath
            )
        }

    }
}