package com.manuel.pagliai.tsol.screens.game

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.animateColor
import androidx.compose.animation.core.updateTransition
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Slider
import androidx.compose.material.SliderDefaults
import androidx.compose.material.Surface
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.TestTags
import com.manuel.pagliai.tsol.domain.CellType
import com.manuel.pagliai.tsol.domain.GameRules
import kotlin.math.ceil
import kotlin.math.min


@ExperimentalAnimationApi
@Composable
fun ChangeCellTypeDialog(
    onDismissRequest: () -> Unit,
    onNewCellType: (CellType, GameRules) -> Unit,
    probabilityValues: FloatArray,
    cellTypes: List<CellType>,
    currentCellType: CellType,
    currentGameRules: GameRules
) {
    var cellTypeIndex by remember {
        mutableStateOf(cellTypes.indexOf(currentCellType))
    }

    val lazyListState = rememberLazyListState()

    LaunchedEffect(key1 = Unit) {
        lazyListState.animateScrollToItem(cellTypeIndex)
    }


    Dialog(onDismissRequest = onDismissRequest) {
        Surface(
            shape = RoundedCornerShape(10.dp),
            color = MaterialTheme.colorScheme.surface,
            modifier = Modifier.padding(vertical = 32.dp)
        ) {
            Column(modifier = Modifier.padding(16.dp)) {
                Text(
                    text = stringResource(id = R.string.change_cell_type_dialog_title),
                    style = MaterialTheme.typography.headlineMedium
                )
                Spacer(modifier = Modifier.height(16.dp))
                LazyColumn(
                    state = lazyListState,
                    modifier = Modifier.testTag(TestTags.CHANGE_CELL_TYPE_DIALOG)
                ) {
                    items(cellTypes.size) { index ->
                        val cellType = cellTypes[index]
                        val isCurrentCellType = cellTypeIndex == index
                        val rules = if (currentCellType == cellType) {
                            currentGameRules
                        } else {
                            cellType.defaultGameRules
                        }
                        ChangeCellTypeItem(
                            cellType,
                            rules,
                            isCurrentCellType,
                            probabilityValues = probabilityValues,
                            onSelect = { cellTypeIndex = index },
                            onNewCellType = onNewCellType,
                            onDismissRequest = onDismissRequest
                        )
                        Spacer(modifier = Modifier.height(16.dp))
                        Divider()
                        Spacer(modifier = Modifier.height(16.dp))
                    }

                }
            }
        }
    }
}

@OptIn(ExperimentalAnimationApi::class)
@ExperimentalAnimationApi
@Composable
private fun ChangeCellTypeItem(
    cellType: CellType,
    rules: GameRules,
    expanded: Boolean,
    probabilityValues: FloatArray,
    onSelect: () -> Unit,
    onNewCellType: (CellType, GameRules) -> Unit,
    onDismissRequest: () -> Unit,
) {
    Column(modifier = Modifier.clickable { onSelect() }) {
        Row(
            Modifier
                .padding(8.dp)
                .fillMaxWidth()
                .height(128.dp),
            verticalAlignment = CenterVertically
        ) {
            Image(
                painter = painterResource(id = cellType.icon),
                contentDescription = stringResource(id = cellType.name),
                colorFilter = ColorFilter.tint(color = MaterialTheme.colorScheme.primary),
                contentScale = ContentScale.FillHeight
            )
            Box(modifier = Modifier.fillMaxSize()) {
                Column(
                    modifier = Modifier.align(
                        Alignment.Center
                    )
                ) {
                    Text(
                        text = stringResource(id = cellType.name),
                        style = MaterialTheme.typography.titleMedium
                    )
                    Text(
                        text = stringResource(
                            id = R.string.cell_type_neighbours_count_label,
                            cellType.neighboursCount
                        ),
                        style = MaterialTheme.typography.titleSmall
                    )
                }
            }

        }
        AnimatedVisibility(expanded) {
            CellRulesContent(
                cellType,
                rules,
                probabilityValues,
                { rules -> onNewCellType(cellType, rules) },
                onDismissRequest
            )
        }
    }

}

@Composable
fun CellRulesContent(
    cellType: CellType,
    rules: GameRules,
    probabilityValues: FloatArray,
    onApply: (GameRules) -> Unit,
    onDismissRequest: () -> Unit,
) {
    /**
     * i-th element of the array contains a boolean representing whether
     * a dead cell with i+1 alive neighbours will be born in the next generation
     *
     * [Does not make sense to define behaviours 0 neighbours]
     */
    var birthWith: BooleanArray by remember {
        mutableStateOf(BooleanArray(cellType.neighboursCount) {
            rules.birthWith.contains(
                it + 1
            )
        })
    }

    /**
     * i-th element of the array contains a boolean representing whether
     * a cell alive with i+1 alive neighbours will live in the next generation
     *
     * [Does not make sense to define behaviours 0 neighbours]
     */
    var surviveWith: BooleanArray by remember {
        mutableStateOf(BooleanArray(cellType.neighboursCount) {
            rules.surviveWith.contains(
                it + 1
            )
        })
    }

    var pRandomBirth by remember { mutableStateOf(probabilityValues.indexOfFirst { it == rules.pRandomBirth }) }
    var pRandomDeath by remember { mutableStateOf(probabilityValues.indexOfFirst { it == rules.pRandomDeath }) }

    Column {


        NeighboursRules(
            isValueSelected = birthWith,
            title = {
                Text(
                    text = stringResource(id = R.string.cells_become_alive_with_title),
                    style = MaterialTheme.typography.titleSmall
                )
            },
            onItemSelectedUpdated = { index, isSelected ->
                birthWith = birthWith.clone().also { it[index] = isSelected }
            },
            label = "born_with"
        )

        Spacer(modifier = Modifier.height(16.dp))

        NeighboursRules(
            isValueSelected = surviveWith,
            title = {
                Text(
                    text = stringResource(id = R.string.cells_survive_with),
                    style = MaterialTheme.typography.titleSmall
                )
            },
            onItemSelectedUpdated = { index, isSelected ->
                surviveWith = surviveWith.clone().also { it[index] = isSelected }
            },
            label = "survive_with"
        )

        Spacer(modifier = Modifier.height(16.dp))

        ProbabilityValueSlider(
            pRandomBirth,
            probabilityValues,
            stringResource(id = R.string.random_birth_probability)
        ) {
            pRandomBirth = it
        }

        ProbabilityValueSlider(
            pRandomDeath,
            probabilityValues,
            stringResource(id = R.string.random_death_probability)
        ) {
            pRandomDeath = it
        }

        TextButton(

            onClick = {
                val defaultGameRules = cellType.defaultGameRules
                birthWith = BooleanArray(cellType.neighboursCount) {
                    defaultGameRules.birthWith.contains(
                        it + 1
                    )
                }
                surviveWith = BooleanArray(cellType.neighboursCount) {
                    defaultGameRules.surviveWith.contains(
                        it + 1
                    )
                }
                pRandomBirth =
                    probabilityValues.indexOfFirst { it == defaultGameRules.pRandomBirth }
                pRandomDeath =
                    probabilityValues.indexOfFirst { it == defaultGameRules.pRandomDeath }
            }) {
            Text(text = stringResource(id = R.string.default_rules_label))
        }

        Row(modifier = Modifier.align(alignment = Alignment.End)) {
            TextButton(onClick = onDismissRequest) {
                Text(text = stringResource(id = R.string.cancel_label))
            }

            TextButton(onClick = {
                onApply(
                    GameRules(
                        surviveWith = surviveWith
                            .withIndex()
                            .filter { (_, selected) -> selected }
                            .map { (index, _) -> index + 1 }
                            .toList(),
                        birthWith = birthWith
                            .withIndex()
                            .filter { (_, selected) -> selected }
                            .map { (index, _) -> index + 1 }
                            .toList(),
                        pRandomDeath = probabilityValues[pRandomDeath],
                        pRandomBirth = probabilityValues[pRandomBirth]
                    )
                )
            }) {
                Text(text = stringResource(id = R.string.apply_label))
            }
        }

    }
}

@Composable
fun NeighboursRules(
    isValueSelected: BooleanArray,
    title: @Composable () -> Unit,
    onItemSelectedUpdated: (Int, Boolean) -> Unit,
    label: String
) {
    val boxPadding = 4.dp
    val boxWidth = 36.dp
    val boxHeight = 36.dp

    Column {
        title()
        BoxWithConstraints(
            Modifier
                .fillMaxWidth()
        ) {
            // arrange element in a grid. Number of elements displayed in the grid depends on how wide the main box is
            val perRowCount = (maxWidth.value / (boxWidth.value + 2f * boxPadding.value)).toInt()
            Column {
                repeat(ceil(isValueSelected.size.toFloat() / perRowCount).toInt()) { row ->
                    Row {
                        val remaining = isValueSelected.size - row * perRowCount
                        repeat(min(perRowCount, remaining)) { col ->
                            val boxValue =
                                row * perRowCount + col // value for the box, 0,1,2... associated to 1,2,3... alive neighbours
                            val isBoxSelected = isValueSelected[boxValue]
                            // Animates changes when `selected` is changed.
                            val transition =
                                updateTransition(
                                    isBoxSelected,
                                    label = "${label}_transition_${boxValue}"
                                )
                            val boxColor by transition.animateColor(label = "${label}_anim_box_color_${boxValue}") { isSelected ->
                                if (isSelected) MaterialTheme.colorScheme.primary else MaterialTheme.colorScheme.background
                            }
                            val textColor by transition.animateColor(label = "${label}_anim_text_color_${boxValue}") { isSelected ->
                                if (isSelected) MaterialTheme.colorScheme.onPrimary else MaterialTheme.colorScheme.onBackground
                            }
                            Box(
                                modifier = Modifier
                                    .padding(boxPadding)
                                    .width(boxWidth)
                                    .height(boxHeight)
                                    .clip(CircleShape)
                                    .background(boxColor)
                                    .clickable {
                                        onItemSelectedUpdated(boxValue, !isBoxSelected)
                                    }
                            ) {
                                Text(
                                    text = "${boxValue + 1}",
                                    color = textColor,
                                    modifier = Modifier.align(Alignment.Center)
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun ProbabilityValueSlider(
    pIndex: Int,
    probabilityValues: FloatArray,
    label: String,
    onValueChange: (Int) -> Unit
) {
    Column {

        var expanded: Boolean by remember {
            mutableStateOf(pIndex > 0)
        }

        Row(horizontalArrangement = Arrangement.SpaceBetween) {
            Text(
                text = label,
                style = MaterialTheme.typography.titleSmall,
                modifier = Modifier.align(CenterVertically)
            )
            if (expanded) {
                IconButton(onClick = { expanded = false }) {
                    Icon(
                        painterResource(id = R.drawable.ic_expand_less_black_24dp),
                        contentDescription = stringResource(
                            id = R.string.hide_slider_cd
                        )
                    )
                }
            } else {
                IconButton(onClick = { expanded = true }) {
                    Icon(
                        painterResource(id = R.drawable.ic_chevron_right_black_24dp),
                        contentDescription = stringResource(
                            id = R.string.show_slider_cd
                        )
                    )
                }
            }
        }

        AnimatedVisibility(expanded) {

            Row {

                Box(Modifier.align(CenterVertically)) {
                    Text(text = "%.4f".format(probabilityValues[pIndex]))
                }
                Spacer(modifier = Modifier.width(8.dp))
                Slider(
                    value = pIndex.toFloat(),
                    valueRange = 0f..(probabilityValues.size - 1).toFloat(),
                    steps = probabilityValues.size,
                    colors = SliderDefaults.colors(
                        thumbColor = MaterialTheme.colorScheme.primary,
                        activeTrackColor = MaterialTheme.colorScheme.primary,
                        activeTickColor = Color.Transparent,
                        inactiveTickColor = Color.Transparent
                    ),
                    onValueChange = { onValueChange(it.toInt()) },
                    modifier = Modifier.fillMaxWidth(),
                )
            }
        }
    }
}