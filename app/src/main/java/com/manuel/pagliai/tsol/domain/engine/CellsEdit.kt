package com.manuel.pagliai.tsol.domain.engine

import com.manuel.pagliai.tsol.domain.Cells

/**
 * An [CellsEdit] is an action the user has performed to change the [Cells] of an automaton
 * that can be undone
 */
interface CellsEdit {

    /**
     * Applies the edit to @param cells and returns the edited cells
     */
    fun applyEdit(cells: Cells): Cells

    /**
     * Creates an edit for the 'undo' action of this [CellsEdit]
     *
     *  it should be the case that for an edit e and cells c:
     *  c == e.createUndo().apply(e.applyEdit(c))
     */
    fun createUndo(): CellsEdit

}

/**
 * A single cell toggles its alive state (user toggles the alive value of a cell)
 */
data class SingleCellEdit(
    val coordinates: Int,
    val alive: Boolean
) : CellsEdit {

    override fun createUndo() = copy(alive = !this.alive)
    override fun applyEdit(cells: Cells) = cells.setCell(coordinates, alive)

}

/**
 * All the cells have been replaced (e.g., the user clears the screen killing all cells)
 */
data class CellsReplacement(val from: Cells, val to: Cells) :
    CellsEdit {

    override fun createUndo() = copy(from = this.to, to = this.from)
    override fun applyEdit(cells: Cells): Cells {
        assert(from == cells)
        return this.to
    }

}
