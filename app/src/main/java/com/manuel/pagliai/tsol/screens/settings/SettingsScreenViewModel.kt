package com.manuel.pagliai.tsol.screens.settings

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.graphics.Color
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.manuel.pagliai.tsol.repository.preferences.PreferencesRepository
import com.manuel.pagliai.tsol.repository.preferences.PreferencesState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * View model backing the [SettingsScreen]
 * Mostly wrapping [PreferencesRepository] calls with [viewModelScope]
 */
@HiltViewModel
class SettingsScreenViewModel @Inject constructor(private val preferencesRepository: PreferencesRepository) :
    ViewModel() {

    private val _preferencesState: MutableState<PreferencesState?> = mutableStateOf(null)
    val preferencesState: State<PreferencesState?> = _preferencesState

    init {
        preferencesRepository.preferencesState.onEach { _preferencesState.value = it }
            .launchIn(viewModelScope)
    }

    fun updateShowNexGenWhenIdle(newValue: Boolean) {
        viewModelScope.launch {
            preferencesRepository.updateShowNexGenWhenIdle(newValue)
        }
    }

    fun updateRandomiseNewGames(newValue: Boolean) {
        viewModelScope.launch {
            preferencesRepository.updateRandomiseNewGamesCells(newValue)
        }
    }

    fun updateUseSystemColors(newValue: Boolean) {
        viewModelScope.launch {
            preferencesRepository.updateUseSystemColors(newValue)
        }
    }

    fun updateCellsColor(newValue: Color) {
        viewModelScope.launch {
            preferencesRepository.updateCellsColor(newValue)
        }
    }

    fun updateGridColor(newValue: Color) {
        viewModelScope.launch {
            preferencesRepository.updateGridColor(newValue)
        }
    }

    fun updateBackgroundColor(newValue: Color) {
        viewModelScope.launch {
            preferencesRepository.updateBackgroundColor(newValue)
        }
    }

    fun updateNewGamesDefaultSpeed(newValue: Int) {
        viewModelScope.launch {
            preferencesRepository.updateNewGamesDefaultSpeed(newValue)
        }
    }

    fun updateNewGamesDefaultCellSize(newValue: Int) {
        viewModelScope.launch {
            preferencesRepository.updateNewGamesDefaultCellSize(newValue)
        }
    }
}