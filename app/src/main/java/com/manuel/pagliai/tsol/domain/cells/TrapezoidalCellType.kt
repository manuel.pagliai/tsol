package com.manuel.pagliai.tsol.domain.cells

import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.domain.CellType
import com.manuel.pagliai.tsol.domain.GameRules.Companion.B34S356
import com.manuel.pagliai.tsol.domain.Grid
import com.manuel.pagliai.tsol.domain.GridFactory
import com.manuel.pagliai.tsol.domain.GridFactory.Companion.stdGridFactory
import com.manuel.pagliai.tsol.domain.TilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShape
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShapesTilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.Vertex
import com.manuel.pagliai.tsol.domain.tiling.convexPolygonPeriodicShape
import javax.inject.Inject

val TrapezoidalCellsGrouping = HorizontalCellsGrouping(6)

class TrapezoidalCellType @Inject constructor(
    override val gridFactory: TrapezoidalGridFactory,
    override val tilingOnCanvasFactory: TrapezoidalTilingOnCanvasFactory
) : CellType {

    @Suppress("unused") // useful when testing/debugging
    constructor() : this(
        gridFactory = TrapezoidalGridFactory(),
        tilingOnCanvasFactory = TrapezoidalTilingOnCanvasFactory()
    )

    override val name = R.string.trapezoidal_cell_type
    override val icon = R.drawable.ic_trapezoidal_cell_type
    override val id = "TRAPEZOIDAL"
    override val neighboursCount = 8
    override val defaultGameRules = B34S356

    override fun setNeighboursCoordinates(
        grid: Grid,
        x: Int,
        y: Int,
        dstArray: IntArray,
        dstOffset: Int
    ) {
        var offset = dstOffset
        val (groupX, groupY) = TrapezoidalCellsGrouping.groupXY(x, y)

        val rcf = groupY % 2 // correction factor

        when (TrapezoidalCellsGrouping.cellGroupIndex(x)) {
            0 -> {
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX, groupY, 1)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX, groupY, 2)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX - rcf, groupY + 1, 1)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX - rcf, groupY + 1, 3)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX - rcf, groupY + 1, 4)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX - 1 - rcf, groupY + 1, 4)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX - 1, groupY, 2)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX - 1, groupY, 5)
            }
            1 -> {
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX, groupY, 0)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX, groupY, 2)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX, groupY, 3)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX - 1, groupY, 4)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX - 1, groupY, 5)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX - rcf, groupY - 1, 2)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX - rcf, groupY - 1, 5)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX + 1 - rcf, groupY - 1, 0)
            }
            2 -> {
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX, groupY, 0)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX, groupY, 1)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX, groupY, 3)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX, groupY, 5)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX + 1, groupY, 0)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX + 0 - rcf, groupY + 1, 4)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX + 1 - rcf, groupY + 1, 1)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX + 1 - rcf, groupY + 1, 3)
            }
            3 -> {
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX, groupY, 1)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX, groupY, 2)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX, groupY, 4)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX, groupY, 5)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX + 1 - rcf, groupY - 1, 0)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX - rcf, groupY - 1, 2)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX - rcf, groupY - 1, 5)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX - 1, groupY, 4)
            }
            4 -> {
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX, groupY, 3)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX, groupY, 5)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX + 1, groupY, 1)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX + 1, groupY, 3)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX + 1 - rcf, groupY - 1, 0)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX + 1 - rcf, groupY - 1, 2)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX + 1 - rcf, groupY - 1, 5)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX + 2 - rcf, groupY - 1, 0)
            }
            5 -> {
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX, groupY, 2)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX, groupY, 3)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX, groupY, 4)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX + 1, groupY, 0)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX + 1, groupY, 1)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX - rcf, groupY + 1, 4)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX + 1 - rcf, groupY + 1, 1)
                dstArray[offset++] =
                    TrapezoidalCellsGrouping.toCellLinear(grid, groupX + 1 - rcf, groupY + 1, 3)
            }
        }

        assert(offset == dstOffset + neighboursCount)
    }
}

class TrapezoidalGridFactory @Inject constructor() : GridFactory {

    override fun createGrid(
        canvasWidth: Float,
        canvasHeight: Float,
        displayDensity: Float,
        cellSize: Int
    ) = stdGridFactory(
        canvasWidth = canvasWidth,
        canvasHeight = canvasHeight,
        displayDensity = displayDensity,
        cellSize = cellSize,
        colGroupCount = 6,
        rowGroupCount = 2,
        correctionFactorY = 0.5f,
        correctionFactorX = 2f
    )
}

class TrapezoidalTilingOnCanvasFactory @Inject constructor() :
    TilingOnCanvas.Factory {
    override fun create(canvasWidth: Float, canvasHeight: Float, grid: Grid): TilingOnCanvas {
        return PeriodicShapesTilingOnCanvas.create(
            grid,
            canvasWidth = canvasWidth,
            canvasHeight = canvasHeight,
            shapeBuilder = TrapezoidPeriodicShapeBuilder(grid, canvasWidth, canvasHeight)
        )
    }


    private class TrapezoidPeriodicShapeBuilder(
        grid: Grid,
        private val canvasWidth: Float,
        private val canvasHeight: Float
    ) : PeriodicShapesTilingOnCanvas.PeriodicShapeBuilder {

        private val triangleBase =
            (canvasWidth / (grid.columns / 6).toFloat())
        private val triangleHeight =
            (canvasHeight / (grid.rows).toFloat())

        override fun createShape(x: Int, y: Int): PeriodicShape {
            val (groupX, groupY) = TrapezoidalCellsGrouping.groupXY(x, y)
            val cellGroupIndex = TrapezoidalCellsGrouping.cellGroupIndex(x)


            val p0X = if (groupY % 2 == 0) {
                triangleBase * groupX
            } else {
                triangleBase * (groupX) - triangleBase / 2
            }
            val p0Y = triangleHeight * (groupY + 1)

            return createFromBase(
                p0X, p0Y, cellGroupIndex
            ).also {
                it.horizontalConstraint(canvasHeight)
                it.verticalConstraint(canvasWidth)
            }
        }

        private fun createFromBase(
            p0X: Float,
            p0Y: Float,
            cellGroupIndex: Int
        ): PeriodicShape {

            val p0 = Vertex(p0X, p0Y)
            val p2 = Vertex(p0X + triangleBase / 2, p0Y - triangleHeight)
            val p1 = vertexOnLine(p0, p2, triangleBase / 6f)
            val p3 = Vertex(p0X + triangleBase / 2, p1.y)
            val p4 = Vertex(p0X + triangleBase - triangleBase / 3f, p0Y)
            val p5 = Vertex(p0X + triangleBase, p0Y)
            val p6 = vertexOnLine(p2, p5, triangleBase / 6)
            val p7 = vertexOnLine(p2, p5, triangleBase / 3f)
            val p8 = Vertex(p0X + 5f / 6f * triangleBase, p0Y - triangleHeight)
            val p9 = Vertex(p0X + 1.5f * triangleBase, p0Y - triangleHeight)
            val p10 = Vertex(p0X + triangleBase, p6.y)
            val p11 = vertexOnLine(p5, p9, triangleBase / 3f)

            return when (cellGroupIndex) {
                0 -> convexPolygonPeriodicShape(
                    p0,
                    p1,
                    p3,
                    p4
                )
                1 -> convexPolygonPeriodicShape(
                    p1,
                    p2,
                    p6,
                    p3
                )
                2 -> convexPolygonPeriodicShape(
                    p4,
                    p3,
                    p6,
                    p5
                )
                3 -> convexPolygonPeriodicShape(
                    p7,
                    p2,
                    p8,
                    p10
                )
                4 -> convexPolygonPeriodicShape(
                    p10,
                    p8,
                    p9,
                    p11
                )
                5 -> convexPolygonPeriodicShape(
                    p5,
                    p7,
                    p10,
                    p11
                )
                else -> throw IllegalStateException()
            }
        }

        /**
         * On a non vertical segment frp, @param [from] to @param [to]
         * return the vertex with x [from].x + [deltaX]
         */
        private fun vertexOnLine(from: Vertex, to: Vertex, deltaX: Float): Vertex {
            val delta = (to.y - from.y) / (to.x - from.x)
            return Vertex(
                x = from.x + deltaX,
                y = from.y + delta * deltaX
            )
        }

    }
}
