package com.manuel.pagliai.tsol.screens.common

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.TestTags

/**
 * Back button as in 'navigate back' meant to be put in the top bar
 */
@Composable
fun TopAppBarBackButton(onBackPressed: () -> Unit) {
    IconButton(onBackPressed) {
        Icon(
            imageVector = Icons.Default.ArrowBack,
            contentDescription = stringResource(id = R.string.back_cd)
        )
    }
}

/**
 * Styles the top bar title
 */
@Composable
fun TopBarAppTitle(title: String) {
    Text(
        title,
        maxLines = 1,
        overflow = TextOverflow.Ellipsis
    )
}

/**
 * Three dots menu, also called overflow menu. Can be used as part of the top app bar,
 * and other components
 */
@Composable
fun ThreeDotsMenu(
    items: List<ThreeDotsMenuItem>,
    modifier: Modifier = Modifier,
    buttonTestTag: String = TestTags.THREE_DOTS_MENU_TEST_TAG
) {
    var showMenu by remember { mutableStateOf(false) }

    Row(modifier = modifier) {
        IconButton(
            onClick = { showMenu = !showMenu },
            modifier = Modifier.testTag(buttonTestTag)
        ) {
            Icon(
                imageVector = Icons.Default.MoreVert, contentDescription = stringResource(
                    id = R.string.menu_cd
                )
            )
        }

        DropdownMenu(
            expanded = showMenu,
            onDismissRequest = { showMenu = false }
        ) {
            items.forEach {
                when (it) {
                    is ThreeDotsMenuItem.Divider -> Divider()
                    is ThreeDotsMenuItem.ClickableLabel -> {
                        DropdownMenuItem(
                            text = { Text(text = it.label) },
                            onClick = {
                                showMenu = false
                                it.onClick()
                            })
                    }
                    is ThreeDotsMenuItem.ClickableLabelWithIcon -> {
                        DropdownMenuItem(
                            text = {
                                Row {
                                    Icon(
                                        imageVector = it.icon,
                                        contentDescription = it.contentDescription,
                                        tint = it.tint ?: LocalContentColor.current
                                    )
                                    Spacer(modifier = Modifier.width(8.dp))
                                    Text(text = it.label)
                                }
                            },
                            onClick = {
                                showMenu = false
                                it.onClick()
                            })
                    }
                }
            }
        }
    }
}

/**
 * Models an entry in a three dots menu
 * @see [ThreeDotsMenu]
 */
sealed class ThreeDotsMenuItem {
    data class ClickableLabel(val label: String, val onClick: () -> Unit) : ThreeDotsMenuItem()
    object Divider : ThreeDotsMenuItem()
    data class ClickableLabelWithIcon(
        val label: String,
        val icon: ImageVector,
        val contentDescription: String,
        val tint: Color? = null,
        val onClick: () -> Unit
    ) : ThreeDotsMenuItem()
}
