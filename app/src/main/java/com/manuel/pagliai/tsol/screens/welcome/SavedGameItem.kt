package com.manuel.pagliai.tsol.screens.welcome

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import androidx.appcompat.content.res.AppCompatResources
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Info
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow.Companion.Ellipsis
import androidx.compose.ui.unit.dp
import com.google.accompanist.drawablepainter.rememberDrawablePainter
import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.TestTags
import com.manuel.pagliai.tsol.domain.Cells
import com.manuel.pagliai.tsol.domain.SavedGame
import com.manuel.pagliai.tsol.screens.common.ThreeDotsMenu
import com.manuel.pagliai.tsol.screens.common.ThreeDotsMenuItem
import com.manuel.pagliai.tsol.screens.game.AutomatonColors
import com.manuel.pagliai.tsol.screens.game.AutomatonRenderer
import com.manuel.pagliai.tsol.screens.game.valuesOr
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * A [SavedGame] in the [GameGallery] of the [WelcomeScreen].
 *
 * Displays a preview of the automaton, looping over a finite amount of generations.
 * Previews are rendered off the main thread, and displayed when ready.
 *
 * If the item is selected [onSelect] is invoked and the game should be loaded in [com.manuel.pagliai.tsol.screens.game.GameScreen].
 *
 * Also handles showing the overflow menu for the saved game, displaying controls to display the details [onDetails] or delete the game [onDelete].
 *
 * [automatonColors] and [animatedPreviewsCache] are used to render the previews of the game.
 * @see [WelcomeScreenViewModel.animatedPreviewsCache]
 */
@Composable
fun SavedGameItem(
    savedGame: SavedGame,
    animatedPreviewsCache: ProcessingOnFutureGenerationsCache<SavedGameAnimatedPreviewKey, Drawable>,
    automatonColors: AutomatonColors,
    onDelete: () -> Unit,
    onSelect: () -> Unit,
    onDetails: () -> Unit,
    modifier: Modifier = Modifier,
) {

    BoxWithConstraints(
        Modifier
            .fillMaxWidth()
            .testTag(TestTags.SAVED_GAME_ITEM)
    ) {

        val previewWidth = with(LocalDensity.current) { maxWidth.toPx() }
        val previewHeight = previewWidth / savedGame.previewAspectRatio

        Column(modifier = modifier.clickable { onSelect() }) {
            SavedGamePreview(
                context = LocalContext.current,
                previewWidth = previewWidth.toInt(),
                previewHeight = previewHeight.toInt(),
                savedGame = savedGame,
                animatedPreviewsCache = animatedPreviewsCache,
                automatonColors = automatonColors
            )
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier.fillMaxWidth()
            ) {
                Text(
                    text = savedGame.name,
                    style = MaterialTheme.typography.bodyLarge,
                    modifier = Modifier
                        .align(
                            Alignment.CenterVertically
                        )
                        .weight(1f),
                    maxLines = 1,
                    overflow = Ellipsis
                )
                SavedGameMenu(
                    modifier = Modifier
                        .align(
                            Alignment.CenterVertically
                        )
                        .weight(0.1f),
                    onDetails = onDetails,
                    onDelete = onDelete
                )
            }
        }
    }
}

@Composable
private fun SavedGameMenu(
    modifier: Modifier = Modifier,
    onDetails: () -> Unit,
    onDelete: () -> Unit
) {
    ThreeDotsMenu(
        items = listOf(
            ThreeDotsMenuItem.ClickableLabelWithIcon(
                label = stringResource(id = R.string.details),
                icon = Icons.Default.Info,
                contentDescription = stringResource(id = R.string.details_cd),
                onClick = onDetails
            ),

            ThreeDotsMenuItem.ClickableLabelWithIcon(
                label = stringResource(id = R.string.delete),
                icon = Icons.Default.Delete,
                contentDescription = stringResource(id = R.string.delete_cd),
                tint = Color.Red,
                onClick = onDelete
            )
        ),
        buttonTestTag = TestTags.SAVED_GAME_THREE_DOTS_MENU_TEST_TAG,
        modifier = modifier,
    )
}


@Composable
private fun SavedGamePreview(
    context: Context,
    previewWidth: Int,
    previewHeight: Int,
    savedGame: SavedGame,
    animatedPreviewsCache: ProcessingOnFutureGenerationsCache<SavedGameAnimatedPreviewKey, Drawable>,
    automatonColors: AutomatonColors
) {

    val (cellsColor, gridColor, backgroundColor) = automatonColors.valuesOr(MaterialTheme.colorScheme)

    val previewDrawable = remember {
        mutableStateOf(
            AppCompatResources.getDrawable(
                context,
                R.drawable.ic_loading_icon_a
            )!!// initialise the preview with a stock loading icon.
        )
    }

    val previewKey = SavedGameAnimatedPreviewKey(
        savedGame.id!!,
        savedGame.modifiedAt,
        cellsColor.value,
        gridColor.value,
        backgroundColor.value
    )

    LaunchedEffect(previewKey) {
        // Evaluate the game preview drawable, and assign it to the preview when ready.
        previewDrawable.value = withContext(Dispatchers.Default) {
            animatedPreviewsCache.getCachedOrEval(
                previewKey,
                savedGame
            ) { generations ->
                generatePreviewAnimatedDrawable(
                    context.resources,
                    savedGame,
                    generations,
                    previewWidth,
                    previewHeight,
                    cellsColor = cellsColor,
                    gridColor = gridColor,
                    backgroundColor = backgroundColor
                )
            }
        }
    }

    SavedGamePreviewContent(previewDrawable.value)
}

@Composable
private fun SavedGamePreviewContent(
    previewDrawable: Drawable
) {
    Image(
        painter = rememberDrawablePainter(previewDrawable),
        contentDescription = "",
        contentScale = ContentScale.None,
        modifier = Modifier.clip(RoundedCornerShape(10.dp)),
    )


    if (previewDrawable is AnimationDrawable) {
        previewDrawable.start()
    }
}

/**
 * @return an [AnimationDrawable] representing a render the [savedGame] looping trough [generations].
 * This must be called off the main thread.
 */
private suspend fun generatePreviewAnimatedDrawable(
    resources: Resources,
    savedGame: SavedGame,
    generations: List<Cells>,
    previewWidth: Int,
    previewHeight: Int,
    cellsColor: Color,
    gridColor: Color,
    backgroundColor: Color,
    longFrameDuration: Int = 1500,
    shortFrameDuration: Int = 500
): AnimationDrawable {

    val renderer = AutomatonRenderer(
        width = previewWidth,
        height = previewHeight,
        gridStrokeWidth = 1f
    )
    try {
        val preview = AnimationDrawable()
        generations.forEachIndexed { index, cells ->
            val duration =
                if (index == 0 || index == generations.size - 1) longFrameDuration else shortFrameDuration
            renderer.prepareRenderer(cellType = savedGame.cellType, grid = cells.grid)
            renderer.render(
                cellType = savedGame.cellType,
                nextGenCells = cells,
                currentGenCells = cells,
                showNextGenCellsAnimation = false,
                cellsColor = cellsColor,
                gridColor = gridColor,
                backgroundColor = backgroundColor,
                dyingCellsColor = cellsColor,
                newBornCellsColor = null
            ) { bitmap ->
                preview.addFrame(
                    BitmapDrawable(resources, Bitmap.createBitmap(bitmap)),
                    duration
                )
            }
            preview.isOneShot = false
        }
        return preview
    } finally {
        renderer.recycle()
    }

}

