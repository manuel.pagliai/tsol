package com.manuel.pagliai.tsol.screens.welcome

import androidx.annotation.GuardedBy
import com.manuel.pagliai.tsol.domain.CellType
import com.manuel.pagliai.tsol.domain.Cells
import com.manuel.pagliai.tsol.domain.GameRules
import com.manuel.pagliai.tsol.domain.SavedGame
import com.manuel.pagliai.tsol.domain.engine.AutomatonNextGenCalculator
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.util.*


/**
 * Caches the result of a processing on the future generations of a [SavedGame].
 * Results are stored associated to a [ProcessingOnFutureGenerationsCacheKey].
 *
 * A cache entry can be will be returned if the key matches with one of the previous entries.
 * A non matching [ProcessingOnFutureGenerationsCacheKey] but with the same [ProcessingOnFutureGenerationsCacheKey.gameId]
 * will be considered stale and removed from the cache.
 *
 * When evaluating future generations, If a still life is encountered, next gen evaluation is halted.
 *
 * The parameter R represents the result of the processing.
 *
 * The cache follows a LRU policy.
 *
 * In practice this is used to cache animated previews of saved games in the games gallery
 */
class ProcessingOnFutureGenerationsCache<K : ProcessingOnFutureGenerationsCacheKey, R>(

    /**
     * Size of the cache
     */
    private val cacheSize: Int,

    /**
     * Maximum number of future generations evaluated. If a still life is encountered, this number of future generations won't be reached
     */
    private val maxGenCount: Int,

    /**
     * Used to evaluate next generations of the automata. The same instance is re-used across different evaluation
     * of future generations.
     */
    @GuardedBy("mutex")
    private val nextGenCalculator: AutomatonNextGenCalculator,
) {
    /**
     * Represents an entry in the [lruCache].
     */
    private data class CacheEntry<K, P>(
        val key : K,
        val payload: P
    )

    /**
     * Used to serialise access to the cache or [lruCache] or [nextGenCalculator]
     */
    private val mutex = Mutex()

    /**
     * Represents the cache. The first element of the list represents the most recent item.
     */
    @GuardedBy("mutex")
    private val lruCache = LinkedList<CacheEntry<K, R>>()

    init {
        assert(maxGenCount > 0)
    }

    /**
     * Retrieves the cached value for [key], or evaluates the payload for [savedGame] and stores the result
     * in the cache with the key [key].
     *
     * If other keys in the cache have the same [ProcessingOnFutureGenerationsCacheKey.gameId], but they do
     * not match [key], they are considered stale and removed from the cache.
     */
    suspend fun getCachedOrEval(
        key : K?,
        savedGame: SavedGame,
        payloadBuilder: suspend (List<Cells>) -> R
    ): R {
        if (key == null) {
            return mutex.withLock {
                payloadBuilder(
                    evalGenerations(
                        initialCells = savedGame.cells,
                        cellType = savedGame.cellType,
                        gameRules = savedGame.rules
                    )
                )
            }
        }

        assert(key.gameId == savedGame.id)

        return mutex.withLock {
            getOrRemoveStale(key) ?: CacheEntry(
                key,
                payloadBuilder
                    (
                    evalGenerations(
                        initialCells = savedGame.cells,
                        cellType = savedGame.cellType,
                        gameRules = savedGame.rules
                    )
                )
            ).also {
                put(it)
            }
        }.payload
    }

    /**
     * Notifies the cache that @param [savedGame] has been deleted and it can be removed from the cache.
     * Deletes all cache entries where the key matches the id of [savedGame]
     */
    suspend fun remove(savedGame: SavedGame) {
        savedGame.id?.let { gameId ->
            mutex.withLock {
                removeImplById(gameId)
            }
        }
    }

    private fun evalGenerations(
        initialCells: Cells,
        cellType: CellType,
        gameRules: GameRules
    ): List<Cells> {
        var cells = initialCells
        val generations = mutableListOf(cells)
        var count = 1
        while (count < maxGenCount) {
            cells = nextGenCalculator.nextGen(cells, cellType, gameRules)
            if (gameRules.isDeterministicGame && generations.last() == cells) {
                // this is a still life. No point in evaluating the next generation  over and over
                break
            }
            generations.add(cells)
            count++
        }
        return generations
    }

    /**
     * Retrieves the cache entry matching [SavedGame.id] and [SavedGame.modifiedAt] or null if none is present.
     * Any entries matching [SavedGame.id] but not [SavedGame.modifiedAt] will still be removed (stale)
     *
     * If found, the entry will be considered as the last recently used entry.
     * [mutex] must be held while calling this method
     */
    private fun getOrRemoveStale(key : K): CacheEntry<K,R>? {
        val cached = removeImplById(key.gameId)
        if (cached != null && cached.key == key) {
            put(cached)
            return cached
        }
        return null
    }

    /**
     * Removes the entry from the cache (if found).
     * @return the [CacheEntry] of the entry matches with [gameId] has been removed from the cache, or null
     * if none of the cache entries matches [gameId]
     * [mutex] must be held while calling this method
     */
    private fun removeImplById(gameId: Long): CacheEntry<K,R>? {
        lruCache.iterator().let {
            while (it.hasNext()) {
                val entry = it.next()
                if (entry.key.gameId == gameId) {
                    it.remove()
                    return entry
                }
            }
        }
        return null
    }

    /**
     * Adds [cacheEntry] to the cache as the most recently used element.
     * Removes the last recently used [CacheEntry] if needed.
     * [mutex] must be held while calling this method
     */
    private fun put(cacheEntry: CacheEntry<K,R>) {
        if (lruCache.size >= cacheSize) {
            lruCache.removeLast()
        }
        lruCache.add(0, cacheEntry)

        // make sure every game id is unique among all keys in the cache.
        assert(lruCache.map { it.key.gameId }.distinct().count() == lruCache.size)
    }

}

/**
 * Used as a key in the cache.
 *
 * This level of abstraction is necessary as the payload itself might have restrictions on when a payload can be reused
 * and when it needs to be re-evaluated instead.
 *
 * For example, in the game gallery, the payload represents the animated drawable of the game,
 * the key is not only the saved game, but also the colors used to render the automaton.
 */
interface ProcessingOnFutureGenerationsCacheKey {
    /**
     * The [SavedGame.id] associated to the game backing the key.
     * @see [ProcessingOnFutureGenerationsCache.getOrRemoveStale]
     * @see [ProcessingOnFutureGenerationsCache.remove],
     */
    val gameId : Long
}
