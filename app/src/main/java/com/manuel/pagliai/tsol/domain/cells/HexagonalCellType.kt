package com.manuel.pagliai.tsol.domain.cells

import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.domain.CellType
import com.manuel.pagliai.tsol.domain.GameRules.Companion.B2S34
import com.manuel.pagliai.tsol.domain.Grid
import com.manuel.pagliai.tsol.domain.GridFactory
import com.manuel.pagliai.tsol.domain.TilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShape
import com.manuel.pagliai.tsol.domain.tiling.PeriodicShapesTilingOnCanvas
import com.manuel.pagliai.tsol.domain.tiling.convexPolygonPeriodicShape
import javax.inject.Inject

class HexagonalCellType @Inject constructor(
    override val gridFactory: HexagonalGridFactory,
    override val tilingOnCanvasFactory: HexagonalTilingOnCanvasFactory
) : CellType {

    override val name = R.string.hexagonal_cell_type
    override val icon = R.drawable.ic_hexagon_cell_type
    override val id = "HEXAGON"
    override val neighboursCount = 6
    override val defaultGameRules = B2S34

    override fun setNeighboursCoordinates(
        grid: Grid,
        x: Int,
        y: Int,
        dstArray: IntArray,
        dstOffset: Int
    ) {
        var offset = dstOffset

        dstArray[offset++] = grid.toLinear(x + 1, y)
        dstArray[offset++] = grid.toLinear(x - 1, y)

        dstArray[offset++] = grid.toLinear(x, y - 1)
        dstArray[offset++] = grid.toLinear(x, y + 1)

        if (x % 2 == 0) {
            dstArray[offset++] = grid.toLinear(x + 1, y + 1)
            dstArray[offset++] = grid.toLinear(x - 1, y + 1)
        } else {
            dstArray[offset++] = grid.toLinear(x + 1, y - 1)
            dstArray[offset++] = grid.toLinear(x - 1, y - 1)
        }

        assert(offset == neighboursCount + dstOffset)
    }
}

class HexagonalGridFactory @Inject constructor() : GridFactory {
    override fun createGrid(
        canvasWidth: Float,
        canvasHeight: Float,
        displayDensity: Float,
        cellSize: Int
    ) = HexagonBasedGridFactoryHelper.createGrid(
        1,
        canvasWidth,
        canvasHeight,
        displayDensity,
        cellSize
    )
}

class HexagonalTilingOnCanvasFactory @Inject constructor() :
    TilingOnCanvas.Factory {
    override fun create(canvasWidth: Float, canvasHeight: Float, grid: Grid): TilingOnCanvas {
        return PeriodicShapesTilingOnCanvas.create(
            grid,
            canvasWidth = canvasWidth,
            canvasHeight = canvasHeight,
            shapeBuilder = HexagonPeriodicShapeBuilder(grid, canvasWidth, canvasHeight)
        )
    }

    private class HexagonPeriodicShapeBuilder(grid: Grid, canvasWidth: Float, canvasHeight: Float) :
        PeriodicShapesTilingOnCanvas.PeriodicShapeBuilder {
        private val helper = HexagonBasedPeriodicShapeBuilderHelper(
            grid = grid,
            canvasWidth = canvasWidth,
            canvasHeight = canvasHeight,
            polygonCount = 1
        )

        override fun createShape(x: Int, y: Int): PeriodicShape {
            val (p0, p1, p2, p3, p4, p5) = helper.vertices(x, y)
            return convexPolygonPeriodicShape(p0, p1, p2, p3, p4, p5)
        }
    }

}