package com.manuel.pagliai.tsol.domain

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.manuel.pagliai.tsol.domain.engine.ScreenOrientation
import com.manuel.pagliai.tsol.screens.game.GameState

/**
 * A game represents an automaton as configured by the user.
 * An automaton will evolve over time when the game is playing.
 */
@Entity(tableName = "games")
data class SavedGame(

    /**
     * The name of the game, as inserted by the user.
     */
    val name: String,

    /**
     * @see [com.manuel.pagliai.tsol.repository.TheShapeOfLifeDBConverters]
     */
    @ColumnInfo(typeAffinity = ColumnInfo.TEXT)
    val cellType: CellType,

    /**
     * @see [com.manuel.pagliai.tsol.repository.TheShapeOfLifeDBConverters]
     */
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    val cells: Cells,

    /**
     * @see [com.manuel.pagliai.tsol.repository.TheShapeOfLifeDBConverters]
     */
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    val rules: GameRules,

    /**
     * The ratio of width and height of the canvas used to display the game.
     * Used when displaying a preview of the game.
     *
     * If we do not maintain this aspect ratio, the automaton will appear "squashed" when displaying
     * the same game in contexts that are different from the one the automaton was originally created in.
     */
    val previewAspectRatio: Float,

    /**
     * Represents the screen orientation when the automaton was last saved.
     * When trying to load a game, we do require the current screen orientation to match with this value.
     */
    val orientation: ScreenOrientation,

    /**
     * Identifies the user preference on how big the cells are with respect to the screen.
     * Lower the value the more the number of cells drawn on the screen.
     * Must be within [GameState.MIN_CELL_SIZE] and [GameState.MAX_CELL_SIZE].
     */
    val cellSize: Int,

    /**
     * Identifies how fast the automaton reaches the next generation when playing.
     * Must be within [GameState.MIN_SPEED] and [GameState.MAX_SPEED]
     */
    val speed: Int,

    val createdAt: Long,
    val modifiedAt: Long,

    @PrimaryKey(autoGenerate = true) val id: Long? = null
) {

    companion object {
        fun from(gameState: GameState): SavedGame = SavedGame(
            name = gameState.gameName,
            cellType = gameState.cellType,
            cells = gameState.lastEditedCells,
            rules = gameState.rules,
            previewAspectRatio = gameState.previewAspectRatio,
            orientation = gameState.screenOrientation,
            cellSize = gameState.cellSize,
            speed = gameState.speed,
            createdAt = gameState.createdAt,
            modifiedAt = gameState.modifiedAt,
            id = gameState.gameId
        )
    }

}