package com.manuel.pagliai.tsol.di.game

import android.graphics.drawable.Drawable
import com.manuel.pagliai.tsol.domain.Cells
import com.manuel.pagliai.tsol.domain.engine.*
import com.manuel.pagliai.tsol.screens.welcome.ProcessingOnFutureGenerationsCache
import com.manuel.pagliai.tsol.screens.welcome.SavedGameAnimatedPreviewKey
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import javax.inject.Named


/**
 * Dependencies for games and games engine.
 */
@Module
@InstallIn(ViewModelComponent::class)
object GameModule {
    /**
     * Determines how many steps of a running automaton (either step by step, or playing)
     * we remember and we can go back to.
     */
    @Provides
    @ExecutionHistory
    fun executionHistory(): HistoryBuffer<Cells> = HistoryBuffer(100)

    /**
     * The undo/redo history
     */
    @Provides
    @EditHistory
    fun editHistory(): HistoryBuffer<CellsEdit> = HistoryBuffer(100)

    @Provides
    fun loopDetector(): ProbabilisticLoopDetector = ProbabilisticLoopDetector(1024 * 1024 * 64)

    /**
     * Used to run game engine/long running operations in a background thread
     */
    @GameEngineThread
    @Provides
    fun gameEngineBackgroundThread(): ExecutorService =
        Executors.newSingleThreadExecutor { runnable ->
            Thread(runnable, "GameEngineBackground").also {
                it.priority =  Thread.MAX_PRIORITY
            }
        }

    /**
     * Used to detect when the user is not performing any activity on a game
     */
    @ViewModelScoped
    @Provides
    fun providesIdleTracker(): IdleTracker = IdleTracker(10_000)

    /**
     * Used to generate game previews (Welcome screen)
     */
    @Provides
    @Named("GamePreviews")
    fun providesGamesPreviewsCache(nextGenCalculator: AutomatonNextGenCalculator):
            ProcessingOnFutureGenerationsCache<SavedGameAnimatedPreviewKey, Drawable> =
        ProcessingOnFutureGenerationsCache(
            cacheSize = 20,
            maxGenCount = 10,
            nextGenCalculator = nextGenCalculator
        )
}