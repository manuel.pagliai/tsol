package com.manuel.pagliai.tsol.repository.games

import com.manuel.pagliai.tsol.domain.SavedGame
import javax.inject.Inject
import javax.inject.Singleton

/**
 * A repository to retrieve and save/update games.
 *
 * @see [SavedGame]
 */
@Singleton
class GamesRepository @Inject constructor(private val gameDAO: GameDAO) {
    fun getAllGames() = gameDAO.getAllGames()
    suspend fun delete(savedGame: SavedGame) = gameDAO.delete(savedGame)
    suspend fun getGameById(gameId: Long) = gameDAO.getGameById(gameId)
    suspend fun saveGame(game: SavedGame) = gameDAO.saveGame(game)
}