package com.manuel.pagliai.tsol

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.manuel.pagliai.tsol.screens.about.AboutScreen
import com.manuel.pagliai.tsol.screens.game.GameScreen
import com.manuel.pagliai.tsol.screens.settings.SettingsScreen
import com.manuel.pagliai.tsol.screens.welcome.WelcomeScreen
import com.manuel.pagliai.tsol.ui.theme.TSoLTheme
import dagger.hilt.android.AndroidEntryPoint

/**
 * The one and only activity for the app.
 *
 * Sets up the initial content and navigation graph.
 */
@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MainActivityContent()
        }
    }

    @OptIn(
        ExperimentalAnimationApi::class,
        androidx.compose.foundation.ExperimentalFoundationApi::class
    )
    @Composable
    fun MainActivityContent() {
        TSoLTheme {
            Surface(
                color = MaterialTheme.colorScheme.background,
            ) {
                val navController = rememberNavController()
                NavHost(
                    navController = navController,
                    startDestination = Screens.Welcome.route
                ) {
                    composable(route = Screens.Welcome.route) {
                        WelcomeScreen(navController = navController)
                    }
                    composable(
                        route = Screens.Game.route + "?${Screens.Game.GAME_ID_PARAM_NAME}={gameId}",
                        arguments = listOf(
                            navArgument(name = Screens.Game.GAME_ID_PARAM_NAME) {
                                type = NavType.LongType
                                defaultValue =
                                    Screens.Game.NEW_GAME_ID /* if absent, create a new game */
                            })
                    ) {
                        GameScreen(
                            navController = navController
                        )
                    }
                    composable(
                        route = Screens.Settings.route
                    ) {
                        SettingsScreen(
                            navController = navController
                        )
                    }

                    composable(
                        route = Screens.About.route
                    ) {
                        AboutScreen(
                            navController = navController
                        )
                    }
                }
            }
        }
    }
}

