package com.manuel.pagliai.tsol.di

import android.app.Application
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import androidx.room.Room
import com.manuel.pagliai.tsol.domain.cells.CellTypes
import com.manuel.pagliai.tsol.repository.TheShapeOfLifeDB
import com.manuel.pagliai.tsol.repository.games.GameDAO
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Named
import javax.inject.Singleton
import kotlin.random.Random

/**
 * Application wide dependencies
 */
@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideDatabase(app: Application, cellTypes: CellTypes): TheShapeOfLifeDB {
        return Room.databaseBuilder(
            app,
            TheShapeOfLifeDB::class.java,
            TheShapeOfLifeDB.DATABASE_NAME
        ).addTypeConverter(cellTypes).build()
    }

    @Provides
    @Singleton
    fun provideGameDao(db: TheShapeOfLifeDB): GameDAO {
        return db.gameDAO()
    }

    @Provides
    @Singleton
    @Named("SettingsDataStore")
    fun providesSettingsDataStore(app: Application): DataStore<Preferences> =
        preferencesDataStore(name = "settings").getValue(app, Unit::javaClass)


    @Provides
    fun providesRandom(): Random = Random(seed = System.currentTimeMillis())
}