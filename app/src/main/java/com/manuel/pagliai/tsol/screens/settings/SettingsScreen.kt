package com.manuel.pagliai.tsol.screens.settings

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.godaddy.android.colorpicker.HsvColor
import com.godaddy.android.colorpicker.harmony.ColorHarmonyMode
import com.godaddy.android.colorpicker.harmony.HarmonyColorPicker
import com.manuel.pagliai.tsol.R
import com.manuel.pagliai.tsol.Screens
import com.manuel.pagliai.tsol.TestTags
import com.manuel.pagliai.tsol.screens.common.TopAppBarBackButton
import com.manuel.pagliai.tsol.screens.common.TopBarAppTitle
import com.manuel.pagliai.tsol.screens.game.GameState
import kotlin.math.roundToInt

/**
 * Screen used to show/update game settings.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SettingsScreen(
    navController: NavController,
    viewModel: SettingsScreenViewModel = hiltViewModel()
) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    TopBarAppTitle(title = stringResource(id = R.string.settings))
                },
                navigationIcon = {
                    TopAppBarBackButton { navController.navigateUp() }
                }
            )
        }
    ) { padding ->
        SettingsContent(viewModel,
            onAbout = {
                navController.navigate(Screens.About.route)
            },
            modifier = Modifier.padding(padding)
        )
    }
}

@Composable
fun SettingsContent(viewModel: SettingsScreenViewModel, onAbout: () -> Unit, modifier: Modifier) {
    viewModel.preferencesState.value?.let { state ->
        Column(
            modifier
                .verticalScroll(rememberScrollState())
                .padding(32.dp)) {
            SwitchOption(
                checked = state.showNextGenAnimationIdle,
                label = stringResource(id = R.string.show_next_gen_animation)
            ) {
                viewModel.updateShowNexGenWhenIdle(it)
            }

            SwitchOption(
                checked = state.randomiseNewGamesCells,
                label = stringResource(id = R.string.randomise_new_games)
            ) {
                viewModel.updateRandomiseNewGames(it)
            }

            SwitchOption(
                checked = state.useSystemColors,
                label = stringResource(id = R.string.use_system_colors),
                testTag = TestTags.USE_SYSTEM_COLOR_SWITCH
            ) {
                viewModel.updateUseSystemColors(it)
            }


            if (!state.useSystemColors) {
                ColorOption(
                    colorValue = state.cellsColor,
                    title = { Text(text = stringResource(id = R.string.cells_color)) },
                    onColorChange = {
                        viewModel.updateCellsColor(it)
                    })
                Spacer(modifier = Modifier.height(12.dp))
                ColorOption(
                    colorValue = state.gridColor,
                    title = { Text(text = stringResource(id = R.string.grid_color)) },
                    onColorChange = {
                        viewModel.updateGridColor(it)
                    })
                Spacer(modifier = Modifier.height(12.dp))
                ColorOption(
                    colorValue = state.backgroundColor,
                    title = { Text(text = stringResource(id = R.string.background_color)) },
                    onColorChange = {
                        viewModel.updateBackgroundColor(it)
                    })
            }

            SliderOption(
                title = { Text(text = stringResource(id = R.string.new_game_default_speed)) },
                value = state.newGamesDefaultSpeed,
                min = GameState.MIN_SPEED,
                max = GameState.MAX_SPEED,
                onValueChange = {
                    viewModel.updateNewGamesDefaultSpeed(it)
                }
            )

            SliderOption(
                title = { Text(text = stringResource(id = R.string.new_game_default_cell_size)) },
                value = state.newGamesDefaultSpeed,
                min = GameState.MIN_CELL_SIZE,
                max = GameState.MAX_CELL_SIZE,
                onValueChange = {
                    viewModel.updateNewGamesDefaultCellSize(it)
                }
            )

            Spacer(modifier = Modifier.height(8.dp))

            HorizontalDivider()

            Spacer(modifier = Modifier.height(8.dp))

            // About
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .clickable(onClick = onAbout)
            ) {
                Text(text = stringResource(id = R.string.about))
            }

        }
    }
}

@Composable
fun SliderOption(
    title: @Composable () -> Unit,
    value: Int,
    min: Int,
    max: Int,
    onValueChange: (Int) -> Unit
) {
    var sliderValue: Float by remember { mutableFloatStateOf(value.toFloat()) }
    Column {
        title()
        Slider(
            valueRange = min.toFloat()..max.toFloat(),
            value = sliderValue,
            onValueChange = { sliderNewValue ->
                sliderValue = sliderNewValue
                sliderNewValue.roundToInt().let { newIntValue ->
                    if (newIntValue != value)
                        onValueChange(newIntValue)
                }
            })
    }
}

/**
 * True/False option rendered as a switch.
 */
@Composable
fun SwitchOption(
    modifier: Modifier = Modifier,
    testTag: String? = null,
    checked: Boolean,
    label: String,
    onCheckedChange: (Boolean) -> Unit = {},
) {
    var uiValue by remember { mutableStateOf(checked) }
    Row(
        verticalAlignment = CenterVertically,
        modifier = Modifier
            .then(modifier)
            .fillMaxWidth()
    ) {
        Text(label)
        Spacer(modifier = Modifier.width(8.dp))
        Switch(checked = uiValue,
            modifier = testTag?.let { Modifier.testTag(it) } ?: Modifier,
            onCheckedChange = {
                uiValue = it
                onCheckedChange(it)
            })
    }
}

@Composable
fun ColorOption(
    colorValue: Color,
    title: @Composable () -> Unit,
    onColorChange: (Color) -> Unit
) {

    var color: Color by remember { mutableStateOf(colorValue) }


    fun newColor(newColor: Color) {
        color = newColor
        onColorChange(newColor)
    }

    Column(Modifier.height(210.dp)) {
        Row {
            title()
            Spacer(modifier = Modifier.width(16.dp))
            Box(
                modifier = Modifier
                    .align(CenterVertically)
                    .size(20.dp)
                    .clip(CircleShape)
                    .background(color)
            )
        }
        Spacer(modifier = Modifier.height(8.dp))
        Row {
            HarmonyColorPicker(
                modifier = Modifier
                    .weight(1f)
                    .fillMaxHeight(),
                harmonyMode = ColorHarmonyMode.NONE,
                color = HsvColor.from(color)
            ) { newHsvColor: HsvColor ->
                newColor(newHsvColor.toColor())
            }
            Column(
                modifier = Modifier
                    .weight(1f)
                    .fillMaxHeight()
            ) {
                ColorChannelOption(
                    color = Color.Red,
                    value = color.red
                ) { newColor(color.copy(red = it)) }
                ColorChannelOption(color = Color.Green, value = color.green) {
                    newColor(
                        color.copy(
                            green = it
                        )
                    )
                }
                ColorChannelOption(color = Color.Blue, value = color.blue) {
                    newColor(
                        color.copy(
                            blue = it
                        )
                    )
                }
            }
        }

    }
}

@Composable
fun ColorChannelOption(color: Color, value: Float, onNewValue: (Float) -> Unit) {

    Row {
        Box(
            modifier = Modifier
                .align(CenterVertically)
                .size(20.dp)
                .clip(CircleShape)
                .background(color)
        )
        Spacer(modifier = Modifier.size(16.dp))
        Slider(value = value, onValueChange = {
            onNewValue(it)
        })
    }
}