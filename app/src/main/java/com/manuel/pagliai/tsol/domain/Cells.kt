package com.manuel.pagliai.tsol.domain

import java.nio.ByteBuffer
import java.util.*

/**
 * [Cells] are always arrange in a rectangular grid i.e., rows and columns.
 *
 * Depending on the cell type, rows and columns might not be 'straight' instead they are dependent on the
 * particular tiling associated with the cell type.
 *
 * For example, with [com.manuel.pagliai.tsol.domain.cells.HexagonalCellType] cells associated to the same row
 * are not all aligned horizontally instead they go 'up and down':
 *
 *            C[1,0]          C[3,0]
 *    C[0,0]          C[2,0]          ....
 *            C[0,1]          C[2,1]
 *                    C[1,1]          ....
 *
 * Where C[y,x] represents the cell at column x and row y
 *
 *
 *  Regardless of how cells are rendered on the screen, the concept of rows and columns is still useful as a way to identify
 *  each cell and its neighbours via a system of coordinates x,y. Within a grid, it is possible
 *  to switch between linear coordinates and (x,y) coordinates
 */
data class Grid(val rows: Int, val columns: Int) {

    init {
        require(rows > 0)
        require(columns > 0)
    }

    val size: Int
        get() = rows * columns

    /**
     * Converts linear coordinates into x,y coordinates (column, row).
     */
    fun toXY(linear: Int) = Pair(
        (linear + columns) % columns,
        ((linear / columns) + rows) % rows
    )

    /**
     * Converts x,y coordinates (column, row) into linear coordinates.
     * Handles periodic conditions.
     */
    fun toLinear(x: Int, y: Int) = ((y + rows) % rows) * columns + (x + columns) % columns
}

/**
 * Represents the cells of the automaton. cells might be alive or dead, and arranged in a [Grid].
 *
 * [Cells] instances are immutable
 */
data class Cells(
    private val bitSet: BitSet,
    val grid: Grid
) {
    constructor(grid: Grid) : this(grid = grid, bitSet = BitSet())
    constructor(grid: Grid, cellInitialValue: (Int) -> Boolean) : this(
        grid = grid,
        bitSet = BitSet(grid.size).also {
            for (i in 0 until grid.size) {
                it[i] = cellInitialValue(i)
            }
        })

    init {
        require(grid.size > 0)
    }

    /**
     * The number of cells.
     */
    val size: Int = grid.size

    /**
     * True if at least one cell is alive.
     */
    val alive: Boolean
        get() = !bitSet.isEmpty

    /**
     * @return true if the cell at position @param [linear] is alive, false if the cell is dead.
     */
    operator fun get(linear: Int): Boolean {
        assert(linear < size)
        return bitSet[linear]
    }

    operator fun get(x: Int, y: Int) = this[grid.toLinear(x, y)]

    /**
     * @return A [Cells] instance where the cell at the coordinates is set to the specified alive state.
     */
    fun setCell(coordinates: Int, alive: Boolean): Cells {
        assert(coordinates < size)
        if (this[coordinates] == alive) {
            return this
        }
        return Cells(
            bitSet = (this.bitSet.clone() as BitSet).also { it[coordinates] = alive },
            grid = this.grid
        )
    }

    fun setCell(x: Int, y: Int, alive: Boolean) = setCell(grid.toLinear(x, y), alive)

    /**
     * Used to encode/decode [Cells] and [Grid] to/from a [ByteArray] used to serialise [Cells] on disk.
     */
    object Encoding {

        private val magic: ByteArray = byteArrayOf(0x63, 0x65, 0x6c, 0x6c, 0x73)
        private const val version = 1

        fun encode(cells: Cells): ByteArray {
            val cellsBytes = cells.bitSet.toByteArray()
            val buffer = ByteBuffer.allocate(
                magic.size +
                        Int.SIZE_BYTES /*version*/ +
                        Int.SIZE_BYTES /*rows*/ +
                        Int.SIZE_BYTES /*columns*/ +
                        cellsBytes.size
            )
            magic.forEach { buffer.put(it) }
            buffer.putInt(version)
            buffer.putInt(cells.grid.rows)
            buffer.putInt(cells.grid.columns)
            buffer.put(cellsBytes)
            return buffer.array()
        }

        fun decode(byteArray: ByteArray): Cells {
            val byteBuffer = ByteBuffer.wrap(byteArray)
            val magic = ByteArray(magic.size) {
                byteBuffer.get()
            }

            if (!magic.contentEquals(this.magic)) {
                throw IllegalStateException("Unrecognised cells format $magic")
            }

            val version = byteBuffer.int
            if (version != this.version) {
                throw IllegalStateException("Unknown version for cells encoding: $version")
            }

            val rows = byteBuffer.int
            val columns = byteBuffer.int
            val bitSet = BitSet.valueOf(byteBuffer)
            return Cells(grid = Grid(rows = rows, columns = columns), bitSet = bitSet)
        }
    }
}