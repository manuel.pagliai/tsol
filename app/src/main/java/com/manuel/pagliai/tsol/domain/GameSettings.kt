package com.manuel.pagliai.tsol.domain

import androidx.compose.ui.graphics.Color

/**
 * Represents the user preferences as applied during the game play.
 *
 * For example [cellsColor] would be null (meaning read from the system theme) if [com.manuel.pagliai.tsol.repository.preferences.PreferencesState.useSystemColors] is true
 * or [com.manuel.pagliai.tsol.repository.preferences.PreferencesState.cellsColor] if not.
 *
 * @see [com.manuel.pagliai.tsol.repository.preferences.PreferencesState]
 * @see [com.manuel.pagliai.tsol.repository.preferences.PreferencesRepository]
 */
data class GameSettings(

    /**
     * If true, when the system is detected as idle, an animation showing the transition
     * of the automaton to the next generation should be shown.
     */
    val showNextGenAnimationIdle: Boolean,

    /**
     * If null retrieve from system theme
     */
    val cellsColor: Color?,

    /**
     * If null retrieve from system theme
     */
    val gridColor: Color?,

    /**
     * If null retrieve from system theme
     */
    val backgroundColor: Color?,

    /**
     * see [com.manuel.pagliai.tsol.repository.preferences.PreferencesState.newGamesDefaultSpeed]
     */
    val newGamesDefaultSpeed: Int,

    /**
     * see [com.manuel.pagliai.tsol.repository.preferences.PreferencesState.newGamesDefaultCellSize]
     */
    val newGamesDefaultCellSize: Int,

    /**
     * see [com.manuel.pagliai.tsol.repository.preferences.PreferencesState.randomiseNewGamesCells]
     */
    val randomiseNewGamesCells: Boolean
)


