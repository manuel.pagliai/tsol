package com.manuel.pagliai.tsol.domain

import com.manuel.pagliai.tsol.domain.cells.SquareCellType
import com.manuel.pagliai.tsol.domain.engine.ScreenOrientation
import com.manuel.pagliai.tsol.screens.game.AutomatonColors
import com.manuel.pagliai.tsol.screens.game.GameControls
import com.manuel.pagliai.tsol.screens.game.GameState

object Stubs {
    fun savedGame(
        id: Long?,
        name: String,
        cellType: CellType,
        update: ((SavedGame) -> SavedGame)? = null
    ): SavedGame {
        val game = SavedGame(
            cells = Cells(Grid(rows = 10, columns = 5)),
            cellSize = 4,
            cellType = cellType,
            createdAt = 100,
            modifiedAt = 200,
            id = id,
            name = name,
            orientation = ScreenOrientation.LANDSCAPE,
            previewAspectRatio = 1.1f,
            rules = GameRules.B2S34,
            speed = 3
        )
        return update?.invoke(game) ?: game
    }


    fun gameState(
        gameName: String = "game #1",
        cellType: CellType = SquareCellType(),
        lastEditedCells: Cells = Cells(Grid(rows = 10, columns = 5)).setCell(3, true).setCell(2, true),
        currentGenCells: Cells = Cells(Grid(rows = 10, columns = 5)).setCell(1, true),
        nextGenCells: Cells = Cells(Grid(rows = 10, columns = 5)).setCell(2, true),
        generation: Int = 3,
        rules: GameRules = GameRules.B2S34,
        previewAspectRatio: Float = 1.2f,
        screenOrientation: ScreenOrientation = ScreenOrientation.LANDSCAPE,
        cellSize: Int = 3,
        speed: Int = 2,
        createdAt: Long = 100,
        modifiedAt: Long = 200,
        controls: GameControls = GameControls(
            addCellsOnSelection = true,
            canChangeCellType = true,
            canClear = true,
            canNextGen = true,
            canPrevGen = true,
            canRedo = true,
            canUndo = true,
            isPlaying = true
        ),
        isLooping: Boolean = false,
        showNextGenPreview: Boolean = false,
        gameId: Long? = 42,
        automatonColors: AutomatonColors = AutomatonColors(
            backgroundColor = null,
            cellsColor = null,
            gridColor = null
        ),
    ) = GameState(
        gameName = gameName,
        cellType = cellType,
        lastEditedCells = lastEditedCells,
        currentGenCells = currentGenCells,
        nextGenCells = nextGenCells,
        generation = generation,
        rules = rules,
        previewAspectRatio = previewAspectRatio,
        screenOrientation = screenOrientation,
        cellSize = cellSize,
        speed = speed,
        createdAt = createdAt,
        modifiedAt = modifiedAt,
        controls = controls,
        isLooping = isLooping,
        showNextGenPreview = showNextGenPreview,
        gameId = gameId,
        automatonColors = automatonColors
    )
}