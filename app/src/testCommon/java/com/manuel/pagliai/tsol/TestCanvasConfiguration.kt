package com.manuel.pagliai.tsol

import com.manuel.pagliai.tsol.domain.engine.ScreenOrientation

@Suppress("MemberVisibilityCanBePrivate")
data class TestCanvasConfiguration(
    val name : String,
    val canvasWidth: Float,
    val canvasHeight: Float,
    val displayDensity: Float,
    val orientation: ScreenOrientation
) {

    companion object {
        val PIXEL_XL_PORTRAIT = TestCanvasConfiguration(
            name = "PIXEL_XL_PORTRAIT",
            canvasWidth = 1440.0f,
            canvasHeight = 1916.0f,
            displayDensity = 1.0f,
            orientation = ScreenOrientation.PORTRAIT
        )

        val PIXEL_XL_LANDSCAPE = TestCanvasConfiguration(
            name = "PIXEL_XL_LANDSCAPE",
            canvasWidth = 2392.0f,
            canvasHeight = 964.0f,
            displayDensity = 1.0f,
            orientation = ScreenOrientation.LANDSCAPE
        )

        val PIXEL2_PORTRAIT = TestCanvasConfiguration(
            name = "PIXEL2_PORTRAIT",
            canvasWidth = 1080.0f,
            canvasHeight = 1437.0f,
            displayDensity = 1.0f,
            orientation = ScreenOrientation.PORTRAIT
        )

        val PIXEL2_LANDSCAPE = TestCanvasConfiguration(
            name = "PIXEL2_LANDSCAPE",
            canvasWidth = 1794.0f,
            canvasHeight = 723.0f,
            displayDensity = 1.0f,
            orientation = ScreenOrientation.LANDSCAPE
        )

        val PIXEL3_PORTRAIT = TestCanvasConfiguration(
            name = "PIXEL3_PORTRAIT",
            canvasWidth = 1080.0f,
            canvasHeight = 1731.0f,
            displayDensity = 1.0f,
            orientation = ScreenOrientation.PORTRAIT
        )

        val PIXEL3_LANDSCAPE = TestCanvasConfiguration(
            name = "PIXEL3_LANDSCAPE",
            canvasWidth = 2160.0f,
            canvasHeight = 662.0f,
            displayDensity = 1.0f,
            orientation = ScreenOrientation.LANDSCAPE
        )

        fun all() = listOf(
            PIXEL_XL_PORTRAIT,
            PIXEL_XL_LANDSCAPE,
            PIXEL2_PORTRAIT,
            PIXEL2_LANDSCAPE,
            PIXEL3_PORTRAIT,
            PIXEL3_LANDSCAPE
        )
    }
}